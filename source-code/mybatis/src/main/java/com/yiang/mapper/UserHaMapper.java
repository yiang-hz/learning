package com.yiang.mapper;

import com.yiang.entity.User;

/**
 * @author HeZhuo
 * @date 2021/1/7
 */
public interface UserHaMapper {

    /**
     * 获取User
     * @param id ID
     * @return User
     */
    User getUser(int id);
}
