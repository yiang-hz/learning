package com.yiang.entity;

import lombok.Data;
import org.apache.ibatis.type.Alias;

/**
 * @author HeZhuo
 * @date 2021/1/7
 */
@Alias("User")
@Data
public class User {

    private Long id;
    private String name;
}
