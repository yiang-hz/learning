import com.yiang.entity.User;
import com.yiang.mapper.UserHaMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.Reader;

/**
 * @author HeZhuo
 * @date 2021/1/7
 */
public class TestMyBatis {

    // 1.需要引入mybatis jar包
    // 2.配置核心mybatis文件 数据源、mapper接口映射
    // 3.需要sql mapper文件 sql数据 orm
    // 4.通过mybatis操作../
    // 疑问：你们在mybatis整合springboot之后需要在每个mapper 需要加入注入spring容器注解 这是为什么呢？
    // 疑问：Mapper如何调用的呢
    public static void main(String[] args) {

        try {
            // 基本mybatis环境
            // 1.定义mybatis_config文件地址
            String resources = "mybatis-config.xml";
            // 2.通过资源加载器，加载mybatis-config 文件 获取 InputStreamReader Io流
            Reader reader = Resources.getResourceAsReader(resources);
            // 3.获取SqlSessionFactory
            SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
            // 4.获取Session
            SqlSession sqlSession = sqlSessionFactory.openSession();
            // 5.操作Mapper接口
            UserHaMapper mapper = sqlSession.getMapper(UserHaMapper.class);
            User user = mapper.getUser(1);
            System.out.println(user.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /*
     * 看源码的时候 看不懂 不知道如何下手。最重要的一点学会画图。 如何加源码呢？
     */
}
