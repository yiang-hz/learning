package com.yiang.test;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author HeZhuo
 * @date 2021/9/9
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultVO {

    private String code;
    private String message;
    private String data;
}
