package com.yiang.test;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author HeZhuo
 * @date 2021/9/9
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ProductVO {

    private int id;
    private String name;
    private String price;
    private String validPurchasingQuantity;
    private Date superiorCommissionsRate;
    private int type;
    private int stockState;
    private int supplyState;
}
