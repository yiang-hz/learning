package com.yiang.activiti.service;

import com.github.pagehelper.Page;
import com.yiang.activiti.domain.PageDomain;
import com.yiang.activiti.domain.dto.ActTaskDTO;
import com.yiang.activiti.domain.dto.ActWorkflowFormDataDTO;

import java.text.ParseException;
import java.util.List;

/**
 * @author 以安
 */
@SuppressWarnings("all")
public interface IActTaskService {

    Page<ActTaskDTO> selectProcessDefinitionList(PageDomain pageDomain);

    List<String>formDataShow(String taskId);

    int formDataSave(String taskId, List<ActWorkflowFormDataDTO> awfs) throws ParseException;
}
