package com.yiang.activiti.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HistoryDataDTO {
    private String taskNodeName;
    private String createName;
    private String createdDate;
    private List<HistoryFormDataDTO> formHistoryDataDTO;
}
