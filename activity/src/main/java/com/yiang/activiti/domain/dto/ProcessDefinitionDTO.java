package com.yiang.activiti.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.yiang.activiti.domain.BaseEntity;
import com.yiang.activiti.domain.vo.ActReDeploymentVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntityImpl;

import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProcessDefinitionDTO extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private String id;

    private String name;

    private String key;

    private int version;

    private String deploymentId;
    private String resourceName;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date deploymentTime;

    /** 流程实例状态 1 激活 2 挂起 */
    private Integer suspendState;

    public ProcessDefinitionDTO(ProcessDefinitionEntityImpl processDefinition, ActReDeploymentVO actReDeploymentVO) {
        this.id = processDefinition.getId();
        this.name = processDefinition.getName();
        this.key = processDefinition.getKey();
        this.version = processDefinition.getVersion();
        this.deploymentId = processDefinition.getDeploymentId();
        this.resourceName = processDefinition.getResourceName();
        this.deploymentTime = actReDeploymentVO.getDeployTime();
        this.suspendState = processDefinition.getSuspensionState();
    }
}
