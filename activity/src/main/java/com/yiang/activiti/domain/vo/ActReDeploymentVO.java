package com.yiang.activiti.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 汇讯数码科技(深圳)有限公司
 * 创建日期:2020/10/23-15:42
 * 版本   开发者     日期
 * 1.0    Danny    2020/10/23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActReDeploymentVO {
    private String id;
    private Date deployTime;
}
