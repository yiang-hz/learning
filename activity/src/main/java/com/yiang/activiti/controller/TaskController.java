package com.yiang.activiti.controller;

import com.github.pagehelper.Page;
import com.yiang.activiti.domain.AjaxResult;
import com.yiang.activiti.domain.PageDomain;
import com.yiang.activiti.domain.TableSupport;
import com.yiang.activiti.domain.dto.ActTaskDTO;
import com.yiang.activiti.domain.dto.ActWorkflowFormDataDTO;
import com.yiang.activiti.service.IActTaskService;
import org.activiti.api.task.runtime.TaskRuntime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;


@RestController
@RequestMapping("/task")
@SuppressWarnings("all")
public class TaskController {

    @Autowired
    private TaskRuntime taskRuntime;

    @Autowired
    private IActTaskService actTaskService;

    //获取我的代办任务
    @GetMapping(value = "/list")
    public Page<ActTaskDTO> getTasks() {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        Page<ActTaskDTO> hashMaps = actTaskService.selectProcessDefinitionList(pageDomain);
        return hashMaps;
    }

    //渲染表单
    @GetMapping(value = "/formDataShow/{taskID}")
    public AjaxResult formDataShow(@PathVariable("taskID") String taskID) {

        return AjaxResult.success(actTaskService.formDataShow(taskID));
    }

    //保存表单
    @PostMapping(value = "/formDataSave/{taskID}")
    public Object formDataSave(@PathVariable("taskID") String taskID,
                                   @RequestBody List<ActWorkflowFormDataDTO> formData) throws ParseException {
        return actTaskService.formDataSave(taskID, formData);
    }

}
