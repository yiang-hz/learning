package com.yiang.activiti.controller;

import com.yiang.activiti.domain.AjaxResult;
import com.yiang.activiti.service.IFormHistoryDataService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author RuoYi Acitivity
 */
@RestController
public class HistoryFormDataCoroller {

    private final IFormHistoryDataService formHistoryDataService;

    public HistoryFormDataCoroller(IFormHistoryDataService formHistoryDataService) {
        this.formHistoryDataService = formHistoryDataService;
    }

    @GetMapping(value = "historyFromData/ByInstanceId/{instanceId}")
    public AjaxResult historyFromData(@PathVariable("instanceId") String instanceId) {
        return AjaxResult.success(formHistoryDataService.historyDataShow(instanceId));

    }
}
