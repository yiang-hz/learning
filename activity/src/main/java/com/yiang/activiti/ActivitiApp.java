package com.yiang.activiti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author HeZhuo
 * @date 2021/6/3
 */
@SpringBootApplication
public class ActivitiApp {

    public static void main(String[] args) {
        SpringApplication.run(ActivitiApp.class, args);
    }
}
