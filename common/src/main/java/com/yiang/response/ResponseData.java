package com.yiang.response;

import java.io.Serializable;

/**
 * 通用返回值父类
 */
public class ResponseData<T> implements Serializable {

    private String statusCode = "000000";
    private String message = "操作成功";
    private T data;

    /**
     * 无参构造函数
     */
    protected ResponseData() {}

    /**
     * 处理只有状态码与信息的情况
     * @param statusCode 状态码
     * @param message 信息
     */
    protected ResponseData(String statusCode, String message) {
        this.statusCode = statusCode;
        this.message = message;
    }

    /**
     * 全参构造，处理返回状态码、信息、数据
     * @param statusCode 返回状态码
     * @param message 信息
     * @param data 数据
     */
    protected ResponseData(String statusCode, String message, T data) {
        this.statusCode = statusCode;
        this.message = message;
        this.data = data;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ResponseData{" +
                "statusCode='" + statusCode + '\'' +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
