package com.yiang.response;

import java.io.Serializable;

/**
 * Feign客户端调用通用返回类
 * @param <T>
 */
public class FeignResponseData<T> extends ResponseData<T> implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String SUCCESS = "SUCCESS";
    public static final String ERROR = "ERROR";

    /**
     * 无参构造
     */
    private FeignResponseData(){
        super();
    }

    /**
     * 处理只有状态码与信息的情况
     * @param statusCode 状态码
     * @param message 信息
     */
    public FeignResponseData(String statusCode, String message) {
        super(statusCode, message);
    }

    /**
     * 全参构造，处理返回状态码、信息、数据
     * @param statusCode 返回状态码
     * @param message 信息
     * @param data 数据
     */
    public FeignResponseData(String statusCode, String message, T data) {
        super(statusCode, message, data);
    }

    /**
     * 处理返回通过状态码封装类返回状态码、信息情况
     * <p>
     *  @param result 通用返回状态码封装类
     * 1.状态码匹配：
     *  @see ResultCode#getCode()
     *  =
     *  @see ResponseData#getStatusCode()
     * 2.信息匹配：
     *  @see ResultCode#getMsg()
     *  =
     * @see ResponseData#getMessage()
     *  </p>
     *
     */
    public FeignResponseData(ResultCode result){
        super(result.getCode(), result.getMsg());
    }

    /**
     * 全参构造，处理返回状态码、信息、数据
     * <p>
     *  @param result 通用返回状态码封装类
     *  详细注释说明：
     *  @see FeignResponseData#FeignResponseData(ResultCode)
     * </p>
     * @param data 数据
     */
    public FeignResponseData(ResultCode result, T data) {
        super(result.getCode(), result.getMsg(), data);
    }
}
