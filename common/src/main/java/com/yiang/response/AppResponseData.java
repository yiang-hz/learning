package com.yiang.response;

import java.io.Serializable;

/**
 * 应用程序通用返回类
 */
public class AppResponseData extends ResponseData implements Serializable {

    private String statusCode;
    private String message;
    private Object data;

    /**
     * 无参构造
     */
    private AppResponseData(){
        super();
    }

    /**
     * 处理只有状态码与信息的情况
     * @param statusCode 状态码
     * @param message 信息
     */
    public AppResponseData(String statusCode, String message) {
        super(statusCode, message);
    }

    /**
     * 全参构造，处理返回状态码、信息、数据
     * @param statusCode 返回状态码
     * @param message 信息
     * @param data 数据
     */
    @SuppressWarnings("unchecked")
    public AppResponseData(String statusCode, String message, Object data) {
        super(statusCode, message, data);
    }

    /**
     * 处理返回通过状态码封装类返回状态码、信息情况。
     * 注意：避免返回给应用程序值NULL，这里默认返回为空字符串
     * <p>
     *  @param result 通用返回状态码封装类
     * 1.状态码匹配：
     *  @see ResultCode#getCode()
     *  =
     *  @see ResponseData#getStatusCode()
     * 2.信息匹配：
     *  @see ResultCode#getMsg()
     *  =
     * @see ResponseData#getMessage()
     *  </p>
     *
     */
    @SuppressWarnings("unchecked")
    public AppResponseData(ResultCode result) {
        super(result.getCode(), result.getMsg(), "");
    }

    /**
     * 全参构造，处理返回状态码、信息、数据
     * <p>
     *  @param result 通用返回状态码封装类
     *  详细注释说明：
     *  @see AppResponseData#AppResponseData(ResultCode)
     * </p>
     * @param data 数据
     */
    @SuppressWarnings("unchecked")
    public AppResponseData(ResultCode result, Object data) {
        super(result.getCode(), result.getMsg(), data);
    }

}