package com.yiang.response;

/**
 * 通用返回状态码封装类
 */
public enum ResultCode {
    SUCCESS("200", "成功"),
    FAILED("400", "失败"),
    TOKENHASEXPIRED("401", "Token已过期"),
    TOKENCHECKFAIL("402", "Token校验失败"),
    MISSIONTOKENPARA("403", "缺少token参数"),
    TOKENERROR("408", "Token异常"),
    EXCEPTION("500", "服务器开小差了,请稍后再试");

    /**
     * 状态码
     */
    private String code;
    /**
     * 状态信息
     */
    private String msg;

    ResultCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}