package com.yiang.pay;

import com.yiang.pay.config.StartupRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * @author HeZhuo
 * @date 2020/6/20
 */
@SpringBootApplication
public class PayApplication {

    public static void main(String[] args) {
        SpringApplication.run(PayApplication.class, args);
    }

    @Bean
    public StartupRunner startupRunner() {
        return new StartupRunner();
    }
}
