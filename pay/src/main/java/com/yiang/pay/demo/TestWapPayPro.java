package com.yiang.pay.demo;

import cn.hutool.captcha.generator.RandomGenerator;
import com.alipay.api.AlipayApiException;
import com.alipay.api.CertAlipayRequest;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeWapPayModel;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.alipay.api.response.AlipayTradeWapPayResponse;

/**
 * @author HeZhuo
 * @date 2020/7/30
 */
public class TestWapPayPro {

    public static void main(String[] args) throws AlipayApiException {
        CertAlipayRequest certAlipayRequest = new CertAlipayRequest();
        certAlipayRequest.setServerUrl("https://openapi.alipay.com/gateway.do");
        certAlipayRequest.setAppId("2021001182614741");
        certAlipayRequest.setPrivateKey("MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCE/LQ/qCYh5thddR3F3QxjOzhKGeLjKHTuvYtzi/M5zZhssARzCpi4qcY2Pl59bbhBMmb+zNsaobgPaJC9Ta+J36e1UeFDcYlSQ2JGUVY5wuY/tCi0/93LIXDXkKIAOe3TZgFuYTrM08Bp/FlN/H6AD0yRY4CraE1l1+aTd+ir0jHEX4MbpW7DJFUcTNzekOMjgyPpHjM4JCUwUA8HoKgiDDK3yVfVXJ2uaI95ltpX2O4Qx3i2C5qCa5EfhMklXkcYR+1pZyifgB4788OD0wwDfoCMqILxycQToB9pWbDXoaZEB0OArMX+4d/a0DrF6oqDp6mkfsEwUNK68LZQib4dAgMBAAECggEATbIaWQqTZMEq7ccr4trXju4dX+Wp0g9j6cuhupssOjws7msT3mZ1fYXrAOwFZoCA4s5gsELdS7zp/U6ZKZBGm2Wo5N12n2Go2f/2XaDNoKeGv/cwxTarzS63b6nXjAxML3LsKSHzKZIZW5KMVmzLN2Q41sRsezhP581wr/2hWShsrGkQr9ZMbFkNkZjDAECqafXUcuR0qle2f5XBHssFGt5sWLhtf1V/hPOYG+lQQo+yRlwbKfWezKIcXis2E6slWJb54jTzd27ROIB+22Bth6qhlFvPwUSCYCTzj7xkMqbp5ThqSr25dLkhoHdTHwmUIDLQgUZWmzKCOTMbY4FvwQKBgQD/vRxvc3fAlUFUZZcFauKlsfCioHEp1AnYLG11JYfNHiQKCUEnhmlX29LNEti89OuuWrq0cghEvn/DPhrjnl2L+dAGethNEy8CbOEXWHQex98GwdgmHo1SuBFQUH3hufozsnd4vFD7kd135SBLbAEkPnkmhcE2NO7KdSrYsOLkUQKBgQCFH3yz5OTf9ywMPiIGD1HErVLI5kCGpdSIl5IETOzJ8KJh5/4wN2VDYsmpjzrtgkVfEWy/TeIMDPB/bpCBrfXtIBWPstAnu4EyVPSF4uLUHqNaG32+Nnl7S5wMmNJ50M32Pd1FyctOGn6oOmmzjJ0VoBJMnYIErvs4Dy34KbZGDQKBgAHYgpEHW1Dngt4yEYH3gIVDoIH7+HUd06hcwbl7uNwxIH92C6NWZAI/Uukp/qh2eBD9FKXiwM6GNXPaSeVkqjYtamPxlQgRYGB7GrDJcaUbOi/ZTE8SH2D+dRmISzBK4FRpMFWNvmPibJ/F5pLEx8lULhmqE3fpr+OELvf7ZtfRAoGAZpAHx6GGJHeOXDBSjlCnMtxGd7nN5O9Ge6YrPVz24e6fbMcMM6G6RtyJejV7tRk00TXfjtQ2YApoEAXjF8YrPvIFKp0dej8gHuMNMUnIcJzaRcYAbtgRaEishZgIMQoKZLjtDaOXGymWg43dehPPwqV888I2Nd2Kd3padGPOptECgYB8ylvWK8+Ap4HAEf/PmZPxxPkdjq/cIzjDnrT+PuJgcklNcLB4ouIvO7FCzPpwKuqxfFKX/jAu/8p1JphCxrkE6J+0X+/f7zcML4t3OtvW8Ymr65kjs8OqLkZmu1kbT7QNDAvRSinuiZbFjbPaRSlUJAatK++bNVveU7AqjAYBnQ==");
        certAlipayRequest.setFormat("json");
        certAlipayRequest.setCharset("utf-8");
        //签名方式
        certAlipayRequest.setSignType("RSA2");
//        certAlipayRequest.setCertPath("D:\\wwwroot\\file\\cert\\alipay\\2021001182614741\\appCertPublicKey_2021001182614741.crt");
//        certAlipayRequest.setAlipayPublicCertPath("D:\\wwwroot\\file\\cert\\alipay\\2021001182614741\\alipayCertPublicKey_RSA2.crt");
//        certAlipayRequest.setRootCertPath("D:\\wwwroot\\file\\cert\\alipay\\2021001182614741\\alipayRootCert.crt");


        certAlipayRequest.setCertPath("/wwwroot/file/cert/alipay/2021001182614741/appCertPublicKey_2021001182614741.crt");
        certAlipayRequest.setAlipayPublicCertPath("/wwwroot/file/cert/alipay/2021001182614741/alipayCertPublicKey_RSA2.crt");
        certAlipayRequest.setRootCertPath("/wwwroot/file/cert/alipay/2021001182614741/alipayRootCert.crt");

        // 发送API请求
        DefaultAlipayClient alipayClient = new DefaultAlipayClient(certAlipayRequest);
        AlipayTradeWapPayRequest alipayRequest = new AlipayTradeWapPayRequest();
        alipayRequest.setReturnUrl("");
        alipayRequest.setNotifyUrl("");
        AlipayTradeWapPayModel alipayTradeWapPayModel = new AlipayTradeWapPayModel();
        alipayTradeWapPayModel.setOutTradeNo(new RandomGenerator(12).generate());
        alipayTradeWapPayModel.setTotalAmount("0.01");
        alipayTradeWapPayModel.setSubject("Red Mi K20 Pro");
        alipayTradeWapPayModel.setProductCode("QUICK_WAP_WAY");
        alipayTradeWapPayModel.setPassbackParams("123");
//        AlipayTradeWapPayResponse response = alipayClient.pageExecute(alipayRequest);// 返回form表单
        //通过以下代码进行提交请求会默认返回请求url字符串
        AlipayTradeWapPayResponse response = alipayClient.pageExecute(alipayRequest,"GET");
        System.out.println(response);
        System.out.println(response.getBody());
    }
}
