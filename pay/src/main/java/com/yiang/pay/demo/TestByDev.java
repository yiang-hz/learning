package com.yiang.pay.demo;

import cn.hutool.captcha.generator.RandomGenerator;
import com.alipay.api.AlipayClient;
import com.alipay.api.CertAlipayRequest;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayFundTransUniTransferModel;
import com.alipay.api.domain.Participant;
import com.alipay.api.request.AlipayFundTransUniTransferRequest;
import com.alipay.api.response.AlipayFundTransUniTransferResponse;

/**
 * @author HeZhuo
 * @date 2020/7/30
 */
public class TestByDev {
    public static void main(String[] args) throws Exception {

        //支付宝网关，(支付宝沙箱网关:https://openapi.alipay.com/gateway.do)
        String serverUrl = "https://openapi.alipaydev.com/gateway.do";

        //支付宝应用的appid
        String appId = ""; //何卓

        //应用私钥
        //HZ
        String merchantPrivateKey = "";

        String publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAljj0ia6ta8xvR39q/+53MQrNTxFayb285NmxJllCtt2bQhJuysxyjd/qddwQ0pD0sipgCZyN/Ea2spB0DaMyjvG6z05moB1UBKIZn83BXuHzd3cshU9zWoxW4Fmfpz/mmFaLxR4atkrsd85LQr6JV3/EZ8J5Vz1Wxl0pqyq+5PCUBQWwQyUY/Bp08vFMwbSNc6k2lnGdiEhVCQ0FtfNXpc0kp190Lm0Uh+MY5GzX7DkK76z3HxQH1Gox763UbC76aJxILqPVlwTsCtipAaUXfYE7ttZR4lcO8uD/pWUGecaiz9pSMRq+8MOEyjS2V8AeiBDxuwEarx769UAZeCMULwIDAQAB";

        //请求参数数据格式(仅json)
        String format = "json";

        //请求使用的编码格式,如utf-8,gbk,gb2312等
        String charset = "utf-8";

        //商户生成签名字符串所使用的签名算法类型，目前支持RSA2和RSA，推荐使用RSA2
        String signType = "RSA2";

        CertAlipayRequest certAlipayRequest = new CertAlipayRequest();
        certAlipayRequest.setServerUrl(serverUrl);
        certAlipayRequest.setAppId(appId);
        certAlipayRequest.setPrivateKey(merchantPrivateKey);
        certAlipayRequest.setFormat(format);
        certAlipayRequest.setCharset(charset);
        certAlipayRequest.setSignType(signType);


        //证书路径必须是绝对路径
        //应用公钥证书绝对路径
//        certAlipayRequest.setCertPath("C:\Users\wb-fcz618414\Desktop\5\appCertPublicKey_appid.crt");
//
//        //支付宝公钥证书绝对路径
//        certAlipayRequest.setAlipayPublicCertPath("C:\Users\wb-fcz618414\Desktop\5\alipayCertPublicKey_RSA2.crt");
//
//        //支付宝根证书绝对路径
//        certAlipayRequest.setRootCertPath("C:\Users\wb-fcz618414\Desktop\5\alipayRootCert.crt");

        AlipayClient alipayClientTan = new DefaultAlipayClient(serverUrl, appId,
                merchantPrivateKey, format, charset, publicKey, signType);



//        AlipayClient alipayClient = new DefaultAlipayClient(certAlipayRequest);

        AlipayFundTransUniTransferRequest request = new AlipayFundTransUniTransferRequest();

        Participant payeeInfo = new Participant();
        payeeInfo.setIdentity("khbmwd2386@sandbox.com");
        payeeInfo.setIdentityType("ALIPAY_LOGON_ID");
        payeeInfo.setName("沙箱环境");

        AlipayFundTransUniTransferModel model = new AlipayFundTransUniTransferModel();
        model.setOutBizNo(new RandomGenerator(8).generate());
        model.setTransAmount("100");
        model.setProductCode("TRANS_ACCOUNT_NO_PWD");
        model.setBizScene("DIRECT_TRANSFER");
        model.setOrderTitle("转账");
        model.setPayeeInfo(payeeInfo);
        request.setBizModel(model);

//        AlipayFundTransUniTransferResponse response = alipayClient.certificateExecute(request);
        //AlipayFundTransUniTransferResponse response = alipayClientTan.certificateExecute(request);
        AlipayFundTransUniTransferResponse response = alipayClientTan.execute(request);

        System.out.println(response);

        //打印请求字符串
        System.out.println(response.getBody());
        if (response.isSuccess()) {
            System.out.println("调用成功");
        } else {
            System.out.println("调用失败");
        }
    }
}
