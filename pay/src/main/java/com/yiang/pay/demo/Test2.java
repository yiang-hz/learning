package com.yiang.pay.demo;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.alipay.api.response.AlipayTradeWapPayResponse;

/**
 * @author HeZhuo
 * @date 2020/7/28
 */
public class Test2 {

    public static void main(String[] args) throws AlipayApiException {

        //支付宝网关，(支付宝沙箱网关:https://openapi.alipay.com/gateway.do)
        String serverUrl = "https://openapi.alipaydev.com/gateway.do";

        //支付宝应用的appid
        String appId = "2016093000633379"; //何卓
//        String appId = "2021000116669716"; //李总
//        String appId = "2021000116658974"; //紫容


        //应用私钥
        //HZ
        String merchantPrivateKey = "";

        String publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAljj0ia6ta8xvR39q/+53MQrNTxFayb285NmxJllCtt2bQhJuysxyjd/qddwQ0pD0sipgCZyN/Ea2spB0DaMyjvG6z05moB1UBKIZn83BXuHzd3cshU9zWoxW4Fmfpz/mmFaLxR4atkrsd85LQr6JV3/EZ8J5Vz1Wxl0pqyq+5PCUBQWwQyUY/Bp08vFMwbSNc6k2lnGdiEhVCQ0FtfNXpc0kp190Lm0Uh+MY5GzX7DkK76z3HxQH1Gox763UbC76aJxILqPVlwTsCtipAaUXfYE7ttZR4lcO8uD/pWUGecaiz9pSMRq+8MOEyjS2V8AeiBDxuwEarx769UAZeCMULwIDAQAB";

        //请求参数数据格式(仅json)
        String format = "json";

        //请求使用的编码格式,如utf-8,gbk,gb2312等
        String charset = "utf-8";

        //商户生成签名字符串所使用的签名算法类型，目前支持RSA2和RSA，推荐使用RSA2
        String signType = "RSA2";


        AlipayClient alipayClient = new DefaultAlipayClient(
                "https://openapi.alipay.com/gateway.do",
                "app_id",
                "your private_key",
                "json","UTF-8",
                "alipay_public_key",
                "RSA2");
        AlipayTradeWapPayRequest request = new AlipayTradeWapPayRequest();
        request.setBizContent("{" +
                "\"body\":\"Iphone6 16G\"," +
                "\"subject\":\"大乐透\"," +
                "\"out_trade_no\":\"70501111111S001111119\"," +
                "\"timeout_express\":\"90m\"," +
                "\"time_expire\":\"2016-12-31 10:05\"," +
                "\"total_amount\":9.00," +
                "\"auth_token\":\"appopenBb64d181d0146481ab6a762c00714cC27\"," +
                "\"goods_type\":\"0\"," +
                "\"quit_url\":\"http://www.taobao.com/product/113714.html\"," +
                "\"passback_params\":\"merchantBizType%3d3C%26merchantBizNo%3d2016010101111\"," +
                "\"product_code\":\"QUICK_WAP_WAY\"," +
                "\"promo_params\":\"{\\\"storeIdType\\\":\\\"1\\\"}\"," +
                "\"extend_params\":{" +
                "\"sys_service_provider_id\":\"2088511833207846\"," +
                "\"hb_fq_num\":\"3\"," +
                "\"hb_fq_seller_percent\":\"100\"," +
                "\"industry_reflux_info\":\"{\\\\\\\"scene_code\\\\\\\":\\\\\\\"metro_tradeorder\\\\\\\",\\\\\\\"channel\\\\\\\":\\\\\\\"xxxx\\\\\\\",\\\\\\\"scene_data\\\\\\\":{\\\\\\\"asset_name\\\\\\\":\\\\\\\"ALIPAY\\\\\\\"}}\"," +
                "\"card_type\":\"S0JP0000\"" +
                "    }," +
                "\"merchant_order_no\":\"20161008001\"," +
                "\"enable_pay_channels\":\"pcredit,moneyFund,debitCardExpress\"," +
                "\"disable_pay_channels\":\"pcredit,moneyFund,debitCardExpress\"," +
                "\"store_id\":\"NJ_001\"," +
                "      \"goods_detail\":[{" +
                "        \"goods_id\":\"apple-01\"," +
                "\"alipay_goods_id\":\"20010001\"," +
                "\"goods_name\":\"ipad\"," +
                "\"quantity\":1," +
                "\"price\":2000," +
                "\"goods_category\":\"34543238\"," +
                "\"categories_tree\":\"124868003|126232002|126252004\"," +
                "\"body\":\"特价手机\"," +
                "\"show_url\":\"http://www.alipay.com/xxx.jpg\"" +
                "        }]," +
                "\"specified_channel\":\"pcredit\"," +
                "\"business_params\":\"{\\\"data\\\":\\\"123\\\"}\"," +
                "\"ext_user_info\":{" +
                "\"name\":\"李明\"," +
                "\"mobile\":\"16587658765\"," +
                "\"cert_type\":\"IDENTITY_CARD\"," +
                "\"cert_no\":\"362334768769238881\"," +
                "\"min_age\":\"18\"," +
                "\"fix_buyer\":\"F\"," +
                "\"need_check_info\":\"F\"" +
                "    }" +
                "  }");
        AlipayTradeWapPayResponse response = alipayClient.pageExecute(request);
        if(response.isSuccess()){
            System.out.println("调用成功");
        } else {
            System.out.println("调用失败");
        }
    }
}
