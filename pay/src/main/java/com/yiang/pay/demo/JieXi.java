package com.yiang.pay.demo;

import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;

/**
 * @author HeZhuo
 * @date 2020/7/30
 */
public class JieXi {

    public static void main(String[] args) throws AlipayApiException {
        String publicKey = AlipaySignature.getAlipayPublicKey("D:\\initpath\\pay\\alipay\\alipayCertPublicKey_RSA2.crt");
        System.out.println("应用公钥数据："+publicKey);
    }
}
