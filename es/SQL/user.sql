/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.65.128
 Source Server Type    : MySQL
 Source Server Version : 50727
 Source Host           : 192.168.65.128:3308
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 50727
 File Encoding         : 65001

 Date: 30/09/2019 11:06:04
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '啊哈', '2019-09-29 06:32:20');
INSERT INTO `user` VALUES (2, '哦豁', '2019-09-29 07:39:41');
INSERT INTO `user` VALUES (3, '哈哈', '2019-09-29 08:49:18');
INSERT INTO `user` VALUES (4, '嗯啊', '2019-09-29 09:44:31');
INSERT INTO `user` VALUES (5, '王者荣耀是我家', '2019-09-30 09:42:38');
INSERT INTO `user` VALUES (6, '保护王者荣耀靠大家', '2019-09-30 09:42:47');

SET FOREIGN_KEY_CHECKS = 1;
