/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.65.128
 Source Server Type    : MySQL
 Source Server Version : 50727
 Source Host           : 192.168.65.128:3308
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 50727
 File Encoding         : 65001

 Date: 30/09/2019 11:06:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods
-- ----------------------------
INSERT INTO `goods` VALUES (1, '香蕉', '2019-09-29 08:49:07');
INSERT INTO `goods` VALUES (2, '苹果', '2019-09-29 08:55:37');
INSERT INTO `goods` VALUES (3, '梨子', '2019-09-29 09:44:38');
INSERT INTO `goods` VALUES (4, '可爱的破锅就是我们', '2019-09-30 03:04:09');
INSERT INTO `goods` VALUES (5, '何小卓是个猪吗', '2019-09-30 03:04:17');
INSERT INTO `goods` VALUES (6, '何小卓是猪', '2019-09-30 03:04:23');

SET FOREIGN_KEY_CHECKS = 1;
