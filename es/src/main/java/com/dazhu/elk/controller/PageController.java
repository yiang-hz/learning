package com.dazhu.elk.controller;

import com.dazhu.elk.entity.CloudDiskEntity;
import com.dazhu.elk.entity.Goods;
import com.dazhu.elk.entity.User;
import com.dazhu.elk.repository.CloudDiskDao;
import com.dazhu.elk.repository.GoodsDao;
import com.dazhu.elk.repository.UserDao2;
import com.google.common.collect.Lists;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class PageController {

    private final CloudDiskDao cloudDiskDao;
    private final UserDao2 userDao2;
    private final GoodsDao goodsDao;


    public PageController(CloudDiskDao cloudDiskDao, UserDao2 userDao2, GoodsDao goodsDao) {
        this.cloudDiskDao = cloudDiskDao;
        this.userDao2 = userDao2;
        this.goodsDao = goodsDao;
    }

    // 实现分页查询
    @RequestMapping("/search")
    public String search(String name, @PageableDefault(page = 0, value = 2) Pageable pageable, HttpServletRequest httpServletRequest) {
        Long startTime = System.currentTimeMillis();
        // 1.创建查询对象
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        if (!StringUtils.isEmpty(name)) {
            MatchQueryBuilder matchQuery = QueryBuilders.matchQuery("name", name);
            boolQuery.must(matchQuery);
        }
        // 2.调用查询接口
        Page<CloudDiskEntity> search = cloudDiskDao.search(boolQuery, pageable);
        httpServletRequest.setAttribute("page", search);
        //返回pageBean信息
//		Page<CloudDiskEntity> search1 = cloudDiskDao.search(boolQuery, pageable);
        // 3.将迭代器转换为集合
        Long endTime = System.currentTimeMillis();
        //计算查询总数
        long total = search.getTotalElements();
        httpServletRequest.setAttribute("total", search.getTotalElements());
        //计算总分页数
        int totalPage = (int) ((total -1 ) / search.getSize() + 1);
        httpServletRequest.setAttribute("totalPage", totalPage);
        httpServletRequest.setAttribute("time", endTime - startTime);
        httpServletRequest.setAttribute("total", search.getTotalElements());
        return "/search";
    }

    // 实现分页查询
    @RequestMapping("/searchUser")
    @ResponseBody
    public List<User> search(String name) {
        // 1.创建查询对象
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        if (!StringUtils.isEmpty(name)) {
            MatchQueryBuilder matchQuery = QueryBuilders.matchQuery("name", name);
            boolQuery.must(matchQuery);
        }
        // 2.调用查询接口
        Iterable<User> search = userDao2.search(boolQuery);
        return Lists.newArrayList(search);
    }

    // 实现分页查询
    @RequestMapping("/searchGoods")
    @ResponseBody
    public List<Goods> searchGoods(String name) {
        // 1.创建查询对象
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        if (!StringUtils.isEmpty(name)) {
            MatchQueryBuilder matchQuery = QueryBuilders.matchQuery("name", name);
            //模糊查询三者中含有关键字的数据
            //MultiMatchQueryBuilder name1 = QueryBuilders.multiMatchQuery("name", "name", "detail", "goods");
            boolQuery.must(matchQuery);
        }
        // 2.调用查询接口
        Iterable<Goods> search = goodsDao.search(boolQuery);
        return Lists.newArrayList(search);
    }
}
