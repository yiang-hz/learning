package com.dazhu.elk.repository;

import com.dazhu.elk.entity.CloudDiskEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface CloudDiskDao extends ElasticsearchRepository<CloudDiskEntity, String> {

}