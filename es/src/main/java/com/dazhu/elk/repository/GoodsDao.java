package com.dazhu.elk.repository;

import com.dazhu.elk.entity.Goods;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface GoodsDao  extends ElasticsearchRepository<Goods, Integer> {
}
