package com.dazhu.elk.entity;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.Date;

@Data
@Document(indexName = "goods", type = "goods")
public class Goods {

    @Id
    private Integer id;
    private String name;
    @JsonAlias("update_time")
    //@JsonFormat(pattern = "yyyy-MM-dd")
    private Date updateTime;
}
