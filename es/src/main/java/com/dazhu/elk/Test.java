package com.dazhu.elk;

import com.sun.jmx.snmp.Timestamp;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Test {

        public static void main(String[] args) throws ParseException {
                String time = "2019-09-28T23:39:41.000+0000";
                if (!StringUtils.isEmpty(time)){
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                        Timestamp c = new Timestamp(sdf.parse(time).getTime());
                        System.out.println(sdf.format(c.getSysUpTime()));
                }
        }
        private static Timestamp getTimestamp(String time) throws ParseException {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                return new Timestamp(sdf.parse(time).getTime());
        }
}
