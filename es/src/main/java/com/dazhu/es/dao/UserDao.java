package com.dazhu.es.dao;

import com.dazhu.es.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserDao extends CrudRepository<User, String> {
}
