package com.dazhu.es.controller;

import com.dazhu.es.dao.UserDao;
import com.dazhu.es.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class UserController {

    private final UserDao userDao;

    @Autowired
    public UserController(UserDao userDao) {
        this.userDao = userDao;
    }

    @RequestMapping("/add")
    public User add(@RequestBody User user){

        return userDao.save(user);
    }

    @RequestMapping("/find")
    public Optional<User> add(String id){
        return userDao.findById(id);
    }
}
