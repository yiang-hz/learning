package com.dazhu.es;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@SpringBootApplication
@EnableElasticsearchRepositories("com.dazhu.es.dao")
public class A {

    public static void main(String[] args) {
        SpringApplication.run(A.class, args);
    }
}
