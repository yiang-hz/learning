package com.dazhu.es.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;

@Document(indexName = "test")
@Data
public class User implements Serializable {

    @Id
    private String id;
    private String name;
    private Integer age;
    private String sex;
}
