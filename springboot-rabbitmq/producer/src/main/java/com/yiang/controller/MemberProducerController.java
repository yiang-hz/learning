package com.yiang.controller;

import com.yiang.rabbitmq.FanoutProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MemberProducerController {

	private final FanoutProducer fanoutProducer;

	@Autowired
	public MemberProducerController(FanoutProducer fanoutProducer) {
		this.fanoutProducer = fanoutProducer;
	}

	@RequestMapping("/sendMsg")
	public String sendMsg(String queueName) {
		fanoutProducer.send(queueName);
		return "success";
	}

}
