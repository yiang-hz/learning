//package com.yiang.rabbitmq.producer;
//
//import org.springframework.amqp.core.AmqpTemplate;
//import org.springframework.amqp.core.Message;
//import org.springframework.amqp.core.MessageBuilder;
//import org.springframework.amqp.core.MessageProperties;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import java.util.UUID;
//
//@Component
//public class FanoutProducer {
//
//	private final AmqpTemplate amqpTemplate;
//
//	@Autowired
//	public FanoutProducer(AmqpTemplate amqpTemplate) {
//		this.amqpTemplate = amqpTemplate;
//	}
//
//	/*
//	 * 发送一条消息
//	 * @param queueName 队列名称
//	 */
//	/*public void send(String queueName) {
//		System.out.println("queueName:" + queueName);
//		String msg = "msg:" + new Date();
//		// 发送消息
//		amqpTemplate.convertAndSend(queueName, msg);
//	}*/
//
//	/**
//	 * 发送一条消息，配置消息ID，解决消息幂等性
//	 * @param queueName 队列名称
//	 */
//	public void send(String queueName) {
//		System.out.println("queueName:" + queueName);
//		String msg = "msg:" + UUID.randomUUID();
//
//		Message message = MessageBuilder.withBody(msg.getBytes())
//				.setContentType(MessageProperties.CONTENT_TYPE_BYTES)
//				.setContentEncoding("UTF-8")
//				.setMessageId(UUID.randomUUID()+"")
//				.build();
//		// 发送消息
//		amqpTemplate.convertAndSend(queueName, message);
//	}
//
//}
