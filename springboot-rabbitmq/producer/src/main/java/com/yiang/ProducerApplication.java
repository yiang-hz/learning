package com.yiang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProducerApplication {

    //http://127.0.0.1:8088/sendMsg?queueName=new_fanout_email_queue
    public static void main(String[] args) {
        SpringApplication.run(ProducerApplication.class, args);
    }
}
