package org.flowable;

import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

/**
 * @author HeZhuo
 * @date 2021/6/8
 */
public class CallExternalSystemDelegate implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) {
        System.out.println("org.flowable - Calling the external system for employee "
            + execution.getVariable("employee"));
    }

}
