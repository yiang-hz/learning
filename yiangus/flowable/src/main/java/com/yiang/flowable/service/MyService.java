package com.yiang.flowable.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yiang.flowable.entity.Person;
import com.yiang.flowable.mapper.PersonMapper;
import lombok.AllArgsConstructor;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.task.api.Task;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author HeZhuo
 * @date 2021/6/8
 */
@Service
@AllArgsConstructor
@Transactional(rollbackFor = Exception.class)
public class MyService extends ServiceImpl<PersonMapper, Person> {

    private final RuntimeService runtimeService;
    private final TaskService taskService;

    public void startProcess(String assignee) {

        Person person = this.getOne(Wrappers.<Person>lambdaQuery().eq(Person::getUsername, assignee));

        Map<String, Object> variables = new HashMap<>(1);
        variables.put("person", person);
        runtimeService.startProcessInstanceByKey("oneTaskProcess", variables);
    }

    public List<Task> getTasks(String assignee) {
        return taskService.createTaskQuery().taskAssignee(assignee).list();
    }

    public void createDemoUsers() {
        if (this.list().size() == 0) {
            this.save(new Person("jbarrez", "Joram", "Barrez", new Date()));
            this.save(new Person("trademakers", "Tijs", "Rademakers", new Date()));
        }
    }

}
