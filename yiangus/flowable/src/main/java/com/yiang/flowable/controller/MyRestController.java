package com.yiang.flowable.controller;

import com.yiang.flowable.service.MyService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.flowable.task.api.Task;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HeZhuo
 * @date 2021/6/8
 */
@RestController
@AllArgsConstructor
public class MyRestController {

    private final MyService myService;

    @GetMapping(value="/index")
    public void startProcessInstance() {
        System.out.println("HTTP任务执行成功");
    }

    @PostMapping(value="/process")
    public void startProcessInstance(@RequestBody StartProcessRepresentation startProcessRepresentation) {
        myService.startProcess(startProcessRepresentation.getAssignee());
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    static class StartProcessRepresentation {
        private String assignee;
    }

    @RequestMapping(value="/tasks", method= RequestMethod.GET, produces= MediaType.APPLICATION_JSON_VALUE)
    public List<TaskRepresentation> getTasks(@RequestParam String assignee) {
        List<Task> tasks = myService.getTasks(assignee);
        List<TaskRepresentation> dtos = new ArrayList<>();
        for (Task task : tasks) {
            dtos.add(new TaskRepresentation(task.getId(), task.getName()));
        }
        return dtos;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    static class TaskRepresentation {
        private String id;
        private String name;
    }

}
