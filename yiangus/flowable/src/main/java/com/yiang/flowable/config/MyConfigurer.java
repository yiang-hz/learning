package com.yiang.flowable.config;

import org.flowable.spring.SpringProcessEngineConfiguration;
import org.flowable.spring.boot.EngineConfigurationConfigurer;
import org.springframework.context.annotation.Configuration;

/**
 * @author HeZhuo
 * @date 2021/6/8
 */
//@Configuration
public class MyConfigurer implements EngineConfigurationConfigurer<SpringProcessEngineConfiguration> {

    @Override
    public void configure(SpringProcessEngineConfiguration processEngineConfiguration) {
        // advanced configuration
    }

}
