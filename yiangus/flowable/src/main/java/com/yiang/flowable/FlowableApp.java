package com.yiang.flowable;

import com.yiang.flowable.service.MyService;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * @author HeZhuo
 * @date 2021/6/7
 */
@SpringBootApplication
@MapperScan("com.yiang.flowable.mapper")
public class FlowableApp {

    public static void main(String[] args) {
        SpringApplication.run(FlowableApp.class, args);
    }

    @Bean
    public CommandLineRunner init(final MyService myService) {

        return strings -> myService.createDemoUsers();
    }

}
