package com.yiang.flowable.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author HeZhuo
 * @date 2021/6/8
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("test_person")
public class Person implements Serializable {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String username;

    private String firstName;

    private String lastName;

    private Date birthDate;

    public Person(String username, String firstName, String lastName, Date birthDate) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
    }

}
