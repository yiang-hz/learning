package com.yiang.flowable.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yiang.flowable.entity.Person;
import org.springframework.stereotype.Repository;

/**
 * @author HeZhuo
 * @date 2021/6/8
 */
@Repository
public interface PersonMapper extends BaseMapper<Person> {

}
