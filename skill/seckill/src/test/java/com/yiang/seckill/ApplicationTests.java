package com.yiang.seckill;

import com.yiang.seckill.observer.subject.SeckillSubject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTests {

    private SeckillSubject myEvent;

    @Test
    public void contextLoads() {
    }
}