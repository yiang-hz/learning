package com.yiang.seckill.manager;

import com.alibaba.fastjson.JSONObject;
import com.yiang.seckill.observer.subject.SeckillSubject;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @author HeZhuo
 * @date 2020/12/1
 */
@Component
public class MessageManager {

    @Async
    public void sendMessage(JSONObject jsonObject) {
        SeckillSubject.abstractSubject.notifyObServer(jsonObject.toJSONString());
    }
}
