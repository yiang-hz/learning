package com.yiang.seckill.exceptions;

import com.baomidou.mybatisplus.extension.exceptions.ApiException;

/**
 * @author HeZhuo
 * @date 2020/12/1
 */
public class VerifyException extends ApiException {

    public VerifyException(String message) {
        super(message);
    }
}
