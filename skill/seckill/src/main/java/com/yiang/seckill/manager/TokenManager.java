package com.yiang.seckill.manager;

import com.yiang.seckill.utils.TokenUtils;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @author HeZhuo
 * @date 2020/12/1
 */
@Component
@AllArgsConstructor
public class TokenManager {

    private final TokenUtils tokenUtils;

    @Async
    public void createSeckillToken (String productId, Long inventory) {
        tokenUtils.createListToken("seckill_", productId, inventory);
    }

    public String getSeckillToken (String key) {
        return tokenUtils.getListKeyToken(key);
    }
}
