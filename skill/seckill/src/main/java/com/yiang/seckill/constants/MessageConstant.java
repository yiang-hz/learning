package com.yiang.seckill.constants;

/**
 * @author HeZhuo
 * @date 2020/12/1
 */
public interface MessageConstant {

    /**
     * 投递消息常量
     */
    String PRODUCT_ID = "productId";
    String PHONE = "phone";
    String SECKILL_TOKEN = "seckillToken";
}
