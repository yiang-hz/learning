package com.yiang.seckill.consumer;

import com.alibaba.fastjson.JSONObject;
import com.yiang.seckill.observer.Observer;
import lombok.extern.slf4j.Slf4j;

/**
 * @author HeZhuo
 * @date 2020/12/1
 */
@Slf4j
public class OtherListener implements Observer {

    @Override
    public void update(String message) {
        JSONObject jsonObject = JSONObject.parseObject(message);
        log.info("其它服务获取消息：" + jsonObject);
    }
}
