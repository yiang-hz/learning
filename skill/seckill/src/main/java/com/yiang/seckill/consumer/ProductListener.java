package com.yiang.seckill.consumer;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.yiang.seckill.constants.MessageConstant;
import com.yiang.seckill.mapper.OrderMapper;
import com.yiang.seckill.mapper.ProductMapper;
import com.yiang.seckill.model.OrderDo;
import com.yiang.seckill.model.ProductDo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author HeZhuo
 * @date 2020/12/1
 */
@Slf4j
@Component
public class ProductListener {

    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private OrderMapper orderMapper;

    private final AtomicInteger atomicInteger = new AtomicInteger(0);

    /**
     * 修改库存
     * @param message 信息
     */
    @Async
    @Transactional(rollbackFor = Exception.class)
    public void update(String message) {

        /*
         * 幂等性问题：
         * 1.使用全局MessageID判断消费方使用同一个，解决幂等性。
         * 2.或者使用业务逻辑保证唯一（比如订单号码）
         * 通过全局消息ID做控制。或者写个唯一标识，时间戳，Token，常用幂等性解决方案皆可。
         *
         * （全局ID基于Redis或其他校验）
         */

        JSONObject jsonObject = JSONObject.parseObject(message);
        log.info("商品服务获取消息：{}", jsonObject);

        //1.根据商品ID判断商品是否存在
        String productId = jsonObject.getString(MessageConstant.PRODUCT_ID);
        String phone = jsonObject.getString(MessageConstant.PHONE);
        ProductDo productDo = productMapper.selectById(productId);
        if (null == productDo) {
            return;
        }

        // 2.对库存实现减去1
        Long version = productDo.getVersion();
        //乐观锁
        int resultSeckill = productMapper.subCountOptimisticLock(productId, version);
        //默认比较大小 根据行锁
//        int resultSeckill = productMapper.subCountCheckout(productId);
        if (resultSeckill <= 0) {
            log.error("库存扣减报错，访问次数超量，请加班改BUG减少数据库IO操作次数...");
            return;
        }

        // 3.插入订单记录
        OrderDo orderEntity = new OrderDo();
        orderEntity.setUserPhone(phone);
        orderEntity.setProductId(productId);
        String seckillId = "order-"
                            + DateUtil.format(DateUtil.date(), DatePattern.PURE_DATETIME_PATTERN)
                            + String.format("-%05d", atomicInteger.incrementAndGet());
        orderEntity.setSeckillId(seckillId);
        int insert = orderMapper.insert(orderEntity);

        log.info("订单插入结果：{}", SqlHelper.retBool(insert));
    }

}
