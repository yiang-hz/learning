package com.yiang.seckill.controller;

import com.baomidou.mybatisplus.extension.api.R;
import com.yiang.seckill.server.ProductService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author HeZhuo
 * @date 2020/11/30
 */
@RestController
@AllArgsConstructor
public class ProductController {

    private final ProductService seckillService;

    @GetMapping("/seckill/panicBuy")
    @ApiOperation("抢购")
    public R<?> panicBuy (String phone, String productId, String type, Integer useToken) {
        return R.ok(seckillService.panicBuy(phone, productId, type, useToken));
    }

    @GetMapping("/seckill/resetCount")
    @ApiOperation("清除计数器信息")
    public void resetCount() {
        seckillService.resetCount();
    }

    @GetMapping("/seckill/addTokenBucket")
    @ApiOperation("生成Redis令牌桶")
    public R<?> addTokenBucket(String productId, Long count) {
        return seckillService.addTokenBucket(productId, count);
    }

    @GetMapping("/seckill/panicBuyAsync")
    @ApiOperation("抢购-MQ解耦")
    public R<?> panicBuyAsync (String phone, String productId) {
        seckillService.panicBuyAsync(phone, productId);
        return R.failed("正在抢购中...");
    }
}
