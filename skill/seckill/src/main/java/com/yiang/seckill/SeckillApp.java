package com.yiang.seckill;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author HeZhuo
 * @date 2020/11/30
 */
@SpringBootApplication
@MapperScan({"com.yiang.seckill.mapper"})
@EnableAsync
public class SeckillApp {

    public static void main(String[] args) {
        SpringApplication.run(SeckillApp.class, args);
    }
}
