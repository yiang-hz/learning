package com.yiang.seckill.observer.subject;

import com.yiang.seckill.consumer.OtherListener;
import com.yiang.seckill.observer.Observer;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HeZhuo
 * @date 2020/12/1
 */
@Slf4j
public class SeckillSubject implements AbstractSubject {

    /**
     * 存放观察者的集合
     */
    private final List<Observer> obServerList = new ArrayList<>();

    /**
     * 创建具体的主题， 并且通过static代码块加载响应服务
     */
    public static AbstractSubject abstractSubject = new SeckillSubject();

    static {
        abstractSubject.addObServer(new OtherListener());
//        abstractSubject.addObServer(new ProductListener());
    }

    @Override
    public void addObServer(Observer obServer) {
        // 注册或者添加观察者
        obServerList.add(obServer);
    }

    @Override
    public void removeServer(Observer obServer) {
        obServerList.remove(obServer);
    }

    @Override
    public void notifyObServer(String message) {
        log.info("异步推送消息：{}", message);
        // 调用观察者通知方案
        for (Observer observer : obServerList) {
            // 调用该方法通知 获取具体的消息  群发
            observer.update(message);
        }
    }

}
