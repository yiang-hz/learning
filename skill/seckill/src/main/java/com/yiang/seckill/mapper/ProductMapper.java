package com.yiang.seckill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yiang.seckill.model.ProductDo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

/**
 * @author HeZhuo
 * @date 2020/11/30
 */
@Repository
public interface ProductMapper extends BaseMapper<ProductDo> {

    /**
     * 操作数量扣减
     * @param productId 商品ID
     * @return int
     */
    @Update("UPDATE product SET " +
            "inventory = inventory - 1, " +
            "version = version + 1 " +
            "WHERE id = #{productId}")
    int subCount(@Param("productId") String productId);

    /**
     * 操作数量扣减，使用数量判断， 利用数据库自带的行锁
     * @param productId 商品ID
     * @return int
     */
    @Update("UPDATE product SET " +
            "inventory = inventory - 1, " +
            "version = version + 1 " +
            "WHERE id = #{productId} " +
            "AND inventory > 0  ")
    int subCountCheckout(@Param("productId") String productId);

    /**
     * 操作数量扣减，使用数量判断 + 乐观锁 利用数据库自带的行锁
     * @param productId 商品ID
     * @param version 乐观锁标志，version版本号
     * @return int
     */
    @Update("UPDATE product SET " +
            "inventory = inventory - 1, " +
            "version = version + 1 " +
            "WHERE id = #{productId} " +
            "AND inventory > 0  " +
            "AND version = #{version};")
    int subCountOptimisticLock(@Param("productId") String productId, @Param("version") Long version);
}
