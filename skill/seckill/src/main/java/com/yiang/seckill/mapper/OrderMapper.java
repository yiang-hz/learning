package com.yiang.seckill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yiang.seckill.model.OrderDo;
import org.springframework.stereotype.Repository;

/**
 * @author HeZhuo
 * @date 2020/11/30
 */
@Repository
public interface OrderMapper extends BaseMapper<OrderDo> {
}
