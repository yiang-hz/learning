package com.yiang.seckill.model;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author HeZhuo
 * @date 2020/11/30
 */
@Data
@ApiModel("秒杀成功订单")
@AllArgsConstructor
@NoArgsConstructor
@TableName("seckill_order")
public class OrderDo {

	@ApiModelProperty("订单秒杀id")
	private String seckillId;

	@ApiModelProperty("商品ID")
	private String productId;

	@ApiModelProperty("用户userId")
	private String userPhone;

	@ApiModelProperty("订单状态")
	private Integer state;

	@ApiModelProperty("创建时间")
	private Date createTime;
}