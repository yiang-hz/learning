package com.yiang.seckill.server;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.enums.ApiErrorCode;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.yiang.seckill.constants.MessageConstant;
import com.yiang.seckill.consumer.ProductListener;
import com.yiang.seckill.exceptions.VerifyException;
import com.yiang.seckill.manager.MessageManager;
import com.yiang.seckill.manager.TokenManager;
import com.yiang.seckill.mapper.ProductMapper;
import com.yiang.seckill.model.ProductDo;
import com.yiang.seckill.utils.RedisUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.concurrent.atomic.AtomicInteger;


/**
 * @author HeZhuo
 * @date 2020/11/30
 */
@Service
@Slf4j
@AllArgsConstructor
public class ProductService extends ServiceImpl<ProductMapper, ProductDo> {

    private final AtomicInteger count = new AtomicInteger(0);

    private final ProductMapper productMapper;
    private final TokenManager tokenManager;
    private final RedisUtil redisUtil;
    private final MessageManager messageManager;
    private final ProductListener productListener;

    public boolean panicBuy(String phone, String productId, String type, Integer useToken) {

        // 1.验证参数
        this.checkArgs(phone, productId);

        if (ObjectUtils.isEmpty(type)) {
            type = "";
        }

        // 2.从Redis中获取令牌
        if (!ObjectUtils.isEmpty(useToken) && useToken == 1) {
            String spikeToken = tokenManager.getSeckillToken(productId);
            if (ObjectUtils.isEmpty(spikeToken)) {
                throw new VerifyException("很抱歉，当前商品已经售空，请下次再来！");
            }
        }

        int resultInt;
        //计数器加1
        count.getAndIncrement();

        //3.根据类型判断走哪个方法
        switch (type) {
            // > 0 判断
            case "checkOut":
                resultInt = productMapper.subCountCheckout(productId);
                break;

            //乐观锁机制
            case "optimisticLock":
                ProductDo productDo = productMapper.selectById(productId);
                if (null == productDo) {
                    throw new VerifyException("该商品不存在！");
                }

                Long version = productDo.getVersion();
                resultInt = productMapper.subCountOptimisticLock(productId, version);

                if (!SqlHelper.retBool(resultInt)) {
                    throw new VerifyException("该商品库存已经没有啦！");
                }
                break;

            //默认无锁
            default:
                resultInt = productMapper.subCount(productId);
                break;
        }

        log.info("总共操作了[{}]次。", count.get());

        return SqlHelper.retBool(resultInt);
    }

    /**
     * 清除计数器数量
     */
    public void resetCount() {
        count.set(0);
    }

    /**
     * 生成令牌桶
     * @param productId 商品ID
     * @return 操作成功
     */
    public R<?> addTokenBucket(String productId, Long count) {

        // 1.验证参数
        if (ObjectUtils.isEmpty(productId)) {
            return R.failed("productId不能为空");
        }

        if (ObjectUtils.isEmpty(count)) {
            ProductDo productDo = productMapper.selectById(productId);
            if (null == productDo) {
                return R.failed("该商品不存在！");
            }

            count = productDo.getInventory();
        }

        //异步创建令牌桶
        tokenManager.createSeckillToken(productId, count);

        return R.ok(ApiErrorCode.SUCCESS);
    }

    public void panicBuyAsync(String phone, String productId) {
        // 1.验证参数
        this.checkArgs(phone, productId);

        // 2.对用户的频率实现限制 10s setNx key存在返回1 key如果不存在的情况0
        Long timeOut = 10L;
        Boolean spikeNx = redisUtil.setNx(phone, productId, timeOut);
//        if (!spikeNx) {
//            throw new VerifyException("您当前在10s内访问频率过多,请稍后重试!");
//        }

        // 3.从Redis中获取令牌
        String seckillToken = tokenManager.getSeckillToken(productId);
        if (ObjectUtils.isEmpty(seckillToken)) {
            throw new VerifyException("很抱歉，当前商品已经售空，请下次再来！");
        }

        // 4.异步发布消息
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(MessageConstant.PHONE, phone);
        jsonObject.put(MessageConstant.PRODUCT_ID, productId);
        jsonObject.put(MessageConstant.SECKILL_TOKEN, seckillToken);

        //异步推送信息
        messageManager.sendMessage(jsonObject);

        //将商品服务消费者拆成异步方法以方便讲解
        productListener.update(jsonObject.toJSONString());
    }


    private void checkArgs (String phone, String productId) {
        if (ObjectUtils.isEmpty(phone)) {
            throw new VerifyException("userPhone不能为空");
        }
        if (ObjectUtils.isEmpty(productId)) {
            throw new VerifyException("productId不能为空");
        }
    }
}
