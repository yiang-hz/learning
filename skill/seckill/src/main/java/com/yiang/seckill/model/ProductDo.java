package com.yiang.seckill.model;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author HeZhuo
 * @date 2020/11/30
 */
@Data
@ApiModel("商品库存表对应实体类")
@AllArgsConstructor
@NoArgsConstructor
@TableName("product")
public class ProductDo implements Serializable {

	@ApiModelProperty("库存id")
	private String id;

	@ApiModelProperty("商品名称")
	private String name;

	@ApiModelProperty("库存数量")
	private Long inventory;

	@ApiModelProperty("开启时间")
	private Date startTime;

	@ApiModelProperty("结束时间")
	private Date endTime;

	@ApiModelProperty("创建时间")
	private Date createTime;

	@ApiModelProperty("秒杀抢购")
	private Long version;

}