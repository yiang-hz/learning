package com.yiang.seckill.observer;

/**
 * 实现该接口获取可观察对象变化
 * @author HeZhuo
 * @date 2020/10/28
 */
public interface Observer {

    /**
     * 通知方法，在被观察对象发生变化时调用。所有观察者收到变更通知。
     * @param message 消息
     */
    void update(String message);
}
