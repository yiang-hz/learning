package com.yiang.seckill.server;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yiang.seckill.exceptions.VerifyException;
import com.yiang.seckill.mapper.OrderMapper;
import com.yiang.seckill.model.OrderDo;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * @author HeZhuo
 * @date 2020/11/30
 */
@Service
public class OrderService extends ServiceImpl<OrderMapper, OrderDo> {

    public List<OrderDo> querySeckill(String phone, String productId) {

        //1.参数校验
        if (ObjectUtils.isEmpty(phone)) {
            throw new VerifyException("userPhone不能为空");
        }
        if (ObjectUtils.isEmpty(productId)) {
            throw new VerifyException("productId不能为空");
        }

        //2.获取订单信息，无则返回在排队中
        List<OrderDo> list = this.list(Wrappers.<OrderDo>lambdaQuery()
                .eq(OrderDo::getProductId, productId)
                .eq(OrderDo::getUserPhone, phone)
        );

        if (CollectionUtils.isEmpty(list)) {
            throw new VerifyException("正在排队中.....");
        }

        //  return message:"秒杀成功"

        return list;
    }
}
