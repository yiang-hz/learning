package com.yiang.seckill.controller;

import com.baomidou.mybatisplus.extension.api.R;
import com.yiang.seckill.model.OrderDo;
import com.yiang.seckill.server.OrderService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author HeZhuo
 * @date 2020/11/30
 */
@RestController
@AllArgsConstructor
public class OrderController {

    private final OrderService orderService;

    @GetMapping("/order/querySeckill")
    @ApiOperation("查询秒杀信息")
    public R<List<OrderDo>> querySeckill (String phone, String productId) {

        //1. Redis Key控制器  （秒杀开始创建，秒杀结束过期 | 通过MQ推送的全局Msg Id）
        //2. Redis 查询限制  （状态）

        // Redis保留Key 或状态 未消费情况下  throw new VerifyException("正在排队中.....");

        return R.ok(orderService.querySeckill(phone, productId));
    }
}
