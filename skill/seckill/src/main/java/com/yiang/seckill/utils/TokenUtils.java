package com.yiang.seckill.utils;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author HeZhuo
 * @date 2020/11/30
 */
@AllArgsConstructor
@Component
public class TokenUtils {

    private final RedisUtil redisUtil;

    public void createListToken(String keyPrefix, String redisKey, Long tokenQuantity) {
        List<String> listToken = getListToken(keyPrefix, tokenQuantity);
        redisUtil.setList(redisKey, listToken);
    }

    public List<String> getListToken(String keyPrefix, Long tokenQuantity) {
        List<String> listToken = new ArrayList<>();
        for (int i = 0; i < tokenQuantity; i++) {
            String token = keyPrefix + UUID.randomUUID().toString().replace("-", "");
            listToken.add(token);
        }
        return listToken;
    }

    public String getListKeyToken(String key) {
        return redisUtil.getStringRedisTemplate().opsForList().leftPop(key);
    }
}
