package com.yiang.distribute.read.controller;

import com.yiang.distribute.read.config.RedissonConfig;
import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Duration;
import java.util.List;

/**
 * @author HeZhuo
 * @date 2020/12/2
 */
@Slf4j
@RestController
public class RedisController {

    @Autowired
    private Redisson redisson;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @RequestMapping("/reduceStock")
    public String reduceStock(String lockKey) {
        RLock redissonLock = redisson.getLock(lockKey);
        try {
            // 加锁，锁续命
            redissonLock.lock();
            Thread.sleep(50);
            // 从redis中获取库存数量
            String stockCount = stringRedisTemplate.opsForValue().get(RedissonConfig.KEY + lockKey);
            if (ObjectUtils.isEmpty(stockCount)) {
                return "为空！";
            }
            int stock = Integer.parseInt(stockCount);
            if (stock > 0) {
                // 减库存
                int restStock = stock - 1;
                // 剩余库存再重新设置到redis中
                stringRedisTemplate.opsForValue().set(RedissonConfig.KEY + lockKey, String.valueOf(restStock));
                log.info("扣减成功，剩余库存：{}, Key：{}", restStock, lockKey);

                System.out.println("执行成功，请快速宕机");
                Thread.sleep(15000);
            } else {
                log.info("库存不足，扣减失败。");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            //正常运行则在finally关闭锁，程序宕机则有20秒时间自动关闭锁
            redissonLock.unlock();
        }
        return "success";
    }

    @RequestMapping("/requestLimit/{id}")
    public Object requestLimit(@RequestBody List<Integer> array, @PathVariable String id) {
        //三秒钟内，拒绝不同数据二次操作
        Boolean spikeNxTime = stringRedisTemplate.opsForValue().setIfAbsent(
                id, "",
                Duration.ofSeconds(3L)
        );
        if (!ObjectUtils.isEmpty(spikeNxTime) && !spikeNxTime) {
            return "同一用户请不要在三秒钟内频繁操作库存数据！";
        }

        long timeOut = 10L;
        Boolean spikeNx = stringRedisTemplate.opsForValue().setIfAbsent("keyList", array.toString(), Duration.ofSeconds(timeOut));

        if (!ObjectUtils.isEmpty(spikeNx) && !spikeNx) {

            String keyList = stringRedisTemplate.opsForValue().get("keyList");
            if (array.toString().equals(keyList)) {
                return "不要一直操作同一条记录！";
            }
            return "您当前在10s内访问频率过多,请稍后重试!";
        }
        return array;
    }
}
