package com.yiang.distribute;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author HeZhuo
 * @date 2020/12/1
 */
@SpringBootApplication
public class LockApp {

    public static void main(String[] args) {
        SpringApplication.run(LockApp.class, args);
    }
}
