package com.yiang.distribute.read.config;

import org.redisson.Redisson;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RedissonConfig {

    @Value("${spring.redis.host}")
    private String host;

    @Value("${spring.redis.port}")
    private String port;

    public static final String KEY = "sale::updateSaleStock:";

    @Bean
    public Redisson redisson() {
        // 单机模式
        Config config = new Config();
        //DEV: redis:// 127.0.0.1
        config.useSingleServer().setAddress("redis://" + host + ":" + port).setDatabase(0);
        return (Redisson) Redisson.create(config);
    }
}