package com.yiangus.login;

import me.zhyd.oauth.config.AuthConfig;
import me.zhyd.oauth.model.AuthCallback;
import me.zhyd.oauth.request.AuthDingTalkRequest;
import me.zhyd.oauth.request.AuthRequest;
import me.zhyd.oauth.utils.AuthStateUtils;

/**
 * @author HeZhuo
 * @date 2021/7/8
 */
public class DingTalkDemo {

    public static void main(String[] args) {

        // 创建授权request
        AuthRequest authRequest = new AuthDingTalkRequest(AuthConfig.builder()
                .clientId("dingtycotzm02eawvuvq")
                .clientSecret("ACD2JTGft56xMJcgs-ITbMnFpIn468NPsxbW6dvzVAvqY_sUCFq66VVJgKir1lXm")
                .redirectUri("http://jgy.flowable.jgy.cwn/")
                .build());
        String authorizeUrl = authRequest.authorize(AuthStateUtils.createState());
        System.out.println(authorizeUrl);
        // 生成授权页面
        authRequest.authorize("STATE");
        // 授权登录后会返回code（auth_code（仅限支付宝））、state，1.8.0版本后，可以用AuthCallback类作为回调接口的参数
        // 注：JustAuth默认保存state的时效为3分钟，3分钟内未使用则会自动清除过期的state
        authRequest.login(AuthCallback.builder().build());
    }
}
