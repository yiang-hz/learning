package com.yiangus.login;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author HeZhuo
 * @date 2021/7/8
 */
@SpringBootApplication
public class JustAuthApp {

    public static void main(String[] args) {
        SpringApplication.run(JustAuthApp.class, args);
    }
}
