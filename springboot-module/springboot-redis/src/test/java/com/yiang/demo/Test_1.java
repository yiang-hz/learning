package com.yiang.demo;

import cn.hutool.core.convert.Convert;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.DefaultTypedTuple;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Test_1{

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;


    @Test
    public void set2() {
        Boolean aBoolean = redisTemplate.opsForValue().setIfAbsent("ifP", "dwdw2");
        System.out.println(aBoolean);
    }

    @Test
    public void set() {

        // 如果redisTemplate 是String，String 添加到redis的值就是 uu
        // 但是如果RedisTemplate是 String，Object 添加到redis的值就是 "uu" 注意，区别带上了双引号
        redisTemplate.opsForZSet().add("myKey", "uu", 1d);
        System.out.println(redisTemplate.opsForZSet().getOperations());

        List<User> userList = new ArrayList<>();
        userList.add(new User("张三", "1"));
        userList.add(new User("李四", "2"));
        Set<ZSetOperations.TypedTuple<Object>> tuples = new HashSet<>();
        userList.forEach((user -> tuples.add(
                new DefaultTypedTuple<>(
                        new User(user.getName(), user.getId()), Convert.toDouble(user.getId())
                )
        )));

        // 返回集合中所有的元素
        Set<Object> key = redisTemplate.opsForZSet().range("KEY", 0, -1);
        LinkedHashSet<Object> linkedHashSets = (LinkedHashSet<Object>) key;
        System.out.println(linkedHashSets);

        //redisTemplate.opsForZSet().add("KEY", tuples);

        //redisTemplate.opsForZSet().removeRange("KEY", 0, -1);
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    static class User{
        String name;
        String id;
    }
}
