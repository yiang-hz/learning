package com.yiang.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author HeZhuo
 * @date 2021/10/26
 */
@RestController
public class RedisController {

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    public void add() {

    }
}
