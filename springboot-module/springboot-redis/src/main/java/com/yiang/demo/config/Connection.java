package com.yiang.demo.config;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.json.JSONUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author HeZhuo
 * @date 2021/10/25
 */
public class Connection {


    public static void main(String[] args) {
        connection();
    }
    //创建连接
    public static void connection(){
        JedisPoolConfig jpc = new JedisPoolConfig();  //创建redis连接池配置对象
        jpc.setMaxIdle(20);     //最大连接空闲数
        jpc.setMaxTotal(50);    //最大连接数
        JedisPool jp = new JedisPool(jpc, "127.0.0.1", 6379); //创建连接池对象
        Jedis jedis = jp.getResource();
        //数据操作
        jp.returnResource(jedis);
        jedis.zadd("a", 2d, "bb");

        Long setnx = jedis.setnx("ifP", "bb");
        // 对应Spring的语法 SetIfAbsent
        System.out.println(setnx);
        Long setnx2 = jedis.setnx("ifP2", "bb2");
        System.out.println(setnx2);

        List<User> userList = new ArrayList<>();
        userList.add(new User("张三", "1"));
        userList.add(new User("李四", "2"));
        userList.forEach((user -> jedis.zadd("KEY_JEDIS", Convert.toDouble(user.getId()), JSONUtil.toJsonStr(user))));

        LinkedHashSet<String> linkedHashSet = (LinkedHashSet<String>) jedis.zrange("KEY_JEDIS", 0, -1);
        LinkedHashSet<User> collect = (LinkedHashSet<User>) linkedHashSet
                .stream()
                .map(user -> BeanUtil.toBean(user, User.class))
                .collect(Collectors.toSet());
        System.out.println(collect);

        // 删除从头到尾的元素
        //jedis.zremrangeByRank("KEY_JEDIS", 0, -1);

        // 删除指定元素
        // jedis.zrem("KEY_JEDIS", JSONUtil.toJsonStr(new User("张三", "1")));
        // 不指定会报错
        // jedis.zrem("KEY_JEDIS");
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    static class User{
        String name;
        String id;
    }
}
