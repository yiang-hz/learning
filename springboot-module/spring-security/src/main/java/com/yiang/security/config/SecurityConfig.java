package com.yiang.security.config;

import com.yiang.security.entity.Permission;
import com.yiang.security.handler.MyAuthenticationFailureHandler;
import com.yiang.security.handler.MyAuthenticationSuccessHandler;
import com.yiang.security.mapper.PermissionMapper;
import com.yiang.security.service.MyUserDetailsService;
import com.yiang.security.utils.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;


// Security 配置
@Component
//通过该注解开启一个过滤器，拦截所有请求。
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final MyAuthenticationFailureHandler failureHandler;
    private final MyAuthenticationSuccessHandler successHandler;

    private final MyUserDetailsService myUserDetailsService;
	private PermissionMapper permissionMapper;

    @Autowired
    public SecurityConfig(MyAuthenticationFailureHandler failureHandler, MyAuthenticationSuccessHandler successHandler, MyUserDetailsService myUserDetailsService, PermissionMapper permissionMapper) {
        this.failureHandler = failureHandler;
        this.successHandler = successHandler;
		this.myUserDetailsService = myUserDetailsService;
		this.permissionMapper = permissionMapper;
	}

    // 配置认证用户信息和权限
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//		// 添加admin账号
//		auth.inMemoryAuthentication().withUser("admin").password("123456").
//		authorities("showOrder","addOrder","updateOrder","deleteOrder");
//		// 添加userAdd账号
//		auth.inMemoryAuthentication().withUser("userAdd").password("123456").authorities("showOrder","addOrder");

		// 如果想实现动态账号与数据库关联 在该地方改为查询数据库
		auth.userDetailsService(myUserDetailsService).passwordEncoder(new PasswordEncoder() {
			@Override
			//对表单密码进行加密
			public String encode(CharSequence requestPassword) {
				return MD5Util.encode(String.valueOf(requestPassword));
			}

			@Override
			//加密密码与数据库密码进行匹配。 basePassword为数据库密码加密字段
			public boolean matches(CharSequence requestPassword, String basePassword) {
				return MD5Util.encode(String.valueOf(requestPassword)).equals(basePassword);
			}
		});
	}

	// 配置拦截请求资源
	protected void configure(HttpSecurity http) throws Exception {

        //此代码表示所有请求都会走httpBasic模式进行拦截
        //http.authorizeRequests().antMatchers("/**").fullyAuthenticated().and().httpBasic();

		// 如何权限控制 给每一个请求路径 分配一个权限名称 让后账号只要关联该名称，就可以有访问权限
//		http.authorizeRequests()
//		// 配置查询订单权限
//		.antMatchers("/showOrder").hasAnyAuthority("showOrder")
//		.antMatchers("/addOrder").hasAnyAuthority("addOrder")
//		.antMatchers("/login").permitAll()
//		.antMatchers("/updateOrder").hasAnyAuthority("updateOrder")
//		.antMatchers("/deleteOrder").hasAnyAuthority("deleteOrder")
//		.antMatchers("/**").fullyAuthenticated().and().formLogin().loginPage("/login").
//		successHandler(successHandler).failureHandler(failureHandler)
//                //关闭验证
//		.and().csrf().disable();

		//从数据库加载菜单权限表格。
		ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry authorizeRequests = http
				.authorizeRequests();
		// 1.读取数据库权限列表
		List<Permission> listPermission = permissionMapper.findAllPermission();
		System.out.println("数据库权限列表：" + listPermission.toString());
		for (Permission permission : listPermission) {
			// 设置权限
			authorizeRequests.antMatchers(permission.getUrl()).hasAnyAuthority(permission.getPermTag());
		}
		authorizeRequests.antMatchers("/login").permitAll().antMatchers("/**").fullyAuthenticated().and().formLogin()
				.loginPage("/login").successHandler(successHandler).and().csrf().disable();
	}

	//设置密码无需加密。
    //There is no PasswordEncoder mapped for the id "null"
    //原因:升级为Security5.0以上密码支持多中加密方式，回复以前模式
	@Bean
	public static NoOpPasswordEncoder passwordEncoder() {
		return (NoOpPasswordEncoder) NoOpPasswordEncoder.getInstance();
	}

}
