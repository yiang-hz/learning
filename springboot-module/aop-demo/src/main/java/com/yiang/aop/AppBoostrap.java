package com.yiang.aop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author HeZhuo
 * @date 2020/5/9
 */
@SpringBootApplication
public class AppBoostrap {

    public static void main(String[] args) {
        SpringApplication.run(AppBoostrap.class, args);
    }
}
