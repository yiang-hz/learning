package com.yiang.aop;

import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author HeZhuo
 * @date 2020/5/9
 */
@Aspect
@Component
@Order(1)
public class ApiAspect {

    @Around("execution(* *..*.*())")
    public void before(){
        System.out.println("start...");
    }
}
