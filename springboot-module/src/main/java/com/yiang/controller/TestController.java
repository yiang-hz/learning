package com.yiang.controller;

import com.alibaba.fastjson.JSON;
import com.yiang.entity.Order;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * @author Administrator
 */
@RestController
@RequestMapping("/a")
public class TestController {

    @RequestMapping("test")
    public String test(Integer i){
        return "1" + i;
    }

    @RequestMapping("money")
    public Object money(){
        BigDecimal bigDecimal = BigDecimal.valueOf(2);
        Order order = new Order(1L, bigDecimal);
        System.out.println(order);
        return JSON.toJSON(order);
    }
}
