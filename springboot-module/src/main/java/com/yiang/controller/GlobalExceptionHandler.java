package com.yiang.controller;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

/**
 * 全局异常处理类
 * 注解:
 * 1、ControllerAdvice 组合注解，里面含有Component
 * 2、ResponseBody 返回DZResponseData
 * @author Administrator
 */
@ControllerAdvice
@ResponseBody
@Slf4j
public class GlobalExceptionHandler {

    /**
     * 处理全局异常
     * @param e 异常
     * @param request 请求
     * @return 返回通用提示语句。
     */
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public String defaultExceptionHandler(MethodArgumentTypeMismatchException e, HttpServletRequest request){

        //打印对应异常信息
        this.logMessage(request, e);

        //系统全部异常
        log.error("[Exception]:Message:{" + e.getMessage() + "}, body:", e);

        return "方法参数非法传递！";
    }

    /**
     * 处理全局异常
     * @param e 异常
     * @param request 请求
     * @return 返回通用提示语句。
     */
    @ExceptionHandler(Exception.class)
    public String defaultExceptionHandler(Exception e, HttpServletRequest request){

        //打印对应异常信息
        this.logMessage(request, e);

        //系统全部异常
        log.error("[Exception]:Message:{" + e.getMessage() + "}, body:", e);

        return "Exception";
    }

    /**
     * 额外信息打印方法，打印需要的方法信息
     * @param request 请求
     */
    private void logMessage(HttpServletRequest request, Exception e){

        // 1.封装异常日志信息
        JSONObject errorJson = new JSONObject();
        JSONObject logJson = new JSONObject();
        logJson.put("request_time", new Date());
        logJson.put("error_info", e);

        //------获取节点IP + 端口号信息
        //IP以及端口号
        logJson.put("address", "127");
        logJson.put("port", "8080");
        //------获取节点IP + 端口号信息

        //3、请求地址
        logJson.put("url", request.getRequestURL().toString());

        //4、参数信息
        StringBuilder stringBuilder = new StringBuilder();
        for (Map.Entry<String, String[]> entry : request.getParameterMap().entrySet()) {
            String key = entry.getKey();
            String[] value = entry.getValue();
            stringBuilder.append("{")
                    //参数名称
                    .append(key)
                    .append(":")
                    //参数值
                    .append(Arrays.toString(value)).append("},");
        }
        logJson.put("args", stringBuilder.toString());

        errorJson.put("hotel_request_error", logJson);
        log.error(errorJson.toJSONString());
    }
}