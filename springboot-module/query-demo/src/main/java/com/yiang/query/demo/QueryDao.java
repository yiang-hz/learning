package com.yiang.query.demo;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

@Mapper
public interface QueryDao {

    @Select("")
    List<Map<String, Object>> query ();
}
