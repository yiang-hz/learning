package com.yiang.query;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.yiang.aop")
public class QueryDemoApp {

    public static void main(String[] args) {
        SpringApplication.run(QueryDemoApp.class, args);
    }
}
