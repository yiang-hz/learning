package com.yiang.file;

import cn.hutool.core.io.FileTypeUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.CharUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.extra.ftp.Ftp;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * @author HeZhuo
 * @date 2020/8/13
 */

@RestController
public class FileController {

    @PostMapping("/upload")
    public Object upload(MultipartFile file) {

        if (file.isEmpty()){
            return "文件为空！";
        }
        try {
            doUpload(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "SUCCESS";
    }

    @PostMapping("/uploads")
    public Object uploads(MultipartFile[] files) {

        if (ObjectUtil.isEmpty(files)){
            return "文件为空！";
        }

        try {
            for (MultipartFile file : files) {
                doUpload(file);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "SUCCESS";
    }

    private void doUpload(MultipartFile file) throws IOException {
        String type = FileTypeUtil.getType(file.getInputStream());
        if (ObjectUtil.isEmpty(type)){
            type = FileUtil.extName(file.getOriginalFilename());
            System.out.println("真名赋值...");
        }
        System.out.println("文件类型：" + type);

        String path = "/wwwroot/file/cert/alipay/" + "aliPayRoot" + CharUtil.DOT + type;

        File image = FileUtil.writeBytes(file.getBytes(), path);

        System.out.println("文件保存路径：" + image.getPath());

//        boolean del = FileUtil.del(path);
//
//        System.out.println(del);
    }

}
