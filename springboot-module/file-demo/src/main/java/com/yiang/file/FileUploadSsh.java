package com.yiang.file;

import cn.hutool.core.io.FileUtil;
import cn.hutool.extra.ftp.Ftp;
import cn.hutool.extra.ftp.FtpConfig;
import cn.hutool.extra.ftp.FtpMode;
import cn.hutool.extra.ssh.JschUtil;
import cn.hutool.extra.ssh.Sftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import org.apache.commons.net.ftp.FTPClient;

import java.io.IOException;
import java.net.InetAddress;

/**
 * @author HeZhuo
 * @date 2020/8/13
 */
public class FileUploadSsh {

    static String host = "192.168.1.11";

    public static void main(String[] args) throws JSchException, SftpException {

        //新建会话，此会话用于ssh连接到跳板机（堡垒机），此处为10.1.1.1:22
        Session session = JschUtil.getSession("192.168.1.11", 22, "root", "t3ims19");
        System.out.println(session.toString());

        try {
            doUploadSsh();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 所需依赖
     *        <dependency>
     *             <groupId>commons-net</groupId>
     *             <artifactId>commons-net</artifactId>
     *             <version>3.6</version>
     *         </dependency>
     * @throws IOException 异常
     */
    private static void doUploadSsh() throws IOException {
        String path = "/wwwroot/file/cert/alipay/";
        //匿名登录（无需帐号密码的FTP服务器）
//        Ftp ftp = new Ftp(host, 22, "root", "t3ims19");
        FtpConfig ftpConfig = new FtpConfig();
        ftpConfig.setHost(host);
        ftpConfig.setPort(22);
        ftpConfig.setUser("root");
        ftpConfig.setPassword("t3ims19");
        ftpConfig.setSoTimeout(5000);
        ftpConfig.setConnectionTimeout(5000);

//        Ftp ftp = new Ftp(ftpConfig, FtpMode.Active);
        FTPClient ftpClient = new FTPClient();
        ftpClient.connect(host);
        ftpClient.port(InetAddress.getByName(host), 21);
        ftpClient.login("root", "t3ims19");
        System.out.println(ftpClient.getReplyCode());

//        //进入远程目录
//        ftp.cd(path);
//        //上传本地文件
//        ftp.upload(path, FileUtil.file("D:\\initpath\\image\\a.txt"));
//        //下载远程文件
//        ftp.download(path, "b.txt", FileUtil.file("D:\\initpath\\"));
//        //关闭连接
//        ftp.close();
    }

    /**
     * 所需依赖
     *        <dependency>
     *             <groupId>commons-net</groupId>
     *             <artifactId>commons-net</artifactId>
     *             <version>3.6</version>
     *         </dependency>
     * @throws IOException 异常
     */
    private static void doUploadSshS() throws IOException {
        String path = "/wwwroot/file/cert/alipay/";
        //匿名登录（无需帐号密码的FTP服务器）
        Sftp ftp = new Sftp(host, 22, "root", "t3ims19");
        System.out.println(ftp.pwd());
        //进入远程目录
        ftp.cd(path);
        //上传本地文件
        ftp.upload(path, FileUtil.file("D:\\initpath\\image\\a.txt"));
        //下载远程文件
        ftp.download(path, FileUtil.file("D:\\initpath\\"));
        //关闭连接
        ftp.close();
    }
}
