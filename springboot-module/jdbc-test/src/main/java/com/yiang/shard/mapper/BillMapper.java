package com.yiang.shard.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yiang.shard.Bill;
import org.springframework.stereotype.Repository;

@Repository
public interface BillMapper extends BaseMapper<Bill> {

    int getCount();
}