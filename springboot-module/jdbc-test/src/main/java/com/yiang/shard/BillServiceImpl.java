package com.yiang.shard;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yiang.shard.mapper.BillMapper;
import org.springframework.stereotype.Service;
@Service
public class BillServiceImpl extends ServiceImpl<BillMapper, Bill> implements BillService {

}