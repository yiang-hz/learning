package com.yiang.shard;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import java.util.Date;
@Data
@TableName("t_bill")
public class Bill {
    @TableId
    private Long orderId;
    private Integer userId;
    private Long addressId;
    private String status;
    private Date createTime;
}