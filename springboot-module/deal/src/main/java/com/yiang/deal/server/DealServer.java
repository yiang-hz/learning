package com.yiang.deal.server;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yiang.deal.entity.Deal;
import com.yiang.deal.mapper.DealMapper;
import org.springframework.stereotype.Service;

/**
 * @author HeZhuo
 * @date 2022/12/6
 */
@Service
public class DealServer extends ServiceImpl<DealMapper, Deal> {

}
