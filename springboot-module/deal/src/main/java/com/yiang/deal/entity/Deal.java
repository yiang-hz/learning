package com.yiang.deal.entity;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author HeZhuo
 * @date 2022/12/6
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("deal")
public class Deal {

    @ApiModelProperty("主键ID")
    private Integer id;

    @ApiModelProperty("代码")
    private String code;

    @ApiModelProperty("名称")
    private String name;

    @ApiModelProperty("交易时间")
    private Date date;

    @ApiModelProperty("成交价格")
    @TableField("price")
    private BigDecimal buyPrice;

    @ApiModelProperty("交易方式：0卖出 sell  buy 1 买入")
    private String method;

    @ApiModelProperty("状态，进行数据区分使用")
    private String status;

    @ApiModelProperty("数量")
    private Integer count;

    @ApiModelProperty("盈亏")
    private String win;

    @ApiModelProperty("盈亏比例")
    private String winRate;

    @ApiModelProperty("仓位总占比")
    @TableField("rate")
    private String allRate;

    @ApiModelProperty("归属人")
    @TableField("type")
    private String belong;

    @ApiModelProperty("单日涨幅")
    private String dateRate;

    @ApiModelProperty("收盘价")
    private BigDecimal closePrice;

    /*
     * 重写获取时间的get方法
     */
    public Date getDate() {
        return DateUtil.parseDateTime(DateUtil.formatDateTime(date));
    }
}
