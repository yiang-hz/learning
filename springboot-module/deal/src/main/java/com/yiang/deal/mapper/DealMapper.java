package com.yiang.deal.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yiang.deal.entity.Deal;
import org.springframework.stereotype.Repository;

/**
 * @author HeZhuo
 * @date 2022/12/6
 */
@Repository
public interface DealMapper extends BaseMapper<Deal> {
}
