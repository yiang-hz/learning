package com.yiang.deal;

import com.yiang.deal.entity.Deal;
import com.yiang.deal.server.DealServer;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author HeZhuo
 * @date 2022/12/6
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class Test {

    @Autowired
    private DealServer dealServer;

    @org.junit.Test
    public void test() {
        List<Deal> list = dealServer.list();
        System.out.println(list);
    }
}
