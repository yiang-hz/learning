package com.yiang.yipay;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.URLUtil;
import cn.hutool.crypto.SecureUtil;
import org.springframework.util.ObjectUtils;

import java.util.*;

/**
 * @author HeZhuo
 * @date 2022/1/5
 */
public class Test {

    public static void main(String[] args) {
        test();
    }

    public static void test()  {

        //易支付网站地址
        String webSite = "https://www.yzfpay.com/";
        HashMap<String, String> map = new HashMap<>();
        //pid
        map.put("pid", "3721");
        //支付方式,alipay:支付宝,wxpay:微信支付,qqpay:QQ钱包
        map.put("type", "alipay");
        //订单号,自己生成
        map.put("out_trade_no", IdUtil.fastUUID());
        //服务器异步通知地址
        map.put("notify_url", "http://xxxx.com/xxx");
        //页面跳转通知地址
        map.put("return_url", "http://xxxx.com/xxx");
        //商品名称
        map.put("name", "玻璃保温杯");
        //金额
        map.put("money", "100");
        //网站名称
        map.put("sitename", "www.yiangus.com");
        //易支付秘钥
        String key= "B3XmfFR1B5M1RfWrFF101BfmiBkif0Ff";

        //参数排序--转码--url拼接(解决重定向中文乱码问题)
        String paramUrl = formatUrlMap(map,true,false);
        //参数排序--不转码--加密用
        String paramUrlNoEncode=formatUrlMap(map,false,false);
        //解决url重定向中文问题和签名问题,所以参数排序分成转码和不转码2部分,转码部分用于url拼接,不转码部分用于签名。


        //加密
        String sign = SecureUtil.md5(paramUrlNoEncode+key);
        //跳转支付地址
        String s1 = webSite + "submit.php?" + paramUrl + "&sign="+sign+"&sign_type=MD5";
        System.out.println(s1);
    }

    /**
     *
     * 方法用途: 对所有传入参数按照字段名的Unicode码从小到大排序（字典序），并且生成url参数串<br>
     * 实现步骤: <br>
     *
     * @param paraMap   要排序的Map对象
     * @param urlEncode   是否需要URLENCODE
     * @param keyToLower    是否需要将Key转换为全小写。true:key转化成小写，false:不转化
     * @return String 字符
     */
    public static String formatUrlMap(Map<String, String> paraMap, boolean urlEncode, boolean keyToLower) {
        String buff;
        try {
            List<Map.Entry<String, String>> infoIds = new ArrayList<>(paraMap.entrySet());
            // 对所有传入参数按照字段名的 ASCII 码从小到大排序（字典序）
            infoIds.sort(Map.Entry.comparingByKey());
            // 构造URL 键值对的格式
            StringBuilder buf = new StringBuilder();
            for (Map.Entry<String, String> item : infoIds) {
                if (!ObjectUtils.isEmpty((item.getKey()))) {
                    String key = item.getKey();
                    String val = item.getValue();
                    if (urlEncode) {
                        val = URLUtil.encode(val);
                    }
                    if (keyToLower) {
                        buf.append(key.toLowerCase()).append("=").append(val);
                    } else {
                        buf.append(key).append("=").append(val);
                    }
                    buf.append("&");
                }

            }
            buff = buf.toString();
            if (!buff.isEmpty()) {
                buff = buff.substring(0, buff.length() - 1);
            }
        } catch (Exception e) {
            return null;
        }
        return buff;
    }

}
