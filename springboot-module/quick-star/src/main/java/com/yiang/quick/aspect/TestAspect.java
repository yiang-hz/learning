package com.yiang.quick.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * @author HeZhuo
 * @date 2021/7/21
 */
@Component
@Aspect
public class TestAspect {

    @AfterReturning(value = "execution(* com.yiang.quick.controller.TestController.test(..))&&args(param)", returning = "resultValue", argNames = "param,resultValue")
    public void afterReturn(String param, String resultValue){
        System.out.println(param + "---" + resultValue);
    }

    @Before("execution(* com.yiang.quick.controller.TestController.test(..))")
    public void before() {
        System.out.println("before ...");
    }

    @After("execution(* com.yiang.quick.controller.TestController.test(..))")
    public void after() {
        System.out.println("After ...");
    }

    @Around("execution(* com.yiang.quick.controller.TestController.test(..))")
    public Object xxx(ProceedingJoinPoint pj) {
        try {
            System.out.println("Around start ...");
            Object proceed = pj.proceed();
            System.out.println("Around end ...");
            return "around：" + proceed;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return "error around";
        }
    }
}
