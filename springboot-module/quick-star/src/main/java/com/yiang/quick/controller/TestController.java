package com.yiang.quick.controller;

import cn.hutool.core.net.multipart.MultipartFormData;
import cn.hutool.core.net.multipart.UploadFile;
import cn.hutool.extra.servlet.ServletUtil;
import com.yiang.quick.aspect.SerializedField;
import com.yiang.quick.aspect.SerializedFields;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileNotFoundException;

/**
 * @author HeZhuo
 * @date 2021/5/10
 */
@RestController
public class TestController {

    @GetMapping("/xxl-get")
    public String test(@RequestParam(required = false) String param) {
        System.out.println(param);
        return param + "成功";
    }

    @GetMapping("/get")
    @SerializedFields({
            @SerializedField(type = A.class, includes = {"password", ""}),
            @SerializedField(type = A.class, includes = {"name", "null"})
    })
    public A testResult() {

        return new A("1", "张三", "123456");
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    static class A {
        private String id;
        private String name;
        private String password;
    }

    @PostMapping(value = "/uploadZipFile/{nid}")
    public String uploadZipFile(HttpServletRequest request) throws Exception {

        MultipartFormData multipart = ServletUtil.getMultipart(request);
        UploadFile files = multipart.getFile("files");
        files.write("C://" + "2" + ".zip");

        return "操作成功！";
    }

    @PostMapping(value = "/uploadZipFile2/{nid}")
    public String importTemplateData(HttpServletRequest request) throws Exception {

        MultipartFile multipartFile;
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        multipartFile = multipartRequest.getFile("files");

        // 文件名称定义为nid加上zip
        File file = new File("C://" + "2" + ".zip");
        if (ObjectUtils.isEmpty(multipartFile)) {
            throw new FileNotFoundException("传递Zip文件为空！");
        }
        multipartFile.transferTo(file);

        return null;
    }
}
