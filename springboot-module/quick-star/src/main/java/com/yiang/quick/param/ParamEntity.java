package com.yiang.quick.param;

import lombok.Data;

import java.util.Date;

/**
 * @author HeZhuo
 * @date 2020/12/2
 */
@Data
public class ParamEntity {

    private Date date;

    private String name;
}
