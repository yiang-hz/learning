package com.yiang.quick;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author HeZhuo
 * @date 2020/12/2
 */
@SpringBootApplication
public class QuickStarApp {

    public static void main(String[] args) {
        SpringApplication.run(QuickStarApp.class, args);
    }
}
