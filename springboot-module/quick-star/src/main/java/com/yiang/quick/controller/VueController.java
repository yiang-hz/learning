package com.yiang.quick.controller;

import lombok.Data;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HeZhuo
 * @date 2021/9/29
 */
@RestController
@CrossOrigin
public class VueController {

    private static final List<String> FRUITS = new ArrayList<>();

    static {
        FRUITS.add("香蕉");
        FRUITS.add("梨子");
        FRUITS.add("苹果");
    }

    @GetMapping("/getData")
    public List<String> getData() {

        return FRUITS;
    }

    @PostMapping("/add")
    public List<String> add(@RequestBody Fruit fruit) {
        FRUITS.add(fruit.getFruit());
        return FRUITS;
    }

    @PostMapping("/delete/{index}")
    public List<String> delete(@PathVariable("index") int index) {
        FRUITS.remove(index);
        return FRUITS;
    }

    @Data
    static class Fruit {
        String fruit;
    }
}
