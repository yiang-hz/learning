package com.yiang.quick.aspect;

import java.lang.annotation.*;

/**
 * @author HeZhuo
 * @date 2021/07/22
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(SerializedFields.class)
public @interface SerializedField {

    Class<?> type();

    /**
     * 需要返回的字段 当返回字段和祛除字段同时存在时，优先返回字段
     *
     * @return String[]
     */
    String[] includes() default {};

    /**
     * 需要去除的字段
     *
     * @return String[]
     */
    String[] excludes() default {};
}
