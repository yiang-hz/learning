package com.yiang.quick.aspect;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.lang.Nullable;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;

/**
 * @author HeZhuo
 * @date 2021/07/22
 */
@Order(-1)
@ControllerAdvice(basePackages = "com.yiang.quick")
public class ResponseBodyDataAdvice<T> implements ResponseBodyAdvice<T> {

    private List<SerializedField> serializedFieldList;

    /**
     * 处理SerializedField/SerializedFields 注解
     *
     * @param returnType 返回类型
     * @param aClass class
     * @return boolean
     */
    @Override
    public boolean supports(MethodParameter returnType, @Nullable Class aClass) {
        return returnType.getMethodAnnotation(SerializedField.class) != null
                || returnType.getMethodAnnotation(SerializedFields.class) != null;
    }

    @Override
    public T beforeBodyWrite(T resultObject, @Nullable MethodParameter returnType,
                             @Nullable MediaType selectedContentType,
                             @Nullable Class<? extends HttpMessageConverter<?>> selectedConverterType,
                             @Nullable ServerHttpRequest request,
                             @Nullable ServerHttpResponse response) {
        // 重新初始化为默认值
        serializedFieldList = new ArrayList<>();

        // 如果为空不做处理直接返回
        if (ObjectUtils.isEmpty(resultObject)) {
            return resultObject;
        }

        // 返回类型或者返回类型的注解为空都不做处理
        if (ObjectUtils.isEmpty(returnType) || ObjectUtils.isEmpty(returnType.getMethodAnnotations())) {
            return resultObject;
        }

        Annotation[] annotations = returnType.getMethodAnnotations();

        // 解析注解，设置过滤条件
        Arrays.asList(annotations).forEach(annotation -> {
            if (annotation instanceof SerializedField) {
                SerializedField serializedField = (SerializedField) annotation;
                serializedFieldList.add(serializedField);
            } else if (annotation instanceof SerializedFields) {
                // 使用多重注解
                SerializedFields serializedFields = (SerializedFields) annotation;
                serializedFieldList.addAll(Arrays.asList(serializedFields.value()));
            }
        });

        //判断返回的对象是单个对象，还是list，或者是map
        if (resultObject instanceof List) {
            //List
            List<?> list = (List<?>) resultObject;
            List<Map<String, Object>> mapList = handleList(list);
            return this.result(mapList);
        } else {
            //Single Object
            Map<String, Object> stringObjectMap = handleObject(resultObject);
            return this.result(stringObjectMap);
        }
    }

    @SuppressWarnings("unchecked")
    private T result(Object result) {
        return (T) result;
    }

    /**
     * 处理返回值是单个entity对象
     *
     * @param o Object
     * @return Object
     */
    private Map<String, Object> handleObject(Object o) {
        Map<String, Object> map = new HashMap<>();

        //包含项
        List<String> includes = new LinkedList<>();
        //排除项
        List<String> excludes = new LinkedList<>();

        for (SerializedField serializedField : serializedFieldList) {
            Class<?> type = serializedField.type();
            if (type.isInstance(o)) {
                includes.addAll(Convert.toList(String.class, serializedField.includes()));
                excludes.addAll(Convert.toList(String.class, serializedField.excludes()));
            }
        }
        //递归获取当前类及父类的所有字段
        List<Field> fields = new ArrayList<>();
        Class<?> tempClass = o.getClass();
        //当父类为null的时候说明到达了最上层的父类(Object类).
        while (tempClass != null && !"java.lang.object".equals(tempClass.getName().toLowerCase())) {
            fields.addAll(Arrays.asList(tempClass.getDeclaredFields()));
            tempClass = tempClass.getSuperclass();
        }

        boolean all = ObjectUtils.isEmpty(includes) && ObjectUtils.isEmpty(excludes);
        boolean in = !ObjectUtils.isEmpty(includes);

        for (Field field : fields) {

            // 如果属于静态字段则不处理
            if (Modifier.isStatic(field.getModifiers())) {
                continue;
            }

            String fieldName = field.getName();

            //如果未配置表示全部的都返回
            if (all) {
                this.setValue(o, field, map);
                continue;
            }

            if (in) {
                // 优先考虑包含字段
                if (this.contain(fieldName, includes)) {
                    this.setValue(o, field, map);
                }
            } else {
                // 保留去除字段中，没有包含的字段
                if (!this.contain(fieldName, excludes)) {
                    this.setValue(o, field, map);
                }
            }
        }
        return map;
    }

    private boolean contain(String fieldName, List<String> compareList) {
        return compareList.contains(StrUtil.trim(fieldName));
    }

    private void setValue(Object o, Field field, Map<String, Object> map) {
        Object newVal = getNewVal(o, field);
        map.put(field.getName(), newVal);
    }

    /**
     * 处理返回值是列表
     *
     * @param list List
     * @return List
     */
    private List<Map<String, Object>> handleList(List<?> list) {
        List<Map<String, Object>> retList = new LinkedList<>();
        for (Object o : list) {
            if (ObjectUtils.isEmpty(o)) {
                continue;
            }
            Map<String, Object> map = handleObject(o);
            retList.add(map);
        }
        return retList;
    }

    /**
     * 获取加密后的新值
     *
     * @param o Object
     * @param field Field
     * @return Object
     */
    private Object getNewVal(Object o, Field field) {
        Object newVal = null;
        try {
            field.setAccessible(true);
            Object val = field.get(o);
            if (val != null) {
                for (SerializedField serializedField : serializedFieldList) {
                    if (serializedField.type().isInstance(val)) {
                        return this.handleObject(val);
                    }
                }
                if (val instanceof String) {
                    newVal = Convert.toStr(val);
                } else if (val instanceof List) {
                    newVal = this.handleList((List<?>) val);
                } else {
                    newVal = val;
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return newVal;
    }

}
