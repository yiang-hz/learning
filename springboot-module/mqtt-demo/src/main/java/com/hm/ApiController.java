package com.hm;

import com.hm.entity.MqMsg;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * @author HeZhuo
 * @date 2022/11/26
 */
@RestController
@CrossOrigin
public class ApiController {

    @RequestMapping("/getMsg")
    public List<MqMsg> getMsg() {
        return MsgUtil.MSG_LIST;
    }

    @RequestMapping("/reset")
    public List<MqMsg> reset() {
        MsgUtil.MSG_LIST.clear();
        return MsgUtil.MSG_LIST;
    }
}
