package com.hm.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author HeZhuo
 * @date 2022/11/26
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class MqMsg {

    private String msg;

    private String revDate;
}
