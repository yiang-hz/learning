package com.hm.demo;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.mqtt.server.ServerConsumer;
import com.alibaba.mqtt.server.callback.MessageListener;
import com.alibaba.mqtt.server.config.ChannelConfig;
import com.alibaba.mqtt.server.config.ConsumerConfig;
import com.alibaba.mqtt.server.model.MessageProperties;

public class MQTTConsumerDemo {
    public static void main(String[] args) throws Exception {
        /**
         * 设置云端SDK的接入点，请参见接入点说明中的云端SDK接入点格式。
         * 接入点地址必须填写分配的域名，不得使用IP地址直接连接，否则可能会导致服务端异常。
         */
        String domain = "mqtt-cn-nwy33lfu303.mqtt.aliyuncs.com";

        /**
         * 使用的协议和端口必须匹配，该参数值固定为5672。
         */
        int port = 5672;

        /**
         * 您创建的微消息队列MQTT版的实例ID。
         */
        String instanceId = "mqtt-cn-nwy33lfu303";

        /**
         * AccessKey ID，阿里云身份验证，在阿里云RAM控制台创建。获取方式，请参见获取AccessKey。
         */
        String accessKey = "LTAI5tKVmNonXi6gSHe6tHAb";

        /**
         * AccessKey Secret，阿里云身份验证，在阿里云RAM控制台创建。仅在签名鉴权模式下需要设置。获取方式，请参见获取AccessKey。
         */
        String secretKey = "0XrfctA1G2saoQH2RufEZFdG1vg83y";

        /**
         * 微消息队列MQTT版消息的一级Topic，需要在控制台创建才能使用。
         * 由于云端SDK订阅消息一般用于云上应用进行消息汇总和分析等场景，因此，云端SDK订阅消息不支持设置子级Topic。
         * 如果使用了没有创建或者没有被授权的Topic会导致鉴权失败，服务端会断开客户端连接。
         */
        String firstTopic = "dlMsg";

        ChannelConfig channelConfig = new ChannelConfig();
        channelConfig.setDomain(domain);
        channelConfig.setPort(port);
        channelConfig.setInstanceId(instanceId);
        channelConfig.setAccessKey(accessKey);
        channelConfig.setSecretKey(secretKey);

        ServerConsumer serverConsumer = new ServerConsumer(channelConfig, new ConsumerConfig());
        serverConsumer.start();
        serverConsumer.subscribeTopic(firstTopic, new MessageListener() {
            @Override
            public void process(String msgId, MessageProperties messageProperties, byte[] payload) {
                System.out.println("Receive:" + msgId + "," + JSONObject.toJSONString(messageProperties) + "," + new String(payload));
            }
        });
    }

}