package com.yiang.shard.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author HeZhuo
 * @date 2021/11/2
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("user")
public class User {

    private Integer id;

    private String name;

    private Integer age;

    private Date createDate;
}
