package com.yiang.shard;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author HeZhuo
 * @date 2021/11/2
 */
@SpringBootApplication
@MapperScan(basePackages = {"com.yiang.shard.mapper", "com.yiang.shard.tools"})
public class ShardApp {

    public static void main(String[] args) {
        SpringApplication.run(ShardApp.class, args);
    }
}
