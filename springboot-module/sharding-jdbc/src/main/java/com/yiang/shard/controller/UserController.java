package com.yiang.shard.controller;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import com.yiang.shard.entity.User;
import com.yiang.shard.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author HeZhuo
 * @date 2021/11/2
 */
@RestController
public class UserController {
    
    @Autowired
    private UserMapper userMapper;

    @RequestMapping("/insertUser")
    public String insertUser(Integer start, Integer end) {

        for (int i = start; i <= end; i++) {
            User user = new User();
            user.setId(i);
            user.setName(Convert.toStr(RandomUtil.randomChinese()) + i);
            user.setAge(RandomUtil.randomInt(1, 120));
            user.setCreateDate(DateUtil.date());
            try {
                userMapper.insert(user);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "success";
    }

    /**
     * 查询所有
     *
     */
    @RequestMapping("/userList")
    public List<User> userList() {
        return userMapper.userList();
    }

    /**
     * 分页查询
     *
     */
    @RequestMapping("/userListPage")
    public List<User> userListPage() {
        return userMapper.userListPage();
    }

    /**
     * 排序
     *
     */
    @RequestMapping("/userOrderBy")
    public List<User> userOrderBy() {
        return userMapper.userOrderBy();
    }

    @RequestMapping("/getByUserId")
    public List<User> getByUserId(Long id) {
        return userMapper.getByUserId(id);
    }
}
