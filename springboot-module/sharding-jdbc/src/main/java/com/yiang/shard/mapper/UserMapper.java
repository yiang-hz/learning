package com.yiang.shard.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yiang.shard.entity.User;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author HeZhuo
 * @date 2021/11/2
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

    /**
     * 查询所有
     *
     */
    @Select("SELECT * FROM user")
    List<User> userList();

    /**
     * 分页查询
     *
     */
    @Select("SELECT * FROM user limit 0,6")
    List<User> userListPage();

    /**
     * 排序
     *
     */
    @Select("SELECT * FROM user order by id desc ")
    List<User> userOrderBy();

    /**
     * get by id
     *
     */
    @Select("SELECT * FROM user where id =#{id} ")
    List<User> getByUserId(Long id);
}
