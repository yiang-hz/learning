package com.yiang.flink;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.datastream.WindowedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

/**
 * @author HeZhuo
 * @date 2022/1/22
 */
public class FlinkDemo {

    public static void main(String[] args) throws Exception {

        // 启动环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        // 链接Socket获取输入数据
        DataStreamSource<String> text = env.socketTextStream("127.0.0.1", 9001);

        SingleOutputStreamOperator<String> wordStream = text.flatMap(new FlatMapFunction<String, String>() {
            @Override
            // 处理数据，将接收到的每一个数据都根据空格切分成单词
            public void flatMap(String value, Collector<String> out) throws Exception {
                String[] words = value.split(" ");
                for (String word : words) {
                    // 将切分的单词发送出去
                    out.collect(word);
                }
            }
        });

        SingleOutputStreamOperator<Tuple2<String, Integer>> countStream = wordStream.map(new MapFunction<String, Tuple2<String, Integer>>() {
            // 将每个单词转化成Tuple2形式， （单词,1）
            @Override
            public Tuple2<String, Integer> map(String word) throws Exception {
                return new Tuple2<>(word, 1);
            }
        });

        // 根据Tuple2中的第一列进行分组
        KeyedStream<Tuple2<String, Integer>, Tuple> keyStream = countStream.keyBy(0);

        // 设置时间窗口为2秒钟，表示每两秒钟计算一次接收到的数据
        WindowedStream<Tuple2<String, Integer>, Tuple, TimeWindow> windowStream = keyStream.timeWindow(Time.seconds(2));

        // 根据Tuple2中的第二列数据进行聚合操作
        SingleOutputStreamOperator<Tuple2<String, Integer>> sunRes = windowStream.sum(1);

        // 使用一个线程执行打印操作
        sunRes.print().setParallelism(1);

        env.execute("FlinkDemo");

    }
}
