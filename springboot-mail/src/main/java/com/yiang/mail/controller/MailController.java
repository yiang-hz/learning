package com.yiang.mail.controller;

import com.yiang.mail.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("mail")
public class MailController {

    private final MailService mailService;

    @Autowired
    public MailController(MailService mailService) {
        this.mailService = mailService;
    }

    @RequestMapping("/sendMessage")
    public String sendMessage(String to, String subject, String content){
        mailService.sendSimpleMail(to, subject, content);
        return "SUCCESS";
    }
}
