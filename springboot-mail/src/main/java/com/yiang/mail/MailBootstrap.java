package com.yiang.mail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MailBootstrap {

    public static void main(String[] args) {
        SpringApplication.run(MailBootstrap.class, args);
    }
}
