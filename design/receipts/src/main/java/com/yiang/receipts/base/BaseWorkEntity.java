package com.yiang.receipts.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author HeZhuo
 * @date 2020/9/15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("基础业务实现实体参数类")
public class BaseWorkEntity {

    @ApiModelProperty("操作ID，参考ServiceEnum类说明")
    private Integer[] arr;

    @ApiModelProperty("对应数据的主键ID，唯一ID")
    private Long objId;

    @ApiModelProperty("实体类数据，会转化为json传递。创建时事务未提交sql是无法查询到的，所以传递实体类")
    private Object jsonObject;

    @ApiModelProperty("账户")
    private String account;

    @ApiModelProperty("租户ID")
    private String tenantId;

    public BaseWorkEntity(Integer[] arr, Object jsonObject) {
        this.arr = arr;
        this.jsonObject = jsonObject;
    }

    /**
     * 双参构造，处理只需要传递code与对象码的情况
     * @param arr 对象码数组
     * @param objId 数据主键ID
     */
    public BaseWorkEntity(Integer[] arr, Long objId) {
        this.arr = arr;
        this.objId = objId;
    }

    public BaseWorkEntity(Integer[] arr) {
        this.arr = arr;
    }
}
