package com.yiang.receipts.service;

import cn.hutool.core.lang.Assert;
import com.yiang.receipts.entity.ServiceEnum;

import java.util.Objects;

/**
 * 服务基类
 * @author HeZhuo
 * @date 2020/9/10
 */
public abstract class BaseService {

    /**
     * 基础执行任务方法
     */
    public abstract void doWork();

    /**
     * 扩展执行任务方法，子类在此实现其它业务。在基础执行方法之后执行
     */
    public abstract void reWork();

    /**
     * 被继承使用构造函数
     */
    protected BaseService() {

    }

    /**
     * 开始执行方法，程序入口。
     * 通过枚举寻找对应实现类，添加对应实现类之后注意需要添加枚举方法
     * @see ServiceEnum 对应服务枚举类
     * @param serviceCodeArray 需要调用的服务数组，支持单个调用
     * @throws ClassNotFoundException 类未找到
     * @throws IllegalAccessException 非法访问异常
     * @throws InstantiationException 实例化异常
     */
    public static void start(Integer... serviceCodeArray) throws ClassNotFoundException,
            IllegalAccessException, InstantiationException {

        Assert.notEmpty(serviceCodeArray, "服务状态码不能为空");

        BaseService service;


        for (Integer serviceCode : serviceCodeArray) {

            //1.通过服务码去寻找类全路径，未找到直接跳过当前循环，避免实例化异常
            //String classPath = ServiceEnum.getPath(serviceCode);
            //if (ObjectUtil.isEmpty(classPath)){
            //    continue;
            //}
            //通过newInstance()获取对象实例，并执行基础业务
            //service = getInstance(classPath).newInstance();

            //2.通过对应的code码获取类class对象，避免通过包名获取找不到的情况
            service = Objects.requireNonNull(
                    ServiceEnum.getClass(ServiceEnum.ORDER_SERVER.getCode())
            ).newInstance();
            service.doWork();
        }
    }

    /**
     * 通过反射获取类对象
     * @param classPath 类全引用路径
     * @return 类对象 （继承BaseService）
     * @throws ClassNotFoundException 类未找到异常
     */
    @SuppressWarnings("unchecked")
    @Deprecated
    private static Class<? extends BaseService> getInstance(String classPath)
            throws ClassNotFoundException {
        return (Class<? extends BaseService>) Class.forName(classPath);
    }

}
