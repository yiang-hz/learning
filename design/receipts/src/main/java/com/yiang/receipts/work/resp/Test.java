package com.yiang.receipts.work.resp;

import com.yiang.receipts.entity.ServiceEnum;
import com.yiang.receipts.service.BaseService;

/**
 * @author HeZhuo
 * @date 2020/9/10
 */
public class Test {

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException {

        //audit 流程结束，根据对应的类型继续获取创建不同的service

        // entity.getCode()
        Integer code = 1;

//        Context context = new Context((PayServer) getInstance(ServiceEnum.getPath(code)).newInstance());

        Context context = new Context((PayServer) ServiceEnum.getClass(code).newInstance());
        context.execute();
    }

    /**
     * 通过反射获取类对象
     * @param classPath 类全引用路径
     * @return 类对象 （继承BaseService）
     * @throws ClassNotFoundException 类未找到异常
     */
    @SuppressWarnings("unchecked")
    public static Class<? extends BaseService> getInstance(String classPath)
            throws ClassNotFoundException {
        return (Class<? extends BaseService>) Class.forName(classPath);
    }
}
