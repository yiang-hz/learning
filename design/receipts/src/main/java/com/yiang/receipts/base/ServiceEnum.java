package com.yiang.receipts.base;

import com.yiang.receipts.base.demo.CaseServiceImpl;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * 服务信息枚举类
 * @author HeZhuo
 * @date 2020/9/10
 */
@ToString
@Getter
@AllArgsConstructor
public enum ServiceEnum {

    /**
     * 所有服务code以及对应的类class
     */

	CASE_SERVER(1,"Case - 案例服务", CaseServiceImpl.class);

    /**
     * 类对应码
     */
    private final Integer code;

    /**
     * 类对应名称(备注亦可)
     */
    private final String name;

	/**
	 * 对应继承了BaseService的类
	 */
	private final Class<?extends BaseService> serviceClass;

    /**
     * 根据类对应码获取对应类class对象
     * @param code 类对应码
     * @return 类class对象
     */
    public static Class<? extends BaseService> getClass(Integer code) {

        //获取当前枚举类所有的对象
        ServiceEnum[] values = ServiceEnum.values();

        //遍历寻找相对应状态码，未找到返回null值
        for (ServiceEnum value : values) {
            if (value.code.equals(code)) {
                return value.serviceClass;
            }
        }

        return null;
    }
}
