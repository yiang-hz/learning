package com.yiang.receipts;

import com.yiang.receipts.service.BaseService;

/**
 * @author HeZhuo
 * @date 2020/9/10
 */
public class ReceiptsApp {

    public static void main(String[] args) {

        System.out.println("多平台调用责任链模式...");

        //单个
//        Integer serviceCodeArray = 1;

        //数组
        Integer[] serviceCodeArray = {1, 4, 2, 3};

        //数组为空
//        Integer[] serviceCodeArray = null;

        try {
            BaseService.start(serviceCodeArray);
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
    }

}
