package com.yiang.receipts.base.demo;

import com.yiang.receipts.base.BaseInterface;
import com.yiang.receipts.base.BaseService;
import com.yiang.receipts.base.BaseWorkEntity;
import lombok.extern.slf4j.Slf4j;

/**
 * 案列实现类
 * @author HeZhuo
 * @date 2020/9/15
 */
@Slf4j
public class CaseServiceImpl extends BaseService implements BaseInterface {

	@Override
	public void doWork(BaseWorkEntity baseWorkEntity) {
		log.info("CaseServiceImpl doWork... Args:{}", baseWorkEntity);
		reWork();
	}

	@Override
	public void reWork() {
		log.info("CaseServiceImpl reWork...");
	}

	@Override
	public void finish(BaseWorkEntity baseWorkEntity) {
		log.info("CaseServiceImpl finish... Args:{}", baseWorkEntity);
	}
}
