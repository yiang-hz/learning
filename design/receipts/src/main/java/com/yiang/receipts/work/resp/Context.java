package com.yiang.receipts.work.resp;

/**
 * @author HeZhuo
 * @date 2020/9/10
 */
public class Context {

   private final PayServer payServer;
 
   public Context(PayServer payServer){
      this.payServer = payServer;
   }
 
   public void execute(){
      payServer.finish();
   }

}