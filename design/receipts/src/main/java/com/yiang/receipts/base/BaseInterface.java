package com.yiang.receipts.base;

/**
 * 策略模式基础实现接口 finish
 * @author HeZhuo
 * @date 2020/9/10
 */
public interface BaseInterface {

	/**
	 * 完成方法，业务后续需执行的方法
	 * @param baseWorkEntity 参数实体类
	 */
    void finish(BaseWorkEntity baseWorkEntity);
}
