package com.yiang.receipts.base;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;

/**
 * 策略模式发起调用类
 * @author HeZhuo
 * @date 2020/9/10
 */
@Slf4j
public class Context {

   private final BaseInterface baseInterface;

   public Context(BaseInterface baseInterface){
      this.baseInterface = baseInterface;
   }

   public void execute(BaseWorkEntity baseWorkEntity){
	   baseInterface.finish(baseWorkEntity);
   }

	/**
	 * 开始执行方法，程序入口。
	 * 通过枚举寻找对应实现类，添加对应实现类之后注意需要添加枚举参数
	 * @see ServiceEnum 对应服务枚举类
	 * @param baseWorkEntity 参数实体类
	 */
   public static void start(BaseWorkEntity baseWorkEntity){

	   if (ObjectUtils.isEmpty(baseWorkEntity.getArr())){
		   return;
	   }

   	   //需要调用的服务数组（Integer类型），且支持单个调用
	   Integer [] serviceCodeArray = baseWorkEntity.getArr();

	   Context context;
	   Class<? extends BaseService> serviceClass;

	   for (Integer serviceCode : serviceCodeArray) {
		   serviceClass = ServiceEnum.getClass(serviceCode);
		   if (!ObjectUtils.isEmpty(serviceClass)){
			   //通过对应的code码获取类class对象，并执行
			   try {
				   context = new Context((BaseInterface) serviceClass.newInstance());
				   context.execute(baseWorkEntity);
			   } catch (InstantiationException | IllegalAccessException e) {
				   log.error("base service get instance exception: {}", e.getMessage());
				   e.printStackTrace();
			   }
		   }
	   }

   }
}
