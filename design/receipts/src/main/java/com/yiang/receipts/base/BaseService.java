package com.yiang.receipts.base;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;

/**
 * 服务基类
 * @author HeZhuo
 * @date 2020/9/10
 */
@Slf4j
public abstract class BaseService {

	/**
	 * 基础执行任务方法
	 * @param baseWorkEntity 参数实体类
	 */
	public abstract void doWork(BaseWorkEntity baseWorkEntity);

	/**
	 * 扩展执行任务方法，子类在此实现其它业务。在基础执行方法之后执行
	 */
	public abstract void reWork();

	/**
	 * 被继承使用构造函数
	 */
	protected BaseService() {

	}

	/**
	 * 开始执行方法，程序入口。
	 * 通过枚举寻找对应实现类，添加对应实现类之后注意需要添加枚举参数
	 * @see ServiceEnum 对应服务枚举类
	 * @param baseWorkEntity 参数实体类
	 */
	public static void start(BaseWorkEntity baseWorkEntity) {

		if (ObjectUtils.isEmpty(baseWorkEntity.getArr())){
			return;
		}

		// 需要调用的服务数组（Integer类型），且支持单个调用
		Integer [] serviceCodeArray = baseWorkEntity.getArr();

		BaseService service;
		Class<? extends BaseService> serviceClass;

		for (Integer serviceCode : serviceCodeArray) {
			serviceClass = ServiceEnum.getClass(serviceCode);
			if (!ObjectUtils.isEmpty(serviceClass)){
				//通过对应的code码获取类class对象，并执行doWork方法
				try {
					service = serviceClass.newInstance();
					service.doWork(baseWorkEntity);
				} catch (InstantiationException | IllegalAccessException e) {
					log.error("base service get instance exception: {}", e.getMessage());
					e.printStackTrace();
				}
			}
		}
	}

	protected static <T> T parseObject(Object obj, Class<T> cls) {
		return JSONObject.parseObject(JSONObject.toJSONString(obj), cls);
	}
}
