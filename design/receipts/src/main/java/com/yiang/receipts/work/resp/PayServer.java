package com.yiang.receipts.work.resp;

/**
 * @author HeZhuo
 * @date 2020/9/10
 */
public interface PayServer {

    void finish();
}
