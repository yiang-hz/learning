package com.yiang.receipts.base.demo;


import com.yiang.receipts.base.BaseService;
import com.yiang.receipts.base.BaseWorkEntity;
import com.yiang.receipts.base.Context;
import com.yiang.receipts.base.ServiceEnum;

/**
 * 案例测试类
 * @author HeZhuo
 * @date 2020/9/15
 */
public class CaseTest {

//	public static void main(String[] args) {
//		//测试时使用，不测试时需要注释，否则jenkins报错
//		test();
//	}

	public static void test() {

		//测试案例的 doWork 与 reWork 方法
		BaseService.start(new BaseWorkEntity(
			new Integer[] {
				ServiceEnum.CASE_SERVER.getCode()
			})
		);

		//测试案例的 finish 方法
		Context.start(new BaseWorkEntity(
			new Integer[] {
				ServiceEnum.CASE_SERVER.getCode()
			})
		);
	}
}
