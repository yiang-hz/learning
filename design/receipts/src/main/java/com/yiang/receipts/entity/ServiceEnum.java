package com.yiang.receipts.entity;

import com.yiang.receipts.service.BaseService;
import com.yiang.receipts.service.OrderService;
import com.yiang.receipts.service.UserService;
import lombok.Getter;
import lombok.ToString;

/**
 * @author HeZhuo
 * @date 2020/9/10
 */
@ToString
@Getter
public enum ServiceEnum {

    /**
     * 所有服务code以及对应的类（全路径）
     */
    ORDER_SERVER(1,"order", "com.yiang.receipts.service.OrderService", OrderService.class),
    USER_SERVER(2, "user", "com.yiang.receipts.service.UserService", UserService.class);

    /**
     * 类对应码
     */
    private final Integer code;

    /**
     * 类对应名称
     */
    private final String name;

    /**
     * 类对应全路径
     */
    private final String path;

    private final Class<?extends BaseService> serviceClass;

    ServiceEnum(Integer code, String name, String path, Class<? extends BaseService> serviceClass) {
        this.code = code;
        this.name = name;
        this.path = path;
        this.serviceClass = serviceClass;
    }

    /**
     * 根据类对应码获取对应全路径
     * @param code 类对应码
     * @return 对应全路径
     */
    public static String getPath(Integer code) {

        //获取当前枚举类所有的对象
        ServiceEnum[] values = ServiceEnum.values();

        //遍历寻找相对应状态码，未找到返回null值
        for (ServiceEnum value : values) {
            if (value.code.equals(code)){
                return value.path;
            }
        }

        return null;
    }

    /**
     * 根据类对应码获取对应类class对象
     * @param code 类对应码
     * @return 类class对象
     */
    public static Class<? extends BaseService> getClass(Integer code) {

        //获取当前枚举类所有的对象
        ServiceEnum[] values = ServiceEnum.values();

        //遍历寻找相对应状态码，未找到返回null值
        for (ServiceEnum value : values) {
            if (value.code.equals(code)){
                return value.serviceClass;
            }
        }

        return null;
    }
}
