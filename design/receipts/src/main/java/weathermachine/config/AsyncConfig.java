package weathermachine.config;

import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

/**
 * 注入支持异步注解
 * @author HeZhuo
 * @date 2020/10/30
 */
@Component
@EnableAsync
public class AsyncConfig {
}
