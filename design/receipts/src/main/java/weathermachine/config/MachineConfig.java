package weathermachine.config;

/**
 * 常量字段
 * @author 以安
 * @date 2020-10-30
 */
public class MachineConfig {

    public static final String TEMP = "temp";
    public static final String HUMIDITY = "humidity";
    public static final String WIND_POWER = "windPower";

}
