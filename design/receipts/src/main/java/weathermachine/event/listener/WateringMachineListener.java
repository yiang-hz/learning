package weathermachine.event.listener;

import com.alibaba.fastjson.JSONObject;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import weathermachine.config.MachineConfig;
import weathermachine.event.MachineMessageEvent;

/**
 * Watering 观察者，异步运行
 * @author 以安
 * @date 2020-10-30
 */
@Component
public class WateringMachineListener implements ApplicationListener<MachineMessageEvent> {

    private boolean status;

    @Async
    @Override
    public void onApplicationEvent(MachineMessageEvent event) {

        JSONObject jsonObject = event.getJsonObject();

        Integer tempChange = 10;
        Integer temp = jsonObject.getInteger(MachineConfig.TEMP);

        Integer humidityChange = 55;
        Integer humidity = jsonObject.getInteger(MachineConfig.HUMIDITY);

        Integer windPowerChange = 4;
        Integer windPower = jsonObject.getInteger(MachineConfig.WIND_POWER);

        if (temp > tempChange && humidity < humidityChange && windPower < windPowerChange) {
            status = true;
        }

        System.out.println("WateringMachineListener" + status);

    }

}