package weathermachine.event.listener;

import com.alibaba.fastjson.JSONObject;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import weathermachine.config.MachineConfig;
import weathermachine.event.MachineMessageEvent;

/**
 * Reaping 观察者，异步运行
 * @author 以安
 * @date 2020-10-30
 */
@Component
public class ReapingMachineListener implements ApplicationListener<MachineMessageEvent> {

    private boolean status;

    @Async
    @Override
    public void onApplicationEvent(MachineMessageEvent event) {

        JSONObject jsonObject = event.getJsonObject();

        int tempChange = 5;
        Integer temp = jsonObject.getInteger(MachineConfig.TEMP);

        int humidityChange = 65;
        Integer humidity = jsonObject.getInteger(MachineConfig.HUMIDITY);

        if (temp > tempChange && humidity > humidityChange){
            this.status = true;
        }

        System.out.println("ReapingMachineListener" + status);

    }

}
