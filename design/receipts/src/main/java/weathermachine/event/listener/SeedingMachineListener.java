package weathermachine.event.listener;

import com.alibaba.fastjson.JSONObject;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import weathermachine.config.MachineConfig;
import weathermachine.event.MachineMessageEvent;

/**
 * Seeding 观察者，异步运行
 * @author 以安
 * @date 2020-10-30
 */
@Component
public class SeedingMachineListener implements ApplicationListener<MachineMessageEvent> {

    private boolean status;

    @Async
    @Override
    public void onApplicationEvent(MachineMessageEvent event) {

        JSONObject jsonObject = event.getJsonObject();

        Integer tempChange = 5;

        if (jsonObject.getInteger(MachineConfig.TEMP) > tempChange){
            status = true;
        }

        System.out.println("SeedingMachineListener" + status);

    }

}
