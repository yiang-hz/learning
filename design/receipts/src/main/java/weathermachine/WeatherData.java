package weathermachine;

import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import weathermachine.config.MachineConfig;
import weathermachine.event.MachineMessageEvent;

/**
 * @author 以安
 * @date 2020-10-30
 */
@AllArgsConstructor
@RestController
public class WeatherData {

    private final ApplicationContext applicationContext;

    @RequestMapping("/say")
    public void measurementsChanged(int temp, int humidity, int windPower) {

        String nullHintMessage = " No empty space allowed ";

        Assert.notNull(temp, "Temp" + nullHintMessage);
        Assert.notNull(humidity, "Humidity" + nullHintMessage);
        Assert.notNull(windPower, "WindPower" + nullHintMessage);

        //1.使用JSON包装参数传递给观察者
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(MachineConfig.TEMP, temp);
        jsonObject.put(MachineConfig.HUMIDITY, humidity);
        jsonObject.put(MachineConfig.WIND_POWER, windPower);

        //2.通知观察者
        MachineMessageEvent orderMessageEvent = new MachineMessageEvent(this, jsonObject);
        applicationContext.publishEvent(orderMessageEvent);

    }

}