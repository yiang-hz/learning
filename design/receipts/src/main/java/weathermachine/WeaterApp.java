package weathermachine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author HeZhuo
 * @date 2020/10/30
 */
@SpringBootApplication
public class WeaterApp {

    public static void main(String[] args) {
        SpringApplication.run(WeaterApp.class, args);
    }
}
