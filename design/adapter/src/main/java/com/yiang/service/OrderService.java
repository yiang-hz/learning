package com.yiang.service;

import java.util.Map;

/**
 * @author HZ
 * @date 2020-11-13
 */
public class OrderService {

    /**
     * V1 版本的时候提供了一个接口，入参是为map类型
     * V2 版本能够支持List类型
     *
     * @param map 参数
     */
    public void forMap(Map<?, ?> map) {
        for (int i = 0; i < map.size(); i++) {
            String value = (String) map.get(i);
            System.out.println("value:" + value);
        }
    }
    //适配器模式 支持新版本List类型  中间使用适配器

}
