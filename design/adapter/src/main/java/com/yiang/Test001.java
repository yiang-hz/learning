package com.yiang;

import com.yiang.adapter.ListAdapter;
import com.yiang.service.OrderService;

import java.util.ArrayList;

/**
 * @author HZ
 * @date 2020-11-13
 */
public class Test001 {
    public static void main(String[] args) {
        // 1.定义源 老版本
        OrderService orderService = new OrderService();
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("yiang");
        arrayList.add("LiBai");
        arrayList.add("DuFu");
        // 2.使用适配器实现转换
        ListAdapter listAdapter = new ListAdapter(arrayList);
        // 3.可以支持list类型
        orderService.forMap(listAdapter);

    }
    /*
     *  写入日志 写入本地文件  后期开发者扩展新功能 支持写入MQ、写入数据库  使用适配器  外部扩展
     *
     *  装饰  内部扩展 使用装饰
     */
}
