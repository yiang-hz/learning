package com.yiang.file.service;

import com.yiang.file.service.adapter.LogAdapter;
import com.yiang.file.service.impl.LogWriteFileServiceImpl;

/**
 * @author HZ
 * @date 2020-11-13
 */
public class Test {

    public static void main(String[] args) {
        new LogAdapter(new LogWriteFileServiceImpl()).logWriteDb(null);
    }
        /*
         * 适配器 主要应用场景是什么 支持新版本和老版本的兼容 新增扩展性
         * 适配器源码分析： mybatis 日志打印  默认有本身自带打印， 常用日志第三方日志框架有那些log4j log4j2 logback slf4j
         *  myBatis 开发第一个版本的 不支持写入本的文件，支打印
         *
         *  如果mybatis 框架假设支持写入本地文件和打印 统一第三方日志收集项目日志写入文件中
         *
         *  目的是让开发统一收集项目所有日志 */
}

