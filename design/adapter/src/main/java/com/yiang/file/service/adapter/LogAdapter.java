package com.yiang.file.service.adapter;


import com.yiang.file.entity.LogBean;
import com.yiang.file.service.LogWriteDbService;
import com.yiang.file.service.impl.LogWriteFileServiceImpl;

import java.util.List;

/**
 * @author HZ
 * @date 2020-11-13
 * public class LogAdapter extends LogWriteFileServiceImpl implements LogWriteDbService {
 */
public class LogAdapter implements LogWriteDbService {

    /**
     * 源 Adapter
     */
    private final LogWriteFileServiceImpl logWriteFileServiceImpl;

    public LogAdapter(LogWriteFileServiceImpl logWriteFileServiceImpl) {
        this.logWriteFileServiceImpl = logWriteFileServiceImpl;
    }

    @Override
    public void logWriteDb(LogBean logBean) {
        // 新增的扩展功能 既能够支持写入本地文件，也能支持写入数据库
        // 1.读取本地文件
        List<LogBean> logBeans = logWriteFileServiceImpl.readLogFile();
        logBeans.add(logBean);
        // 2.将数据写入到数据库中..
        System.out.println(">>>写入到数据库中..");
        // 1. 写入本地文件
        logWriteFileServiceImpl.logWriteFile();
    }

    // 万一只需要写数据库的话
}
