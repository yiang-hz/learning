package com.yiang.file.entity;

import lombok.Data;
/**
 * @author HZ
 * @date 2020-11-13
 */
@Data
public class LogBean {

    /**
     * 日志ID
     */
    private String logId;
    /**
     * 日志内容
     */
    private String logText;

}
