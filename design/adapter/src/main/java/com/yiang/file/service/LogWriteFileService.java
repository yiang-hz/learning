package com.yiang.file.service;

import com.yiang.file.entity.LogBean;
import java.util.List;

/**
 * @author HZ
 * @date 2020-11-13
 */
public interface LogWriteFileService {

    /**
     * 将日志写入本地文件
     */
    void logWriteFile();

    /**
     * 从本地文件中读取日志
     *
     * @return List<LogBean>
     */
    List<LogBean> readLogFile();

}
