package com.yiang.file.service;


import com.yiang.file.entity.LogBean;

/**
 * @author HZ
 * @date 2020-11-13
 */
public interface LogWriteDbService {

    /**
     * 将本地文件写入到数据库中 新的目标
     * @param logBean LogBean
     */
    void logWriteDb(LogBean logBean);
}
