package com.yiang.singleton;

import cn.hutool.core.thread.ThreadUtil;

/**
 * @author HeZhuo
 * @date 2022/1/13
 */
public class SingletonDemo {

    public static void main(String[] args) {
        ThreadUtil.newThread(SingletonCheck::getInstance, "singleton");
    }

    /**
     * 初始版本，没有锁
     */
    static class Singleton {

        private static Singleton singleton;

        private Singleton() {
        }

        public static Singleton getInstance() {
            if (singleton == null) {
                singleton = new Singleton();   // 创建实例
            }
            return singleton;
        }
    }

    /**
     * 含锁，性能不够，还能优化
     */
    static class SingletonSyn {
        private static SingletonSyn singleton;

        private SingletonSyn() {
        }

        public static synchronized SingletonSyn getInstance() {
            if (singleton == null) {
                singleton = new SingletonSyn();    //创建实例
            }
            return singleton;
        }
    }

    /**
     * 含锁，含判断，内部还需要判断  线程CPU切换
     */
    static class SingletonMethod {
        private static SingletonMethod singleton;

        private SingletonMethod() {
        }

        public static SingletonMethod getInstance() {
            if (singleton == null) {
                synchronized (SingletonMethod.class) {
                    singleton = new SingletonMethod();   //创建实例
                }
            }
            return singleton;
        }
    }

    /**
     * 含锁，含双检，但仍然有问题，JVM数据对象返回
     */
    static class SingletonCheck {
        private static SingletonCheck singleton;

        private SingletonCheck() {
            System.out.println("初始化...");
        }

        public static SingletonCheck getInstance() {
            if (singleton == null) {
                synchronized (SingletonCheck.class) {
                    if (singleton == null) {
                        singleton = new SingletonCheck();
                    }
                }
            }
            return singleton;
        }
    }

    /**
     * 含锁，含双检，含volatile  JDK1.5之后实现
     * 1.5之前参考 内存模型和线程规范  修订。
     * https://jcp.org/aboutJava/communityprocess/final/jsr133/index.html
     *
     * 禁止指令重排序，保证不会出现内存分配、返回对象引用、初始化这样的顺序，从而使得双重检测真正发挥作用
     */
    static class SingletonTrue {

        private volatile static SingletonTrue instance = null;

        private SingletonTrue() {
        }

        public static SingletonTrue getInstance() {
            if (instance == null) {
                synchronized (SingletonTrue.class) {
                    if (instance == null) {
                        instance = new SingletonTrue();
                    }
                }
            }
            return instance;
        }
    }
}
