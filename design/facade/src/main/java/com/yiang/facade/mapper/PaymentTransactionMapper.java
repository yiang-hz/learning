package com.yiang.facade.mapper;

/**
 * @author HZ
 * @date 2020-11-16
 */
public interface PaymentTransactionMapper {
    void updatePaymentStatus();
}
