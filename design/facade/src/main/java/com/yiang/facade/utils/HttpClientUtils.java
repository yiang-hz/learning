package com.yiang.facade.utils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author HZ
 * @date 2020-11-16
 */
@Slf4j
public class HttpClientUtils {

    public static String doPost(String url, String text) {
        log.info(">>>Url:{},text:{}", url, text);
        return "success";
    }
}
