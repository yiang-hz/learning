package com.yiang.facade.service;

import org.springframework.stereotype.Service;

/**
 * @author HZ
 * @date 2020-11-16
 */
@Service
public class LogService {

    public void logService() {
        System.out.println("第一个模块:日志的收集与打印...");
    }
}
