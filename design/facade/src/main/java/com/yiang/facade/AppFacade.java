package com.yiang.facade;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author HZ
 * @date 2020-11-16
 */
@SpringBootApplication
public class AppFacade {
    public static void main(String[] args) {
        SpringApplication.run(AppFacade.class);
    }

}
