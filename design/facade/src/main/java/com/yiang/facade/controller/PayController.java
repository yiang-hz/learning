package com.yiang.facade.controller;

import com.yiang.facade.service.PayCallbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author HZ
 * @date 2020-11-16
 */
@RestController
public class PayController {

    @Autowired
    private PayCallbackService payCallbackService;

    @RequestMapping("/payCallback")
    public String payCallback() {
        payCallbackService.callback();
        return "success";
    }
}
