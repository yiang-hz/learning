package com.yiang.chain.handler.impl;

import com.yiang.chain.handler.GatewayHandler;
import org.springframework.stereotype.Component;

/**
 * @author HZ
 * @date 2020-11-06
 */
@Component
public class BlacklistHandler extends GatewayHandler {

    @Override
    public void service() {
        System.out.println("第二关 黑名单拦截.......");
        nextService();
    }
}
