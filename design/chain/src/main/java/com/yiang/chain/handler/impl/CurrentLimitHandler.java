package com.yiang.chain.handler.impl;

import com.yiang.chain.handler.GatewayHandler;
import org.springframework.stereotype.Component;

/**
 * @author HZ
 * @date 2020-11-06
 */
@Component
public class CurrentLimitHandler extends GatewayHandler {


    @Override
    public void service() {
        System.out.println("第一关 API接口限流操作.....");
        nextService();// 指向下一关黑名单
    }
}
