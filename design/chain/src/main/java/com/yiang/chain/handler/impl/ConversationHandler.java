package com.yiang.chain.handler.impl;

import com.yiang.chain.handler.GatewayHandler;
import org.springframework.stereotype.Component;

/**
 * @author HZ
 * @date 2020-11-06
 */
@Component
public class ConversationHandler extends GatewayHandler {
    @Override
    public void service() {
        System.out.println("第三关 用户的会话信息拦截.......");
//        nextService();
    }
}
