package com.yiang.chain.handler.factory;


import com.yiang.chain.handler.GatewayHandler;
import com.yiang.chain.handler.impl.BlacklistHandler;
import com.yiang.chain.handler.impl.ConversationHandler;
import com.yiang.chain.handler.impl.CurrentLimitHandler;

/**
 * @author HZ
 * @date 2020-11-06
 */
public class FactoryHandler {

    public static GatewayHandler getGatewayHandler() {
        // 1.使用工厂模式封装Handler责任链
        GatewayHandler gatewayHandler1 = new CurrentLimitHandler();
        GatewayHandler gatewayHandler2 = new BlacklistHandler();
        gatewayHandler1.setNextGatewayHandler(gatewayHandler2);
        GatewayHandler gatewayHandler3 = new ConversationHandler();
        gatewayHandler2.setNextGatewayHandler(gatewayHandler3);
        return gatewayHandler1;
    }

}
