package com.yiang.chain.controller;

import com.yiang.chain.handler.GatewayHandler;
import com.yiang.chain.service.GatewayHandlerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author HZ
 * @date 2020-11-06
 */
@RestController
public class HandlerController {
    @Autowired
    private GatewayHandlerService gatewayHandlerService;

    @RequestMapping("/client")
    public String client() {

//        GatewayHandler gatewayHandler = FactoryHandler.getGatewayHandler();
//        gatewayHandler.service();


        GatewayHandler firstGatewayHandler = gatewayHandlerService.getFirstGatewayHandler();
        firstGatewayHandler.service();
        return "success";
    }
}

