package com.yiang.chain;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author HZ
 * @date 2020-11-06
 */
@SpringBootApplication
@MapperScan("com.yiang.chain.mapper")
public class AppHandler {
    public static void main(String[] args) {
        SpringApplication.run(AppHandler.class);
    }
}
