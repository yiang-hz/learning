package com.yiang.subject;

import com.yiang.observer.Observer;

/**
 * @author HeZhuo
 * @date 2020/10/28
 */
public interface AbstractSubject {

    /**
     * 添加观察者
     * @param obServer 观察者对象
     */
    void addObServer(Observer obServer);

    /**
     * 移除观察者
     * @param obServer 观察者对象
     */
    void removeServer(Observer obServer);

    /**
     * 通知消息
     * @param message 消息主体
     */
    void notifyObServer(String message);

}
