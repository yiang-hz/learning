package com.yiang.subject.impl;


import com.yiang.observer.Observer;
import com.yiang.subject.AbstractSubject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HeZhuo
 * @date 2020/10/28
 */
public class WeChatSubject implements AbstractSubject {

    /**
     * 存放观察者的集合
     */
    private final List<Observer> obServerList = new ArrayList<>();


    @Override
    public void addObServer(Observer observer) {
        // 注册或者添加观察者
        obServerList.add(observer);
    }

    @Override
    public void removeServer(Observer observer) {
        obServerList.remove(observer);
    }

    @Override
    public void notifyObServer(String message) {
        System.out.println("开始设置微信群发消息：" + message);
        // 调用观察者通知方案
        for (Observer observer : obServerList) {
            // 调用该方法通知 获取具体的消息  群发
            observer.update(message);
        }
    }

}
