package com.yiang;

import com.yiang.observer.impl.UserObserver;
import com.yiang.subject.AbstractSubject;
import com.yiang.subject.impl.WeChatSubject;

/**
 * @author HeZhuo
 * @date 2020/10/28
 */
public class Test001 {

    public static void main(String[] args) {

        // 1.创建具体的主题
        AbstractSubject abstractSubject = new WeChatSubject();
        // 2.开始注册或者添加观察者
        abstractSubject.addObServer(new UserObserver("小薇"));
        abstractSubject.addObServer(new UserObserver("小敏"));
        // 3.群发消息
        abstractSubject.notifyObServer("Hello World！");
    }

}