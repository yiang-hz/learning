package com.yiang.observer.impl;

import com.yiang.observer.Observer;
import lombok.AllArgsConstructor;

/**
 * @author HeZhuo
 * @date 2020/10/28
 */
@AllArgsConstructor
public class UserObserver implements Observer {

    private String name;

    @Override
    public void update(String message) {
        System.out.println(name + ", get Message:" + message);
    }

}
