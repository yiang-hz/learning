package com.yiang.weathermachine.observable;

import java.util.Observable;

/**
 * @author HeZhuo
 * @date 2020/10/28
 */
public class MachineObservable extends Observable {

    @Override
    public void notifyObservers(Object arg) {
        // 1.修改状态为可以群发
        setChanged();
        // 2.调用父类方法群发消息
        super.notifyObservers(arg);
    }

}
