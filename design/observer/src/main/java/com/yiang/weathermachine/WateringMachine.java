package com.yiang.weathermachine;

import com.alibaba.fastjson.JSONObject;
import com.yiang.weathermachine.observable.MachineConfig;
import lombok.Getter;

import java.util.Observable;
import java.util.Observer;

/**
 * @author 以安
 * @date 2020-10-28
 */
@Getter
public class WateringMachine implements Observer {

    private boolean status;

    @Override
    public void update(Observable o, Object arg) {

        JSONObject jsonObject = JSONObject.parseObject(String.valueOf(arg));

        Integer tempChange = 10;
        Integer temp = jsonObject.getInteger(MachineConfig.TEMP);

        Integer humidityChange = 55;
        Integer humidity = jsonObject.getInteger(MachineConfig.HUMIDITY);

        Integer windPowerChange = 4;
        Integer windPower = jsonObject.getInteger(MachineConfig.WIND_POWER);

        if (temp > tempChange && humidity < humidityChange && windPower < windPowerChange) {
            status = true;
        }

    }

}