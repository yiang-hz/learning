package com.yiang.weathermachine;

import com.alibaba.fastjson.JSONObject;
import com.yiang.weathermachine.observable.MachineConfig;
import lombok.Getter;

import java.util.Observable;
import java.util.Observer;

/**
 * @author 以安
 * @date 2020-10-28
 */
@Getter
public class ReapingMachine implements Observer {

    private boolean status;

    @Override
    public void update(Observable o, Object arg) {

        JSONObject jsonObject = JSONObject.parseObject(String.valueOf(arg));

        int tempChange = 5;
        Integer temp = jsonObject.getInteger(MachineConfig.TEMP);

        int humidityChange = 65;
        Integer humidity = jsonObject.getInteger(MachineConfig.HUMIDITY);

        if (temp > tempChange && humidity > humidityChange){
            status = true;
        }

    }

}