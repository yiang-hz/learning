package com.yiang.weathermachine;

import com.alibaba.fastjson.JSONObject;
import com.yiang.weathermachine.observable.MachineConfig;
import lombok.Getter;

import java.util.Observable;
import java.util.Observer;

/**
 * @author 以安
 * @date 2020-10-28
 */
@Getter
public class SeedingMachine implements Observer {

    private boolean status;

    @Override
    public void update(Observable o, Object arg) {

        JSONObject jsonObject = JSONObject.parseObject(String.valueOf(arg));

        Integer tempChange = 5;

        if (jsonObject.getInteger(MachineConfig.TEMP) > tempChange){
            status = true;
        }

    }

}