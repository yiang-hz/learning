package com.yiang.state.impl;

/**
 * @author HeZhuo
 * @date 2020/11/16
 */
public interface OrderState {
    void orderService();
}