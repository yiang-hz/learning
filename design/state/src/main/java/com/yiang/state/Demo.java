package com.yiang.state;

import com.yiang.state.impl.AlreadySignedOrderState;
import com.yiang.state.impl.InTransitOrderState;
import com.yiang.state.impl.OrderState;
import com.yiang.state.impl.ShippedAlreadyOrderState;

/**
 * @author HeZhuo
 * @date 2020/11/16
 */
public class Demo {

    public static void main(String[] args) {
        //目前使用clz传递，可以通过springBean灵活加载
        orderStateNew(ShippedAlreadyOrderState.class);
        orderStateNew(InTransitOrderState.class);
        orderStateNew(AlreadySignedOrderState.class);
        System.out.println(orderState("0"));
    }

    public static void orderStateNew(Class<?> clz) {
        try {
            OrderState contextState = (OrderState) clz.newInstance();
            contextState.orderService();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * 需要优化的代码，订单状态判断
     * @param state 状态码
     * @return 信息
     */
    public static String orderState(String state) {
        if ("0".equals(state)) {
            return "已经发货";
        }
        if ("1".equals(state)) {
            return "正在运送中...调用第三方快递接口 展示 运送信息";
        }
        if ("2".equals(state)) {
            return "正在派送中... 返回派送人员信息";
        }
        if ("3".equals(state)) {
            return "已经签收，提示给用户快递员评价";
        }
        if ("4".equals(state)) {
            return "拒绝签收， 重新开始申请退单";
        }
        if ("5".equals(state)) {
            return "订单交易失败，调用短信接口提示 ";
        }
        return "未找到对应的状态";
    }

}

