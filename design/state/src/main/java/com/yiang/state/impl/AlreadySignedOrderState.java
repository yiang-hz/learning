package com.yiang.state.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author HeZhuo
 * @date 2020/11/16
 */
@Slf4j
@Component
public class AlreadySignedOrderState implements OrderState {
    @Override
    public void orderService() {
        log.info(">>>切换为已经签收状态..");
    }
}