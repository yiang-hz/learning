package com.yiang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author HeZhuo
 * @date 2020/11/2
 */
@SpringBootApplication
public class StrateayApp {

    public static void main(String[] args) {
        SpringApplication.run(StrateayApp.class, args);
    }
}
