package com.yiang.controller;

import com.yiang.strateay.context.PayContextStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author HeZhuo
 * @date 2020/11/2
 */
@RestController
public class StrategyController {

    @Autowired
    private PayContextStrategy payContextStrategy;

    @RequestMapping("/pay")
    public String getPayHtml(String arg) {
        return payContextStrategy.toPayHtml(arg);
    }
}
