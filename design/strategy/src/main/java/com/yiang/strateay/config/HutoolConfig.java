package com.yiang.strateay.config;

import org.springframework.context.annotation.ComponentScan;

/**
 * @author HeZhuo
 * @date 2020/11/2
 */
@ComponentScan(basePackages={"cn.hutool.extra.spring"})
public class HutoolConfig {
}
