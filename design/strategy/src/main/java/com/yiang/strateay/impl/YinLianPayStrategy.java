package com.yiang.strateay.impl;

import com.yiang.strateay.PayStrategy;
import org.springframework.stereotype.Component;

/**
 * @author HZ
 * @date 2020-11-02
 */
@Component
public class YinLianPayStrategy implements PayStrategy {

    @Override
    public String toPayHtml() {
        return "调用银联支付接口...";
    }

}
