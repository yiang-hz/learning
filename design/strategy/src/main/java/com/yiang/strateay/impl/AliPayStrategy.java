package com.yiang.strateay.impl;

import com.yiang.strateay.PayStrategy;
import org.springframework.stereotype.Component;

/**
 * @author HZ
 * @date 2020-11-02
 */
@Component
public class AliPayStrategy implements PayStrategy {

    @Override
    public String toPayHtml() {
        return "调用支付宝支付接口...";
    }
    //AliPayStrategy 注入spring容器 aliPayStrategy
}
