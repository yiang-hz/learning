package com.yiang.strateay;

/**
 * @author HZ
 * @date 2020-11-02
 */
public interface PayStrategy {

    /**
     *  策略模式共同算法的骨架
     * @return String
     */
    String toPayHtml();
}
