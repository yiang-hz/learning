package com.yiang.strateay.context;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.yiang.strateay.PayStrategy;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author HZ
 * @date 2020-11-02
 */
@Component
@Import(cn.hutool.extra.spring.SpringUtil.class)
public class PayContextStrategy {

    static Map<String, String> paymentChannelMap = new HashMap<>();

    static {
        paymentChannelMap.put("aliPay", "aliPayStrategy");
        paymentChannelMap.put("wxPay", "wxPayStrategy");
        paymentChannelMap.put("yinLian", "yinLianPayStrategy");
    }

    public String toPayHtml(String payCode){

         //1.使用payCode参数查询数据库获取 beanId
        String paymentChannel = paymentChannelMap.get(payCode);

        if (ObjectUtil.isEmpty(paymentChannel)) {
            return "没有该渠道信息";
        }

        //2.获取到bean的id之后，使用spring容器获取实例对象

        // 3.执行该实现的方法即可.... aliPayStrategy
        PayStrategy payStrategy = SpringUtil.getBean(paymentChannel, PayStrategy.class);
        return payStrategy.toPayHtml();
     }
    // 优化 支付渠道存放内存中...
}
