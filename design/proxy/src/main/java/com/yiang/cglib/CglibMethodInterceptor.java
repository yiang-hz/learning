package com.yiang.cglib;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * Cglib代理  拦截器
 * @author HeZhuo
 * @date 2021/7/14
 */
public class CglibMethodInterceptor implements MethodInterceptor {

    @Override
    public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        System.out.println("cglib动态代理开始执行");
        //这里是执行invokeSuper，而不是invoke
        Object invoke = proxy.invokeSuper(obj, args);
        System.out.println("cglib动态代理结束执行");
        return invoke;
    }

}
