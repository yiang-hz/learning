package com.yiang.cglib;

import java.lang.reflect.Method;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import net.sf.cglib.core.ReflectUtils;
import net.sf.cglib.core.Signature;
import net.sf.cglib.proxy.Callback;
import net.sf.cglib.proxy.Factory;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

public class MemberImpl$$EnhancerByCGLIB$$7f88fc6c extends MemberImpl {
  private boolean CGLIB$BOUND;

  public static Object CGLIB$FACTORY_DATA;

  private static ThreadLocal CGLIB$THREAD_CALLBACKS;

  private static Callback[] CGLIB$STATIC_CALLBACKS;

  private MethodInterceptor CGLIB$CALLBACK_0;

  private static Object CGLIB$CALLBACK_FILTER;

  private static Method CGLIB$addMember$0$Method;

  private static MethodProxy CGLIB$addMember$0$Proxy;

  private static Object[] CGLIB$emptyArgs;

  private static Method CGLIB$equals$1$Method;

  private static MethodProxy CGLIB$equals$1$Proxy;

  private static Method CGLIB$toString$2$Method;

  private static MethodProxy CGLIB$toString$2$Proxy;

  private static Method CGLIB$hashCode$3$Method;

  private static MethodProxy CGLIB$hashCode$3$Proxy;

  private static Method CGLIB$clone$4$Method;

  private static MethodProxy CGLIB$clone$4$Proxy;

  static void CGLIB$STATICHOOK1() throws ClassNotFoundException {
    CGLIB$THREAD_CALLBACKS = new ThreadLocal();
    CGLIB$emptyArgs = new Object[0];
    Class clazz1 = Class.forName("com.yiang.cglib.MemberImpl$$EnhancerByCGLIB$$7f88fc6c");
    Class clazz2;
    CGLIB$equals$1$Method = ReflectUtils.findMethods(new String[] { "equals", "(Ljava/lang/Object;)Z", "toString", "()Ljava/lang/String;", "hashCode", "()I", "clone", "()Ljava/lang/Object;" }, (clazz2 = Class.forName("java.lang.Object")).getDeclaredMethods())[0];
    CGLIB$equals$1$Proxy = MethodProxy.create(clazz2, clazz1, "(Ljava/lang/Object;)Z", "equals", "CGLIB$equals$1");
    CGLIB$toString$2$Method = ReflectUtils.findMethods(new String[] { "equals", "(Ljava/lang/Object;)Z", "toString", "()Ljava/lang/String;", "hashCode", "()I", "clone", "()Ljava/lang/Object;" }, (clazz2 = Class.forName("java.lang.Object")).getDeclaredMethods())[1];
    CGLIB$toString$2$Proxy = MethodProxy.create(clazz2, clazz1, "()Ljava/lang/String;", "toString", "CGLIB$toString$2");
    CGLIB$hashCode$3$Method = ReflectUtils.findMethods(new String[] { "equals", "(Ljava/lang/Object;)Z", "toString", "()Ljava/lang/String;", "hashCode", "()I", "clone", "()Ljava/lang/Object;" }, (clazz2 = Class.forName("java.lang.Object")).getDeclaredMethods())[2];
    CGLIB$hashCode$3$Proxy = MethodProxy.create(clazz2, clazz1, "()I", "hashCode", "CGLIB$hashCode$3");
    CGLIB$clone$4$Method = ReflectUtils.findMethods(new String[] { "equals", "(Ljava/lang/Object;)Z", "toString", "()Ljava/lang/String;", "hashCode", "()I", "clone", "()Ljava/lang/Object;" }, (clazz2 = Class.forName("java.lang.Object")).getDeclaredMethods())[3];
    CGLIB$clone$4$Proxy = MethodProxy.create(clazz2, clazz1, "()Ljava/lang/Object;", "clone", "CGLIB$clone$4");
    ReflectUtils.findMethods(new String[] { "equals", "(Ljava/lang/Object;)Z", "toString", "()Ljava/lang/String;", "hashCode", "()I", "clone", "()Ljava/lang/Object;" }, (clazz2 = Class.forName("java.lang.Object")).getDeclaredMethods());
    CGLIB$addMember$0$Method = ReflectUtils.findMethods(new String[] { "addMember", "(Ljava/lang/String;)Ljava/lang/String;" }, (clazz2 = Class.forName("com.yiang.cglib.MemberImpl")).getDeclaredMethods())[0];
    CGLIB$addMember$0$Proxy = MethodProxy.create(clazz2, clazz1, "(Ljava/lang/String;)Ljava/lang/String;", "addMember", "CGLIB$addMember$0");
    ReflectUtils.findMethods(new String[] { "addMember", "(Ljava/lang/String;)Ljava/lang/String;" }, (clazz2 = Class.forName("com.yiang.cglib.MemberImpl")).getDeclaredMethods());
  }

  final String CGLIB$addMember$0(String paramString) {
    return super.addMember(paramString);
  }

  @SneakyThrows
  @Override
  public final String addMember(String paramString) {
    if (this.CGLIB$CALLBACK_0 == null) {
      CGLIB$BIND_CALLBACKS(this);
    }
    return (this.CGLIB$CALLBACK_0 != null) ?
            (String)this.CGLIB$CALLBACK_0.intercept(
                    this, CGLIB$addMember$0$Method, new Object[] { paramString }, CGLIB$addMember$0$Proxy
            )
            : super.addMember(paramString);
  }

  public MemberImpl$$EnhancerByCGLIB$$7f88fc6c() {
    CGLIB$BIND_CALLBACKS(this);
  }

  private static final void CGLIB$BIND_CALLBACKS(Object paramObject) {
    MemberImpl$$EnhancerByCGLIB$$7f88fc6c memberImpl$$EnhancerByCGLIB$$7f88fc6c = (MemberImpl$$EnhancerByCGLIB$$7f88fc6c)paramObject;
    if (!memberImpl$$EnhancerByCGLIB$$7f88fc6c.CGLIB$BOUND) {
      memberImpl$$EnhancerByCGLIB$$7f88fc6c.CGLIB$BOUND = true;
      if (CGLIB$THREAD_CALLBACKS.get() == null) {
        CGLIB$THREAD_CALLBACKS.get();
        if (CGLIB$STATIC_CALLBACKS == null) {
          return;
        }
      }
      memberImpl$$EnhancerByCGLIB$$7f88fc6c.CGLIB$CALLBACK_0 = (MethodInterceptor)((Callback[])CGLIB$THREAD_CALLBACKS.get())[0];
    }
  }

  public void setCallback(Callback paramArrayOfCallback) {
    this.CGLIB$CALLBACK_0 = (MethodInterceptor) paramArrayOfCallback;
  }

  static {
    try {
      CGLIB$STATICHOOK1();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }
}
