package com.yiang.cglib;

import net.sf.cglib.core.DebuggingClassWriter;
import net.sf.cglib.proxy.Enhancer;

/**
 * @author HeZhuo
 * @date 2021/7/14
 */
public class CglibProxy {

    public static void main(String[] args) {
//        System.setProperty(DebuggingClassWriter.DEBUG_LOCATION_PROPERTY, "D:\\code");
//        Enhancer enhancer = new Enhancer();
//        enhancer.setSuperclass(MemberImpl.class);
//        enhancer.setCallback(new CglibMethodInterceptor());
//        // 创建代理代理对象
//        MemberService memberService = (MemberService) enhancer.create();
//        memberService.addMember("yiang");

        MemberImpl$$EnhancerByCGLIB$$7f88fc6c memberServiceImpl=
                new MemberImpl$$EnhancerByCGLIB$$7f88fc6c();
        memberServiceImpl.setCallback(new CglibMethodInterceptor());
        memberServiceImpl.addMember("yiang");

    }
}
