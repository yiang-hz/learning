package com.yiang.cglib;

/**
 * @author HeZhuo
 * @date 2021/7/14
 */
public interface MemberService {

    String addMember(String userName);
}
