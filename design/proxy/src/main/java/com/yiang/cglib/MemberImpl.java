package com.yiang.cglib;

/**
 * @author HeZhuo
 * @date 2021/7/14
 */
public class MemberImpl implements MemberService{

    @Override
    public String addMember(String userName) {
        System.out.println("方法执行：" + userName);
        return "create member success：" + userName;
    }
}
