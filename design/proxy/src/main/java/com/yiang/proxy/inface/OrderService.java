package com.yiang.proxy.inface;

/**
 * @author HeZhuo
 * @date 2021/7/13
 */
public interface OrderService {

    String addOrder(String id, String name);
}
