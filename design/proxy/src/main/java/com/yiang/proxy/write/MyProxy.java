package com.yiang.proxy.write;

import com.yiang.proxy.inface.OrderService;
import lombok.SneakyThrows;

import javax.tools.JavaCompiler;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

/**
 * @author HeZhuo
 * @date 2021/7/13
 */
public class MyProxy {

    private static final String RT = "\r\t";

    /**
     * 获取代理类对象
     * @param loader 类加载器
     * @param classInfoArray 继承的接口信息，由于java支持继承多个接口，所以参数是数据形式
     * @param handler 处理器
     * @return Object代理类
     */
    @SneakyThrows
    @SuppressWarnings("rawtypes, unchecked")
    public static Object newProxyInstance(JavaClassLoader loader,
                                          Class<?>[] classInfoArray,
                                          MyInvocationHandler handler) {
        Class<?> classInfo = classInfoArray[0];
        //  1.使用java反射机制拼接$Proxy.java类的源代码
        Method[] methods = classInfo.getMethods();
        String proxyClass = "package com.yiang.proxy.write;" + RT
                + "import java.lang.reflect.Method;" + RT
                + "import com.yiang.proxy.write.MyInvocationHandler;" + RT
                + "import java.lang.reflect.UndeclaredThrowableException;" + RT
                + "public class $Proxy2 implements " + classInfo.getName() + "{" + RT
                + "MyInvocationHandler h;" + RT
                + "public $Proxy2(MyInvocationHandler h)" + "{" + RT
                + "this.h= h;" + RT + "}"
                + getMethodString(methods, classInfo) + RT + "}";

        // 2. 将代理类源码文件写入硬盘中，路径需要提前创建好
        String filename = "d:/code/$Proxy2.java";
        File f = new File(filename);
        FileWriter fw = new FileWriter(f);
        fw.write(proxyClass);
        fw.flush();
        fw.close();

        // 3.需要将$Proxy.java编译成$Proxy.class
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        StandardJavaFileManager fileMgr = compiler.getStandardFileManager(null, null, null);
        Iterable units = fileMgr.getJavaFileObjects(filename);
        JavaCompiler.CompilationTask t = compiler.getTask(null, fileMgr, null, null, null, units);
        t.call();
        fileMgr.close();

        // 4.将class文件加入到内存中
        Class<?> proxy0Class = loader.findClass("$Proxy2");

        //5.使用java反射机制给函数中赋值
        Constructor<?> m = proxy0Class.getConstructor(MyInvocationHandler.class);

        return m.newInstance(handler);
    }

    @SuppressWarnings("unused")
    public static String getMethodString(Method[] methods, Class<?> clz) {

        String proxyMe = "";

        for (Method method : methods) {
            Class<?>[] parameterTypes = method.getParameterTypes();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < parameterTypes.length; i++) {
                sb.append(parameterTypes[i].getName()).append(" var").append(i + 1);
                if (i < parameterTypes.length - 1) {
                    sb.append(" ,");
                }
            }
            String parameterStr = sb.toString();
            proxyMe = "public " + method.getReturnType().getName() + " " + method.getName() + " ( " + parameterStr + " ) { " +
                        "try {   " +
                        "   Method m3 = Class.forName(\"com.yiang.proxy.inface.OrderService\").getMethod(\"addOrder\", Class.forName(\"java.lang.String\"), Class.forName(\"java.lang.String\"));" +
                        "   return (String) h.invoke(this, m3, new Object[]{var1, var2}); " +
                        "} catch (RuntimeException | Error var4) {  " +
                        "   throw var4;  " +
                        "} catch (Throwable var5) {   " +
                        "   throw new UndeclaredThrowableException(var5); " +
                        "} " +
                        " " +
                    "} ";

        }
        return proxyMe;
    }

    public static void main(String[] args) {
        Class<?>[] classInfoArray = new Class[1];
        classInfoArray[0] = OrderService.class;
        MyProxy.newProxyInstance(new JavaClassLoader(), classInfoArray, null);
    }
}
