package com.yiang.proxy.write;

import lombok.SneakyThrows;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * @author HeZhuo
 * @date 2021/7/13
 */
public class JavaClassLoader extends ClassLoader {

    private final File classPathFile;

    public JavaClassLoader() {
        // 通过获取类当前路径的方法 此处没必要使用 JavaClassLoader.class.getResource("").getPath();
        String classPath = "D:\\code";
        this.classPathFile = new File(classPath);
    }

    @SneakyThrows
    @Override
    public Class<?> findClass(String name) {

        String className = JavaClassLoader.class.getPackage().getName() + "." + name;
        File classFile = new File(classPathFile,name.replaceAll("\\.","/") + ".class");
        if (classFile.exists()) {
            try (FileInputStream in = new FileInputStream(classFile); ByteArrayOutputStream out = new ByteArrayOutputStream()) {
                byte[] buff = new byte[1024];
                int len;
                while ((len = in.read(buff)) != -1) {
                    out.write(buff, 0, len);
                }
                return defineClass(className, out.toByteArray(), 0, out.size());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        throw new FileNotFoundException("找不到对应的文件！");
    }
}
