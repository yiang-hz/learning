package com.yiang.proxy.dynamic;

import com.yiang.proxy.inface.OrderService;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.UndeclaredThrowableException;

/**
 * @author HeZhuo
 * @date 2021/7/13
 */
public class $Order0 implements OrderService {

    private final InvocationHandler h;

    public $Order0(InvocationHandler h) {
        this.h = h;
    }

    @Override
    public String addOrder(String id, String name) {
        try {
            //实际上是一个简单的方法对象，执行对应的方法还是需要通过反射才能执行。
            Method m3 = Class
                    .forName("com.yiang.proxy.inface.OrderService")
                    .getMethod("addOrder", Class.forName("java.lang.String"),
                            Class.forName("java.lang.String")
                    );
            return (String) this.h.invoke(this, m3, new Object[]{id, name});
        } catch (RuntimeException | Error var4) {
            throw var4;
        } catch (Throwable var5) {
            throw new UndeclaredThrowableException(var5);
        }
    }

}
