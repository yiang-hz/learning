package com.yiang.proxy.inface;

/**
 * @author HeZhuo
 * @date 2021/7/13
 */
public class OrderListener implements OrderService {

    @Override
    public String addOrder(String id, String name) {
        System.out.println("addOrder-- id:" + id + " name:" + name);
        return "执行结束";
    }
}
