package com.yiang.proxy.dynamic;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 *
 * 核心：拦截
 * @author HeZhuo
 * @date 2021/7/13
 */
public class JdkHandler implements InvocationHandler {

    /**
     * 目标对象（被代理类）
     */
    private final Object target;

    public JdkHandler(Object target) {
        this.target = target;
    }

    /**
     *
     * @param proxy Jdk动态生成的代理类
     * @param method 不是真正的目标方法，而是接口中的方法
     * @param args 代理类的参数
     * @return Object
     * @throws Throwable 异常
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        System.out.println("JDK动态代理开始执行....");
        Object result = method.invoke(target, args);
        System.out.println("JDK动态代理结束执行....");

        return result;
    }

    @SuppressWarnings("unchecked")
    public <T> T getProxy() {
        return (T) Proxy.newProxyInstance(
                target.getClass().getClassLoader(), target.getClass().getInterfaces(), this
        );
    }

    @SuppressWarnings("unchecked")
    public <T> T getProxyWriter() {
        return (T) new $Order0(this);
    }

}
