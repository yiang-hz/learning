package com.yiang.proxy.write;

import java.lang.reflect.Method;

/**
 * @author HeZhuo
 * @date 2021/7/13
 */
public class MyInvocationHandlerImpl implements MyInvocationHandler {

    /**
     * 目标对象（被代理类）
     */
    private final Object target;

    public MyInvocationHandlerImpl(Object target) {
        this.target = target;
    }

    /**
     *
     * @param proxy Jdk动态生成的代理类
     * @param method 不是真正的目标方法，而是接口中的方法
     * @param args 代理类的参数
     * @return Object
     * @throws Throwable 异常
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        System.out.println("JDK动态代理开始执行....");
        Object result = method.invoke(target, args);
        System.out.println("JDK动态代理结束执行....");

        return result;
    }

    @SuppressWarnings("unchecked")
    public <T> T getProxyAllWriter() {
        return (T) MyProxy.newProxyInstance(
                new JavaClassLoader(), target.getClass().getInterfaces(), this
        );
    }
}
