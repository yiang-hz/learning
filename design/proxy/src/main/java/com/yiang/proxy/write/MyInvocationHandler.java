package com.yiang.proxy.write;

import java.lang.reflect.Method;

/**
 * 同JDK的 InvocationHandler
 * @author HeZhuo
 * @date 2021/7/13
 */
public interface MyInvocationHandler {

    Object invoke(Object proxy, Method method, Object[] args) throws Throwable;
}
