package com.yiang.proxy;

import com.yiang.proxy.dynamic.JdkHandler;
import com.yiang.proxy.inface.OrderService;
import com.yiang.proxy.inface.OrderListener;
import com.yiang.proxy.inface.OrderImpl;
import com.yiang.proxy.write.MyInvocationHandlerImpl;

/**
 * 代理模式测试类
 * @author HeZhuo
 * @date 2021/7/13
 */
@SuppressWarnings("unuserd")
public class TestProxy {

    public static void main(String[] args) {

        System.out.println("\n" + "1.静态代理 类代理");
        inClassProxy();

        System.out.println("\n" + "2.静态代理 接口代理");
        inFaceProxy();

        System.out.println("\n" + "3.动态代理");
        dynamicProxy();

        System.out.println("\n" + "4.动态代理 半封装");
        dynamicProxyWrite();

        System.out.println("\n" + "5.动态代理 纯手写（内容还有许多可优化）");
        dynamicProxyAllWrite();
    }

    /**
     * 静态代理，通过类实现 （Cglib代理的实现原理）
     * 缺点：因为是继承实现，Java不能多继承
     */
    private static void inClassProxy() {
        com.yiang.proxy.inclass.OrderService orderService = new com.yiang.proxy.inclass.OrderService();
        orderService.addOrder("1", "小米18");
    }

    /**
     * 静态代理，通过接口实现 （JavaJDK动态代理的实现原理）
     */
    private static void inFaceProxy() {
        OrderImpl orderImpl = new OrderImpl(new OrderListener());
        orderImpl.addOrder("1短袜", "小米18");
    }

    /**
     * 动态代理
     */
    private static void dynamicProxy() {
        /*
         * 查看JDK动态生成的代理类源码
         * 类头：
         * public final class $Proxy0 extends Proxy implements OrderService {
         * 可以查看到代理类是继承了代理接口的，所以如果使用实现子类接收其代理对象则会抛出转换异常
         * 固这里使用接口接收
         */
        System.getProperties().put("sun.misc.ProxyGenerator.saveGeneratedFiles", "true");

        JdkHandler jdkHandler = new JdkHandler(new OrderListener());
        OrderService proxy = jdkHandler.getProxy();
        proxy.addOrder("1短袜", "小米18");
    }

    /**
     * 动态代理（代码实现代理类操作）
     */
    private static void dynamicProxyWrite() {
        System.getProperties().put("sun.misc.ProxyGenerator.saveGeneratedFiles", "true");

        JdkHandler jdkHandler = new JdkHandler(new OrderListener());
        OrderService proxy = jdkHandler.getProxyWriter();
        proxy.addOrder("1短袜", "小米18");
    }

    /**
     * 动态代理（纯手写模式）
     */
    private static void dynamicProxyAllWrite() {
        System.getProperties().put("sun.misc.ProxyGenerator.saveGeneratedFiles", "true");

        MyInvocationHandlerImpl handler = new MyInvocationHandlerImpl(new OrderListener());
        OrderService proxy = handler.getProxyAllWriter();
        proxy.addOrder("1短袜", "小米18");
    }
}
