package com.yiang.proxy.inclass;

import com.yiang.proxy.inface.OrderListener;

/**
 * @author HeZhuo
 * @date 2021/7/13
 */
public class OrderService extends OrderListener {

    @Override
    public String addOrder(String id, String name) {
        System.out.println("OrderService... 类代理开始");
        return super.addOrder(id, name);
    }
}
