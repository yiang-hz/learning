package com.yiang.proxy.inface;

/**
 * @author HeZhuo
 * @date 2021/7/13
 */
public class OrderImpl implements OrderService {

    private final OrderListener order;

    public OrderImpl(OrderListener order) {
        this.order = order;
    }

    @Override
    public String addOrder(String id, String name) {
        System.out.println("OrderService... 接口代理开始");
        return order.addOrder(id, name);
    }

}
