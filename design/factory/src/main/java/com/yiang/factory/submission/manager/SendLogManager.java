package com.yiang.factory.submission.manager;

import com.yiang.factory.submission.dto.LogDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @author HeZhuo
 * @date 2021/11/29
 */
@Component
@Slf4j
public class SendLogManager {

    /**
     * 发送日志方法，处理逻辑存放在该方法内
     * @param paramDTO 日志参数实体类
     */
    private static void doSendLog(LogDTO paramDTO) {
        log.info("日志输入 写入数据库....LogDTO:{}", paramDTO);
    }

    /**
     * 异步兼容方法，用于Spring环境启动时使用
     * @param paramDTO 日志参数实体类
     */
    @Async
    public void sendAsyncLog(LogDTO paramDTO) {
        doSendLog(paramDTO);
    }

    /**
     * 同步兼容方法，用于单元测试非Spring或Main test环境启动时使用
     * @param paramDTO 日志参数实体类
     */
    public static void sendLog(LogDTO paramDTO) {
        doSendLog(paramDTO);
    }

}
