package com.yiang.factory.submission.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author HeZhuo
 * @date 2021/11/29
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SubmissionParamDTO {

    private String userName;

    private String phone;
}
