package com.yiang.factory.submission.worker;

import com.yiang.factory.submission.dto.LogDTO;
import com.yiang.factory.submission.dto.SubmissionParamDTO;
import com.yiang.factory.submission.exception.SubmissionParamException;
import com.yiang.factory.submission.template.AbstractSubmissionTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

/**
 * @author HeZhuo
 * @date 2021/11/29
 */
@Slf4j
@Component
public class UserSubmission extends AbstractSubmissionTemplate {


    @Override
    protected void verify(SubmissionParamDTO paramEntity) {

    }

    @Override
    protected void depacketize(SubmissionParamDTO paramEntity) {

    }

    @Override
    public void loadLogParam(LogDTO logDTO) {
        logDTO.setType("User Submission");
    }

    @Override
    public void sendFile(SubmissionParamDTO paramEntity) {
        log.info("文件发送完成......");
    }

    @Override
    public void checkParam(SubmissionParamDTO paramEntity) {
        log.info("user Submission answer...");
        if (ObjectUtils.isEmpty(paramEntity.getUserName())) {
            throw new SubmissionParamException("用户名称不得为空！");
        }
    }

}
