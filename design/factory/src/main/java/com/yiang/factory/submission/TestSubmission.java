package com.yiang.factory.submission;

import com.yiang.factory.submission.dto.SubmissionParamDTO;
import com.yiang.factory.submission.template.AbstractSubmissionTemplate;
import com.yiang.factory.submission.worker.UserSubmission;

/**
 * @author HeZhuo
 * @date 2021/11/29
 */
public class TestSubmission {

    public static void main(String[] args) {
        AbstractSubmissionTemplate submissionTemplate = new UserSubmission();
        SubmissionParamDTO paramDTO = new SubmissionParamDTO();
        paramDTO.setPhone("13387373751");
        paramDTO.setUserName("hezhuo");
        Object result = submissionTemplate.beginSubmission(paramDTO);
        System.out.println(result);
    }
}
