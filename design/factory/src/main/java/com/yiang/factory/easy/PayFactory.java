package com.yiang.factory.easy;

import com.yiang.factory.easy.impl.AliPay;
import com.yiang.factory.easy.impl.Pay;
import com.yiang.factory.easy.impl.WxPay;

/**
 * @author HeZhuo
 * @date 2020/11/11
 */
public class PayFactory {

    public static Pay getPay(String type) {
        switch (type) {
            case "aliPay":
                return new AliPay();
            case "wxPay":
            default:
                return new WxPay();
        }
    }
}
