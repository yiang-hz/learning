package com.yiang.factory.boot.impl;

import com.yiang.factory.boot.AbstractPayCallbackTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author HeZhuo
 * @date 2020/11/11
 */
@Slf4j
@Component
public class UnionPayCallbackTemplate extends AbstractPayCallbackTemplate {
    @Override
    protected Map<String, String> verifySignature() {
        //>>>>假设一下为银联回调报文>>>>>>>>>>>>>>>>
        log.info(">>>>>第一步 解析银联据报文.....verifySignature()");
        Map<String, String> verifySignature = new HashMap<>();
        verifySignature.put("price", "1399");
        verifySignature.put("orderDes", "充值蚂蚁课堂永久会员");
        // 支付状态为1表示为成功....
        verifySignature.put("unionPayMentStatus", "1");
        verifySignature.put("unionPayOrderNumber", "201910101011");
        //>>>>假设一下为阿里pay回调报文结束>>>>>>>>>>>>>>>>

        // 解析报文是否成功 或者验证签名成功返回 200 为成功..
        verifySignature.put("analysisCode", "200");
        return verifySignature;

    }

    @Override
    protected String asyncService(Map<String, String> verifySignature) {
        log.info(">>>>>第三步 银联回调  asyncService()verifySignatureMap:{}", verifySignature);
        String paymentStatus = verifySignature.get("unionPayMentStatus");
        if ("1".equals(paymentStatus)) {
            String aliPayOrderNumber = verifySignature.get("unionOrderNumber");
            log.info(">>>>orderNumber:{},已经支付成功 修改订单状态为已经支付...", aliPayOrderNumber);
        }

        return resultSuccess();
    }

    @Override
    protected String resultFail() {
        return "fail";
    }

    @Override
    protected String resultSuccess() {
        return "ok";
    }
}
