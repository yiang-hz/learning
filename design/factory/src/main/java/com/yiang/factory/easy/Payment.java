package com.yiang.factory.easy;

/**
 * @author HeZhuo
 * @date 2020/11/11
 */
public interface Payment {

    void pay();
}
