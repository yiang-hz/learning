package com.yiang.factory.easy;

import com.yiang.factory.easy.impl.AliPay;
import com.yiang.factory.easy.impl.Pay;
import com.yiang.factory.easy.impl.WxPay;

/**
 * 抽象工厂的案例
 * @author HeZhuo
 * @date 2020/11/11
 */
public class TestFactory {

    public static void main(String[] args) {
        testPay();
    }

    public static void testPay() {
        Pay aliPay = new AliPay();
        Pay wxPay = new WxPay();
        aliPay.pay();wxPay.pay();

        Pay aliPayFactory = PayFactory.getPay("aliPay");
        aliPayFactory.pay();

        //Spring 源码提供的工厂模式的接口
//        BeanFactory;
    }
}
