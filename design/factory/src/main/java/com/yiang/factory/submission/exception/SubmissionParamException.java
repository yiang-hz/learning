package com.yiang.factory.submission.exception;

/**
 * @author HeZhuo
 * @date 2021/11/29
 */
public class SubmissionParamException extends RuntimeException {

    public SubmissionParamException(String message) {
        super(message);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public Throwable fillInStackTrace() {
        return super.fillInStackTrace();
    }

}
