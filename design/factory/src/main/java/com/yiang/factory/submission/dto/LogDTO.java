package com.yiang.factory.submission.dto;

import lombok.*;

/**
 * @author HeZhuo
 * @date 2021/11/29
 */
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString(callSuper = true)
public class LogDTO extends SubmissionParamDTO {

    private String type;
}
