package com.yiang.factory.submission.controller;

import cn.hutool.extra.spring.SpringUtil;
import com.yiang.factory.submission.template.AbstractSubmissionTemplate;
import com.yiang.factory.submission.dto.SubmissionParamDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author HeZhuo
 * @date 2021/11/29
 */
@RestController
@Slf4j
public class SubmissionController {

    @PostMapping("/submission/{name}")
    public Object submission(@PathVariable("name") String submissionName) {
        AbstractSubmissionTemplate submissionTemplate = SpringUtil.getBean(submissionName);
        SubmissionParamDTO paramDTO = new SubmissionParamDTO();
        paramDTO.setPhone("13387373751");
        paramDTO.setUserName("hezhuo");
        return submissionTemplate.beginSubmission(paramDTO);
    }
}
