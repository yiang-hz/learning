package com.yiang.factory.boot.controller;

import com.yiang.factory.boot.AbstractPayCallbackTemplate;
import com.yiang.factory.boot.factory.TemplateFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author HeZhuo
 * @date 2020/11/11
 */
@RestController
public class TemplateController {

    /**
     * 支付回调
     *
     * @return String
     */
    @RequestMapping("/asyncCallBack")
    public String asyncCallBack(String templateId) {
        AbstractPayCallbackTemplate payCallbackTemplate = TemplateFactory.getPayCallbackTemplate(templateId);
        // 使用模版方法模式 执行共同的骨架
        return payCallbackTemplate.asyncCallBack();
    }

}
