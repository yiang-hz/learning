package com.yiang.factory.submission;

import cn.hutool.extra.spring.EnableSpringUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author HeZhuo
 * @date 2021/11/29
 */
@SpringBootApplication
@EnableAsync
@EnableSpringUtil
public class AppSubmission {

    public static void main(String[] args) {
        SpringApplication.run(AppSubmission.class, args);
    }
}
