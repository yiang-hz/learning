package com.yiang.factory.easy.impl;

/**
 * @author HeZhuo
 * @date 2020/11/11
 */
public class AliPay extends Pay {
    @Override
    public void pay() {
        System.out.println("AliPay");
    }
}
