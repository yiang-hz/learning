package com.yiang.factory.boot.factory;


import com.yiang.factory.boot.AbstractPayCallbackTemplate;
import com.yiang.factory.boot.utils.SpringUtils;

/**
 * @author HeZhuo
 * @date 2020/11/11
 */
public class TemplateFactory {

    /**
     * 使用工厂模式获取模版
     *
     * @param templateId templateId
     * @return AbstractPayCallbackTemplate
     */
    public static AbstractPayCallbackTemplate getPayCallbackTemplate(String templateId) {
        return (AbstractPayCallbackTemplate) SpringUtils.getBean(templateId);
    }
}
