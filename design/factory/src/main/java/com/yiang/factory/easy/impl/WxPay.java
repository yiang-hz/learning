package com.yiang.factory.easy.impl;

import com.yiang.factory.easy.Payment;

/**
 * @author HeZhuo
 * @date 2020/11/11
 */
public class WxPay extends Pay{
    @Override
    public void pay() {
        System.out.println("wxPay");
    }
}
