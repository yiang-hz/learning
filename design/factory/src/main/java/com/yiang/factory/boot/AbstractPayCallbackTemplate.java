package com.yiang.factory.boot;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;

import java.util.Map;

/**
 * @author HeZhuo
 * @date 2020/11/11
 */
@Slf4j
public abstract class AbstractPayCallbackTemplate {

    /**
     * 定义共同行为的骨架
     *
     * @return String
     */
    public String asyncCallBack() {
        // 1.验证参数和验证签名
        Map<String, String> verifySignature = verifySignature();
        // 2.日志收集 相同
        payLog(verifySignature);
        // 3. 获取验证签名状态
        String analysisCode = verifySignature.get("analysisCode");
        if (!"200".equals(analysisCode)) {
            return resultFail();
        }
        // 3.更改数据库状态同时返回不同支付结果
        return asyncService(verifySignature);

    }

    /**
     * 使用多线程异步写入日志
     *
     * @param verifySignature verifySignature
     */
    @Async
    public void payLog(Map<String, String> verifySignature) {
        log.info("第二步骤 写入数据库....verifySignature:{}", verifySignature);
    }

    /**
     * 验证参数...
     * @return Map<String, String>
     */
    protected abstract Map<String, String> verifySignature();

    /**
     * 执行修改订单状态和返回不用的结果..
     *
     * @param verifySignature verifySignature
     * @return String
     */
    protected abstract String asyncService(Map<String, String> verifySignature);

    /**
     * 返回失败结果
     *
     * @return String
     */
    protected abstract String resultFail();

    /**
     * 返回成功结果
     *
     * @return String
     */
    protected abstract String resultSuccess();

}
