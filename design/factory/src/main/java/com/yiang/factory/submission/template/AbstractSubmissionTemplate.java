package com.yiang.factory.submission.template;

import cn.hutool.core.bean.BeanUtil;
import com.yiang.factory.submission.manager.SendLogManager;
import com.yiang.factory.submission.dto.LogDTO;
import com.yiang.factory.submission.dto.SubmissionParamDTO;
import com.yiang.factory.submission.exception.SubmissionParamException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author HeZhuo
 * @date 2021/11/26
 */
@Slf4j
public abstract class AbstractSubmissionTemplate {

    /**
     * 定义共同行为的骨架 启动报送
     * @return Object
     */
    public final Object beginSubmission(SubmissionParamDTO paramEntity) {

        log.info("param check start...");
        // 方法内参数校验
        checkParam(paramEntity);
        log.info("param check end...");

        // 报送前的校验方法
        verify(paramEntity);

        // 报文组装方法
        depacketize(paramEntity);

        // 执行文件报送方法
        log.info("send file start...");
        sendFile(paramEntity);
        log.info("send file end...");

        log.info("send log start...");
        // 报送报送日志发送
        sendLog(paramEntity);
        log.info("send log end...");

        return true;
    }

    /**
     * 报送前的校验方法
     * @param paramEntity 参数实体类
     */
    protected abstract void verify(SubmissionParamDTO paramEntity);

    /**
     * 报文组装方法
     * @param paramEntity 参数实体类
     */
    protected abstract void depacketize(SubmissionParamDTO paramEntity);

    @Autowired
    private SendLogManager sendLogManager;

    /**
     * 使用多线程异步写入日志
     *
     * @param paramEntity paramEntity
     */
    private void sendLog(SubmissionParamDTO paramEntity) {

        // 1.默认复制所有参数值
        LogDTO logDTO = BeanUtil.copyProperties(paramEntity, LogDTO.class);

        // 2.实现类根据该方法进行独立的参数封装，而不是在模板类进行处理，模板类只处理公共参数
        loadLogParam(logDTO);

        // 3.通过异步发送日志，兼容单元测试|main入口测试（同步），故设置了两个入口
        if (null != sendLogManager) {
            sendLogManager.sendAsyncLog(logDTO);
        } else {
            SendLogManager.sendLog(logDTO);
        }

    }

    /**
     * 日志方法参数传递加载...
     * 子类实现该方法，进行独立的参数封装
     * @param logDTO 日志参数实体
     */
    protected abstract void loadLogParam(LogDTO logDTO);

    /**
     * 文件报送方法
     * @param paramEntity 返回值
     */
    protected abstract void sendFile(SubmissionParamDTO paramEntity);

    /**
     * 参数校验
     * @param paramEntity 参数实体类
     * <p>
     *      参数校验未通过时，统一抛出该异常
     *      @throws SubmissionParamException
     * </p>
     */
    protected abstract void checkParam(SubmissionParamDTO paramEntity);

}
