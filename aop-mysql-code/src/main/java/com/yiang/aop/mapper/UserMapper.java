package com.yiang.aop.mapper;

import org.apache.ibatis.annotations.Insert;
import org.springframework.stereotype.Repository;

/**
 * @author HeZhuo
 * @date 2021/8/11
 */
@Repository
public interface UserMapper {

    @Insert("insert exam_user values(#{id},'张三', '',1,19)")
    int insert(String id);
}
