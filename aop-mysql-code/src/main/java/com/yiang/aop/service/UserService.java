package com.yiang.aop.service;

import cn.hutool.core.convert.Convert;
import com.yiang.aop.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

/**
 * @author HeZhuo
 * @date 2021/8/11
 */
@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

    // @Transactional
    public int addUser(String id) {
        int insert = userMapper.insert(id);
        try {
            int i = 1 / Convert.toInt(id);
        } catch (Exception e) {
            // 如果手动去抓捕了异常，则需要自行回滚
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return 123321;
        }
        return insert;
    }
}
