package com.yiang.aop;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author HeZhuo
 * @date 2021/8/11
 */
@SpringBootApplication
@MapperScan("com.yiang.aop.mapper")
@EnableAspectJAutoProxy
@EnableTransactionManagement
public class AopApp {

    public static void main(String[] args) {
        SpringApplication.run(AopApp.class, args);
    }
}
