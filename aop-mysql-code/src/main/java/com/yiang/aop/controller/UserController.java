package com.yiang.aop.controller;

import com.yiang.aop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author HeZhuo
 * @date 2021/8/11
 */
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/addUser/{id}")
    public Object addUser(@PathVariable("id") String id) {

        return userService.addUser(id);
    }
}
