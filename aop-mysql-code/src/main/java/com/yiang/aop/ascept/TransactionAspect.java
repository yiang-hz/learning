package com.yiang.aop.ascept;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionStatus;

/**
 * @author HeZhuo
 * @date 2021/8/11
 */
@Aspect
@Component
public class TransactionAspect {

    @Autowired
    private TransactionalUtils transactionalUtils;

    @Pointcut("execution (* com.yiang.aop.service..*.*(..))")
    public void loginAop() {}

    @Around("loginAop()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        TransactionStatus begin = null;
        try {
            begin = transactionalUtils.begin();
            // 执行目标方案
            Object proceed = joinPoint.proceed();
            transactionalUtils.commit(begin);
            return proceed;
        } catch (Exception e) {
            if (begin != null) {
                transactionalUtils.rollback(begin);
            }
            return "操作失败，回滚";
        }
    }
}
