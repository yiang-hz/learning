package com.yiang.aop.ascept;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.interceptor.DefaultTransactionAttribute;

/**
 * @author HeZhuo
 * @date 2021/8/11
 */
@Component
public class TransactionalUtils {

    /**
     * 获取当前事务管理器
     */

        @Autowired
    private DataSourceTransactionManager dataSourceTransactionManager;

    public TransactionStatus begin() {
        return dataSourceTransactionManager.getTransaction(new DefaultTransactionAttribute());
    }


    public void rollback(TransactionStatus transaction) {
        dataSourceTransactionManager.rollback(transaction);
    }

    public void commit(TransactionStatus transaction) {
        dataSourceTransactionManager.commit(transaction);
    }
}
