package com.yiang.aop.chain;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * @author HeZhuo
 * @date 2021/8/13
 */
public class TestChain {

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        List<MethodInterface> interfaceList = new ArrayList<>();
        interfaceList.add(new BeforeMethodImpl());
        interfaceList.add(new AfterMethodImpl());
        interfaceList.add(new AroundMethodImpl());

        Method doWork = TargetService.class.getMethod("doWork", String.class);
        new DefaultMethodChainHandler(interfaceList, doWork, new Object[] {"yiang"},
                new TargetService()).invoke();
    }
}
