package com.yiang.aop.chain;

import java.lang.reflect.InvocationTargetException;

/**
 * @author HeZhuo
 * @date 2021/8/13
 */
public class AfterMethodImpl implements MethodInterface {

    @Override
    public void doMethod(MethodChainHandler methodChainHandler) throws InvocationTargetException, IllegalAccessException {
        methodChainHandler.invoke();
        System.out.println("后置通知执行...");
    }
}
