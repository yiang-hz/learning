package com.yiang.aop.chain;

import java.lang.reflect.InvocationTargetException;

/**
 * @author HeZhuo
 * @date 2021/8/13
 */
public class BeforeMethodImpl implements MethodInterface{

    @Override
    public void doMethod(MethodChainHandler methodChainHandler) throws InvocationTargetException, IllegalAccessException {
        System.out.println("前置通知执行...");
        methodChainHandler.invoke();
    }
}
