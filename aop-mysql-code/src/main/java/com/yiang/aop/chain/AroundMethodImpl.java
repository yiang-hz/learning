package com.yiang.aop.chain;

import java.lang.reflect.InvocationTargetException;

/**
 * @author HeZhuo
 * @date 2021/8/13
 */
public class AroundMethodImpl implements MethodInterface {

    @Override
    public void doMethod(MethodChainHandler methodChainHandler) throws InvocationTargetException, IllegalAccessException {
        System.out.println("环绕通知前置开始执行...");
        methodChainHandler.invoke();
        System.out.println("环绕通知后置开始执行...");
    }
}
