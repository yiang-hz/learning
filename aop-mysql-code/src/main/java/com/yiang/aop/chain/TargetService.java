package com.yiang.aop.chain;

/**
 * @author HeZhuo
 * @date 2021/8/13
 */
public class TargetService {

    public void doWork(String name) {
        System.out.println("目标方法执行..." + name);
    }
}
