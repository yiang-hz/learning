package com.yiang.aop.chain;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

/**
 * @author HeZhuo
 * @date 2021/8/13
 */
public class DefaultMethodChainHandler implements MethodChainHandler{

    private final List<MethodInterface> methodInterfaceList;
    private Method method;
    private Object[] args;
    private Object target;

    private int count;

    public DefaultMethodChainHandler(List<MethodInterface> methodInterfaceList,
                                     Method method, Object[] args, Object target) {
        this.methodInterfaceList = methodInterfaceList;
        this.method = method;
        this.args = args;
        this.target = target;
    }

    @Override
    public void invoke() throws InvocationTargetException, IllegalAccessException {
        if (count == methodInterfaceList.size()) {
            method.invoke(target, args);
            return;
        }
        MethodInterface methodInterface = methodInterfaceList.get(count++);
        methodInterface.doMethod(this);
    }
}
