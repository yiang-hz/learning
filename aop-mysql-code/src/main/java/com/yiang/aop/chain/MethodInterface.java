package com.yiang.aop.chain;

import java.lang.reflect.InvocationTargetException;

/**
 * @author HeZhuo
 * @date 2021/8/13
 */
public interface MethodInterface {

    void doMethod(MethodChainHandler methodChainHandler) throws InvocationTargetException, IllegalAccessException;
}
