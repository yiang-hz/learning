package com.yiang.pay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author HeZhuo
 * @date 2020/6/29
 */
@SpringBootApplication
public class JingWeiPayApplication {

    public static void main(String[] args) {
        SpringApplication.run(JingWeiPayApplication.class, args);
    }

}
