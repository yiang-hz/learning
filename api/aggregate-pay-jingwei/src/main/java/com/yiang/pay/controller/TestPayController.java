package com.yiang.pay.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author HeZhuo
 * @date 2020/6/29
 */
@RequestMapping("/test")
@RestController
public class TestPayController {

    @RequestMapping("/response")
    public void getResponse() {
        System.out.println("同步回调了...");
    }
}
