package com.yiang;

import lombok.SneakyThrows;
import org.smartboot.license.client.License;
import org.smartboot.license.client.LicenseEntity;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * @author HeZhuo
 * @date 2020/10/19
 */
@SpringBootApplication
public class SmartLicenseApp {

    public static void main(String[] args) {
        SpringApplication.run(SmartLicenseApp.class, args);
    }

    @Component
    @Order(-1)
    public static class LicenseTest implements ApplicationRunner {
        @SneakyThrows
        @Override
        public void run(ApplicationArguments args) {

            File file = new File("D:\\Hz\\learn\\smart-license\\bin\\license.txt");
            License license = new License();
            LicenseEntity licenseEntity = license.loadLicense(file);
            System.out.println(new String(licenseEntity.getData()));

            if ("HelloWorld".equals(new String(licenseEntity.getData()))) {
                System.out.println("证书过期");
                throw new Exception("证书验证失败！请联系提供商处理！");
            }

        }
    }
}
