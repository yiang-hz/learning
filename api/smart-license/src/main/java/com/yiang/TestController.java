package com.yiang;

import org.springframework.web.bind.annotation.RestController;

/**
 * @author HeZhuo
 * @date 2020/10/19
 */
@RestController
public class TestController {

    public String getA(){
        return "A";
    }
}
