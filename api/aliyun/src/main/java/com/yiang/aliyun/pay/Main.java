package com.yiang.aliyun.pay;

import com.alipay.easysdk.factory.Factory;
import com.alipay.easysdk.factory.Factory.Payment;
import com.alipay.easysdk.kernel.Config;
import com.alipay.easysdk.kernel.util.ResponseChecker;
import com.alipay.easysdk.payment.wap.models.AlipayTradeWapPayResponse;

public class Main {

    /**
     * Easy SDK Maven Home：https://mvnrepository.com/artifact/com.alipay.sdk/alipay-easysdk
     * 手机网站支付快速接入接口调用：https://opendocs.alipay.com/open/203/105285
     * 手机网站支付Demo （页面）：https://opendocs.alipay.com/open/54/106682
     * 相关于关闭订单与查询 统一通用
     * 密钥等：https://opendocs.alipay.com/open/200/105311
     * 手机支付Api：https://opendocs.alipay.com/apis/api_1/alipay.trade.wap.pay#%E5%85%AC%E5%85%B1%E5%8F%82%E6%95%B0
     * 在线调试：https://openhome.alipay.com/platform/demoManage.htm#/alipay.fund.trans.uni.transfer
     */

    public static void main(String[] args) {
        // 1. 设置参数（全局只需设置一次）
        Factory.setOptions(getOptions());
        try {
            // 2. 发起API调用（以创建当面付收款二维码为例）
            AlipayTradeWapPayResponse response = Payment.Wap()
                    .pay("Apple iPhone11 128G", "2234567890", "5799.00", "", "");

//            Factory.Payment.FaceToFace()
//                    // 调用asyncNotify扩展方法，可以为每次API调用，设置独立的异步通知地址
//                    // 此处设置的异步通知地址的优先级高于全局Config中配置的异步通知地址
//                    .asyncNotify("https://www.test.com/callback")
//                    .preCreate("Apple iPhone11 128G", "2234567890", "5799.00");

            // 3. 处理响应或异常
            if (ResponseChecker.success(response)) {
                System.out.println("调用成功" + response.body);
            } else {
//                System.err.println("调用失败，原因：" + response.msg + "，" + response.subMsg);
                System.err.println("调用失败，原因：" + response.body);
            }
        } catch (Exception e) {
            System.err.println("调用遭遇异常，原因：" + e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private static Config getOptions() {
        Config config = new Config();
        config.protocol = "https";
//        config.gatewayHost = "openapi.alipay.com";
        config.gatewayHost = "openapi.alipaydev.com/gateway.do";
        config.signType = "RSA2";

        config.appId = "2016093000633379";

        // 为避免私钥随源码泄露，推荐从文件中读取私钥字符串而不是写入源码中
        //config.merchantPrivateKey = "<-- 请填写您的应用私钥，例如：MIIEvQIBADANB ... ... -->";
        config.merchantPrivateKey = "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQC4GJRjgf8UcM6bVZsUqs0dZSxyU0rag5Ct0Zx7Yq0wMh9siDP7iFMng9brtE4lhUAEzXkqIpfbd9ygncNRNRiJ9hSBmTFcTBgmWTRVTbiGCA/QsmcYCirSR8ZLAhXQTlDSxS0Ji24/dASNVIOh1c1VxMfpiT2Hub0wwvDO/Ha3U/t4KQ7ZSzHdVr+2h5BlDqN1lBk1Rd/cMfIdL2o9mQeagKOXclvs/1CiI0tZVsLb5QuPhi7J5gM1qZC/7zMBQNr8SRu0G97/cNEMrsv0yttdmjyyEpemobVoE/oFNlFKRf45ossiEUFv4v6ie7PdriHag90s19lRfSiEpZW51XI5AgMBAAECggEAbZj0fmkd2qVt2rwUHzGv+xgIZeeAYKaCs3hemHYedNriKlLL36tBFf0/LgOhomS5wCIzSApgLaAmENjbNrVNbpS5kI4K/M+wcOQaa85hVCnogG41QfKLm7RvMKiFJAX7x/7q2QvRDqWbMChoVShbnfwQp8+0eeDx6N2lTxSt+GVbrRoXZ03QZSv4A4eD5Ublr4VNI+uMI3zl+gISFr1MI2ZNjtdgMFM69dkSHetH/MifmhkpabPsDYlRRmwWYXLhzLfu/FQ7IqGC41+g+4cvcUYfy4OSLhdp7KWRCwUQgvTPrJvMLY/7+CMMKioYm3uUr9wtHiTupq3OKSmoph4vIQKBgQDrqGcUVDfvViMBi5rkISci06sPkZ/7UDiAX1wkpraBwtBOWhhKbNFHoPPzqZ2MGInTn6L/rLKVZVmhry77ongh5k2HTP4BogXR5K+keD8q+tqpofwIGtAyBf8CVgBD4kHM1WvWPV1a1MLc2LWvoMk0w6lxC8QbdKdDYGVCOwMB/wKBgQDH/MHlg5+tENuuUY+pgRdABqyJgAB+CaG4xPa6o7/OpLQolpttJwb1ySZ4wzb/LKZbdK91A39mCRKUT0kHkR1sMFJtsFImSGQVibgNbhwFQtl/hnyDkTbtIF28UBv+V1d6tmSr3ntAriJBgt6DBSxrC7gqV6eUAN4oLgQN5LcbxwKBgQCZJDXgax5yZVqYXX3JtJW3NzCaDHuXmLkvC0lY9BH0X4J4+GmCrBaQYtNFzDr62NNwd/Q4DKV9GoDV5n80vY8uEHjfbTRAIWeA0Akoa5A5QQ2qYP7k0W/soiLnBLZlZvp1v1e1wOkS5uK/HAFt4aH/rQtG0Gufn/42dcU39httRwKBgQCsCRHfV1gv79Wa4ky8WvFeQZ+A9Rz3T1/Qa5d3vM+Oev8D/3Ma9by2CODQd9ZCsu00tW8OMQESDHHwJThEiyANZpAyDcDNb+6w3HT5EKSIHhat7koEgBII8JPSH/PE2uvFA0wc0VmjrWiIhITXnsa0GzPFMPxWhFyXoeYY8YxXFwKBgQCiOEav7OJHsKnwDFtrTHPaSUJg4VzQ9Z+4g6LlS7pa+ybikjX6qbUBMBlnEKVtbmdpkzeRecfm4DDe4fBCNSUPQ2vC1hBhY9fbYdxY2Uy4HqHilGZER9/gnzVaJKB8LEXhqoEEPOXi7tt2DHa3ue7jX+JeGvVY0xQRowlcqpPOGQ==";

        //注：证书文件路径支持设置为文件系统中的路径或CLASS_PATH中的路径，优先从文件系统中加载，加载失败后会继续尝试从CLASS_PATH中加载
//        config.merchantCertPath = "<-- 请填写您的应用公钥证书文件路径，例如：/foo/appCertPublicKey_2019051064521003.crt -->";
//        config.alipayCertPath = "<-- 请填写您的支付宝公钥证书文件路径，例如：/foo/alipayCertPublicKey_RSA2.crt -->";
//        config.alipayRootCertPath = "<-- 请填写您的支付宝根证书文件路径，例如：/foo/alipayRootCert.crt -->";

        //注：如果采用非证书模式，则无需赋值上面的三个证书路径，改为赋值如下的支付宝公钥字符串即可
//        config.alipayPublicKey = "<-- 请填写您的支付宝公钥，例如：MIIBIjANBg... -->";
        config.alipayPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAljj0ia6ta8xvR39q/+53MQrNTxFayb285NmxJllCtt2bQhJuysxyjd/qddwQ0pD0sipgCZyN/Ea2spB0DaMyjvG6z05moB1UBKIZn83BXuHzd3cshU9zWoxW4Fmfpz/mmFaLxR4atkrsd85LQr6JV3/EZ8J5Vz1Wxl0pqyq+5PCUBQWwQyUY/Bp08vFMwbSNc6k2lnGdiEhVCQ0FtfNXpc0kp190Lm0Uh+MY5GzX7DkK76z3HxQH1Gox763UbC76aJxILqPVlwTsCtipAaUXfYE7ttZR4lcO8uD/pWUGecaiz9pSMRq+8MOEyjS2V8AeiBDxuwEarx769UAZeCMULwIDAQAB";

        //可设置异步通知接收服务地址（可选）
//        config.notifyUrl = "<-- 请填写您的支付类接口异步通知接收服务地址，例如：https://www.test.com/callback -->";

        //可设置AES密钥，调用AES加解密相关接口时需要（可选）
//        config.encryptKey = "<-- 请填写您的AES密钥，例如：aa4BtZ4tspm2wnXLb1ThQA== -->";

        return config;
    }
}