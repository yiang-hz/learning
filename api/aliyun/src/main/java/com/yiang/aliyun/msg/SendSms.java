package com.yiang.aliyun.msg;

import cn.hutool.captcha.generator.RandomGenerator;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.yiang.aliyun.msg.entity.Param;
import com.yiang.aliyun.msg.entity.ZYHL;

/**
 * @author Administrator
 */
public class SendSms {
    /**
     * 依赖Jar
     * <p>
     * <dependency>
     *   <groupId>com.aliyun</groupId>
     *   <artifactId>aliyun-java-sdk-core</artifactId>
     *   <version>4.5.0</version>
     * </dependency>
     *  </p>
     */
    public static void main(String[] args) {
        Param param = new ZYHL();
        DefaultProfile profile = DefaultProfile.getProfile(param.getRegionId(),
                param.getAccessKeyId(), param.getSecret());
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain(param.getDomain());
        request.setSysVersion(param.getVersion());
        request.setSysAction("SendSms");
        request.putQueryParameter("RegionId", param.getRegionId());
        //sara
//        request.putQueryParameter("PhoneNumbers", "18655448810");
        //HZ
        request.putQueryParameter("PhoneNumbers", "13387373751");
        //RTY
//        request.putQueryParameter("PhoneNumbers", "17389599646");
        //JJJ
//        request.putQueryParameter("PhoneNumbers", "15073371281");
        //ZR
//        request.putQueryParameter("PhoneNumbers", "18173268750");
        //HJ
//        request.putQueryParameter("PhoneNumbers", "18229866765");

        request.putQueryParameter("SignName", param.getSignName());
        request.putQueryParameter("TemplateCode", "SMS_193821073");
        RandomGenerator randomGenerator = new RandomGenerator("0123456789", 4);
        String generate = randomGenerator.generate();
        System.out.println(generate);

        request.putQueryParameter("TemplateParam", "{\"code\":\"" + generate + "\"}");
        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }
}