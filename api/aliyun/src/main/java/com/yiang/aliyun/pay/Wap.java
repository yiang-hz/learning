package com.yiang.aliyun.pay;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.alipay.api.response.AlipayTradeWapPayResponse;

/**
 * @author HeZhuo
 * @date 2020/7/8
 */
public class Wap {

    public static void main(String[] args) throws AlipayApiException {
        AlipayClient alipayClient = new DefaultAlipayClient("https://openapi.alipaydev.com/gateway.do",
                "2016093000633379","MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQC4GJRjgf8UcM6bVZsUqs0dZSxyU0rag5Ct0Zx7Yq0wMh9siDP7iFMng9brtE4lhUAEzXkqIpfbd9ygncNRNRiJ9hSBmTFcTBgmWTRVTbiGCA/QsmcYCirSR8ZLAhXQTlDSxS0Ji24/dASNVIOh1c1VxMfpiT2Hub0wwvDO/Ha3U/t4KQ7ZSzHdVr+2h5BlDqN1lBk1Rd/cMfIdL2o9mQeagKOXclvs/1CiI0tZVsLb5QuPhi7J5gM1qZC/7zMBQNr8SRu0G97/cNEMrsv0yttdmjyyEpemobVoE/oFNlFKRf45ossiEUFv4v6ie7PdriHag90s19lRfSiEpZW51XI5AgMBAAECggEAbZj0fmkd2qVt2rwUHzGv+xgIZeeAYKaCs3hemHYedNriKlLL36tBFf0/LgOhomS5wCIzSApgLaAmENjbNrVNbpS5kI4K/M+wcOQaa85hVCnogG41QfKLm7RvMKiFJAX7x/7q2QvRDqWbMChoVShbnfwQp8+0eeDx6N2lTxSt+GVbrRoXZ03QZSv4A4eD5Ublr4VNI+uMI3zl+gISFr1MI2ZNjtdgMFM69dkSHetH/MifmhkpabPsDYlRRmwWYXLhzLfu/FQ7IqGC41+g+4cvcUYfy4OSLhdp7KWRCwUQgvTPrJvMLY/7+CMMKioYm3uUr9wtHiTupq3OKSmoph4vIQKBgQDrqGcUVDfvViMBi5rkISci06sPkZ/7UDiAX1wkpraBwtBOWhhKbNFHoPPzqZ2MGInTn6L/rLKVZVmhry77ongh5k2HTP4BogXR5K+keD8q+tqpofwIGtAyBf8CVgBD4kHM1WvWPV1a1MLc2LWvoMk0w6lxC8QbdKdDYGVCOwMB/wKBgQDH/MHlg5+tENuuUY+pgRdABqyJgAB+CaG4xPa6o7/OpLQolpttJwb1ySZ4wzb/LKZbdK91A39mCRKUT0kHkR1sMFJtsFImSGQVibgNbhwFQtl/hnyDkTbtIF28UBv+V1d6tmSr3ntAriJBgt6DBSxrC7gqV6eUAN4oLgQN5LcbxwKBgQCZJDXgax5yZVqYXX3JtJW3NzCaDHuXmLkvC0lY9BH0X4J4+GmCrBaQYtNFzDr62NNwd/Q4DKV9GoDV5n80vY8uEHjfbTRAIWeA0Akoa5A5QQ2qYP7k0W/soiLnBLZlZvp1v1e1wOkS5uK/HAFt4aH/rQtG0Gufn/42dcU39httRwKBgQCsCRHfV1gv79Wa4ky8WvFeQZ+A9Rz3T1/Qa5d3vM+Oev8D/3Ma9by2CODQd9ZCsu00tW8OMQESDHHwJThEiyANZpAyDcDNb+6w3HT5EKSIHhat7koEgBII8JPSH/PE2uvFA0wc0VmjrWiIhITXnsa0GzPFMPxWhFyXoeYY8YxXFwKBgQCiOEav7OJHsKnwDFtrTHPaSUJg4VzQ9Z+4g6LlS7pa+ybikjX6qbUBMBlnEKVtbmdpkzeRecfm4DDe4fBCNSUPQ2vC1hBhY9fbYdxY2Uy4HqHilGZER9/gnzVaJKB8LEXhqoEEPOXi7tt2DHa3ue7jX+JeGvVY0xQRowlcqpPOGQ==",
                "json","GBK","MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAljj0ia6ta8xvR39q/+53MQrNTxFayb285NmxJllCtt2bQhJuysxyjd/qddwQ0pD0sipgCZyN/Ea2spB0DaMyjvG6z05moB1UBKIZn83BXuHzd3cshU9zWoxW4Fmfpz/mmFaLxR4atkrsd85LQr6JV3/EZ8J5Vz1Wxl0pqyq+5PCUBQWwQyUY/Bp08vFMwbSNc6k2lnGdiEhVCQ0FtfNXpc0kp190Lm0Uh+MY5GzX7DkK76z3HxQH1Gox763UbC76aJxILqPVlwTsCtipAaUXfYE7ttZR4lcO8uD/pWUGecaiz9pSMRq+8MOEyjS2V8AeiBDxuwEarx769UAZeCMULwIDAQAB",
                "RSA2");
        AlipayTradeWapPayRequest request = new AlipayTradeWapPayRequest();
        request.setBizContent("{" +
                "\"body\":\"Iphone6 16G\"," +
                "\"subject\":\"大乐透\"," +
                "\"out_trade_no\":\"70501111111S001111119\"," +
                "\"timeout_express\":\"90m\"," +
                "\"time_expire\":\"2016-12-31 10:05\"," +
                "\"total_amount\":9.00," +
                "\"auth_token\":\"appopenBb64d181d0146481ab6a762c00714cC27\"," +
                "\"goods_type\":\"0\"," +
                "\"quit_url\":\"http://www.taobao.com/product/113714.html\"," +
                "\"passback_params\":\"merchantBizType%3d3C%26merchantBizNo%3d2016010101111\"," +
                "\"product_code\":\"QUICK_WAP_WAY\"," +
                "\"promo_params\":\"{\\\"storeIdType\\\":\\\"1\\\"}\"," +
                "\"extend_params\":{" +
                "\"sys_service_provider_id\":\"2088511833207846\"," +
                "\"hb_fq_num\":\"3\"," +
                "\"hb_fq_seller_percent\":\"100\"," +
                "\"industry_reflux_info\":\"{\\\\\\\"scene_code\\\\\\\":\\\\\\\"metro_tradeorder\\\\\\\",\\\\\\\"channel\\\\\\\":\\\\\\\"xxxx\\\\\\\",\\\\\\\"scene_data\\\\\\\":{\\\\\\\"asset_name\\\\\\\":\\\\\\\"ALIPAY\\\\\\\"}}\"," +
                "\"card_type\":\"S0JP0000\"" +
                "    }," +
                "\"merchant_order_no\":\"20161008001\"," +
                "\"enable_pay_channels\":\"pcredit,moneyFund,debitCardExpress\"," +
                "\"disable_pay_channels\":\"pcredit,moneyFund,debitCardExpress\"," +
                "\"store_id\":\"NJ_001\"," +
                "      \"goods_detail\":[{" +
                "        \"goods_id\":\"apple-01\"," +
                "\"alipay_goods_id\":\"20010001\"," +
                "\"goods_name\":\"ipad\"," +
                "\"quantity\":1," +
                "\"price\":2000," +
                "\"goods_category\":\"34543238\"," +
                "\"categories_tree\":\"124868003|126232002|126252004\"," +
                "\"body\":\"特价手机\"," +
                "\"show_url\":\"http://www.alipay.com/xxx.jpg\"" +
                "        }]," +
                "\"specified_channel\":\"pcredit\"," +
                "\"business_params\":\"{\\\"data\\\":\\\"123\\\"}\"," +
                "\"ext_user_info\":{" +
                "\"name\":\"李明\"," +
                "\"mobile\":\"16587658765\"," +
                "\"cert_type\":\"IDENTITY_CARD\"," +
                "\"cert_no\":\"362334768769238881\"," +
                "\"min_age\":\"18\"," +
                "\"fix_buyer\":\"F\"," +
                "\"need_check_info\":\"F\"" +
                "    }" +
                "  }");
        AlipayTradeWapPayResponse response = alipayClient.pageExecute(request);
        if(response.isSuccess()){
            System.out.println("调用成功");
        } else {
            System.out.println("调用失败");
        }
    }
}
