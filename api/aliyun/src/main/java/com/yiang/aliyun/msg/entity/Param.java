package com.yiang.aliyun.msg.entity;

import lombok.Data;

/**
 * @author HeZhuo
 * @date 2020/7/1
 */
@Data
public class Param {

    private String regionId;
    private String accessKeyId;
    private String secret;
    private String domain;
    private String version;
    private String signName;
}
