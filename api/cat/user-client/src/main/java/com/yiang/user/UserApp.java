package com.yiang.user;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author HeZhuo
 * @date 2021/10/15
 */
@SpringBootApplication
@MapperScan("com.yiang.user.mapper")
public class UserApp {

    public static void main(String[] args) {
        SpringApplication.run(UserApp.class);
    }
}
