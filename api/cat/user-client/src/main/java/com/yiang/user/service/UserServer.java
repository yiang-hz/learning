package com.yiang.user.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yiang.user.entity.User;
import com.yiang.user.mapper.UserMapper;
import org.springframework.stereotype.Service;

/**
 * @author HeZhuo
 * @date 2021/10/15
 */
@Service
public class UserServer extends ServiceImpl<UserMapper, User> {
}
