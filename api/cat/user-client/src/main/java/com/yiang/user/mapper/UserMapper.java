package com.yiang.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yiang.user.entity.User;
import org.springframework.stereotype.Repository;

/**
 * @author HeZhuo
 * @date 2021/10/15
 */
@Repository
public interface UserMapper extends BaseMapper<User> {
}
