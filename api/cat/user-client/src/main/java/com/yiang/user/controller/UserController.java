package com.yiang.user.controller;

import com.yiang.user.entity.User;
import com.yiang.user.service.UserServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author HeZhuo
 * @date 2021/10/15
 */
@RestController
public class UserController {

    @Autowired
    private UserServer userServer;

    @RequestMapping("/get")
    public Object get() {
        return "Hello World";
    }

    @RequestMapping("/add")
    @Transactional(rollbackFor = Exception.class)
    public Object add(@RequestParam Integer id) {
        User user = new User();
        user.setName("何卓");
        userServer.save(user);
        int i = 1 / id;
        return userServer.save(user);
    }
}
