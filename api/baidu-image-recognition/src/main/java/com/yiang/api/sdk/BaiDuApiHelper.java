package com.yiang.api.sdk;

import cn.hutool.core.lang.Assert;
import com.baidu.aip.ocr.AipOcr;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

/**
 * @author HeZhuo
 * @date 2020/7/3
 */
@Service
public class BaiDuApiHelper {

	/**
	 * 百度文字识别方法
	 * @param url 图片路径，OSS匹配无问题。若是存在&mp之类特殊符号，Java的 URL类有可能存在问题
	 * @param type 1为图片识别。 2为
	 * @return 识别结果
	 * @throws IOException 可能抛出IOE异常 URL识别。
	 */
	public org.json.JSONObject characterAnalysis(String url, Integer type) throws IOException {
		Assert.notNull(type, "类型[type]不能为空！");
		String formatName = url.substring(url.lastIndexOf(".") + 1);
		HashMap<String, String> options = new HashMap<>(1);
		AipOcr client = BaiDuApiHelper.getAipOcrInstance();
		org.json.JSONObject res;
		byte[] bytes = encodeImageToBase64(url, formatName);

		switch (type){
			case 1:
				res = client.businessLicense(bytes, options);
				break;
			case 2:
				res = client.bankcard(bytes, options);
				break;
			default:
				res = new JSONObject();
				break;
		}
		return res;
	}

	/**
     * // 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
	 * @param imageUrl 图片路径
	 * @param formatName 根据路径获取后缀的图片格式
	 * @return byte数组
	 * @throws IOException 抛出IOE异常捕捉
	 */
	private static byte[] encodeImageToBase64(String imageUrl, String formatName) throws IOException {

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		BufferedImage bufferedImage = ImageIO.read(new URL(imageUrl));

		ImageIO.write(bufferedImage, formatName, outputStream);
		byte[] bytes = outputStream.toByteArray();

		outputStream.close();
		bufferedImage.flush();
		return bytes;
	}

	/**
	 * 获取AipOcr静态实例
	 */
	private static AipOcr getAipOcrInstance() {
		return AipOcrBuilder.CLIENT;
	}

	private static class AipOcrBuilder {
		static final AipOcr CLIENT = new AipOcr(
			BaiDuApiConstant.OCR_APP_ID, BaiDuApiConstant.OCR_API_KEY, BaiDuApiConstant.OCR_SECRET_KEY);
	}
}

