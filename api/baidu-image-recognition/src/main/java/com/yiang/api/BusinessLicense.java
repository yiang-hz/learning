package com.yiang.api;


import com.alibaba.fastjson.JSONObject;
import com.yiang.api.util.Base64Util;
import com.yiang.api.util.FileUtil;
import com.yiang.api.util.HttpUtil;
import org.springframework.util.ObjectUtils;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * @author Administrator
 * 营业执照识别
 */
public class BusinessLicense {


    private static String encodeImageToBase64(URL imageUrl) throws IOException {
        // 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
        ByteArrayOutputStream outputStream;
        BufferedImage bufferedImage = ImageIO.read(imageUrl);
        outputStream = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, "jpg", outputStream);
        // 对字节数组Base64编码
        BASE64Encoder encoder = new BASE64Encoder();
        // 返回Base64编码过的字节数组字符串
        return encoder.encode(outputStream.toByteArray());
    }

    private static String businessLicense2(String imgStr) throws Exception {
        // 请求url
        String url = "https://aip.baidubce.com/rest/2.0/ocr/v1/business_license";
        // 文件路径
        String imgParam = URLEncoder.encode(imgStr, "UTF-8");

        String param = "image=" + imgParam;

        // 注意这里仅为了简化编码每一次请求都去获取access_token，
        // 线上环境access_token有过期时间， 客户端可自行缓存，过期后重新获取。
        String accessToken = "24.fda53b978e3bac61268f33543b237f2a.2592000.1595923252.282335-20533263";
        return HttpUtil.post(url, accessToken, param);
    }

    /**
    * 重要提示代码中所需工具类
    * FileUtil,Base64Util,HttpUtil,GsonUtils请从
    * https://ai.baidu.com/file/658A35ABAB2D404FBF903F64D47C1F72
    * https://ai.baidu.com/file/C8D81F3301E24D2892968F09AE1AD6E2
    * https://ai.baidu.com/file/544D677F5D4E4F17B4122FBD60DB82B3
    * https://ai.baidu.com/file/470B3ACCA3FE43788B5A963BF0B625F3
    * 下载
    */
    public static String businessLicense() {
        // 请求url
        String url = "https://aip.baidubce.com/rest/2.0/ocr/v1/business_license";
        try {
            // 本地文件路径
            String filePath = "D:\\initpath\\image\\zhizhao.jpg";
            byte[] imgData = FileUtil.readFileByBytes(filePath);
            String imgStr = Base64Util.encode(imgData);
            String imgParam = URLEncoder.encode(imgStr, "UTF-8");

            String param = "image=" + imgParam;

            // 注意这里仅为了简化编码每一次请求都去获取access_token，
            // 线上环境access_token有过期时间， 客户端可自行缓存，过期后重新获取。
            String accessToken = "24.f3f5d9fd7479cafc7351bc6f724e5683.2592000.1596078313.282335-20533263";

            String result = HttpUtil.post(url, accessToken, param);
            System.out.println(result);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) throws Exception {
        //BusinessLicense.businessLicense();

        URL url = new URL("https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=3986993312,1775561828&fm=26&gp=0.jpg");
        String s = BusinessLicense.businessLicense2(BusinessLicense.encodeImageToBase64(url));
        JSONObject jsonObject = JSONObject.parseObject(s);
        Object log_id = jsonObject.get("log_id");
        System.out.println(log_id);
        System.out.println(s);
    }

    /**
     * 将Base64位编码的图片进行解码，并保存到指定目录
     *
     * @param base64
     *            base64编码的图片信息
     * @return
     */
    public static void decodeBase64ToImage(String base64, String path,
                                           String imgName) {
        BASE64Decoder decoder = new BASE64Decoder();
        try {
            FileOutputStream write = new FileOutputStream(new File(path
                    + imgName));
            byte[] decoderBytes = decoder.decodeBuffer(base64);
            write.write(decoderBytes);
            write.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}