package com.yiang.api.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Auto-generated: 2020-06-29 17:14:43
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaiDuToken {

    private String refreshToken;
    private long expiresIn;
    private String sessionKey;
    private String accessToken;
    private String scope;
    private String sessionSecret;
}