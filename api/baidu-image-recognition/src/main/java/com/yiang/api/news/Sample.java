package com.yiang.api.news;

import cn.hutool.core.io.file.FileReader;
import com.baidu.aip.ocr.AipOcr;
import org.json.JSONObject;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;

public class Sample {

    //设置APPID/AK/SK
    public static final String APP_ID = "20629516";
    public static final String API_KEY = "dqOwXaMdk5VvrRXpO1FaXTvz";
    public static final String SECRET_KEY = "uVlR164IFOX9hW6l5DRlU1PEDDj9X7L3";

    public static void main(String[] args) {
        // 初始化一个AipOcr
        AipOcr client = new AipOcr(APP_ID, API_KEY, SECRET_KEY);

        // 可选：设置网络连接参数
//        client.setConnectionTimeoutInMillis(2000);
//        client.setSocketTimeoutInMillis(60000);

        // 可选：设置代理服务器地址, http和socket二选一，或者均不设置
//        client.setHttpProxy("proxy_host", proxy_port);  // 设置http代理
//        client.setSocketProxy("proxy_host", proxy_port);  // 设置socket代理

        // 可选：设置log4j日志输出格式，若不设置，则使用默认配置
        // 也可以直接通过jvm启动参数设置此环境变量
//        System.setProperty("aip.log4j.conf", "path/to/your/log4j.properties");

        // 传入可选参数调用接口
        HashMap<String, String> options = new HashMap<>();

        // 参数为本地图片路径
        String image = "D:\\initpath\\image\\yhcard.jpg";
//        JSONObject res = client.bankcard(image, options);
//        System.out.println(res.toString(2));

        // 参数为本地图片二进制数组
        FileReader fileReader = FileReader.create(new File(image));
        byte[] bytes = fileReader.readBytes();
        System.out.println("Bytes:" + Arrays.toString(bytes));
        JSONObject res = client.bankcard(bytes, options);
        System.out.println(res.toString(2));
        
    }
}