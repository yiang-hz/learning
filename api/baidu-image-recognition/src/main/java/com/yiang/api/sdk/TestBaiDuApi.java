package com.yiang.api.sdk;

import org.json.JSONObject;

import java.io.IOException;

/**
 * @author HeZhuo
 * @date 2020/7/3
 */
public class TestBaiDuApi {

    public static void main(String[] args) {
        BaiDuApiHelper baiDuApiHelper = new BaiDuApiHelper();
        try {
            JSONObject jsonObject = baiDuApiHelper.characterAnalysis("file:///D://initpath/image/zhizhao.jpg", 1);
//            JSONObject jsonObject = baiDuApiHelper.characterAnalysis("file:///D://initpath/image/yhcard.jpg", 2);
            System.out.println("data：" + jsonObject);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
