package com.yiang.api;

import cn.hutool.core.io.file.FileReader;
import cn.hutool.http.HtmlUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import com.baidu.aip.ocr.AipOcr;
import com.yiang.response.AppResponseData;
import com.yiang.response.ResultCode;
import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sun.misc.BASE64Encoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;

/**
 * @author HeZhuo
 * @date 2020/7/2
 */
@RestController
@RequestMapping("/baidu")
@SpringBootApplication
public class BaiduApp {

    public static final String APP_ID = "20629516";
    public static final String API_KEY = "dqOwXaMdk5VvrRXpO1FaXTvz";
    public static final String SECRET_KEY = "uVlR164IFOX9hW6l5DRlU1PEDDj9X7L3";

    @RequestMapping("/bankCard")
    public AppResponseData bankCard(@RequestBody byte[] bytes) {
        HashMap<String, String> options = new HashMap<>(10);
        AipOcr client = new AipOcr(APP_ID, API_KEY, SECRET_KEY);
        JSONObject res = client.bankcard(bytes, options);
        return new AppResponseData(ResultCode.SUCCESS, res);
    }

    @RequestMapping("/bankCardUrl")
    public AppResponseData bankCardUrl(String url) {
        HttpResponse execute = HttpRequest.post(HtmlUtil.escape(url)).execute();
        HashMap<String, String> options = new HashMap<>(10);
        AipOcr client = new AipOcr(APP_ID, API_KEY, SECRET_KEY);
        JSONObject res = client.bankcard(execute.bodyBytes(), options);
        return new AppResponseData(ResultCode.SUCCESS, res);
    }

//    public static void main(String[] args) {
//        SpringApplication.run(BaiduApp.class, args);
//    }

    public static void main(String[] args) throws IOException {
        String url = "http://bipinyun.oss-cn-shenzhen.aliyuncs.com/yyzz.jpg";
        String formatName = url.substring(url.lastIndexOf(".") + 1);
        HashMap<String, String> options = new HashMap<>(10);
        AipOcr client = new AipOcr(APP_ID, API_KEY, SECRET_KEY);
        JSONObject res = client.businessLicense(encodeImageToBase64(new URL(url), formatName), options);
        System.out.println(res);
    }

    private static byte[] encodeImageToBase64(URL imageUrl, String formatName) throws IOException {// 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
        ByteArrayOutputStream outputStream;
        BufferedImage bufferedImage = ImageIO.read(imageUrl);
        outputStream = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, formatName, outputStream);
        return outputStream.toByteArray();
    }


}
