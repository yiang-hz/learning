package weathermachine;

import com.alibaba.fastjson.JSONObject;
import weathermachine.observable.MachineConfig;

/**
 * @author 以安
 * @date 2020-10-28
 */
public class WeatherData {

    public void measurementsChanged(int temp, int humidity, int windPower) {

        //1.使用JSON包装参数传递给观察者
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(MachineConfig.TEMP, temp);
        jsonObject.put(MachineConfig.HUMIDITY, humidity);
        jsonObject.put(MachineConfig.WIND_POWER, windPower);

        //2.通过静态常量实例调用父类方法通知观察者
        MachineConfig.observable.notifyObservers(jsonObject);

    }

}