package weathermachine.observable;

import weathermachine.ReapingMachine;
import weathermachine.SeedingMachine;
import weathermachine.WateringMachine;

import java.util.Observable;

/**
 * @author HeZhuo
 * @date 2020/10/28
 */
public class MachineConfig {

    public static Observable observable = new MachineObservable();
    public static final String TEMP = "temp";
    public static final String HUMIDITY = "humidity";
    public static final String WIND_POWER = "windPower";

    static {
        observable.addObserver(new SeedingMachine());
        observable.addObserver(new ReapingMachine());
        observable.addObserver(new WateringMachine());
    }

}
