package com.yiang.poi;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author HeZhuo
 * @date 2022/10/27
 */
public class TestExcel {

    public static void main(String[] args) throws IOException {
        test2();
//        test3();
    }
    @Data
    @AllArgsConstructor
    static class A {
        private String name;
    }

    @Data
    static class B {
        private Integer name;
    }

    public static void test3() {
        B b = BeanUtil.copyProperties(new A(" 1 "), B.class);
        System.out.println(b);
    }

    public static void test2() throws IOException {
        List<StudentEntity> studentList = new ArrayList<>();
        StudentEntity studentEntity = new StudentEntity("1", "何卓", 1, DateUtil.date(), null);
        StudentEntity studentEntity2 = new StudentEntity("2", "张万锋", 1, DateUtil.date(), null);
        StudentEntity studentEntity3 = new StudentEntity("3", "李春泉", 1, DateUtil.date(), null);
        studentList.add(studentEntity);
        studentList.add(studentEntity2);
        studentList.add(studentEntity3);

        List<CourseEntity> courseEntityList = new ArrayList<>();
        courseEntityList.add(new CourseEntity("1", "语文", new TeacherEntity("1", "老王"), studentList));
        Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("合肥特战队", "测试"),
                CourseEntity.class, courseEntityList);
        workbook.write(new FileOutputStream(new File("D://a.xlsx")));
        workbook.close();
    }

}
