package com.yiang.period;

import cn.hutool.http.HtmlUtil;
import cn.hutool.http.HttpUtil;
import lombok.SneakyThrows;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author HeZhuo
 * @date 2022/8/22
 */
public class GetMath {

    public static void main(String[] args) throws IOException {
        // 最简单的HTTP请求，可以自动通过header等信息判断编码，不区分HTTP和HTTPS
        String result= HttpUtil.get("https://www.dabanke.com/");
        Document doc = Jsoup.parse(result);
        Element body = doc.body();
        Element table = body.select("table").first();
       //System.out.println(table);


        // 转义HTML 字符
        String escape = HtmlUtil.escape(result);

        System.out.println(run());
    }

    public static List<Map<String, String>> run() throws IOException {

        // 网页地址
        String url = "https://www.dabanke.com/";
        List<Map<String, String>> list =new ArrayList<>();

        Document doc = Jsoup.connect(url).get();
        // 获取第一个表格
        Element element = doc.select("table").first();

        // 表头
        Elements thead = element.select("thead");

        // 表身
        Elements tbody = element.select("tbody");

        // 表身的tr 行  每一行的前两列是进度和几率
        Elements tr = tbody.select("tr");


        Map<String, String> map;
        // 遍历每一行
        for (Element el : tr) {

            // 拿到列元素
            Elements td = el.select("td");

            map = new HashMap<>();

            for (int i = 0, tdSize = td.size(); i < tdSize; i++) {

                Element elem = td.get(i);

                // 前置标题部分
                if (i == 0) {
                    map.put("id", elem.text());
                } else if (i == 1) {
                    map.put("name", elem.text());
                } else {
                    // 列身数据部分 外层包裹的div
                    Element div = elem.select("div").first();
                    Elements data = div.children();
                    StringBuilder resultT = new StringBuilder();
                    for (Element datum : data) {
                        resultT.append(datum.text());
                        resultT.append(",");
                    }
                    map.put("remark", resultT.substring(0, resultT.length() - 1));
                }

            }
            list.add(map);
        }
        return list;

    }

}
