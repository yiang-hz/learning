package com.yiang.period;

import cn.hutool.core.comparator.CompareUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.NumberUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author HeZhuo
 * @date 2022/9/22
 */
public class TheMoney {

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    static class Money {
        /**
         * 资金余额
         */
        private BigDecimal balance;
        private BigDecimal freeze;
        private BigDecimal usable;
        private BigDecimal advisable;
        private BigDecimal market;
        private BigDecimal all;
        private BigDecimal win;
        private BigDecimal todayWin;
        private BigDecimal todayRate;
    }

    public static void main(String[] args) {
        // 资金余额、冻结金额、可用金额、可取金额、股票市值、总资产、持仓盈亏、当日盈亏比
        BigDecimal balance, freeze, usable, advisable, market, all, win, todayWin, todayRate;
        Money money = new Money();
        money.setBalance(Convert.toBigDecimal("43449.38"));
        balance = money.getBalance();
        // 单价
        BigDecimal price = new BigDecimal("48.28");
        BigDecimal count = new BigDecimal("300");
        BigDecimal buy = price.multiply(count);
        System.out.println("买入金额：" + buy);
        // 0.01%
        BigDecimal serviceMoney = buy.multiply(Convert.toBigDecimal("0.0002")).setScale(2, RoundingMode.HALF_UP);
        BigDecimal oldSM = serviceMoney;

        // 万五
        BigDecimal five = Convert.toBigDecimal(5);
        int compare = CompareUtil.compare(serviceMoney, five);
        if (compare < 1) {
            serviceMoney = five;
        }

        System.out.println("手续费：" + serviceMoney + " 原" + oldSM);

        // 0.001% 过户费仅沪市收取
        BigDecimal changeMoney = buy.multiply(Convert.toBigDecimal("0.00001")).setScale(2, RoundingMode.HALF_UP);
        System.out.println("过户费：" + changeMoney);

        BigDecimal subMoney = serviceMoney.add(changeMoney);
        System.out.println("扣除金额：" + subMoney);

        // 减去所有扣除金额、买入金额

        freeze = BigDecimal.ZERO;
        usable = balance.subtract(subMoney).subtract(buy);
        advisable = balance.subtract(subMoney).subtract(buy);

        // 收盘价
        BigDecimal closePrice = Convert.toBigDecimal(49.18);
        BigDecimal rate = closePrice.divide(price, 4, RoundingMode.HALF_UP);
        System.out.println("比例：" + rate);

        market = buy.multiply(rate).setScale(2, RoundingMode.HALF_UP);

        all = market.add(usable);

        win = market.subtract(buy).subtract(subMoney);

        todayWin = market.subtract(buy);

        todayRate = todayWin.divide(all.subtract(todayWin), 4, RoundingMode.HALF_UP);

        String format = NumberUtil.decimalFormat("#.##%", todayRate);

        System.out.println("资金余额：" + balance + "，冻结金额：" + freeze +
                "，可用金额：" +  usable + "，可取金额：" +  advisable +
                "，股票市值：" +  market + "，总资产：" +  all +
                "，持仓盈亏：" +  win + "，当日盈亏：" +  todayWin
                + "，当日盈亏比：" +  format);
        balance = usable;
        Money money1 = new Money(balance, freeze, usable, advisable, market, all, win, todayWin, todayRate);
        System.out.println(money1);

        // 余额 43449.38

        // 买入提供 隆基绿能 沪市 买入价格 买入数量 收盘价

        BigDecimal money1Market = money1.getMarket();
        // 只改变涨幅比
        BigDecimal multiply = money1Market.multiply(Convert.toBigDecimal("-0.013")).setScale(2, RoundingMode.HALF_UP);
        System.out.println("单日盈亏："+multiply);
        money1.setMarket(money1Market.add(multiply));

        money1.setWin(money1.getWin().add(multiply));
        money1.setTodayWin(multiply);
        BigDecimal money1All = money1.getAll();
        money1.setAll(money1All.add(multiply));

        money1.setTodayRate(multiply.divide(money1All, 4, RoundingMode.HALF_UP));

        System.out.println(money1);

    }
}
