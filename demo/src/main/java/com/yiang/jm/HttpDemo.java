package com.yiang.jm;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.yiang.jm.entity.*;
import com.yiang.jm.manager.ApiManager;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HeZhuo
 * @date 2022/11/21
 */
public class HttpDemo {

    public static void main(String[] args) {

        // 一、下单

        // 1、参数封装
        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setUid(ApiManager.UID);
        orderRequest.setOid("1");
        orderRequest.setPhone("13387373751");
        // http://testapp.wimift.com:18087/weMall/v3.1/mini/notify/jm/virtual
        orderRequest.setAsyncUrl("");

        ProductRequest productRequest = new ProductRequest();
        productRequest.setPid("123456");
        productRequest.setCount(1);

        List<ProductRequest> list = new ArrayList<>();
        list.add(productRequest);
        orderRequest.setProductList(list);

//        JSONObject orderResponse = ApiManager.sendRequest(ApiManager.ORDER, orderRequest);
//
//        // 2、检测是否成功
//        ApiManager.checkSuccess(orderResponse);
//
//        // 3、获取并转化返回数据
//        ApiResponse apiResponse = JSONUtil.toBean(orderResponse, ApiResponse.class);
//        System.out.println("返回信息：" + apiResponse);

        // TODO 4、业务逻辑...


        // 二、订单查询方法

        // 1、参数封装
        OrderQueryRequest orderQueryRequest = new OrderQueryRequest();
        orderQueryRequest.setUid(ApiManager.UID);
        orderQueryRequest.setOid("HZ1957413030");
        JSONObject queryResponse = ApiManager.sendRequest(ApiManager.QUERY_ORDER, orderQueryRequest);

        // 2、检测是否成功
        ApiManager.checkSuccess(queryResponse);

        // 3、获取并转化返回数据
        OrderQueryResponse orderQueryResponse = JSONUtil.toBean(queryResponse, OrderQueryResponse.class);
        System.out.println("数据集合：" + orderQueryResponse);

        // TODO 4、业务逻辑...
    }

}
