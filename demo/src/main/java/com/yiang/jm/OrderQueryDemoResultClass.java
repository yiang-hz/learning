package com.yiang.jm;

import cn.hutool.core.date.DateUtil;
import com.yiang.jm.entity.CardInfoResponse;
import com.yiang.jm.entity.OrderQueryResponse;
import com.yiang.jm.entity.ProductResponse;
import com.yiang.jm.manager.ApiManager;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HeZhuo
 * @date 2022/11/21
 */
public class OrderQueryDemoResultClass {

    public static OrderQueryResponse getDemoResult() {

        OrderQueryResponse orderQueryResponse = new OrderQueryResponse();
        orderQueryResponse.setCode(ApiManager.SUCCESS);
        orderQueryResponse.setMsg("成功");
        orderQueryResponse.setOid(ApiManager.UID);

        // 测试返回
        List<CardInfoResponse> resultList = new ArrayList<>();
        CardInfoResponse cardInfo = new CardInfoResponse();
        cardInfo.setExpire(DateUtil.now());
        cardInfo.setNum("JM2022112100001");
        cardInfo.setPwd("qwertyuasdfghjzxcvbn");
        resultList.add(cardInfo);

        resultList.add(new CardInfoResponse("JM2022112100002", "dawdwadwadwaddw", DateUtil.now()));

        List<ProductResponse> productResponseList = new ArrayList<>();
        ProductResponse productResponse = new ProductResponse();
        productResponse.setCount(2);
        productResponse.setName("京东卡券100");
        productResponse.setPid("JD-100");
        productResponse.setCount(2);
        productResponse.setMoney("200.00");
        productResponseList.add(productResponse);
        productResponse.setCardInfoList(resultList);

        orderQueryResponse.setProductList(productResponseList);
        return orderQueryResponse;
    }

    public static void main(String[] args) {
        System.out.println(getDemoResult());
    }
}
