package com.yiang.jm;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileReader;
import cn.hutool.http.Header;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.List;

/**
 * @author HeZhuo
 * @date 2022/11/21
 */
@Slf4j
public class 上线轮流复测 {


    @SneakyThrows
    public static void main(String[] args) {

        // 1、复测时更改报警邮箱 yiangus@163.com;2992322850@qq.com

        File file = FileUtil.file("D:\\testOrder.txt");
        FileReader fileReader = FileReader.create(file);
        List<String> strings = fileReader.readLines();
        int i = 0;
        for (String string : strings) {
            JSONObject jsonObject = JSONUtil.parseObj(string);
            // 移除以前的回调地址，换成本地的避免出现问题
            jsonObject.remove("asyncUrl");
            jsonObject.set("asyncUrl", "http://127.0.0.1:8181/wh/asynCallback");
            jsonObject.set("oid", "20230209hz" + (++i));
            System.out.println(jsonObject);

            log.info("同步订单..." + jsonObject);

            Thread.sleep(500);

            // 地址固定预发布，请勿乱更改，小心挨打，很危险的说
            String result = HttpRequest.post("http://47.113.186.107:8181/api/order")
                    //头信息，多个头信息多次调用此方法即可
                    .header(Header.CONTENT_TYPE, "application/json")
                    // 参数信息
                    .body(jsonObject.toString())
                    //超时，毫秒
                    .timeout(2000000)
                    .execute().body();
            log.info("同步订单成功！" + result);
        }

    }

}
