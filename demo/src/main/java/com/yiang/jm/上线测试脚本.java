package com.yiang.jm;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.yiang.jm.entity.*;
import com.yiang.jm.manager.ApiManager;

import java.util.ArrayList;
import java.util.List;

/**
 * 上线测试脚本
 *
 * @author HeZhuo
 * @date 2022/11/21
 */
public class 上线测试脚本 {

    public static void main(String[] args) {

        // 卡券包
        List<ProductRequest> list = new ArrayList<>();
//        // 1、不存在供应商。 2、不存在商品编码、3库存不足OR成交
//        list.add(new ProductRequest("P1610543154629271552", 1));
//        list.add(new ProductRequest("P161348143799889920011", 1));
//        list.add(new ProductRequest("P1611296444962893824", 1))
//        sendOrder(list);

        // 1、不存在供应商。 2、不存在商品编码、3库存不足OR成交
        list.add(new ProductRequest("P1610543154629271552", 1));
        list.add(new ProductRequest("P161348143799889920011", 1));
        list.add(new ProductRequest("P1611296444962893824", 1));
        sendOrder(list);

        // 课程
//        sendOrder(new ProductRequest("P1613481437998899200", 1));
//
//        // E卡
//        sendOrder(new ProductRequest("P1611296444962893824", 1));


    }

    public static void sendOrder(List<ProductRequest> list) {
        // 一、下单
        // 1、参数封装
        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setUid(ApiManager.UID);
        int i = RandomUtil.randomInt();
        System.out.println("订单ID" + i);
        orderRequest.setOid("HZ" + Math.abs(i));
        orderRequest.setPhone("13387373751");
        // http://testapp.wimift.com:18087/weMall/v3.1/mini/notify/jm/virtual
        orderRequest.setAsyncUrl("http://127.0.0.1:8181/wh/asynCallback");

        orderRequest.setProductList(list);

        JSONObject orderResponse = ApiManager.sendRequest(ApiManager.ORDER, orderRequest);

        // 2、检测是否成功
        ApiManager.checkSuccess(orderResponse);

        // 3、获取并转化返回数据
        ApiResponse apiResponse = JSONUtil.toBean(orderResponse, ApiResponse.class);
        System.out.println("返回信息：" + apiResponse);

        System.out.println("订单查询...");
        // 1、参数封装
        OrderQueryRequest orderQueryRequest = new OrderQueryRequest();
        orderQueryRequest.setUid(ApiManager.UID);
        orderQueryRequest.setOid("HZ" + Math.abs(i));
        JSONObject queryResponse = ApiManager.sendRequest(ApiManager.QUERY_ORDER, orderQueryRequest);

        // 2、检测是否成功
        ApiManager.checkSuccess(queryResponse);

        // 3、获取并转化返回数据
        OrderQueryResponse orderQueryResponse = JSONUtil.toBean(queryResponse, OrderQueryResponse.class);
        System.out.println("数据集合：" + orderQueryResponse);
    }

}
