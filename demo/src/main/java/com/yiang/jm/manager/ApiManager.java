package com.yiang.jm.manager;

import cn.hutool.crypto.SecureUtil;
import cn.hutool.http.Header;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.Charset;

/**
 * @author HeZhuo
 * @date 2022/11/19
 */
@Slf4j
public class ApiManager {

    static public final String ENV = "http://127.0.0.1:8181";
    static public final String QUERY_ORDER = "/api/order/query";
    static public final String ORDER = "/api/order";
    static public final String SUCCESS = "200";
    static public final String UID = "6c3e5fb235ae71c4c6ad12c0543473c7";

    public static JSONObject sendRequest(String url, Object object) {

        JSONObject paramObj = new JSONObject(object, true);

        String paramUrl = HttpUtil.urlWithForm(url, paramObj,
                Charset.defaultCharset(), true
        );
        String signUrl = paramUrl.substring(paramUrl.indexOf("?") + 1);
        log.info("signUrl:" + signUrl);

        String sign = SecureUtil.md5(signUrl);
        log.info("sign:" + sign);
        paramObj.set("sign", sign);

        log.info("paramObj:" + paramObj);

        String result = HttpRequest.post(ENV + url)
                //头信息，多个头信息多次调用此方法即可
                .header(Header.CONTENT_TYPE, "application/json")
                // 参数信息
                .body(paramObj.toString())
                //超时，毫秒
                .timeout(2000000)
                .execute().body();

        //        checkSuccess(jsonObject);

        return JSONUtil.parseObj(result);
    }

    public static void checkSuccess(JSONObject jsonObject) {
        Object resultCode = jsonObject.getStr("code");
        if (!SUCCESS.equals(resultCode)) {
            // 异常逻辑处理...
            log.error("异常...{}", jsonObject);
        }
    }

}
