package com.yiang.jm.manager;

import cn.hutool.core.util.ObjUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;

import java.nio.charset.StandardCharsets;

/**
 * 数据库敏感字段加密/解密处理，默认使用AES
 *
 * @author hz
 * @date 2022/11/9
 */
public class ColumnEncryptDecryptTools {

    public static void main(String[] args) {
        String decrypt1 = ColumnEncryptDecryptTools.decrypt("eptd:TbEDMeH4uafOQ5GQZw+vY7QudHpVlzPSpEsXVZD51WM=");
        System.out.println(decrypt1);
        System.out.println(ColumnEncryptDecryptTools.decrypt(decrypt1));
    }

    private static final String PRIVATE_KEY = "HNT4qiDhx0wt4tnwt7LeIe9wZFnzk2oZ";

    /**
     * AES加解密key
     */
    private static final byte[] KEY = PRIVATE_KEY.getBytes(StandardCharsets.UTF_8);

    /**
     * 密文前缀，用于判断是否要解密
     */
    private static final String ENCRYPT_TEXT_PREFIX = "eptd:";

    /**
     * 解密
     *
     * @param encryptText 需要解密的密文
     */
    public static String decrypt(String encryptText) {
        String plainText = null;
        if (ObjUtil.isNotEmpty(encryptText)) {
            if (StrUtil.startWith(encryptText, ENCRYPT_TEXT_PREFIX)) {
                // 去除前缀后解密
                plainText = SecureUtil.aes(KEY).decryptStr(StrUtil.removePrefix(encryptText, ENCRYPT_TEXT_PREFIX));
            } else {
                plainText = encryptText;
            }
        }
        return plainText;
    }
}
