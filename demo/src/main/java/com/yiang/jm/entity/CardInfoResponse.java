package com.yiang.jm.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author HeZhuo
 * @date 2022/11/21
 */
@ApiModel("返回卡券信息类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CardInfoResponse {

    @ApiModelProperty("卡号")
    private String num;

    @ApiModelProperty("密码")
    private String pwd;

    @ApiModelProperty("过期时间")
    private String expire;

}
