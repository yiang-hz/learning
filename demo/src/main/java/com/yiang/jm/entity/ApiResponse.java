package com.yiang.jm.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author HeZhuo
 * @date 2022/11/19
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiResponse {

    private String code;

    private String msg;

}


