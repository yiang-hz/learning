package com.yiang.jm.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;


/**
 * @author HeZhuo
 * @date 2022/11/21
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@ToString(callSuper = true)
@ApiModel("商品查询返回类")
public class OrderQueryResponse extends ApiResponse {

    @ApiModelProperty("下游渠道ID")
    private String oid;

    @ApiModelProperty("卡券详情")
    private List<ProductResponse> productList;

}
