package com.yiang.jm.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
 * @author HeZhuo
 * @date 2022/11/21
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@ToString(callSuper = true)
@ApiModel("订单请求参数类")
public class OrderRequest extends OrderQueryRequest {

    @ApiModelProperty("下单手机号")
    private String phone;

    @ApiModelProperty("异步回调地址")
    private String asyncUrl;

    @ApiModelProperty("商品参数集合")
    private List<ProductRequest> productList;
}
