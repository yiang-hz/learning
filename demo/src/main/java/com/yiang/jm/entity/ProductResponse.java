package com.yiang.jm.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author HeZhuo
 * @date 2022/11/21
 */
@ApiModel("返回商品信息类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductResponse {

    @ApiModelProperty("商品名称")
    private String name;

    @ApiModelProperty("商品ID")
    private String pid;

    @ApiModelProperty("数量")
    private Integer count;

    @ApiModelProperty("金额")
    private String money;

    private List<CardInfoResponse> cardInfoList;
}
