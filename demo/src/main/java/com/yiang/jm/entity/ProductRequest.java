package com.yiang.jm.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author HeZhuo
 * @date 2022/11/21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@ApiModel("商品请求类")
public class ProductRequest {

    @ApiModelProperty("商品ID")
    private String pid;

    @ApiModelProperty("数量")
    private Integer count;

}
