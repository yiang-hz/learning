package com.yiang.jm.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * @author HeZhuo
 * @date 2022/11/21
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@ApiModel("订单查询请求参数类")
public class OrderQueryRequest extends ApiRequest {

    @ApiModelProperty("下游渠道订单号")
    private String oid;

}
