package com.yiang.jm.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author HeZhuo
 * @date 2022/11/19
 */
@ApiModel("下游渠道参数")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiRequest {

    @ApiModelProperty("分配给下游的渠道ID")
    private String uid;

    @ApiModelProperty("签名")
    private String sign;

}


