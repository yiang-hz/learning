package com.yiang.java8.trywithresource;

/**
 * @author HeZhuo
 * @date 2020/7/3
 */
public class TryWithResourceDemo {

    public static void main(String[] args) {
        //1.通过TryWithResources操作可以优雅代码
        //2.通过该操作可以查看内部异常，而JDK1.7会造成内部异常嵌套

        test7Close();
        test8Close();

    }

    private static void test8Close() {
        try (Resource resource = new Resource()) {
            resource.sayHello();
        }
    }

    private static void test7Close() {
        Resource resource = null;
        try {
            resource = new Resource();
            resource.sayHello();
        } finally {
            if (null != resource){
                resource.close();
            }
        }
    }

}
