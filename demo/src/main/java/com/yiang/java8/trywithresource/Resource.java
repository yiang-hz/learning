package com.yiang.java8.trywithresource;

/**
 * @author HeZhuo
 * @date 2020/7/3
 */
public class Resource implements AutoCloseable {

    public void sayHello()  {
        System.out.println("Resource: Hello World!");
        throw new StringIndexOutOfBoundsException("Resource: String Index Out Error!");
    }

    @Override
    public void close() {
        System.out.println("Resource: Closed!");
        throw new NullPointerException("Resource: Close Null Error!");
    }
}
