package com.yiang.third.checkParam.check;

/**
 * @author HeZhuo
 * @date 2021/6/29
 */
public interface Check {

    boolean check(String checkParam) throws InterruptedException;
}
