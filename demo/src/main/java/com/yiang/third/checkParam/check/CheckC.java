package com.yiang.third.checkParam.check;

/**
 * @author HeZhuo
 * @date 2021/6/29
 */
public class CheckC implements Check{

    @Override
    public boolean check(String checkParam) throws InterruptedException {
        Thread.sleep(200);
        System.out.println("Check C... over");

        return !"C".equals(checkParam);
    }
}
