package com.yiang.third.checkParam.check;

/**
 * @author HeZhuo
 * @date 2021/6/29
 */
public class CheckA implements Check{

    @Override
    public boolean check(String checkParam) throws InterruptedException {
        Thread.sleep(100);
        System.out.println("Check A... over");

        return !"A".equals(checkParam);
    }
}
