package com.yiang.third.checkParam;

import cn.hutool.core.lang.caller.Caller;

import java.util.Random;
import java.util.concurrent.*;

/**
 * @author HeZhuo
 * @date 2021/6/29
 */
public class Test {

    private static ExecutorService threadPool = Executors.newFixedThreadPool(5);

    private static CountDownLatch counter = new CountDownLatch(1);

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        try {
            Thread A = new Thread(() -> {
                System.out.println("逻辑A");
                counter.countDown();
            });
            A.start();

            for (int i=0;i<5;i++){
                Future<Integer> future = threadPool.submit(new Caller(12));
                System.out.println(future.get());
                if (future.get() > 20) {
                    threadPool.shutdownNow();
                }
            }
        } finally {
            threadPool.shutdown();
        }

    }

    static class Caller implements Callable{

        private int param;

        public Caller(int param) {
            this.param = param;
        }


        @Override
        public Object call() throws Exception {
            counter.await();
            System.out.println("逻辑B");
            param = param + new Random().nextInt(20);
            return param;
        }
    }

}
