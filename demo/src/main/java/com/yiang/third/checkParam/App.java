package com.yiang.third.checkParam;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import cn.hutool.core.thread.ThreadUtil;
import com.yiang.third.checkParam.check.Check;
import com.yiang.third.checkParam.check.CheckA;
import com.yiang.third.checkParam.check.CheckB;
import com.yiang.third.checkParam.check.CheckC;
import lombok.SneakyThrows;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author HeZhuo
 * @date 2021/6/29
 */
public class App {

    static ExecutorService executorService = ThreadUtil.newExecutor(3);

    @SneakyThrows
    public static void main(String[] args) {
        TimeInterval timer = DateUtil.timer();
        String checkParam = "A";

        ThreadGroup threadGroup = new ThreadGroup("checkGroup");

        Thread threadA = new Thread(threadGroup, () -> {
            Check check = new CheckA();
            try {
                check.check(checkParam);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "checkA");
        Thread threadB = new Thread(threadGroup, () -> {
            Check check = new CheckB();
            try {
                check.check(checkParam);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "checkB");
        Thread threadC = new Thread(threadGroup, () -> {
            Check check = new CheckC();
            try {
                check.check(checkParam);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "checkC");
        threadA.start();threadB.start();threadC.start();

        CountDownLatch countDownLatch = ThreadUtil.newCountDownLatch(3);




//        boolean f = first(checkParam);
        System.out.println(timer.interval());
        System.out.println(timer.intervalSecond());
//        if (f) {
//            System.out.println("正常执行...");
//        }
    }

    private static boolean first(String checkParam) throws InterruptedException {
        Check check = new CheckA();
        if (!check.check(checkParam)) {
            return false;
        }
        check = new CheckB();
        if (!check.check(checkParam)) {
            return false;
        }
        check = new CheckC();
        return check.check(checkParam);
    }
}
