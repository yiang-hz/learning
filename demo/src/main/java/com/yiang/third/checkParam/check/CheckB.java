package com.yiang.third.checkParam.check;

/**
 * @author HeZhuo
 * @date 2021/6/29
 */
public class CheckB implements Check{

    @Override
    public boolean check(String checkParam) throws InterruptedException {
        Thread.sleep(150);
        System.out.println("Check B... over");

        return !"B".equals(checkParam);
    }
}
