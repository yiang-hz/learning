package com.yiang.single;

import cn.hutool.core.date.TimeInterval;
import cn.hutool.crypto.SecureUtil;

import javax.swing.*;
import java.util.TimerTask;

/**
 * @author HeZhuo
 * @date 2020/12/10
 */
public class SpendTimeDemo {
    public static void main(String[] args) {
        System.out.println(SecureUtil.md5("sss"));
        TimeInterval timeInterval = new TimeInterval();
        timeInterval.start();
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                try {
                    ImageIcon imageIcon = new ImageIcon();
                    imageIcon.getAccessibleContext();
                    Thread.sleep(101);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        timerTask.run();
        System.out.println(timerTask.scheduledExecutionTime());
        System.out.println();
        timeInterval.interval();
        System.out.println("ms"+timeInterval.intervalMs());
        System.out.println(timeInterval.intervalMinute());
        System.out.println(timeInterval.intervalSecond());
        System.out.println(timeInterval.interval());
    }
}
