package com.yiang.demo;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author HeZhuo
 * @date 2022/4/7
 */
public class Atomicdemo {

    static AtomicInteger atomicInteger;

    public static void main(String[] args) {
        atomicInteger = new AtomicInteger();
        int i = 0;
        intt(i);
        System.out.println(atomicInteger.incrementAndGet());
        System.out.println(atomicInteger);
        System.out.println(i);
        System.out.println(atomicInteger.get());
    }

    private static void intt (int i) {
        i++;
        System.out.println(atomicInteger.incrementAndGet());
    }
}
