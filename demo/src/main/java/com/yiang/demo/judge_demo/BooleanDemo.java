package com.yiang.demo.judge_demo;

import cn.hutool.core.util.ObjectUtil;

/**
 * @author HeZhuo
 * @date 2020/8/7
 */
public class BooleanDemo {

    public static void main(String[] args) {
        String a = a();
        if (ObjectUtil.isEmpty(a)){
            System.out.println(1);
        } else{
            System.out.println(a);
        }
        System.out.println("end...");
    }

    private static String a (){
        return null;
    }
}
