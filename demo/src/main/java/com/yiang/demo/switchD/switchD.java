package com.yiang.demo.switchD;

import org.springframework.util.ObjectUtils;

/**
 * @author HeZhuo
 * @date 2021/12/4
 */
public class switchD {

    public static void main(String[] args) {
        System.out.println(check(null));
    }

    public static String check(String str) {

        if (!ObjectUtils.isEmpty(str)) {
            switch (str) {
                case "01":
                    return "01";
                case "02":
                    return "02";
                default:
                    return "";
            }
        }
        return "";
    }
}
