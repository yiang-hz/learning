package com.yiang.demo.enum_;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.EnumUtil;

import java.util.List;
import java.util.concurrent.ExecutorService;

/**
 * @author HeZhuo
 * @date 2021/5/26
 */
public class TestEnum {

    public static void main(String[] args) {
        Integer type = 1;
//        System.out.println(check(type));
//        System.out.println(check(type));

        System.out.println(UserEnum.getName(1));

        ExecutorService executorService = ThreadUtil.newExecutor(10);

        for (int i = 1; i <= 10; i++) {
            int index = i;
            executorService.submit(() -> {
                System.out.print("");
                System.out.println(Thread.currentThread() + "---" + index + "---" + check(type));
            });
        }
        executorService.shutdown();
        System.out.println("结束....");
    }

    private static List<Object> objectList;

    synchronized //同步锁
    private static List<Object> getObjectList() {
        if (CollectionUtil.isNotEmpty(objectList)) {
            return objectList;
        }
        System.out.println("初始化...");
        objectList = EnumUtil.getFieldValues(UserEnum.class, "code");
        return objectList;
    }

    private static boolean check(Object value) {
        return getObjectList().contains(value);
    }
}
