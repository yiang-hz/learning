package com.yiang.demo.enum_;

import lombok.AllArgsConstructor;

/**
 * @author HeZhuo
 * @date 2021/5/26
 */
@AllArgsConstructor
public enum UserEnum {

    /**
     * 枚举类
     */


    AM_QUIT_HOME_MESSAGE(1,"自动退房信息"),
    STAY_ORDER_MESSAGE(0,"待接单信息"),
    WARN_MESSAGE(2,"提醒"),
    REFUSE_ORDER(3,"退房通知"),
    OVER_TIME(4,"设置"),


    USER_AGE_NINE(9, "年龄为9岁"),
    USER_AGE_ONE(1, "年龄为1岁"),

    ;

    private Integer code;
    private String name;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getName(Integer code) {
        for (UserEnum c : UserEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.getName();
            }
        }
        return null;
    }

}
