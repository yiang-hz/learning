package com.yiang.demo.enum_;

import lombok.AllArgsConstructor;

/**
 * @author HeZhuo
 * @date 2021/5/26
 */
@AllArgsConstructor
public enum UserAgeEnum {

    /**
     * 枚举类
     */

    USER_AGE_NINE(9, "年龄为9岁"),
    USER_AGE_ONE(1, "年龄为1岁")
    ;

    private Integer code;
    private String name;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getName(Integer code) {
        for (UserEnum c : UserEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.getName();
            }
        }
        return null;
    }
}
