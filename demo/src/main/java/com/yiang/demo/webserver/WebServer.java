package com.yiang.demo.webserver;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;

/**
 * @author HeZhuo
 * @date 2020/7/9
 */
@WebService
public class WebServer {

    @WebMethod
    public String get(String say) {
        return "Yiang:" + say;
    }

    public static void main(String[] args) {
        Endpoint.publish("http://192.168.0.115:8090/webServer/get", new WebServer());
        System.out.println("Start...");
    }

}
