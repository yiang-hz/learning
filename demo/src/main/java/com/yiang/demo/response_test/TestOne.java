package com.yiang.demo.response_test;

import com.yiang.response.AppResponseData;
import com.yiang.response.FeignResponseData;
import com.yiang.response.ResultCode;

public class TestOne {

    public static void main(String[] args) {

        System.out.println(a());
        System.out.println(b());

        FeignResponseData<String> b = b();
        System.out.println(b.getData());

    }

    private static AppResponseData a(){
        return new AppResponseData(ResultCode.SUCCESS);
    }

    private static FeignResponseData<String> b(){
        String a = "231";
        return new FeignResponseData<>(ResultCode.SUCCESS, a);
    }


}
