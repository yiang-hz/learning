package com.yiang.demo.爬虫;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FensPage {
    private int followNum;

    private int fansNum;

    private int pageNo;

    private int replyNum;

    private List<FensDo> list;

    private int pageNum;

    private int shuoNum;

}