package com.yiang.demo.爬虫;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileWriter;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;

import java.io.File;
import java.util.List;

public class 淘股吧粉丝 {

    public static void main(String[] args) throws InterruptedException {

        for (int i = 1; i <= 50; i++) {
            String result = HttpUtil.get("https://shuo.taoguba.com.cn/shuo/getUserBlogFans?userID=6825534&pageNo=" + i);
            System.out.println("返回值：" + result);

            FensResult fensResult = JSONUtil.toBean(result, FensResult.class);

            File file = FileUtil.newFile("D:\\initpath\\pc\\fens\\" + "fens.txt");
            File info = FileUtil.newFile("D:\\initpath\\pc\\fens\\" + "info.txt");
            FileWriter fileWriter = new FileWriter(file);
            FileWriter infoWriter = new FileWriter(info);
            fileWriter.write(result + "\n", true);

            FensPage fensPage = fensResult.getDto();
            List<FensDo> fensList = fensPage.getList();
            for (FensDo fensDo : fensList) {
                String userInfo = "用户ID：" + fensDo.getUserID() + " -- " +
                        "用户名称：" + fensDo.getUserName() + " -- " +
                        "用户性别：" + fensDo.getGender();
                infoWriter.write(userInfo + "\n", true);
                System.out.println("用户信息：" + userInfo);
            }

            int sleep = RandomUtil.randomInt(2, 5);
            System.out.println("休眠：" + sleep + "秒：" + DateUtil.date());
            Thread.sleep(sleep * 1000L);
            System.out.println("休眠结束：" + DateUtil.date());
            System.out.println("---------------------------------------------------------------");
        }
    }

}
