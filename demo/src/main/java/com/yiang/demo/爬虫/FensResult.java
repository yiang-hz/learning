package com.yiang.demo.爬虫;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FensResult {

    private boolean status;

    private int errorCode;

    private String errorMessage;

    private FensPage dto;

    private int _t;

}