package com.yiang.demo.爬虫;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FensDo {

    private int seq;

    private String seqStr;

    private String createDT;

    private String creator;

    private String modifyDT;

    private String modifier;

    private int totalviews;

    private int totalTopic;

    private int totalReplay;

    private String createTime;

    private String remarks;

    private String gender;

    private String shieldFlag;

    private String specialFocus;

    private String portrait;

    private int groupID;

    private String auth;

    private int isFollow;

    private int fansNum;

    private int followNum;

    private String createDate;

    private int usefulNum;

    private String userName;

    private int userID;

    private int bestBBSNums;

}




