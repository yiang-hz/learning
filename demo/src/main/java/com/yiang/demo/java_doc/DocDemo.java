package com.yiang.demo.java_doc;

/**
 * 文档注释Demo类
 * @author Weizhu, HZ
 * @since 2020/01/09
 * @date  2020/01/09 16:16
 * @see DemoEntity 实体类案例
 * @version 1.0
 */
public class DocDemo {

    public static void main(String[] args) {
        Object o = new Object();

    }

    /**
     * 传递一个{@code str}，用于输出。
     * @param str
     *             字符串参数 {@code String}
     * @see DemoEntity 案例实体类
     * @date 2020/01/09
     * @since 1.0
     * @throws NullPointerException 当实体类找不到
     * @exception Exception 抛出异常
     */
    public static void demoMethod(String str) throws Exception{
        DemoEntity demoEntity = new DemoEntity();
        System.out.println(demoEntity.toString() + "--" + str);
    }
}
