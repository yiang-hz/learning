package com.yiang.demo.java_doc;

public class DemoEntity {

    /** 默认数量 {@value QUANTITY} */
    private int age = QUANTITY;

    /** 默认数量 {@value} */
    private static final Integer QUANTITY = 1;

}
