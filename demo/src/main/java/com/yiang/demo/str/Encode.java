package com.yiang.demo.str;

import cn.hutool.core.codec.Base64;

/**
 * @author HeZhuo
 * @date 2020/10/22
 */
public class Encode {

    public static void main(String[] args) {
        //字符串编码转换 Base64 转换还原
        String a = "红米Note7 + 红米K20Pro 尊享版";
        //5Lym5a625piv5LiA5Liq6Z2e5bi46ZW/55qE5a2X56ym5Liy
        String encode = Base64.encode(a);

        // 还原为a
        String decodeStr = Base64.decodeStr(encode);
        System.out.println(encode);
        System.out.println(decodeStr);

    }
}
