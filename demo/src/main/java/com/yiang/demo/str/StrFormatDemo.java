package com.yiang.demo.str;

/**
 * @author HeZhuo
 * @date 2020/11/9
 */
public class StrFormatDemo {

    public static void main(String[] args) {
        int a = 111;

        // String b = String.format("-%02d", ++a); // -02
        String b = String.format("%02d", a);

        System.out.println(b);
    }
}
