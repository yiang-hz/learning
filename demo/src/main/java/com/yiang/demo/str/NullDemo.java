package com.yiang.demo.str;

import cn.hutool.core.convert.Convert;
import com.alibaba.fastjson.util.TypeUtils;

/**
 * @author HeZhuo
 * @date 2020/11/5
 */
public class NullDemo {

    @SuppressWarnings("uncheck")
    public static void main(String[] args) {
        Long sss = null;
        String aaa = sss + "";
        String bbb = String.valueOf(sss);
        String bbbAlibaba = TypeUtils.castToString(sss);
        String bbbHutool = Convert.toStr(sss);
        System.out.println(bbb);
        System.out.println(aaa);
        System.out.println(bbbAlibaba);
        System.out.println(bbbHutool);

        System.out.println(aaa.equals(bbb));
        System.out.println(aaa.equals(bbbAlibaba));
        System.out.println(aaa.equals(bbbHutool));

        System.out.println("null".equals(aaa));
        System.out.println("null".equals(bbbAlibaba));
        System.out.println("null".equals(bbbHutool));
    }
}
