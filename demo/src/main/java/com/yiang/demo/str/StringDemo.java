package com.yiang.demo.str;

import cn.hutool.captcha.generator.RandomGenerator;

/**
 * @author HeZhuo
 * @date 2020/6/19
 */
public class StringDemo {

    public static void main(String[] args) {

        String str = "羽绒棉被";
        String s = "羽绒";
        System.out.println(str.startsWith(s));
        System.out.println(new RandomGenerator(32).generate());
        System.out.println(new RandomGenerator(32).generate());

        String state = "f79996a76c370648f405ff2fda8759ee*hezhuo";
        state = state.substring(state.lastIndexOf("*"));
        System.out.println(state);
    }
}
