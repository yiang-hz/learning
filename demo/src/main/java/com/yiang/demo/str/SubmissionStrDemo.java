package com.yiang.demo.str;

import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.CharUtil;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author HeZhuo
 * @date 2021/12/29
 */
public class SubmissionStrDemo {

    public static void main(String[] args) {
        List<String> fileNameList = ListUtil.toList("D:\\zip\\20211203\\zzlcHoldInfo\\Z70033-202-20211203-07.zip",
                "D:\\zip\\20211203\\zzlcIdentifyInfo\\Z70033-203-20211203-10.zip",
                "D:\\zip\\20211203\\zzlcDetailInfo\\Z70033-201-20211203-01.zip",
                "D:\\zip\\20211203\\zzlcDetailInfo\\Z70033-201-20211203-02.zip",
                "D:\\zip\\20211203\\zzlcDetailInfo\\Z70033-201-20211203-03.zip",
                "D:\\zip\\20211203\\zzlcDetailInfo\\Z70033-201-20211203-05.zip",
                "D:\\zip\\20211203\\zzlcDetailInfo\\Z70033-201-20211203-07.zip",
                "D:\\zip\\20211203\\zzlcDetailInfo\\Z70033-201-20211203-09.zip"
        );
        Set<Integer> collect = fileNameList.stream().map((fileName -> Convert.toInt(
                fileName.substring(fileName.lastIndexOf(CharUtil.DASHED) + 1, fileName.lastIndexOf(CharUtil.DOT)), 1
        ))).collect(Collectors.toSet());
        Integer integer1 = collect.stream().max(Integer::compareTo).orElse(1);
        System.out.println("--"+integer1);

        if (CollectionUtils.isEmpty(fileNameList)) {
            System.out.println(1);
            return;
        }
        fileNameList = fileNameList.stream().filter(s -> s.contains("-201-")).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(fileNameList)) {
            System.out.println(1);
            return;
        }
        System.out.println(fileNameList);
        int num = 1;
        for (String fileName : fileNameList) {
            int compareNum = Convert.toInt(fileName.substring(fileName.lastIndexOf("-") + 1, fileName.lastIndexOf(".")), 1);
            if (compareNum >= num) {
                num = compareNum + 1;
            }
        }
        System.out.println(num);
    }
}
