package com.yiang.demo.str;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.CharUtil;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;

/**
 * @author HeZhuo
 * @date 2021/1/11
 */
public class GetOneContainsCount {

    public static void main(String[] args) {
        String str = "23-32";
        System.out.println(checkToleranceRange(str, Convert.toBigDecimal(25)));
        System.out.println(checkToleranceRange(str, Convert.toBigDecimal(25),
                true, BigDecimal.valueOf(-0.5), BigDecimal.valueOf(0.5)));
    }

    private static int loadCount(String str) {
        int count = 0;
        for (String s : str.split("")) {
            if (String.valueOf(CharUtil.DASHED).equals(s)) {
                count++;
            }
        }
        return count;
    }

    private static boolean checkToleranceRange(String toleranceRange, BigDecimal tolerance) {

        //如果为空，或者不包含分隔符 -，直接返回true， 例如：任意自然数或空字符串
        if (ObjectUtils.isEmpty(toleranceRange) ||
                !toleranceRange.contains(String.valueOf(CharUtil.DASHED))) {
            return true;
        }

        //去掉所有空格
        toleranceRange = toleranceRange.replace(" ", "");
        int dashedCount = loadCount(toleranceRange);

        BigDecimal minTolerance;
        BigDecimal maxTolerance;

        /*
         * substring: 取头不取尾
         * case 1: （只包含一个‘-’ 代表所有数字都为正数）
         *  min：找到’-‘取前面的数字 例如  23-32 取 23
         *  max：找到‘-’取后面的数字 例如  23-32 取 32
         * case 2: （包含两个'-'，代表第一个数字为负数，因为不可能存在 1 - -1 的数字，只可能是 -1 - 1）
         *  min：找到最后一个‘-’取前面的数字 例如 -23-32 最后一个‘-’为其分隔符，取-23
         *  max：找到最后一个‘-’取后面的数字 例如 -23-32 最后一个‘-’为其分隔符，取32
         * case 3：（包含三个'-'，代表所有数字都为负数）
         *  min： 找到最后一个'-'前面的分隔符'-'，并且取前面的数字 例如 -23--32 取-23
         *  max：找到最后一个‘-’取最后一个'-'与后面的数字 例如 -23--32 取-32
         */
        switch (dashedCount){
            case 1:
                minTolerance = Convert.toBigDecimal(
                        toleranceRange.substring(0, toleranceRange.indexOf(CharUtil.DASHED))
                );
                maxTolerance = Convert.toBigDecimal(
                        toleranceRange.substring(toleranceRange.indexOf(CharUtil.DASHED) + 1)
                );
                break;
            case 2:
                minTolerance = Convert.toBigDecimal(
                        toleranceRange.substring(0, toleranceRange.lastIndexOf(CharUtil.DASHED))
                );
                maxTolerance = Convert.toBigDecimal(
                        toleranceRange.substring(toleranceRange.lastIndexOf(CharUtil.DASHED) + 1)
                );
                break;
            case 3:
                minTolerance = Convert.toBigDecimal(
                        toleranceRange.substring(0, toleranceRange.lastIndexOf(CharUtil.DASHED) - 1)
                );
                maxTolerance = Convert.toBigDecimal(
                        toleranceRange.substring(toleranceRange.lastIndexOf(CharUtil.DASHED))
                );
                break;
            default:
                return false;
        }
        minTolerance = minTolerance.add(Convert.toBigDecimal("-0.5"));
        maxTolerance = maxTolerance.add(Convert.toBigDecimal("0.5"));
        return tolerance.compareTo(minTolerance) > -1 && tolerance.compareTo(maxTolerance) < 1;
    }

    /**
     * 确认是否符合公差范围
     */
    @SuppressWarnings("all")
    public static boolean checkToleranceRange(String toleranceRange, BigDecimal tolerance, boolean check, BigDecimal min, BigDecimal max) {
        String[] tolerances = toleranceRange.split("-");
        BigDecimal minTolerance;
        BigDecimal maxTolerance;
        if (tolerances.length == 2) {
            minTolerance = new BigDecimal(tolerances[0]);
            maxTolerance = new BigDecimal(tolerances[1]);
        } else if (tolerances.length == 3) {
            minTolerance = new BigDecimal(tolerances[1]).negate();
            maxTolerance = new BigDecimal(tolerances[2]);
        } else if (tolerances.length == 4) {
            minTolerance = new BigDecimal(tolerances[1]).negate();
            maxTolerance = new BigDecimal(tolerances[3]).negate();
        } else {
            return false;
        }
        if (check) {
            minTolerance = minTolerance.add(min);
            maxTolerance = maxTolerance.add(max);
        }
        // 公差范围需要除100
        minTolerance = minTolerance.divide(new BigDecimal(100));
        maxTolerance = maxTolerance.divide(new BigDecimal(100));
        return tolerance.compareTo(minTolerance) > -1 && tolerance.compareTo(maxTolerance) < 1;
    }
}
