package com.yiang.demo.job_demo;

/**
 * @author HeZhuo
 * @date 2021/5/8
 */
public class Demo03Quartz {

    public static void main(String[] args) {
        /*
         * 伪代码  实现思想：
         * 1.引入Maven依赖
         * 2.任务调度实现类
         * 3.任务启动类
         *      创建工厂 -> 获取实例 -> 创建任务 -> 配置定时器Trigger -> 注册任务与定时器 -> 启动
         */

        //http://cron.qqe2.com/ Quartz表达式网站
    }
}
