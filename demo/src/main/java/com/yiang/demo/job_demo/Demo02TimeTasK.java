package com.yiang.demo.job_demo;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author HeZhuo
 * @date 2021/5/8
 */
public class Demo02TimeTasK {

    /**
     * 使用TimerTask类实现定时任务
     */
    static long count = 0;

    public static void main(String[] args) {
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                count++;
                System.out.println(count);
            }
        };

        /*
         * 多线程并行处理定时任务时，Timer运行多个TimeTask时，只要其中之一没有捕获抛出的异常，
         * 其它任务便会自动终止运行，使用ScheduledExecutorService则没有这个问题。
         */
        Timer timer = new Timer();
        // 天数
        long delay = 0;
        // 秒数
        long period = 1000;
        timer.scheduleAtFixedRate(timerTask, delay, period);
    }

    /**
     * JavaSE5的java.util.concurrent，做为并发工具类被引进
     * 最理想的定时任务实现方式。
     *
     * 使用ScheduledExecutorService代替Timer
     */
    static class Demo03ScheduledExecutorService {

        static long count = 0;

        @SuppressWarnings("all")
        public static void main(String[] args) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    // task to run goes here
                    count++;
                    System.out.println(count);
                }
            };
            //线程池，四种线程池格式的其中一种，也是唯一一个单线程
            //1.单线程 2.定长 3.周期定长 4.缓存 自动回收与创建
            ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
            // 第二个参数为首次执行的延时时间，第三个参数为定时执行的间隔时间
            service.scheduleAtFixedRate(runnable, 1, 1, TimeUnit.SECONDS);
        }
    }

}
