package com.yiang.demo.job_demo;

/**
 * @author HeZhuo
 * @date 2021/5/8
 */
public class Demo01Task {

    static long count = 0;

    @SuppressWarnings("all")
    public static void main(String[] args) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(1000);
                        //每秒count增加1
                        count++;
                        System.out.println(count);
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }
}
