package com.yiang.demo.tree_demo;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 排程表
 * </p>
 *
 * @author HZ
 * @since 2020-10-27
 */
@Data
@ApiModel(value = "Schedule对象", description = "排程表")
@AllArgsConstructor
@NoArgsConstructor
public class Schedule {

    private static final long serialVersionUID = 1L;

    private String id;

    @ApiModelProperty(value = "工单id，关联工单表")
    private String stationOrderId;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

}
