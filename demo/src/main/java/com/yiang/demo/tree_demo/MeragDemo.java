package com.yiang.demo.tree_demo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author HeZhuo
 * @date 2020/11/9
 */
public class MeragDemo {

    public static void main(String[] args) {

        List<Person> findPerson = new ArrayList<>();
        findPerson.add(new Person("adam",18, new ArrayList<>()));
        findPerson.add(new Person("adam",18, new ArrayList<>()));
        findPerson.add(new Person("bill",31, new ArrayList<>()));


        List<Person> merged = findPerson.stream()
            .collect(Collectors.collectingAndThen(
                Collectors.toMap(
                    (p) -> new AbstractMap.SimpleEntry<>(p.getName(), p.getAge()),
                        Function.identity(),
                        (left, right) -> {
                            left.getAddress().addAll(right.getAddress());
                            return left;
                        }
                    ),
                    ps -> new ArrayList<>(ps.values())
                )
            );

        System.out.println(merged);
    }

    @Data
    @AllArgsConstructor
    static class Person {
        String name;
        int age;
        List<Address> address;
    }

    @Data
    static class Address {
        String street;
        String city;
        String country;
    }
}
