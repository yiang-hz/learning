package com.yiang.demo.tree_demo;

import cn.hutool.core.date.DateUtil;
import org.springframework.util.ObjectUtils;

import java.util.*;

/**
 * @author HeZhuo
 * @date 2020/11/9
 */
public class ParentListToMap {

    public static void main(String[] args) {
        
        //将List使用map转化为父子集合形式，实用于Key - Object类型
        
        List<Schedule> list = new LinkedList<>();
        list.add(new Schedule("1-001", "1", DateUtil.date()));
        list.add(new Schedule("1-002", "1", DateUtil.date()));
        list.add(new Schedule("2-001", "2", DateUtil.date()));
        list.add(new Schedule("2-002", "2", DateUtil.date()));
        list.add(new Schedule("3-001", "3", DateUtil.date()));
        list.add(new Schedule("4-001", "4", DateUtil.date()));
        list.add(new Schedule("4-002", "4", DateUtil.date()));



        Map<String, List<String>> map = new TreeMap<>(Collections.reverseOrder());
        List<String> childList;
        String stationOrderId;

        for (Schedule schedule : list) {
            stationOrderId = schedule.getStationOrderId();
            childList = map.get(stationOrderId);
            if (ObjectUtils.isEmpty(childList)) {
                childList = new LinkedList<>();
                childList.add(schedule.getId());
                map.put(stationOrderId, childList);
                continue;
            }
            childList.add(schedule.getId());
            map.put(stationOrderId, childList);
        }

        System.out.println(map);
    }

}
