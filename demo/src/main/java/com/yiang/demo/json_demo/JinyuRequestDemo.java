package com.yiang.demo.json_demo;

import cn.hutool.crypto.SecureUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;

/**
 * @author HeZhuo
 * @date 2021/9/1
 */
public class JinyuRequestDemo {

    public static void main(String[] args) {
        String key = "8ca981d1869edff7f7b39e18babf491f";
        String userName = "15659633065";
        getProduct(key, userName);
        getOrder(key, userName);
    }

    public static void getProduct(String key, String userName) {
        JSONObject param  = new JSONObject();
        // 业务参数，每个接口不同
        param.set("commodityBranchId", 20000);
        // 格式类似于："{\"orderNo\":\"2120210420105917155202648\"}";
        String sign = SecureUtil.md5(param.toString() + key);
        String url = "http://open.shangmeng.top/api/UserOrder/GetCommodityInfo?userName="
                + userName + "&sign=" + sign;
        String post = HttpUtil.post(url, param.toString());
        System.out.println(post);
    }

    public static void getOrder(String key, String userName) {
        JSONObject param  = new JSONObject();
        param.set("orderNo", 20000);
        // 格式类似于："{\"orderNo\":\"2120210420105917155202648\"}";
        String sign = SecureUtil.md5(param.toString() + key);
        String url = "http://open.shangmeng.top/api/UserOrder/QueryOrderModel?userName="
                + userName + "&sign=" + sign;
        String post = HttpUtil.post(url, param.toString());
        System.out.println(post);
    }
}
