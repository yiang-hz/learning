package com.yiang.demo.json_demo;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @author HeZhuo
 * @date 2020/8/5
 */
public class MapInSqlToJson {

    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        map.put("1", "2");
        System.out.println(map.toString());
        JSONObject jsonObject = new JSONObject(map);
        System.out.println(jsonObject.toString());
        //缩进美化输出
        System.out.println(jsonObject.toStringPretty());
        JSONObject parse = JSONUtil.parseObj(jsonObject.toString());
        System.out.println(parse.getByPath("1"));
        System.out.println(parse);
    }
}
