package com.yiang.demo.json_demo;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class DemoJson {

    public static void main(String[] args) {
        JSONObject jsonObject = new JSONObject();
        //ID
        jsonObject.put("deptId", "4A3AA9C2752A4F319EDF0892D6E9ADFB");
        //数组
        JSONArray jsonArray = new JSONArray();
        JSONObject gradeItemsOne = new JSONObject();
        gradeItemsOne.put("gradeItemId", "8244C346-0FED-49C7-BC3F-163402813F5E");
        gradeItemsOne.put("deptItemScore", "5");
        JSONObject gradeItemsTwo = new JSONObject();
        gradeItemsTwo.put("gradeItemId", "90F1A9CE-BF31-481E-8E01-913D09C5E205");
        gradeItemsTwo.put("deptItemScore", "2");
        JSONObject gradeItemsThree = new JSONObject();
        gradeItemsThree.put("gradeItemId", "90CE5722-47D1-4D86-81DE-B1D738AA33C6");
        gradeItemsThree.put("deptItemScore", "5");
        jsonArray.add(gradeItemsOne);
        jsonArray.add(gradeItemsTwo);
        jsonArray.add(gradeItemsThree);

        //存放数组到json中
        jsonObject.put("gradeItems", jsonArray);

        System.out.println("完整JSON串：" + jsonObject);

        //获取gradeItems
        JSONArray gradeItems = (JSONArray) jsonObject.get("gradeItems");
        System.out.println(gradeItems);


        JSONObject gradeItemJSONObject;
        for (Object gradeItem : gradeItems) {
            //遍历集合gradeItems  并将gradeItem转化为JSONObject
            gradeItemJSONObject = (JSONObject) gradeItem;
            System.out.println("gradeItem的JSONObject" + gradeItemJSONObject);
            System.out.println("获取的gradeItemId：" + gradeItemJSONObject.get("gradeItemId"));
            System.out.println("获取的deptItemScore：" + gradeItemJSONObject.get("deptItemScore"));
        }

    }
}
