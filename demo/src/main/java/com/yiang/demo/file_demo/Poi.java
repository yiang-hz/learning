package com.yiang.demo.file_demo;

import cn.hutool.core.convert.Convert;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author HeZhuo
 * @date 2021/6/18
 */
public class Poi {

    public static void main(String[] args) {
        String path = "C:\\Users\\以安\\Desktop\\景观云\\供应链系统\\脚本更新\\脱敏.xlsx";
        ExcelReader reader = ExcelUtil.getReader(path);
        List<Map<String,Object>> readAll = reader.readAll();
        String str = "徐英豪自动化网络科技公司";
        for (Map<String, Object> stringObjectMap : readAll) {
            str = str.replace(Convert.toStr(stringObjectMap.get("替换前")),
                    Convert.toStr(stringObjectMap.get("替换后")));
        }
        System.out.println(str);
    }
}
