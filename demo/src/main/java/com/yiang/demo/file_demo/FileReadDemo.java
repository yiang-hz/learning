package com.yiang.demo.file_demo;

import cn.hutool.core.io.FileUtil;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

/**
 * @author HeZhuo
 * @date 2020/9/10
 */
public class FileReadDemo {

    public static void main(String[] args) {
//        String imageUrl = "http://cdn.yiangus.com/alipayCertPublicKey_RSA2.crt";
//        BufferedImage bufferedImage = ImageIO.read(new URL(imageUrl));
//        ImageIO.write(bufferedImage, formatName, outputStream);
//        byte[] bytes = outputStream.toByteArray();
//        FileUtil.writeBytes(bytes)


//        File file = FileUtil.file("http://file.bpy.com/alipay/2021001182614741/alipayRootCert");
//        System.out.println(file);
//        File f2 = FileUtil.file("http://file.bpy.com/alipay/2021001182614741/alipayRootCert.crt");
//        System.out.println(f2);

    }
    /**
     * @param certPath 证书路径
     */
    public static X509Certificate getCertFromPath(String certPath) throws Exception {
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(certPath);
            CertificateFactory cf = CertificateFactory.getInstance("X.509", "BC");
            return (X509Certificate) cf.generateCertificate(inputStream);
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                throw new Exception(e);
            }
        }
    }

}
