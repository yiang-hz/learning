package com.yiang.demo.file_demo;

import cn.hutool.core.io.FileUtil;
import cn.hutool.crypto.SecureUtil;

/**
 * @author HeZhuo
 * @date 2020/9/14
 */
public class EncryptFilePath {

    public static void main(String[] args) {

        String s = encryptPath("\\wwwroot\\file\\cert\\alipay\\2021001182614741\\20200914141410\\appCertPublicKey_2021001182614741.crt");
        System.out.println(s);
    }

    public static String encryptPath(String path) {
        String fileName = FileUtil.mainName(path);
        String md5Name = SecureUtil.md5(fileName);
        String getPath = path.replace(fileName, md5Name);
        System.out.println(path);
        System.out.println(getPath);
        return getPath;
    }
}
