package com.yiang.demo.file_demo;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.http.HttpUtil;

import java.io.File;

/**
 * @author HeZhuo
 * @date 2020/9/11
 */
public class FileDownHttpDemo {

    public static void main(String[] args) {
        String prePath = "/wwwroot/file/cert/";
        String preUrl = "http://file.bpy.com/";
        String two = "alipay/2021001182614741/";
        DateTime date = DateUtil.date();
        String format = DateUtil.format(date, "yyyyMMddHHmmss");
        String fileName = "/alipayRootCert.crt";
        boolean exist = FileUtil.exist(prePath + two + format + fileName);
        if (exist){
            System.out.println("获取成功！");
        } else{
            byte[] bytes = HttpUtil.get(preUrl + two + fileName).getBytes();
            File file = FileUtil.writeBytes(bytes, prePath + two + format + fileName);
            System.out.println(file.getPath() + file.getAbsolutePath());
        }
    }
}
