package com.yiang.demo.file_demo;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileReader;
import cn.hutool.core.io.file.FileWriter;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 替换文件类容工具类
 * @author HeZhuo
 * @date 2021/6/18
 */
public class ReplaceFileContext {

    static Map<String, String> replaceNameKey = new HashMap<>();

    static {
        replaceNameKey.put("张芳芳", "赵敏");
        replaceNameKey.put("徐英豪", "周芷若");
        replaceNameKey.put("刘士礼", "殷离");
        replaceNameKey.put("吴浩", "殷素素");
        replaceNameKey.put("罗威", "纪晓芙");
        replaceNameKey.put("卢星", "杨逍");
        replaceNameKey.put("艾青", "谢逊");
        replaceNameKey.put("陈潇", "杨不悔");
        replaceNameKey.put("杨正明", "张中");
        replaceNameKey.put("杨zm", "李天恒");
        replaceNameKey.put("谢伟军", "张三丰");
        replaceNameKey.put("罗谭珍", "张翠山");
        replaceNameKey.put("郝安康", "宋青书");
        replaceNameKey.put("琬湘杰", "成昆");
        replaceNameKey.put("刘雪洋", "汝阳王");
        replaceNameKey.put("wuhao", "赵玲珠");
        replaceNameKey.put("xiaobing", "唐洋");
        replaceNameKey.put("肖兵", "韦一笑");
        replaceNameKey.put("陈彦博", "黛绮丝");
        replaceNameKey.put("庄鹏成", "周颠");
    }

    public static void main(String[] args) {

        //本地文件夹路径，会遍历文件夹下所有文件
        String path = "C:\\Users\\以安\\Desktop\\景观云\\供应链系统\\脚本更新\\20210617-after\\";
        List<File> files = FileUtil.loopFiles(path);
        for (File file : files) {
            System.out.println("文件替换中...当前文件为：" + file.getName());
            replace(file);
        }
        System.out.println("文件变更完成....");
    }

    private static void replace(File file) {
        //默认UTF-8编码，可以在构造中传入第二个参数做为编码
        FileReader fileReader = new FileReader(file);
        String result = fileReader.readString();

        for (Map.Entry<String, String> entry : replaceNameKey.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            result = result.replaceAll(key, value);
        }

        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write(result);
    }
}
