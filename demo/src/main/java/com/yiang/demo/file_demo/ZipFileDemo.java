package com.yiang.demo.file_demo;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.ZipUtil;
import org.springframework.util.ObjectUtils;

import java.io.File;
import java.util.List;

/**
 * @author HeZhuo
 * @date 2022/3/3
 */
public class ZipFileDemo {

    public static void main(String[] args) throws Exception {

        // 1.读取Zip所有的文件，并且读取文件夹内部的PDF名称
        String zipPath = "D:\\zip\\test\\1.zip";
        File unzip = ZipUtil.unzip(zipPath, CharsetUtil.CHARSET_GBK);
        File[] files1 = unzip.listFiles();
        if (ObjectUtils.isEmpty(files1)) {
            throw new Exception("FUCK");
        }
        for (File file : files1) {
            String name = file.getName();
            System.out.println(name);
            if (name.contains("09")) {
                continue;
            }
            boolean exist = FileUtil.exist(file.getAbsolutePath(), ".+([.][Pp][Dd][Ff]){1}");
            if (!exist) {
                System.out.println("糟了，文件格式不匹配了");
            }
        }

    }

    private static void a() {
        String xmlPath = "D:\\zip\\20220126\\filingRegister\\C99999-113-20220126-04.xml";

        // 1.读取Zip所有的文件，并且读取文件夹内部的PDF名称
        String zipPath = "D:\\zip\\1\\1.zip";

        File copy = FileUtil.copy(zipPath, "D:\\zip\\20220126\\filingRegister2\\" + "1.zip", true);

        File unzip = ZipUtil.unzip(copy, CharsetUtil.CHARSET_GBK);
        System.out.println(unzip.getName());

        if (unzip.isDirectory()) {
            List<File> files = FileUtil.loopFiles(unzip);

            for (File file : files) {
                // 获取父类文件夹名称
                System.out.println(FileUtil.getParent(file, 1).getName());
                System.out.println("附件名称:" + file.getName());
            }
        }


        // 2.文件复制
        unzip = FileUtil.copy(xmlPath, unzip.getAbsolutePath(), true);
        System.out.println(unzip);

        File zip = ZipUtil.zip(unzip);

        FileUtil.del(unzip);
        FileUtil.rename(zip, "awdwadwad", true,true);
    }
}
