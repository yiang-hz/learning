package com.yiang.demo.file_demo;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileReader;
import cn.hutool.core.io.file.FileWriter;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * 替换文件类容工具类
 * @author HeZhuo
 * @date 2021/6/18
 */
public class ReplaceFileContextByExcel {

    public static void main(String[] args) {

        //本地文件夹路径，会遍历文件夹下所有文件
        String path = "C:\\Users\\以安\\Desktop\\景观云\\供应链系统\\脚本更新\\20210617-after\\";
        List<File> files = FileUtil.loopFiles(path);

        String rulePath = "C:\\Users\\以安\\Desktop\\景观云\\供应链系统\\脚本更新\\脱敏.xlsx";
        ExcelReader reader = ExcelUtil.getReader(rulePath);
        List<Map<String,Object>> readAll = reader.readAll();

        for (File file : files) {
            System.out.println("文件替换中...当前文件为：" + file.getName());
            replace(file, readAll);
        }
        System.out.println("文件变更完成....");
    }

    private static void replace(File file, List<Map<String,Object>> readAll) {
        //默认UTF-8编码，可以在构造中传入第二个参数做为编码
        FileReader fileReader = new FileReader(file);
        String result = fileReader.readString();

        for (Map<String, Object> stringObjectMap : readAll) {
            result = result.replaceAll(Convert.toStr(stringObjectMap.get("替换前")),
                    Convert.toStr(stringObjectMap.get("替换后")));
        }

        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write(result);
    }
}
