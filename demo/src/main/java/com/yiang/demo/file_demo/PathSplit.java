package com.yiang.demo.file_demo;

import java.util.Arrays;

/**
 * @author HeZhuo
 * @date 2021/12/15
 */
public class PathSplit {

    public static void main(String[] args) {
        String path = "D:\\zip\\20211203\\zzlcIdentifyInfo\\Z70033-203-20211203-03.xml";
        String[] split = path.split("\\\\");
        System.out.println(path);
        System.out.println(Arrays.toString(split));
        String pathL = "/dyuu/dwd./dwdw";
        String[] split1 = pathL.split("/");
        System.out.println(Arrays.toString(split1));
    }
}
