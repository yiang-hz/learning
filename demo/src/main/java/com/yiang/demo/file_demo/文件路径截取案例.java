package com.yiang.demo.file_demo;

import cn.hutool.core.io.FileUtil;

/**
 * @author HeZhuo
 * @date 2022/3/21
 */
public class 文件路径截取案例 {

    public static void main(String[] args) {

        // 纯Windows 双\\
        String filePathWindowsDouble = "D:\\zip\\1\\09--\\住宿发票 - 副本 (9).pdf";
        System.out.println("windows路径：" + filePathWindowsDouble);
        System.out.println("返回名称：" + getFileNameByPath(filePathWindowsDouble));

        // 纯Linux 双//
        String filePathLinuxDouble = "D://zip//1//09--//住宿发票 - 副本 (9).pdf";
        System.out.println("Linux双//路径：" + filePathLinuxDouble);
        System.out.println("返回名称：" + getFileNameByPath(filePathWindowsDouble));

        // 纯Linux 单//
        String filePathLinux = "D:/zip/1/09--/住宿发票 - 副本 (9).pdf";
        System.out.println("Linux路径：" + filePathLinux);
        System.out.println("返回名称：" + getFileNameByPath(filePathLinux));
    }

    private static String getFileNameByPath(String filePath) {
        String fileName = filePath.replace(FileUtil.FILE_SEPARATOR, "/");
        return fileName.substring(fileName.lastIndexOf("/") + 1);
    }
}
