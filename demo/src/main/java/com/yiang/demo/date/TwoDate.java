package com.yiang.demo.date;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author HeZhuo
 * @date 2020/7/9
 */
@Component
public class TwoDate {

    public static void main(String[] args) {
        String dateStr1 = "2020-07-09 10:43:36";
        Date date1 = DateUtil.parse(dateStr1);

        String dateStr2 = "2020-07-09 10:32:35";
        Date date2 = DateUtil.parse(dateStr2);

        //相差一个月，31天
        long betweenDay = DateUtil.between(date1, date2, DateUnit.MINUTE, false);

        System.out.println(betweenDay);

        System.out.println(
                //结束日期减去开始日期
                (date1.getTime() - date2.getTime())
                        /
                //除以分钟（1分钟 = 60秒 = 60*1000毫秒）
                (60 * 1000)
        );

    }
}
