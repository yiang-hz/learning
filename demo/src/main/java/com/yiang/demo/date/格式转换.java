package com.yiang.demo.date;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;

import java.util.Date;

/**
 * @author HeZhuo
 * @date 2020/9/21
 */
public class 格式转换 {

    public static void main(String[] args) {
        String str = "2020-09-21T03:06:32.000Z";
        Date date = new Date("2020-09-21T03:06:32.000Z");
        DateTime parse = DateUtil.parse(date.toString());
        System.out.println(parse);
    }
}
