package com.yiang.demo.date;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.CharUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.script.ScriptUtil;

import javax.script.CompiledScript;
import javax.script.ScriptEngine;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author HeZhuo
 * @date 2020/12/23
 */
public class StringToDate {

    public static void main(String[] args) {

        System.out.println(Integer.valueOf("2.50".substring("2.50".indexOf(CharUtil.DOT) + 1)));

        System.out.println(DateUtil.parseDate(DateUtil.formatDate(new Date())));

        //开平需求的价格取值
        String str = "ZYDJ2020122300066";
        String regEx="[^0-9]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);

        str = m.replaceAll("").trim();
        System.out.println(str);

        String dateStr = StrUtil.sub(str, 0, 8);
        System.out.println(dateStr);
        DateTime dateTime = DateUtil.parse(dateStr, DatePattern.PURE_DATE_PATTERN);
        System.out.println(dateTime);

        String countStr = str.substring(8);
        System.out.println(countStr);
    }
}
