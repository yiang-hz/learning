package com.yiang.demo.date;

import cn.hutool.core.date.StopWatch;

/**
 * @author HeZhuo
 * @date 2020/9/1
 */
public class CalcTimeDemo {

    public static void main(String[] args) {
        //计算程序运行的时间 JAVA程序计时器
        StopWatch stopwatch = new StopWatch();
        stopwatch.start();

        for (int i = 0; i < 99999; i++) {
            System.out.print(i);
        }
        System.out.println();

        stopwatch.stop();
        System.out.println("mappingBySpringBeanUtils cost :" + stopwatch.getTotalTimeMillis());
    }
}
