package com.yiang.demo.date;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author HeZhuo
 * @date 2022/3/8
 */
public class 日期排序 {

    public static void main(String[] args) {
        for (int j = 1; j <= 100; j++) {
            System.out.println(j);
        }
        List<Date> dateList = new ArrayList<>();
        dateList.add(new Date());
        dateList.add(DateUtil.parse("20220308"));
        dateList.add(DateUtil.parse("20220307"));
        dateList.add(DateUtil.parse("20220310"));
        Date maxDate = null;
        Date minDate = null;
        for (Date date : dateList) {
            DateTime parse = DateUtil.parse(Convert.toStr(date));
            if (ObjectUtils.isEmpty(maxDate)) {
                maxDate = parse;
            }
            if (ObjectUtils.isEmpty(minDate)) {
                minDate = parse;
            }
            int compare = DateUtil.compare(maxDate, parse);
            if (compare < 1) {
                maxDate = parse;
            }
            int min = DateUtil.compare(minDate, parse);
            if (min == 1) {
                minDate = parse;
            }
        }
        System.out.println(minDate);
        System.out.println(maxDate);

    }
}
