package com.yiang.demo.date;

import cn.hutool.core.date.DateUtil;

/**
 * @author HeZhuo
 * @date 2020/8/5
 */
public class Now {

    public static void main(String[] args) {
        System.out.println(DateUtil.date());
        System.out.println(DateUtil.date().toDateStr());
    }
}
