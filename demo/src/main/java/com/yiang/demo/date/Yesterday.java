package com.yiang.demo.date;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;

import java.util.Date;

/**
 *
 * @author HeZhuo
 * @date 2020/8/4
 */
public class Yesterday {

    public static void main(String[] args) {
        //https://www.hutool.cn/docs/#/core/%E6%97%A5%E6%9C%9F%E6%97%B6%E9%97%B4/%E6%97%A5%E6%9C%9F%E6%97%B6%E9%97%B4%E5%B7%A5%E5%85%B7-DateUtil
        DateTime yesterday = cn.hutool.core.date.DateUtil.yesterday();
        System.out.println(yesterday);
        //一天的开始，结果：2017-03-01 00:00:00
        Date beginOfDay = DateUtil.beginOfDay(yesterday);

        //一天的结束，结果：2017-03-01 23:59:59
        Date endOfDay = DateUtil.endOfDay(yesterday);

        System.out.println("start:" + beginOfDay + " -- end:" + endOfDay);
    }
}
