package com.yiang.demo.date;

import cn.hutool.core.date.DateUtil;

/**
 * @author HeZhuo
 * @date 2020/9/15
 */
public class FormatDemo {

    public static void main(String[] args) {
        //默认为 2020-09-15 年月日
        System.out.println(DateUtil.formatDate(DateUtil.date()));
        System.out.println(DateUtil.format(DateUtil.date(), "yyyyMMddHHmmss"));
    }
}
