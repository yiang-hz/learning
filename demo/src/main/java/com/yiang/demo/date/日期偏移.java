package com.yiang.demo.date;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;

/**
 * @author HeZhuo
 * @date 2021/12/4
 */
public class 日期偏移 {

    public static void main(String[] args) {
        System.out.println(DateUtil.offsetDay(DateUtil.date(), -1));
        DateTime yesterday = DateUtil.yesterday();
        System.out.println(yesterday);

        DateTime date = DateUtil.date();
        System.out.println(date);
        String day = DateUtil.formatDate(date);
        System.out.println(day);

        System.out.println(FileUtil.writeBytes("11".getBytes(), "utt/dd"));
    }
}
