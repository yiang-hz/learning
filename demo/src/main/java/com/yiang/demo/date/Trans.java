package com.yiang.demo.date;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;

import java.util.Date;

/**
 * @author HeZhuo
 * @date 2020/9/9
 */
public class Trans {

    public static void main(String[] args) {
        String s = DateUtil.formatDate(new Date());
        System.out.println(s);
        DateTime dateTime = DateUtil.parseDate(s);
        System.out.println(dateTime);
        String a = "2020-09-09 20:20:20";

        System.out.println(DateUtil.dayOfWeekEnum(DateUtil.date()));

    }
}
