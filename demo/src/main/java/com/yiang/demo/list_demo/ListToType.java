package com.yiang.demo.list_demo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author HeZhuo
 * @date 2021/10/20
 */
public class ListToType {

    public static void main(String[] args) {
        List<A> aList = new ArrayList<>();
        aList.add(new A("1", "张三"));
        aList.add(new A("2", "李四"));
        List<String> list = aList.stream().map(A::getName).collect(Collectors.toList());
        System.out.println(list);
    }

    @Data
    @AllArgsConstructor
    static class A {
        private String id;
        private String name;
    }
}
