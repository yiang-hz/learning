package com.yiang.demo.list_demo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author HeZhuo
 * @date 2020/12/17
 */
public class SetDemo {

    public static void main(String[] args) {
        List<Person> people = new ArrayList<>();
        people.add(new Person("1", "张三"));
        people.add(new Person("2", "张三"));
        people.add(new Person("1", "李四"));
        people.add(new Person("1", "张三"));
        people.add(new Person("2", "李四"));
        people.add(new Person("2", "王五"));
        Set<Person> personSet = new HashSet<>(people);
        System.out.println(personSet);
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    static class Person {
        private String id;
        private String name;
    }
}
