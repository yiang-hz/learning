package com.yiang.demo.list_demo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author HeZhuo
 * @date 2020/8/21
 */
public class Filter {

    public static void main(String[] args) {

        List<String> userList = new ArrayList<>();
        userList.add("2");
        userList.add("1");
        System.out.println("before Filter：" + userList);
        //过滤掉当前租户
        userList = userList.stream().filter(user -> !"2".equals(user)).collect(Collectors.toList());
        System.out.println("After Filter：" + userList);
    }
}
