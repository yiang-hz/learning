package com.yiang.demo.list_demo;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HeZhuo
 * @date 2020/12/3
 */
public class Compare {

    public static void main(String[] args) {
        List<Integer> a = new ArrayList<>();
        List<Integer> b = new ArrayList<>();
        a.add(1);
        a.add(2);
        a.add(3);
        a.add(4);
        a.add(1);
        a.add(1);

        b.add(1);
        b.add(2);
        b.add(3);
        b.add(4);
        b.add(1);
        b.add(1);

        System.out.println(JSONObject.parseArray(JSONObject.toJSON(a).toString()).toJSONString());
        boolean b1 = CollUtil.containsAll(a, b);
        System.out.println(b1);
    }
}
