package com.yiang.demo.list_demo;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author HeZhuo
 * @date 2020/12/10
 */
public class SortPrice {

    public static void main(String[] args) {
        DateUtil.date();
        new Date();
        List<String> strList = Convert.toList(String.class, "1,2,3,4");
        System.out.println(strList);

        List<A> aList = new ArrayList<>();
        aList.add(new A("-2"));
        aList.add(new A("-3"));
        aList.add(new A("2"));
        aList.add(new A("3"));
        aList.add(new A("1"));
        aList.add(new A("-1"));

        List<BigDecimal> bigDecimals = aList
                .stream()
                .map(a -> Convert.toBigDecimal(a.getPrice()))
                .collect(Collectors.toList());
        CollectionUtil.sort(bigDecimals, Comparator.comparing(BigDecimal::abs));
        bigDecimals.forEach(a -> System.out.println(a.intValue()));
        System.out.println(bigDecimals);
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    static class A {
        private String price;
    }
}
