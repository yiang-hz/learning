package com.yiang.demo.list_demo;

import cn.hutool.core.collection.ListUtil;

import java.util.List;

public class SubDemo {

    public static void main(String[] args) {

        List<Integer> of = ListUtil.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
        List<Integer> sub = ListUtil.sub(of, 0, 10);
        List<Integer> sub2 = ListUtil.sub(of, 10, 12);
        System.out.println(sub);
        System.out.println(sub2);

    }
}
