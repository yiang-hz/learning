package com.yiang.demo.list_demo;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.convert.Convert;

import java.util.List;

/**
 * @author HeZhuo
 * @date 2020/12/5
 */
public class LazyLoad {

    public static void main(String[] args) {
        List<String> ruleList = null;
        for (int i = 1; i <= 5; i++) {
            if (CollectionUtil.isEmpty(ruleList)) {
                ruleList = setList();
            }
            System.out.println(ruleList);
        }
    }

    private static List<String> getList(int i) {
        System.out.println("调用了" + i);
        return Convert.toList(String.class, "1,2,3");
    }

    private static List<String> setList() {
        System.out.println("setList用了");
        return Convert.toList(String.class, "1,2,3");
    }
}
