package com.yiang.demo.list_demo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HeZhuo
 * @date 2021/10/4
 */
public class MoreList {

    public static void main(String[] args) {
        success();
    }

    private static void success() {

        // 新增的商品 ID为1 数量为1
        CatObj newObj = new CatObj(1,1);
        List<CatObj> objectList = new ArrayList<>();

        // 模拟购物车原有数据 ID为1的1条 ID为2的1条 可以通过注释ID为1的数据模拟不同场景
        //objectList.add(new CatObj(1, 1));
        objectList.add(new CatObj(1, 2));

        // 设置控制器决定最后是否需要添加
        boolean b = true;

        for (CatObj o : objectList) {
            // 寻找相同ID的数据，如果有就数量+1 关闭控制器b
            if (o.getId().equals(newObj.getId())) {
                // 数量 + 1
                o.setCount(o.getCount() + 1);
                b = false;
                // 找到后直接终止循环，避免额外执行
                break;
            }
        }

        // 如果集合不存在数据，则代表需要增加一条新数据
        if (b) {
            objectList.add(newObj);
        }

        System.out.println(objectList);
    }

    static class CatObj {

        private Integer count;
        private Integer id;

        public CatObj(Integer count, Integer id) {
            this.count = count;
            this.id = id;
        }

        @Override
        public String toString() {
            return "CatObj{" +
                    "count=" + count +
                    ", id=" + id +
                    '}';
        }

        public Integer getCount() {
            return count;
        }

        public Integer getId() {
            return id;
        }

        public void setCount(Integer count) {
            this.count = count;
        }

        public void setId(Integer id) {
            this.id = id;
        }
    }
}
