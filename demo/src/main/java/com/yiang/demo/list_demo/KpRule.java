package com.yiang.demo.list_demo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author HeZhuo
 * @date 2020/12/4
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("宽厚长基础匹配规则表")
public class KpRule {

    @ApiModelProperty("匹配名称KEY")
    private String key;

    @ApiModelProperty("匹配名称Value")
    private String value;

    @ApiModelProperty("匹配类型 厚度 1  宽度 2  长度3")
    private Integer type;

    @ApiModelProperty("品名ID")
    private String productBrandId;
}