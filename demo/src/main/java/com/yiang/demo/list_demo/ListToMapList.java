package com.yiang.demo.list_demo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author HeZhuo
 * @date 2020/12/5
 */
public class ListToMapList {

    private Map<Integer, List<KpRule>> getRuleListMap() {
        // 找到厚度与宽度的匹配规则
        List<KpRule> kpRuleList = new ArrayList<>();

        //将规则List根据type类型分割成Map<Integer,List> 以类型作为Key
        Map<Integer, List<KpRule>> mapRule = kpRuleList.stream().collect(Collectors.toMap(KpRule::getType,
                kpRule ->  {
                    List<KpRule> getNameList = new ArrayList<>();
                    getNameList.add(kpRule);
                    return getNameList;
                },
                (List<KpRule> v1, List<KpRule> v2) -> {
                    v1.addAll(v2);
                    return v1;
                }
        ));
        return mapRule;
    }
}
