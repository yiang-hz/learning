package com.yiang.demo.list_demo;

import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.lang.Assert;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @author HeZhuo
 * @date 2020/12/25
 */
public class GetAndGet {

    public static void main(String[] args) {

        Assert.notEmpty(new LinkedHashMap<>(), "fuckYou");

        List<Bu> bus = new ArrayList<>();
        Bu zs = new Bu("张三", ListUtil.toList("1,2,3"));
        bus.add(zs);

        Bu ls = new Bu("李四", ListUtil.toList("1,2,3"));

        System.out.println(bus.get(bus.indexOf(ls)));

        System.out.println(bus);
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    static class Bu {
        private String name;
        private List<String> stringList;
    }
}
