package com.yiang.demo.list_demo;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.convert.Convert;

import java.util.Comparator;
import java.util.List;

/**
 * @author HeZhuo
 * @date 2020/12/5
 */
public class ListSort {

    public static void main(String[] args) {
        String a = "25,30,45,50,29,18";
        List<Integer> objects = Convert.toList(Integer.class, a);
        System.out.println(objects);
        System.out.println(objects.get(0));

//        Collections.sort(objects);
//        System.out.println(objects);

        List<Integer> sort = CollectionUtil.sort(objects, Comparator.reverseOrder());
        System.out.println(objects);
        System.out.println(sort);

        sort.forEach(System.out::println);

    }
}
