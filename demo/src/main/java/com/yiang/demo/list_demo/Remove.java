package com.yiang.demo.list_demo;

import cn.hutool.core.collection.ListUtil;

import java.util.ArrayList;

/**
 * @author HeZhuo
 * @date 2021/6/24
 */
public class Remove {

    public static void main(String[] args) {
        ArrayList<Integer> integers = ListUtil.toList(1, 2, 3, 4, 5);
        for (Integer integer : integers) {
            System.out.println(integers.size());
            if (integer > 2) {
                integers.remove(integer);
            }
        }
        System.out.println(integers);
    }
}
