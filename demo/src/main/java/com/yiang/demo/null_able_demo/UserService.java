package com.yiang.demo.null_able_demo;

import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Service
public interface UserService {

    User get(@NotNull Integer id);

    Optional<User> getUser(@NotNull Integer id);
}
