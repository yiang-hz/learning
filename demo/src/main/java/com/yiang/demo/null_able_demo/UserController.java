package com.yiang.demo.null_able_demo;

import java.util.Optional;

public class UserController {

    UserService userService;

    public void findUser(){
        Integer id = null;
        User user = userService.get(id);
        System.out.println(user);
    }

    public static void main(String[] args) {
        UserServiceImpl userService = new UserServiceImpl();
        Integer id = 1;
        User user2 = userService.get(id);
        System.out.println(user2);

        Optional<User> user = userService.getUser(id);
        user.ifPresent(user1 -> user1.setId(2));
        if (user.isPresent()){
            System.out.println(user.get());
            System.out.println(1);
        }

    }
}
