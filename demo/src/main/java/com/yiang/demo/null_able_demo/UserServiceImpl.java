package com.yiang.demo.null_able_demo;

import javax.validation.constraints.NotNull;
import java.util.Optional;

public class UserServiceImpl implements UserService{
    @Override
    public User get(@NotNull Integer id) {
        return getIs(id);
    }

    @Override
    public Optional<User> getUser(@NotNull Integer id) {
        return Optional.ofNullable(getIs(id));
    }

    public User getIs(Integer id){
        if (id == 1){
            return new User(1, "张三");
        }
        return null;
    }
}
