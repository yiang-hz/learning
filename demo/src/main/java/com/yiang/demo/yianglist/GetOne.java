package com.yiang.demo.yianglist;

import java.util.*;

/**
 * @author HeZhuo
 * @date 2020/6/9
 */
public class GetOne {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20
        );
        Collections.shuffle(list);
        System.out.println(list);
        List randomList = createRandomList2(list, 6);
        System.out.println(randomList);

    }

    private static List createRandomList2(List list, int n) {
        HashMap map = new HashMap(list.size());
        List listNew = new ArrayList();
        if (list.size() <= n) {
            return list;
        } else {
            Random r = new Random();
            while (map.size() < n) {
                int random = (r.nextInt(list.size()));
                if (!map.containsKey(random)) {
                    map.put(random, n);
                    listNew.add(list.get(random));
                }
            }
            return listNew;
        }
    }
}
