package com.yiang.demo.arithmetic;

import java.util.Scanner;

/**
 * 回文数
 */
public class Test06 {
 
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入一行字符串");
		String str = sc.nextLine();
		System.out.println(check(str.toUpperCase())); //将输入的字符全转为大写字母
	}

	private static boolean check(String t) {
		for (int i = 0; i < t.length() / 2; i++) {
			if (t.charAt(i) != t.charAt(t.length() - i - 1)) {
				return false;
			}
		}
		return true;
	}
 
}