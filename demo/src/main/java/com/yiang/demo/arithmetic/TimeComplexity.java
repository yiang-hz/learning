package com.yiang.demo.arithmetic;

/**
 * 时间复杂度引发的log运算
 * @author HeZhuo
 * @date 2020/6/3
 */
public class TimeComplexity {

    public static void main(String[] args) {
        getLog(2, 3);
        getLog(3, 4);
    }

    /**
     * 输入底数与对数，求幂值以及log公司
     * @param a 底数
     * @param k 对数
     */
    private static void getLog(int a, int k) {
        // sum 为计算后的总值 也就是 真数
        int sum = 1;
        for (int i = 0; i < k; i++) {
            sum *= a;
        }
        System.out.println();
        System.out.println("底数：" + a + "对数：" + k + "真数：" + sum);
        System.out.println(a + " ^ " + k + " = " + + sum);
        System.out.println("公式为 对数 = log(底数)(真数)");
        System.out.println("结果为：" + k + " = log(" + a + ")(" + sum + ")");
    }
}
