package com.yiang.demo.arithmetic;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HeZhuo
 * @date 2021/8/13
 */
public class Recursion {

    private static final List<Integer> LIST = new ArrayList<>();
    private static Integer count = 0;

    public static void main(String[] args) {
        // 一个简单的通过递归算法来代理循环的打印
        LIST.add(1);LIST.add(2);LIST.add(3);
        recursion();
    }

    private static void recursion() {
        if (count >= LIST.size()) {
            return;
        }
        System.out.println(LIST.get(count++));
        recursion();
    }
}
