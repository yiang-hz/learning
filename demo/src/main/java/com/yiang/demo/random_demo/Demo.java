package com.yiang.demo.random_demo;

import cn.hutool.core.util.RandomUtil;

import java.util.Random;

/**
 * @author HeZhuo
 * @date 2020/8/19
 */
public class Demo {

    public static void main(String[] args) {
        RandomUtil randomUtil = new RandomUtil();
        int i = RandomUtil.randomInt(0, 1);
        System.out.println(i);
    }
}
