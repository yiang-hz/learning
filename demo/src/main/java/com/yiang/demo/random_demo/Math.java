package com.yiang.demo.random_demo;

import cn.hutool.core.util.RandomUtil;

/**
 * @author HeZhuo
 * @date 2021/2/19
 */
public class Math {

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            int result = RandomUtil.randomInt(0, 2);
            System.out.println(result >= 1);
        }
    }
}
