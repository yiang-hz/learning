package com.yiang.demo.math;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author HeZhuo
 * @date 2021/8/4
 */
public class Come {

    public static void main(String[] args) {
        // 10000元 每年福利百分之十个点 那么三十年就是十七万。
        BigDecimal i = BigDecimal.valueOf(10000);
        for (int j = 0; j < 30; j++) {
            i = i.multiply(BigDecimal.valueOf(1.1)).setScale(2, RoundingMode.HALF_UP);
        }
        System.out.println(i);
    }
}
