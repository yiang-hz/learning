package com.yiang.demo.math;

import cn.hutool.core.collection.CollectionUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author HeZhuo
 * @date 2021/5/7
 */
public class 交集与并集 {

    public static void main(String[] args) {
        List<Integer> a = CollectionUtil.toList(1, 3, 5, 6);
        List<Integer> b = CollectionUtil.toList(1, 2, 3, 4);
        List<Integer> c = CollectionUtil.toList(1, 2, 3, 4 , 5);
        ArrayList<List<Integer>> toList = CollectionUtil.toList(a, b, c);
        System.out.println(retainElementList(toList));
        System.out.println(getResult(toList));
    }

    private static List<Integer> retainElementList(List<List<Integer>> elementLists) {
        Optional<List<Integer>> result = elementLists.parallelStream()
                .filter(CollectionUtil::isNotEmpty)
                .reduce((a, b) -> {
                    a.retainAll(b);
                    return a;
                });
        return result.orElse(new ArrayList<>());
    }

    private static List<Integer> getResult(List<List<Integer>> elementLists){
        /*
         * 类推内容与结果：[{1,2} {1,2,3} {1,2,3,4}] -> {1,2}
         * 过程：
         * [{1,2} {1,2,3} {1,2,3,4}]
         * 遍历集合
         * for 1： resultStoreIds{1,2}
         * for 2：{1,2} | {1,2,3}
         *  == {1,2}
         * for 3：
         *  =={1,2}
         */
        List<Integer> resultStoreIds = new ArrayList<>();
        for (List<Integer> list : elementLists) {
            if (CollectionUtil.isEmpty(list)) {
                continue;
            }
            if (CollectionUtil.isEmpty(resultStoreIds)) {
                resultStoreIds = list;
                continue;
            }
            resultStoreIds = resultStoreIds.stream().filter(list::contains).collect(Collectors.toList());
        }
        return resultStoreIds;
    }
}
