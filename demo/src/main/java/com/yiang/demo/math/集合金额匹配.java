package com.yiang.demo.math;

import java.util.*;

public class 集合金额匹配 {

    public static void main(String[] args) {
        List<Integer> a = new LinkedList<>();
        a.add(20);
        a.add(50);
        a.add(100);
        a.sort(Comparator.comparingInt(value -> (int) value).reversed());
        System.out.println(a);

        Integer price = 17001;

        // 金额，数量
        Map<Integer, Integer> map = new LinkedHashMap<>();

        for (Integer money : a) {
            boolean dy = money > price;
            if (dy) {
                continue;
            }
            int count = price / money;
            map.put(money, count);

            price = price - (count * money);

            if (price == 0) {
                System.out.println("匹配完成");
                break;
            }
        }

        if (price != 0) {
            System.out.println("匹配未完成");
        }
        System.out.println(map);
    }
}
