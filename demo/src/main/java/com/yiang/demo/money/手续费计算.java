package com.yiang.demo.money;

import cn.hutool.core.convert.Convert;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author HeZhuo
 * @date 2022/7/18
 */
public class 手续费计算 {

    public static void main(String[] args) {
        BigDecimal money = Convert.toBigDecimal("100542.20");
        BigDecimal price = Convert.toBigDecimal("100.54");
        BigDecimal a = price.divide(money, 4, RoundingMode.HALF_UP);
        System.out.println("手续费率为：" + a);

        BigDecimal b = money.multiply(a).setScale(2, RoundingMode.HALF_UP);
        System.out.println("手续费为：" + b);

        BigDecimal a1 = new BigDecimal("31.45");
        BigDecimal a2 = new BigDecimal("36.21");
        BigDecimal a3 = new BigDecimal("39.83");
        BigDecimal a4 = new BigDecimal("43.81");
        BigDecimal bigDecimal = a3.divide(a1).setScale(2);
        System.out.println(bigDecimal);

    }
}
