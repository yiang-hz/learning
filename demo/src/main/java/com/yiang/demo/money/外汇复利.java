package com.yiang.demo.money;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author HeZhuo
 * @date 2021/11/18
 */
public class 外汇复利 {

    static final List<Object> MONEY_LIST = new ArrayList<> (
            Arrays.asList(
                    // 10.11 - 10.14
                    "166.20", "218.60", "266.00", "273.48",
                    // 10.17 - 10.21  周六 10.22 周日 10.23
                    "141.53", "1406.48", "321.00", "1,531.87", "-8290.53",
                    // 10.24 - 10.28
                    "1,133.79", "1671.99", "10000.00", "2,681.60", "-12522.01",
                    // 10.31 - 11.04
                    "-2,559.8", "1,155.18", "1,506", "-11000", "268.68",
                    // e跟单
                    "266.66", "62.1", "733.11",
                    "179.30", "347.7", "-139.6", "559.92", "188.32", "104.9", "700.87",
                    "112.7", "1499.5", "1236.79", "239.42",
                    // 11.07 - 11.08
                    "-3.68", "-17.25",
                    // 11.09 - 11.20
                    "378.22",
                    // 11.21 -  11.25
                    "463", "213.61", "190.79", "19.30", "62.1",
                    // 11.28 - 12.02
                    "179.30", "559.92", "188.32", "700.87", "239.42", "-70000"
            )
    );




    public static void main(String[] args) {

        System.out.println("目前已过天数：" + MONEY_LIST.size());

        // 资金任务栏目
        test(10000, 0,"1.02",  2, 1);
//        test2(10000, 0,"1.02",  1);

        // 资金计划栏目
//        test(10000, 0,"1.04", 50, 1);


//        List<Object> help = new ArrayList<> (
//                Arrays.asList(
//                        // e跟单
//                        "266.66", "62.1", "733.11",
//                        "179.30", "347.7", "-139.6", "559.92", "188.32", "104.9", "566.62",
//                        "112.7", "1499.5", "1236.79"
//                        )
//        );
//
//        BigDecimal sum = BigDecimal.ZERO;
//        for (Object o : help) {
//            BigDecimal bigDecimal = Convert.toBigDecimal(o);
//            sum = sum.add(bigDecimal);
//        }
//        System.out.println("综合：" + sum);

    }

    //    @SuppressWarnings("all")
    private static void test2(Integer money, Integer rise, String rate, Integer money2) {

        // 目前的总金额
        BigDecimal sum = Convert.toBigDecimal(money);

        BigDecimal principal = Convert.toBigDecimal(money);
        BigDecimal old = principal;

        BigDecimal principal2 = Convert.toBigDecimal(money2);

        for (int i = 0; i < MONEY_LIST.size(); i++) {

            BigDecimal t = Convert.toBigDecimal(MONEY_LIST.get(i));
            sum = sum.add(t);

            principal = principal.multiply(Convert.toBigDecimal(rate)).setScale(2, RoundingMode.HALF_UP);
            principal = principal.add(Convert.toBigDecimal(rise));

            principal2 = principal2.multiply(Convert.toBigDecimal(rate)).setScale(2, RoundingMode.HALF_UP);
            principal2 = principal2.add(Convert.toBigDecimal(rise));

            BigDecimal toNo = sum.subtract(principal);

            // 为空跳段
            if (ObjectUtil.isEmpty(MONEY_LIST.get(i))) {
                System.out.println(
                        "跳段 第" + (i + 1) + "天=" + sum + " 原计划：" + principal +
                                " -- 差值：" + toNo +
                                " -- 资金需增值：" + principal.subtract(old) +
                                " 资金已增值：" + MONEY_LIST.get(i) +
                                " -- 后续操作手数：" + principal2
                );
            } else {
                System.out.println(
                        "第" + (i + 1) + "天=" + sum + " 原计划：" + principal +
                                " -- 差值：" + toNo +
                                " -- 资金需增值：" + principal.subtract(old) +
                                " 资金已增值：" + MONEY_LIST.get(i) +
                                " -- 后续操作手数：" + principal2
                );
            }

            old = principal;

            if (i + 1 == MONEY_LIST.size()) {
                System.out.println("----------------------------------------------------");
                principal = principal.multiply(Convert.toBigDecimal(rate)).setScale(2, RoundingMode.HALF_UP);
                principal = principal.add(Convert.toBigDecimal(rise));
                System.out.println(
                        "本日计划：目前=" + sum + " 原计划：" + principal +
                                " -- 日前差值：" + toNo +
                                " -- 本日需增值（负数时跳段）：" + principal.subtract(sum) +
                                " -- 原资金需增值：" + principal.subtract(old) +
                                " -- 操作手数：" + principal2
                );
            }

        }
    }

    @SuppressWarnings("all")
    private static void test(Integer money, Integer rise, String rate, Integer count,
                             Integer money2) {
        BigDecimal principal = Convert.toBigDecimal(money);
        BigDecimal old = principal;

        BigDecimal principal2 = Convert.toBigDecimal(money2);
        BigDecimal old2 = principal2;

        System.out.println(0 + "=" + principal + " -- 资金增值：" + principal.subtract(old) +
                " --资金段对应操作手数：" + principal2 + " --手数增值：" + principal2.subtract(old2));
        for (int i = 1; i <= count; i++) {
            principal = principal.multiply(Convert.toBigDecimal(rate)).setScale(2, RoundingMode.HALF_UP);
            principal = principal.add(Convert.toBigDecimal(rise));

            principal2 = principal2.multiply(Convert.toBigDecimal(rate)).setScale(2, RoundingMode.HALF_UP);
            principal2 = principal2.add(Convert.toBigDecimal(rise));
            System.out.println(i + "=" + principal + " -- 资金增值：" + principal.subtract(old) +
                    " --资金段对应操作手数：" + principal2 + " --手数增值：" + principal2.subtract(old2));
            old = principal;
            old2 = principal2;
        }

        System.out.println("-----------------------");
        System.out.println("复利：" +
                " 本金：" + money +
                " 周期续投入：" + rise +
                " 周期增长比例：" + rate +
                " 周期数：" + count +
                " 最终收益为：" + principal.setScale(2, RoundingMode.HALF_UP));
    }
}
