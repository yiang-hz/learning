package com.yiang.demo.money;

import cn.hutool.core.convert.Convert;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author HeZhuo
 * @date 2021/11/18
 */
public class CompoundInterest {

    public static void main(String[] args) {

        // 暂存1
        // paramDTO = 本金 * 率 + 本金 * 时间
        // 40年  每年百分之二十的复利  如果是1万 那么就是一千四百万  如果是十万 那么就是一点四个亿
        // 35年*1万*百分之二十的复利  ≈ 40年*一万*百分之一十的复利

        // 每个周赚三分之二本金的十个点。这是我的想法。跟着ruirui的操作，我觉得是可以做到的。
        // 也就是说4年做到三百万。
        // 年前，涨乐财富通+4，你才能玩成，第一步目标。并且17.7W是需要存在你的资金体系内的。

        // 3.15计算  0.1

//        // 一天百分之0.1  一年两百天 六年 23W
//        test(10000, 0,"1.001", 1200);
//        // 一天百分之0.3  一年两百天 六年 250W  十年 2800W
//        test(10000, 0,"1.003", 2000);
//        // 一天百分之0.5  一年两百天 六年 2800W
        test(10000, 0,"1.005", 400);
//        // 一天百分之0.7  一年两百天 六年 3亿
//        test(10000, 0,"1.007", 1200);
//        // 一天百分之1  一年两百天 六年 107亿
//        test(10000, 0,"1.01", 1200);

        // 一个月百分之2  将近30W
//        test(10000, 0,"1.02", 72);
        // 一个月百分之5  两千三百多W
//        test(10000, 0,"1.05", 72);
        // 一个月百分之十，那就是 6千多W  6年   月化10%，比较好的结果  压力不是很大的结果
//        test(10000, 0,"1.1", 72);

//        test(100000, 0,"1.30", 40);
//        test(5000000, 0,"1.05", 40);
//
//        // ruirui 7年千倍 月均+8.6个点 太可怕了。 倍数甚至没有达到月均10个点。 主要是资金越高越不好翻倍。
//        test(10000, 0,"1.086", 84);

        /*
         * 存钱习惯的重要性  规划的重要性  稳定的重要性
         * 0.定个小目标，2022年拿到本金10000，硬性存入 判定死刑， 每月5000增长
         * 1.一月实现百分之10个点的增长， 存入5000
         * 2.
          */

    }

    @SuppressWarnings("all")
    private static void test(Integer money, Integer rise, String rate, Integer count) {
        BigDecimal principal = Convert.toBigDecimal(money);
        BigDecimal old = principal;
        for (int i = 1; i <= count; i++) {
            principal = principal.multiply(Convert.toBigDecimal(rate)).setScale(2, RoundingMode.HALF_UP);
            principal = principal.add(Convert.toBigDecimal(rise));
            BigDecimal needAddMoney = principal.subtract(old);
            System.out.print(i + "=" + principal + " -- 需增值：" + needAddMoney);

            BigDecimal doLessCount = needAddMoney.divide(Convert.toBigDecimal(50), 2, RoundingMode.HALF_UP);
            System.out.print(" -- 50点手数：" + doLessCount);
            BigDecimal doMoreCount = needAddMoney.divide(Convert.toBigDecimal(100), 2, RoundingMode.HALF_UP);
            System.out.print(" -- 100点手数：" + doMoreCount);

            BigDecimal doCount = needAddMoney.divide(Convert.toBigDecimal(1000), 2, RoundingMode.HALF_UP);
            System.out.print(" -- 最小手数：" + doCount);


            System.out.println();
            old = principal;
        }

        System.out.println("复利：" +
                " 本金：" + money +
                " 周期续投入：" + rise +
                " 周期增长比例：" + rate +
                " 周期数：" + count +
                " 最终收益为：" + principal.setScale(2, RoundingMode.HALF_UP));
    }
}
