package com.yiang.demo.money;

import cn.hutool.core.convert.Convert;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author HeZhuo
 * @date 2022/7/7
 */
public class 成本计算 {

    private final static String SELL = "卖：+";
    private final static String BUY = "买：-";


    public static void main(String[] args) {
        // 操作价--买入
        List<Operate> operateList = new ArrayList<>();
        operateList.add(new Operate(30.00, 100, BUY));
        operateList.add(new Operate(30.10, 100, SELL));
        getMoney(operateList);
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    public static class Operate {
        private Object price;
        private Object count;
        private String type;
    }

    public static BigDecimal getMoney(List<Operate> operateList) {
        BigDecimal price, count, sum = BigDecimal.ZERO;
        for (Operate operate : operateList) {
            price = Convert.toBigDecimal(operate.getPrice());
            count = Convert.toBigDecimal(operate.getCount());
            BigDecimal multiply = price.multiply(count);

            BigDecimal servicePrice = multiply.multiply(Convert.toBigDecimal(0.00013));
            System.out.println("单笔服务费[不满5按5计算]：" + servicePrice);
            System.out.println("[操作总额]" + operate.getType() + multiply);
            sum = sum.subtract(Convert.toBigDecimal(
                    servicePrice.compareTo(Convert.toBigDecimal(5)) > 0 ? servicePrice : 5
            ));

            if (operate.getType().equals(SELL)) {
                BigDecimal pr = multiply.multiply(Convert.toBigDecimal(0.001));
                System.out.println("卖出减去印花税：" + pr);
                sum = sum.subtract(pr);
                sum = sum.add(multiply);
            } else {
                sum = sum.subtract(multiply);
            }
            System.out.println("----------------------------------------");
        }
        System.out.println("最终的做T盈亏为：" + sum);
        return sum;
    }
}
