package com.yiang.demo.money;

import cn.hutool.core.math.MathUtil;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author HeZhuo
 * @date 2020/9/14
 */
public class TouServerMonthMoney {

    public static void main(String[] args) {

        //正常工资
        BigDecimal normal = BigDecimal.valueOf(10000);
        //试用期工资
        BigDecimal probation = BigDecimal.valueOf(8000);

        //应到日期
        BigDecimal date = BigDecimal.valueOf(22);

        System.out.println("计算方式：相差天数 -> (10000/22*相差天数) - (8000/22)*相差天数");

        //实际日期 i
        for (int i = 1; i <= 15; i++) {
            BigDecimal bigDecimal = normal.divide(date, 2, RoundingMode.DOWN)
                    .multiply(BigDecimal.valueOf(i));
            BigDecimal bigDecimal2 = probation.divide(date, 2, RoundingMode.DOWN).
                    multiply(BigDecimal.valueOf(i));
            System.out.println(i + " -> " + bigDecimal + " - " + bigDecimal2 +
                    " = " + bigDecimal.subtract(bigDecimal2));
        }
    }
}
