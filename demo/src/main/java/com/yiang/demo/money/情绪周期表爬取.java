package com.yiang.demo.money;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.http.HtmlUtil;
import cn.hutool.http.HttpRequest;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.LinkedList;
import java.util.List;

import static cn.hutool.core.date.DatePattern.PURE_DATE_PATTERN;

public class 情绪周期表爬取 {

    public static void main(String[] args) throws InterruptedException {


        // 开始日期与结束日期
        DateTime startDate = DateUtil.parseDate("2023-11-01");
        // 最后一天不会被执行
        DateTime endDate = DateUtil.parseDate("2023-11-07");

        List<Integer> maxList = new LinkedList<>();
        List<Integer> threeList = new LinkedList<>();
        List<Integer> twoList = new LinkedList<>();
        List<String> dateList = new LinkedList<>();

        while (startDate.isBefore(endDate)) {

            System.out.println("执行日期：" + DateUtil.formatDate(startDate));

            String url = "https://www.dabanke.com/index-" + DateUtil.format(startDate, PURE_DATE_PATTERN) + ".html";
            System.out.println("url：" + url);
            String body = HttpRequest.get(url).execute().body();

            if (body.contains("页面错误！请稍后再试～")) {
                System.out.println("跳过了..");
            }
            else {
                /*
                 * JSoup 文档：https://jsoup.org/cookbook/extracting-data/dom-navigation
                 */

                // 转化为框架的页面内容
                Document doc = Jsoup.parse(body);
                // 获取所有表格（本网站有两个）
                Elements allTable = doc.getElementsByClass("table table-bordered border-primary");
                // 只获取页面第一个表格
                Element table = allTable.get(0);
                // 获取到表格内容
                Elements tbody = table.getElementsByTag("tbody");
                Elements trList = tbody.get(0).getElementsByTag("tr");

                // 最高板数
                int max = 0;
                // 3板及以上数量
                int three = 0;
                // 2板及以上数量
                int two = 0;

                // 循环表行，跳过首板
                for (int i = 0, trListSize = trList.size(); i < trListSize; i++) {
                    Element tr = trList.get(i);
                    if (tr.text().contains("首板")) {
                        continue;
                    }

                    // 获取表行的表列
                    Elements td = tr.getElementsByTag("td");
                    // 进度 N进N
                    Element progress = td.get(0);
                    String progressStr = HtmlUtil.cleanHtmlTag(progress.text());
                    // 晋级几率
                    Element probability = td.get(1);
                    String probabilityStr = HtmlUtil.cleanHtmlTag(probability.text());
                    // 涨停连板雁阵图  包括‘（成）’  即当前连板为最高板
                    Element content = td.get(2);

                    //System.out.println("进度：" + progressStr + " 晋级几率：" + probabilityStr + " 内容：" + content);

                    // 1、确认最高板
                    if (content.text().contains("(成)") && max == 0) {
                        max = Convert.toInt(progressStr.substring(progressStr.indexOf("进") + 1));
                    }

                    // 晋级数
                    int count = Convert.toInt(probabilityStr.substring(0, probabilityStr.indexOf("/")));

                    // 如果连板层级不小于2层，那么就代表有3板及以上的个股 （极少数情况）
                    if (trListSize - i > 2) {
                        // 2、获取3板及以上
                        three += count;
                    }

                    // 3、获取二板及以上
                    two += count;
                }

                System.out.println("最高板：" + max + " 3板及以上：" + three + " 2板及以上：" + two);
                maxList.add(max);
                threeList.add(three);
                twoList.add(two);
                dateList.add("'" + DateUtil.formatDate(startDate) + "'");
            }

            // 休眠2秒钟避免太快
//            int i = RandomUtil.randomInt(2222, 6666);
            int i = RandomUtil.randomInt(1000, 1500);
            System.out.println("休眠：" + i + "毫秒");
            Thread.sleep(i);

            // 将日期退后一天继续遍历
            startDate = DateUtil.offsetDay(startDate, 1);
            System.out.println("---------------");
        }

        System.out.println("连板数据爬取结束，周期时间：" + DateUtil.format(endDate, PURE_DATE_PATTERN) );
        System.out.println("最高连板：" + maxList);
        System.out.println("3板及以上：" + threeList);
        System.out.println("2板及以上：" + twoList);
        System.out.println("日期集合：" + dateList);
    }
}
