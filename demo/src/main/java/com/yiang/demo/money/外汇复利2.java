package com.yiang.demo.money;

import cn.hutool.core.convert.Convert;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author HeZhuo
 * @date 2021/11/18
 */
public class 外汇复利2 {

    /*
     * 手数计算基础价格，每 10W 一手
     */
    private static final BigDecimal calculate = Convert.toBigDecimal(100000);

    /**
     * 计算可操作金额比，10%
     */
    private static final BigDecimal moneyRate = Convert.toBigDecimal(0.1);

    public static void main(String[] args) {

        // 资金计划栏目
        test(126451, "1.10", 22);

    }

    private static void test(Integer sumM, String rate, Integer count) {

        /*
         * 原 10% 的2%  依次复利，但是有问题，资金越大，本金就被超过了
         *
         * 所以现在改成
         * 10% 的2%  1W 1手，保底有5W，最大浮亏控制在2W。 那也就是每满10W才能跨越一手（基础操盘）。
         * 0.1手 1W。 1.1手 6W  0.1=1W 1手=10W
         */

        // 托单金额
        BigDecimal sum = Convert.toBigDecimal(sumM);
        // 建议操作金额
        BigDecimal money = sum.multiply(moneyRate).setScale(2, RoundingMode.DOWN);
        // 单量
        BigDecimal quantity = sum.divide(calculate, 2, RoundingMode.DOWN);
        System.out.println("建议操作金额：" + money + " 托单金额：" + sum + " 单量：" + quantity);

        BigDecimal principal = Convert.toBigDecimal(money);

        BigDecimal old = principal;

        BigDecimal principal2 = sum.divide(calculate, 2, RoundingMode.DOWN);
        BigDecimal old2 = principal2;

        BigDecimal q =  BigDecimal.ZERO;

        for (int i = 1; i <= count; i++) {

            // 计算下一个交易日需要达成的金额
            principal = principal.multiply(Convert.toBigDecimal(rate)).setScale(2, RoundingMode.HALF_UP);

            principal2 = sum.divide(calculate, 2, RoundingMode.DOWN);

            System.out.print(i + "=" + principal);

            System.out.print(" -- 增值：" + principal.subtract(old));
            sum = sum.add(principal.subtract(old));

            BigDecimal qq = principal2.multiply(Convert.toBigDecimal(1000));
            System.out.print(" -- 千点增值：" + qq);
            q = q.add(qq);

            System.out.print(" -- 手数：" + principal2);


            System.out.print(" -- 点数：" + principal.subtract(old).divide(principal2, 0, RoundingMode.UP));

            System.out.print(" -- 手数增值：" + principal2.subtract(old2));

            System.out.print(" -- 总资金：" + sum);

            old = principal;
            old2 = principal2;

            System.out.println();
        }

        System.out.println("----------------------------------------------------------");
        System.out.println(
                "复利："
                + " 本金：" + money
                + " 托底资金：" + sumM
                + " 周期增长比例：" + rate
                + " 周期数：" + count
                + " 最终资金为：" + sum
                + " 纯利润：" + old.subtract(money)
                + " 千点利润：" + q
        );
    }
}
