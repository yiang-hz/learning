package com.yiang.demo.money;

import com.alibaba.fastjson.util.TypeUtils;

import java.math.BigDecimal;

public class Demo1 {

    public static void main(String[] args) {

        BigDecimal bigDecimal = TypeUtils.castToBigDecimal("222.22");
        System.out.println(bigDecimal);

        System.out.println(BigDecimal.valueOf(59).subtract(BigDecimal.valueOf(55)));

        int count = 3;
        BigDecimal price = BigDecimal.valueOf(59);
        BigDecimal divide = price.multiply(BigDecimal.valueOf(0.3)).multiply(BigDecimal.valueOf(count));
        System.out.println(divide.toString() + " - " + divide.intValue());


        BigDecimal awardMoney = price.multiply(BigDecimal.valueOf(30 * 0.01));
        System.out.println(awardMoney + " - " + awardMoney.intValue());

        int rewardRate = 1;
        BigDecimal oldPrice = price;
        //获取单价的百分之比，配置 1- 100 计算百分比
        BigDecimal awardMoney2 = price.multiply(BigDecimal.valueOf(rewardRate * 0.01)).multiply(BigDecimal.valueOf(1));
        oldPrice = oldPrice.add(awardMoney2);
        System.out.println(oldPrice);
    }

}
