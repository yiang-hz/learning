package com.yiang.demo.money;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileReader;
import cn.hutool.core.io.file.FileWriter;
import cn.hutool.http.HttpRequest;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author HeZhuo
 * @date 2022/10/17
 */
public class 淘股吧搜索 {

    static String cookieValue = "Hm_lvt_8875c662941dbf07e39c556c8d97615f=1652322019; UM_distinctid=18288218f61549-08b54c265d5e17-26021d51-1bcab9-18288218f62667; gdp_user_id=gioenc-10de7ced%2C88eg%2C53gc%2Ccd61%2C84280gcgcg6b; Actionshow2=true; tgbuser=6825534; tgbpwd=778cdddf2c4c4f88d9083408c747474db278f315f77fc7c7f2b2a4d7729197ded2kkrh6d5n74o1t; Hm_lvt_cc6a63a887a7d811c92b7cc41c441837=1665891379,1665969906,1666013225,1666056088; 893eedf422617c96_gdp_gio_id=gioenc-7934425; 893eedf422617c96_gdp_cs1=gioenc-7934425; 893eedf422617c96_gdp_sequence_ids=%7B%22globalKey%22%3A65%2C%22PAGE%22%3A57%2C%22CUSTOM%22%3A4%2C%22VISIT%22%3A6%7D; acw_tc=0b32807616660746766794212e013299a0aded4e77945120b9223b38fc323f; JSESSIONID=YjYzMDk3Y2EtZjE1YS00ZTJiLWE5NGItNThkZDQ2MTc5ZTg2; 893eedf422617c96_gdp_session_id=790e17d0-f2b3-44a5-8404-aa04cdf70b5c; CNZZDATA1574657=cnzz_eid%3D493867853-1644404897-https%253A%252F%252Fwww.baidu.com%252F%26ntime%3D1666073696; 893eedf422617c96_gdp_sequence_ids=%7B%22globalKey%22%3A81%2C%22PAGE%22%3A68%2C%22CUSTOM%22%3A8%2C%22VISIT%22%3A7%7D; 893eedf422617c96_gdp_session_id_790e17d0-f2b3-44a5-8404-aa04cdf70b5c=true; Hm_lpvt_cc6a63a887a7d811c92b7cc41c441837=1666075258";


    public static void main(String[] args) {

//        List<String> index = findIndex();
//        System.out.println(index);
//        getWz(index);

//        findWz("战法");

        // 获取文章序号（主页文章3个）
        //getIndex();

        // 从本地查找发言
        findSay("三言两语", "ruirui", false, true);

        // 从淘股吧获取发言
//        getSay();

//        int count = 0;
//        for (String s : index) {
//            String url = "https://www.taoguba.com.cn/Article/"+s+"/1";
//            String body = HttpRequest.get(url).header("cookie", cookieValue).execute().body();
//            System.out.println("url:" + url);
//            System.out.println("内容：" + body);
//            File file = FileUtil.newFile("D:\\say\\文章\\" + (++count) + ".txt");
//            FileWriter fileWriter = new FileWriter(file);
//            fileWriter.write(body);
//
//            try {
//                Thread.sleep(5000L);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }


    }

    /**
     * 从本地主贴中查找
     *
     * @param context 内容
     */
    private static void findWz(String context) {

        for (int i = 0; i <= 283; i++) {
            File file = FileUtil.file("D:\\say\\主贴\\" + (i + 1) + ".txt");
            FileReader fileReader = new FileReader(file);
            String body = fileReader.readString();
            System.out.println(file.getName());
            if (body.contains(context)) {
                System.out.println("文件名称：" + i + ".txt");
                System.out.println(body);
                break;
            }
        }
    }

    /**
     * 从淘股吧读取文章
     * BUG：第一次访问没有值
     */
    private static void getIndex() {
        for (int i = 1; i <= 3; i++) {

            System.out.println(" ------------第" + i + "次请求..... ------------");
            String url = "https://www.taoguba.com.cn/user/blog/moreTopic?pageNum=3&pageNo=" + i + "&sortFlag=T&userID=4817731";
            String body = HttpRequest.get(url).header("cookie", cookieValue).execute().body();

            File file = FileUtil.newFile("D:\\say\\文章\\" + i + ".txt");
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(body);
        }
    }

    private static void getWz(List<String> idList) {
        for (int i = 57; i < 283; i++) {


            System.out.println(" ------------第" + (i + 1) + "次请求..... ------------");
            String url = "https://www.taoguba.com.cn/Article/" + idList.get(i) + "/1";
            String body = HttpRequest.get(url).header("cookie", cookieValue).execute().body();
            System.out.println("url:" + url);
            System.out.println("内容：" + body);
            File file = FileUtil.newFile("D:\\say\\主贴\\" + (i + 1) + ".txt");
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(body);

            try {
                Thread.sleep(10000L);
                if (i % 60 == 0 && i != 0) {
                    System.out.println("满60休眠中.......");
                    Thread.sleep(1000L * 60);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 获取发言
     */
    private static void getSay() {

        for (int i = 1; i <= 125; i++) {
            try {
                Thread.sleep(5000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println(" ------------第" + i + "次请求..... ------------");
            // ruirui0724 去最新跟帖栏目寻找链接地址即可。
            String url = "https://www.taoguba.com.cn/user/blog/moreReplyMod?time=2023-11-04&userID=4817731&pageNo=" + i;
            String body = HttpRequest.get(url).header("cookie", cookieValue).execute().body();

            System.out.println(body);
            File file = FileUtil.newFile("D:\\say\\ruirui\\" + i + ".txt");
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(body);
        }
    }

    private static List<String> findIndex() {
        List<String> arId = new ArrayList<>();
        for (int i = 1; i <= 3; i++) {
            File file = FileUtil.file("D:\\say\\文章\\" + i + ".txt");
            FileReader fileReader = new FileReader(file);
            List<String> strings = fileReader.readLines();
            for (String string : strings) {
                boolean contains = string.contains("Article/");
                if (contains) {
//                    System.out.println(string);
                    String id = string.substring(string.indexOf("Article/") + 8, string.indexOf("/1"));
                    arId.add(id);
                }
            }
        }
        return arId;
    }

    private static void findSay(String context, String pathPre, boolean isClose, boolean onlyFile) {

        for (int i = 1; i <= 125; i++) {
            File file = FileUtil.file("D:\\say\\" + pathPre + "\\" + i + ".txt");
            FileReader fileReader = new FileReader(file);
            String body = fileReader.readString();

            if (body.contains(context)) {

                if (onlyFile) {
                    System.out.println("文件名称：" + file.getName());
                } else {
                    System.out.println("文件名称：" + file.getName());
                    System.out.println(body);
                }

                if (isClose) {
                    break;
                }

            }
        }
    }
}
