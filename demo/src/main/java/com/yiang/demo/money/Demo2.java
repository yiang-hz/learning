package com.yiang.demo.money;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Demo2 {

    //金额字段乘法计算取余案例
    public static void main(String[] args) {
        BigDecimal bigDecimal = new BigDecimal(30);
        bigDecimal = bigDecimal.multiply(BigDecimal.valueOf(0.01));
        BigDecimal divide = BigDecimal.valueOf(101).multiply(bigDecimal).setScale(2, RoundingMode.DOWN);
        System.out.println(divide);

        //大于 1 等于0 小于 -1
        BigDecimal bigDecimal2 = new BigDecimal(30);
        BigDecimal bigDecimal3 = new BigDecimal(28);
        System.out.println(bigDecimal2.compareTo(bigDecimal3));
    }
}
