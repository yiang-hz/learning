package com.yiang.demo.money;

import cn.hutool.core.comparator.CompareUtil;

/**
 * @author HeZhuo
 * @date 2020/10/28
 */
public class IntegerCompare {

    public static void main(String[] args) {
        Integer a = 1;
        Integer b = 1;
        System.out.println(a.compareTo(b));
        System.out.println(a.compareTo(2));
        System.out.println(a.compareTo(0));

        System.out.println(CompareUtil.compare(a, b));
    }
}
