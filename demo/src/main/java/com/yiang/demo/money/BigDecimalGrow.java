package com.yiang.demo.money;

import cn.hutool.core.convert.Convert;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author HeZhuo
 * @date 2020/12/5
 */
public class BigDecimalGrow {

    public static void main(String[] args) {
        BigDecimal bigDecimal = new BigDecimal("2.2000003657746456534");
        System.out.println(bigDecimal.setScale(6, RoundingMode.CEILING));
        System.out.println(bigDecimal.setScale(6, RoundingMode.UP));
        System.out.println(bigDecimal.setScale(6, RoundingMode.DOWN));
        System.out.println(bigDecimal.setScale(6, RoundingMode.FLOOR));

        BigDecimal decimal = Convert.toBigDecimal("5.12345");
        BigDecimal divide = decimal.divide(
                Convert.toBigDecimal("100"), 7, RoundingMode.CEILING);
        System.out.println(divide.toEngineeringString());
        System.out.println(divide.scale());
    }
}
