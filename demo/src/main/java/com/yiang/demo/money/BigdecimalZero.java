package com.yiang.demo.money;

import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author HeZhuo
 * @date 2020/9/21
 */
public class BigdecimalZero {

    public static void main(String[] args) {
        BigDecimal bigDecimal = new BigDecimal(1);
        System.out.println(bigDecimal.negate());
        System.out.println(bigDecimal.abs());
        List<Integer>  a = new ArrayList<>();
        System.out.println(CollectionUtils.isEmpty(a));
    }
}
