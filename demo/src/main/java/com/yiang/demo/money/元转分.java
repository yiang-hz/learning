package com.yiang.demo.money;

import cn.hutool.core.util.NumberUtil;
import com.alibaba.fastjson.util.TypeUtils;

import java.math.BigDecimal;

/**
 * @author HeZhuo
 * @date 2020/8/4
 */
public class 元转分 {

    public static void main(String[] args) {
        String str = "11.11";
        BigDecimal bigDecimal = TypeUtils.castToBigDecimal(str);
        bigDecimal = bigDecimal.multiply(BigDecimal.valueOf(100));
        String money = NumberUtil.toStr(bigDecimal);
        System.out.println("金额：" + money);
    }
}
