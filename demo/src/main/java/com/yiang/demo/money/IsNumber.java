package com.yiang.demo.money;

import cn.hutool.core.util.NumberUtil;
import com.alibaba.fastjson.util.TypeUtils;

import java.math.BigDecimal;

/**
 * @author HeZhuo
 * @date 2020/8/4
 */
public class IsNumber {

    public static void main(String[] args) {
        String str = "112.";
        System.out.println(NumberUtil.isNumber(str));
        System.out.println(TypeUtils.castToBigDecimal(str));

        BigDecimal multiply = BigDecimal.valueOf(11.11).multiply(BigDecimal.valueOf(100));
        System.out.println(TypeUtils.castToString(multiply));
        System.out.println(multiply.toString());
        System.out.println(NumberUtil.toStr(multiply));
    }
}
