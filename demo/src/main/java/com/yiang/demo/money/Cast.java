package com.yiang.demo.money;

import cn.hutool.core.convert.Convert;
import com.alibaba.fastjson.util.TypeUtils;

import java.math.BigDecimal;

/**
 * @author HeZhuo
 * @date 2020/12/2
 */
public class Cast {

    public static void main(String[] args) {

        System.out.println(Convert.toBigDecimal(-0.5));
        System.out.println(Convert.toBigDecimal("-0.5"));
        System.out.println(BigDecimal.valueOf(-0.5));

        BigDecimal a = new BigDecimal(0.01);
        BigDecimal aUtil = TypeUtils.castToBigDecimal(0.01);
        BigDecimal aConvert = Convert.toBigDecimal(0.01);

        BigDecimal aStr = new BigDecimal("0.01");
        BigDecimal b = BigDecimal.valueOf(0.01);

        BigDecimal mu = BigDecimal.valueOf(9.8);

        System.out.println(a.multiply(mu));
        System.out.println(aUtil.multiply(mu));
        System.out.println(aConvert.multiply(mu));

        System.out.println(aStr.multiply(mu));
        System.out.println(b.multiply(mu));

    }
}
