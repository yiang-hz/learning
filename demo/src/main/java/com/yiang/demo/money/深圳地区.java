package com.yiang.demo.money;//'main' method must be in a class 'Rextester'.
//Compiler version 1.8.0_111

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class 深圳地区
{
    private static final BigDecimal ALL_MONEY =
            new BigDecimal("500000");

    public static void main(String[] args) {

        // 优化点：比率自动补充0

        // 持仓计算 数量 + 成本价格 + 现价 + 总资产（计算百分比） + 当日涨幅
        // 20W
        getMoney("新诺威", "300765", "6000", "18.36", "37.97", "+6.72%", true);
        // 15W
        getMoney("青岛食品", "001219", "8000", "18.06", "20.61", "-1.62%", true);
        // 2~5W
        getMoney("翠微股份", "603123", "2000", "13.27", "12.64", "+0.24%", true);

    }

    public static void getMoney(String name, String code, String countStr, String priceStr,
                                String newPriceStr, String grow, boolean flag) {
        // 计算：总余额(手数*最新价)、盈利（总余额-手数*成本价）、盈亏比现价*手数/ 成本价*手数
        BigDecimal count = new BigDecimal(countStr);
        BigDecimal price = new BigDecimal(priceStr);
        BigDecimal newPrice = new BigDecimal(newPriceStr);
        BigDecimal sum;
        sum = count.multiply(newPrice).setScale(3, RoundingMode.HALF_UP);

        BigDecimal buySum = price.multiply(count).setScale(3, RoundingMode.HALF_UP);

        // 计算成功后扣减手续费
        BigDecimal service = buySum.multiply(new BigDecimal("0.0012")).setScale(3, RoundingMode.DOWN);
        sum = sum.subtract(service);

        BigDecimal win = sum.subtract(buySum);
        BigDecimal winRate = sum.divide(buySum, 4, RoundingMode.HALF_UP);
        BigDecimal ofRate = sum.divide(ALL_MONEY, 4, RoundingMode.HALF_UP);

        String winRateGood = decimalFormat("#.##%", winRate.subtract(BigDecimal.ONE));
        String ofRateGood = decimalFormat("#.##%", ofRate);

        if (!flag) {
            System.out.print(" 总余额：" + sum);
            System.out.print(" 成本：" + buySum);
            System.out.print(" 盈亏：" + win);
            System.out.print(" 盈亏比：" + winRate);
            System.out.print("|" + winRateGood);
            System.out.print(" 仓位占比：" + ofRate);
            System.out.print("|" + ofRateGood);
            System.out.print(" 手续费：" + service);
            System.out.println();
            System.out.println("-----------------------------------------------------------------------------------------");
        }


        String str = "<tr>\n" +
                // 代码
                "<td name=\"证券代码\">" + code + "</td>\n" +
                // 名称
                "<td name=\"证券名称\">" + name + "</td>\n" +
                // 总余额
                "<td name=\"股票余额\">" + sum + "</td>\n" +
                // 可用余额 （当天买入的可用余额为0）
                "<td name=\"可用余额\">0</td>\n" +
                // 可用数量 （当天买入的可用数量为0）
                "<td name=\"可用数量\">0</td>\n" +
                // 冻结数量 （当天买入的数量为冻结数量）
                "<td name=\"冻结数量\">" + countStr + "</td>\n" +
                // 成本价格
                "<td name=\"成本价\">" + price + "</td>\n" +
                // 现价
                "<td name=\"市价\">" + newPrice + "</td>\n" +
                // 盈亏
                "<td name=\"盈亏\">" + (win.compareTo(BigDecimal.ZERO) > 0 ? "+" + win : win) + "</td>\n" +
                // 盈亏比
                "<td name=\"盈亏比（%）\">" + (win.compareTo(BigDecimal.ZERO) > 0 ? "+" + winRateGood : winRateGood) + "</td>\n" +
                // 当日盈亏比
                "<td name=\"当日盈亏比（%）\">" + (win.compareTo(BigDecimal.ZERO) > 0 ? "+" + winRateGood : winRateGood) + "</td>\n" +
                // 持仓总额
                "<td name=\"市值\">" + sum + "</td>\n" +
                // 仓位占比
                "<td name=\"仓位占比（%）\">" + ofRateGood + "</td>\n" +
                "<td name=\"涨幅（%）\">" + grow + "</td>\n" +
                "</tr>";

        if (flag) {
            System.out.println(str);
        }



    }

    public static String decimalFormat(String pattern, Object value) {
        final DecimalFormat decimalFormat = new DecimalFormat(pattern);
        return decimalFormat.format(value);
    }
}