package com.yiang.demo.money;

import java.math.BigDecimal;

/**
 * @author HeZhuo
 * @date 2020/10/19
 */
public class CompareDemo {

    public static void main(String[] args) {
        //金额比较Demo
        BigDecimal bigDecimal = BigDecimal.valueOf(200);
        BigDecimal two = BigDecimal.valueOf(199);
        BigDecimal three = BigDecimal.valueOf(201);
        //调用方大于比较方 1
        System.out.println(bigDecimal.compareTo(two));
        //调用方小于比较方 -1
        System.out.println(bigDecimal.compareTo(three));
        //调用方等于比较方 0
    }
}
