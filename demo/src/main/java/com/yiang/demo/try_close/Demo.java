package com.yiang.demo.try_close;

import java.io.*;

import static org.springframework.util.FileCopyUtils.BUFFER_SIZE;

public class Demo {

    public static void main(String[] args) {

    }

    static String firstLineOfFile(String path) throws IOException {

        try (BufferedReader br = new BufferedReader(new FileReader(path))) {

            return br.readLine();
        }
    }

    static void copy(String src, String dst) throws IOException {

        try (InputStream in = new FileInputStream(src)) {

            try (OutputStream out = new FileOutputStream(dst)) {

                byte[] buf = new byte[BUFFER_SIZE];

                int n;

                while ((n = in.read(buf)) >= 0) {

                    out.write(buf, 0, n);
                }
            }
        }
    }
}
