package com.yiang.demo.sqltoentity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
*  用户信息
* @author 大狼狗 2020-05-27
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * 用户id
    */
    private Integer userId;

    /**
    * 用户名
    */
    private String username;

    /**
    * 创建时间
    */
    private Date addtime;

}
