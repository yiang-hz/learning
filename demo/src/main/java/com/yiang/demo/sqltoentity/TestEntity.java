package com.yiang.demo.sqltoentity;

/**
 * @author HeZhuo
 * @date 2020/5/27
 */
public class TestEntity {

    static String str = "public class UserInfo implements Serializable {\n" +
            "\n" +
            "    private static final long serialVersionUID = 1L;\n" +
            "\n" +
            "    /**\n" +
            "    * 用户id\n" +
            "    */\n" +
            "    private Integer userId;\n" +
            "\n" +
            "    /**\n" +
            "    * 用户名\n" +
            "    */\n" +
            "    private String username;\n" +
            "\n" +
            "    /**\n" +
            "    * 创建时间\n" +
            "    */\n" +
            "    private Date addtime;\n" +
            "\n" +
            "}";

    public static void main(String[] args) {
        System.out.println(str);
        StringBuilder s = new StringBuilder();
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if ((chars[i] + chars[i+1] + chars[i+2]+"").equals("/**")){
                s.append("");
            }
        }
    }
}
