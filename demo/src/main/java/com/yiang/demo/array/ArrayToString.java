package com.yiang.demo.array;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.CharUtil;

import java.util.Arrays;

/**
 * @author HeZhuo
 * @date 2020/9/17
 */
public class ArrayToString {


    public static void main(String[] args) {
        //图片数组转换
        Integer[] a = Convert.toIntArray("1,2");
        String join = ArrayUtil.join(a, String.valueOf(CharUtil.COMMA));
        System.out.println(join);

        String [] s = {"1", "2"};
        System.out.println(Arrays.toString(s));

        String joinS = ArrayUtil.join(s, String.valueOf(CharUtil.COMMA));
        System.out.println(joinS);
    }
}
