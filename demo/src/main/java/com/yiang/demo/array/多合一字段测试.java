package com.yiang.demo.array;

/**
 * @author HeZhuo
 * @date 2022/2/16
 */
public class 多合一字段测试 {

    public static void main(String[] args) {
        String fieldValueStr = "CYY,1000,100";
        String[] fileValueArr = fieldValueStr.split(";");

        // CYY,1000,1000;CNY,1000,1000;
        for (String columnValue : fileValueArr) {
            String[] values = columnValue.split(",");
            System.out.println("REF:" + values[0]);
            System.out.println("AMOUNT1:" + values[1]);
            System.out.println("AMOUNT2:" + values[2]);
        }
    }
}
