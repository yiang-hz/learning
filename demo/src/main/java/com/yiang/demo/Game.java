package com.yiang.demo;

import com.alibaba.fastjson.JSONObject;

public class Game {

    public static void main(String[] args) {
        testIQ();
    }

    private static void testJson(){
        JSONObject answer = new JSONObject();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("A", "酒精");
        jsonObject.put("B", "酒鬼");
        jsonObject.put("C", "茅台");
        jsonObject.put("D", "国窖1573");
        System.out.println(jsonObject.toString());
        answer.put("answer", jsonObject);
        System.out.println(answer.toString());
    }

    private static void testIQ(){
        for (int i = 1; i <= 50; i++) {
            if (i < 5){
                continue;
            }
            System.out.println("第" + i + "次 ：" + (i / 10 + 1));
        }
    }
}
