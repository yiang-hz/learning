package com.yiang.demo.map_demo;

import cn.hutool.core.util.HexUtil;
import lombok.Data;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.util.HashMap;
import java.util.Map;

/**
 * @author HeZhuo
 * @date 2020/12/28
 */
public class HasKeyDemo {

    public static void main(String[] args) {
        Map<String, Object> map = new HashMap<>();
        map.put("1", null);
        System.out.println(map.containsKey("1"));
        System.out.println(map.containsKey(new A().getName()));

        try {
            byte[] desKeyData = "forshopexv201005".getBytes();
            SecretKeySpec secretKey = new SecretKeySpec(desKeyData, "AES");
            Cipher c = Cipher.getInstance("AES");
            c.init(Cipher.DECRYPT_MODE, secretKey);
            byte[] cipherText = c.doFinal(HexUtil.decodeHex("5de1cacd41d1dc43c8fe11f6c80d6f9a"));
            String retValue = new String(cipherText);
            System.out.println(retValue);


        } catch (Exception e) {

        }
    }

    @Data
    private static class A {
        private String name;
    }
}
