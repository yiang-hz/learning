package com.yiang.demo.map_demo;

import java.util.HashMap;
import java.util.Map;

/**
 * @author HeZhuo
 * @date 2020/8/4
 */
public class InitCapacity {

    public static void main(String[] args) {
        HashMap<String, Object> map = new HashMap<>(2);
        map.put("1", 1);
        map.put("2", 2);

        System.out.println(map);
    }
}
