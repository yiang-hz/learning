package com.yiang.demo;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Token {

    public static void main(String[] args) {
        Date current = new Date();
        for (int i = 0; i < 3; i++) {
            getNextDay(current, "1");
            System.out.println("当前时间："+current.toString());
            getNextDay(current, "-1");
            System.out.println("当前时间："+current.toString());
            System.out.println("--------");
        }
    }

    private static void getNextDay(Date date, String delay) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        long myTime = date.getTime() / 1000L + (long)(Integer.parseInt(delay) * 24 * 60 * 60);
        date.setTime(myTime * 1000L);
        format.format(date);
    }
}
