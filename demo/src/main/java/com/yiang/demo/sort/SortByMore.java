package com.yiang.demo.sort;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * 多条件排序
 */
public class SortByMore {

    public List<Student> mList;
    public List<Comparator<Student>> mCmpList = new ArrayList<>();

    public SortByMore(List<Student> list){
        mList = list;
        mCmpList.add(compareAgeASC);
        mCmpList.add(comparePointDESC);
        sort(mList, mCmpList);
    }


    public void sort(List<Student> list, final List<Comparator<Student>> comList) {
        if (comList == null) {
            return;
        }
        Comparator<Student> cmp = (o1, o2) -> {
            for (Comparator<Student> comparator : comList) {
                if (comparator.compare(o1, o2) > 0) {
                    return 1;
                } else if (comparator.compare(o1, o2) < 0) {
                    return -1;
                }
            }
            return 0;
        };
        list.sort(cmp);
    }

    private Comparator<Student> compareAgeASC = (o1, o2) -> o1.age > o2.age ? 1 : -1;

    private Comparator<Student> comparePointDESC = (o1, o2) -> o1.point < o2.point ? 1 : -1;


}
