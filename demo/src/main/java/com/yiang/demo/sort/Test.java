package com.yiang.demo.sort;

import java.util.ArrayList;
import java.util.List;

public class Test {

    public static void main(String[] args) {
        List<Student> students = new ArrayList<>();
        students.add(new Student(10,"赵六",4));
        students.add(new Student(11,"张三",1));
        students.add(new Student(11,"李四",3));
        students.add(new Student(10,"王五",2));
        System.out.println(students.toString());
        SortByMore sortByMore = new SortByMore(students);
        System.out.println(sortByMore.mList);
        System.out.println(students.toString());
    }
}
