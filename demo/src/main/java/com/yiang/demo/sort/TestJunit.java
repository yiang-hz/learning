package com.yiang.demo.sort;
import java.util.ArrayList;
import java.util.Collections;
public class TestJunit {

    public static void main(String[] args) {
        test();
    }

    public static void test() {
        User u1 = new User(1, 1, "A");
        User u2 = new User(3, 2, "B");
        User u3 = new User(3, 3, "C");
        User u4 = new User(5, 4, "D");
        User u5 = new User(5, 5, "E");
        ArrayList<User> list = new ArrayList<>();
        list.add(u2);
        list.add(u5);
        list.add(u3);
        list.add(u4);
        list.add(u1);
        System.out.println("排序前：");
        for (User value : list) {
            System.out.println(value.toString());
        }
        System.out.println("排序后：");
        //id升序，age降序排序
        Collections.sort(list);
        for (User user : list) {
            System.out.println(user.toString());
        }
    }

}