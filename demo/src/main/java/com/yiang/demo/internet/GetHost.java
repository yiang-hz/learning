package com.yiang.demo.internet;

import cn.hutool.core.date.DateUtil;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Calendar;
import java.util.Date;

/**
 * @author HeZhuo
 * @date 2021/11/30
 */
public class GetHost {

    public static void main(String[] args) {
        try {
            System.out.println(InetAddress.getLocalHost().getHostAddress());
            System.out.println(DateUtil.date(Calendar.getInstance()));
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.add(Calendar.DATE, -1);
            System.out.println(DateUtil.date(calendar));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
