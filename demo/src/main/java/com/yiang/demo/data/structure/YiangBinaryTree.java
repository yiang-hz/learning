package com.yiang.demo.data.structure;

/**
 * 二叉树
 * @author HeZhuo
 * @date 2020/6/5
 */
public class YiangBinaryTree {

    private int data;

    private YiangBinaryTree right;

    private YiangBinaryTree left;

    public YiangBinaryTree(int data) {
        this.data = data;
    }

    /**
     * 添加元素方法
     * @param root
     * @param data
     */
    public void insert(YiangBinaryTree root, int data){
        //如果元素比根节点大，那么是右子树
        if (data >= root.data){
            //判断根节点的右边是否存在值，如果为空则直接赋值
            if (null == root.right){
                root.right = new YiangBinaryTree(data);
            } else {
                //如果根节点右边存在值，那么递归该方法将根节点右边的值作为父值，并将data传入继续比较。
                insert(root.right, data);
            }
        } else {
            //左子树与右子树相反
            if (null == root.left){
                root.left = new YiangBinaryTree(data);
            } else {
                insert(root.left, data);
            }
        }
    }

    public void search(YiangBinaryTree root) {
        if (root != null){
            search(root.left);
            System.out.println(root.data);
            search(root.right);
        }
    }

    public static void main(String[] args) {
        int[] arr = {3, 6, 5, 2, 4, 1};
        YiangBinaryTree root = new YiangBinaryTree(arr[0]);
        for (int i = 1; i < arr.length; i++) {
            root.insert(root, arr[i]);
        }
        root.search(root);
    }
}
