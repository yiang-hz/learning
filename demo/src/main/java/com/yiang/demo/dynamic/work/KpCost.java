package com.yiang.demo.dynamic.work;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author HeZhuo
 * @date 2020/12/4
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("开平费用规则表实体类")
public class KpCost {

    @ApiModelProperty("最小值")
    private int min;

    @ApiModelProperty("最小值匹配规则 小于 LT  小于等于 LE 等于 EQ 不等于 NE 大于 GT 大于等于 GE")
    private String minRule;

    @ApiModelProperty("最大值匹配规则 小于 LT  小于等于 LE 等于 EQ 不等于 NE 大于 GT 大于等于 GE")
    private String maxRule;

    @ApiModelProperty("最大值")
    private int max;

    @ApiModelProperty("对应单价")
    private BigDecimal price;

    @ApiModelProperty("品名ID")
    private String productBrandId;

}
