package com.yiang.demo.dynamic;

/**
 * @author HeZhuo
 * @date 2020/12/7
 */
public class Demo {

    public static void main(String[] args) {
        // Create an expression object
        // String jexlExp = "foo.innerFoo.bar()";
        int count = 0;
        count = a(count);
        System.out.println(count);
    }

    public static int a(int count) {
        for (int i = 0; i < 100; i++) {
            count = count + 1;
        }
        return count;
    }
}
