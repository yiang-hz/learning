package com.yiang.demo.dynamic.work;

import cn.hutool.core.convert.Convert;
import org.apache.commons.jexl2.Expression;
import org.apache.commons.jexl2.JexlEngine;

/**
 * @author HeZhuo
 * @date 2020/12/7
 */
public class Test {
    /**
     * 计算长度是否属于配置的规则其中，如果属于为true，
     * Java动态执行String字符串
     * @param kpCost 规则
     * @param length 长度
     * @return 是否匹配
     */
    private static boolean calculate(KpCost kpCost, Integer length) {
        // 创建表达式引擎对象
        JexlEngine engine = JexlBuilder.create();

        // min minRule length && maxRule rule max
        // 例如 min:2, minRule：<=, length:2, maxRule:<, max:3 执行 2<= 2 && 2 < 3 执行结果为true
        // 创建表达式语句
        String expressionStr = kpCost.getMin() + kpCost.getMinRule() + length
                + "&&" + length + kpCost.getMaxRule() + kpCost.getMax();
        Expression expression = engine.createExpression(expressionStr);
        Object evaluate = expression.evaluate(null);
        return Convert.toBool(evaluate);
    }

    static class JexlBuilder {
        private static JexlEngine jexlEngine;
        static JexlEngine create() {
            if (null == jexlEngine) {
                jexlEngine = new JexlEngine();
            }
            return jexlEngine;
        }
    }

    public static void main(String[] args) {
        KpCost kpCost = new KpCost();
        kpCost.setMin(2);
        kpCost.setMax(3);
        kpCost.setMinRule("<=");
        kpCost.setMaxRule("<");
        int length = 2;
        System.out.println(calculate(kpCost, length));
    }
}
