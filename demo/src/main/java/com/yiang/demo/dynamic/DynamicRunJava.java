package com.yiang.demo.dynamic;

import lombok.Data;
import org.apache.commons.jexl2.*;

import java.util.Date;


/**
 * @author HeZhuo
 * @date 2020/12/7
 */
public class DynamicRunJava {

    public static void main(String[] args) {
        // 创建表达式引擎对象
        JexlEngine engine = JexlBuilder.create();
        // 创建表达式语句
        String expressionStr = "1 > 5000";
        // 创建Context对象，为表达式中的未知数赋值
        JexlContext context = new MapContext();
        context.set("money","10000");
        // context.set("StringUtil", new StringUtil());  // set之后可以调用StringUtil中的所有方法
        // 使用表达式引擎创建表达式对象
        Expression expression = engine.createExpression(expressionStr);
        // expressionStr可以是方法调用，如StringUtil.contains("yesss")
        // 使用表达式对象计算
        Object evaluate = expression.evaluate(null);
        // 输出结果：true
        System.out.println(evaluate);
        // 判断提交时间是否大于某一个时间点
//        String expressionStr = "submitTime.getTime() >= 1583856000000";
        context.set("submitTime",new Date());
        // 判断字符串是否包含“成功”
//        String expressionStr = "text.contains('成功')";
        context.set("text","请求成功");
        // 判断字符串是否为空
//        String expressionStr = "text eq null || text.size() == 0";
        // 判断是否属于数组中的任意一个
//        String expressionStr = "text =~ ['请求成功','啦啦','吧啦吧啦']";
        // 判断是否不属于数组中的任意一个
//        String expressionStr = "text !~ ['请求成功','啦啦','吧啦吧啦']";
        // 表达式为逻辑语句，运算结果为：2
//        String expressionStr = "if(a>b){c=a;}else{c=b};";
        context.set("a", 1);
        context.set("b", 2);
        // 表达式为对象调用方法
//        String expressionStr = "person.getName()";
        Person person = new Person();
        person.setName("Sixj");
        context.set("person", person);
        JexlBuilder.create();

        System.out.println(1 == 1 && 1== 2);
    }

    @Data
    static class Person{
        private String name;
    }

    static class JexlBuilder {
        private static JexlEngine jexlEngine;
        static JexlEngine create() {
            if (null == jexlEngine) {
                System.out.println("new");
                jexlEngine = new JexlEngine();
            }
            return jexlEngine;
        }
    }
}
