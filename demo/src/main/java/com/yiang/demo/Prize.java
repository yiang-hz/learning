package com.yiang.demo;

import java.time.LocalDate;
import java.util.*;

/**
 * 一页获取五百万的代码
 * @author HeZhuo
 * @date 2020/4/30
 * @version 1.0
 */
public class Prize {

    /** 红球数字集合 (HZ)*/
    private static final ArrayList<Integer> str32 = new ArrayList<>(
        Arrays.asList(
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
            11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
            31, 32
        )
    );

    /** 蓝球数字集合 (HZ)*/
    private static final ArrayList<Integer> str16 = new ArrayList<>(
        Arrays.asList(
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
            11, 12, 13, 14, 15, 16
        )
    );

    public static void main(String[] args) {

        //生成数据
        //getMath();

        //获取结果：红：1-32选 6  蓝：1-16选1 无重复

        //1、定义获奖数字集合 （打开笔记本开始选好）
        List<Integer> answer = new LinkedList<>();

        //红球需要6个
        final int red = 6;

        //2、选择红球 （仔细认真的看表看走势选红球）
        for (int i = 1; i <= red; i++) {
            getWinRandom(answer, str32);
        }

        //2.50、偷偷排个序为了便于观赏 （莫非是床说中从小选到大）
        Collections.sort(answer);

        //3、选择篮球 （选着选着发现不对劲，怎么是篮球？）
        getWinRandom(answer, str16);

        //4、选好号码啦！ 第一次运行结果：[5, 10, 18, 27, 28, 30, 9]
        System.out.println("[" + LocalDate.now() + "]" + "期双色球开奖结果："+answer);

        //5、 赶紧去领奖吧！  嘿嘿！

    }

    /**
     * 获取一位得奖数字
     * @param answer 最终答案集合
     * @param question 原可选数字集合
     */
    private static void getWinRandom(List<Integer> answer, ArrayList<Integer> question){

        //随机获取一位获奖数字
        Integer winNumber = question.get(new Random().nextInt(question.size()));

        //答案集合添加该数字
        answer.add(winNumber);
        //原集合删除该数字
        question.remove(winNumber);

    }

    /**
     * 生成数据
     * 生成初始1-32的排列，现已生成
     * @deprecated 未使用
     */
    @Deprecated
    @SuppressWarnings(value = "unused")
    private static void getMath(){

        final int count = 32;

        for (int i = 1; i <= count; i++) {
            System.out.print(i + ", ");
        }

    }

}
