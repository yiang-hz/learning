package com.yiang.demo.reflect;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.ReflectUtil;
import lombok.Data;
import org.apache.poi.ss.formula.functions.T;

import java.lang.reflect.Field;

/**
 * @author HeZhuo
 * @date 2022/9/23
 */
public class Set {

    @Data
    static class A {
        private String memberIda;
        private String memberName;
    }

    public static void main(String[] args) {
        A a = new A();
        System.out.println(a);

        setValue(a);
        System.out.println(a);
    }

    public static <T> void setValue (Object obj) {
        Field memberId = ReflectUtil.getField(obj.getClass(), "memberId");
        if (ObjectUtil.isEmpty(memberId)) {
            System.out.println();
        }
        ReflectUtil.setFieldValue(obj, "memberId", 1);
        ReflectUtil.setFieldValue(obj, "memberName", 2);
    }
}
