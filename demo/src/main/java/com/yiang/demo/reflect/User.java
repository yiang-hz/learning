package com.yiang.demo.reflect;

import lombok.Data;

/**
 * @author HeZhuo
 * @date 2021/7/7
 */
@Data
public class User {

    private String name;
    private String sex;

    public User(String name, String sex) {
        System.out.println("全参构造创建...");
        this.name = name;
        this.sex = sex;
    }

    public User() {
        System.out.println("无参构造创建...");
    }
}
