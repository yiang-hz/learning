package com.yiang.demo.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * 使用反射操作实体类对象
 */
public class Demo {

    public static void main(String[] args) throws Exception {

        /*
         * 1、首先，除了New可以创建对象，还可以通过反射创建对象
         * 2、forName  必须传入class 类的完整路径
         * 3、newInstance使用无参构造函数 创建对象 new
         */
        Class<?> cls = Class.forName("com.yiang.demo.reflect.User");
        /*
         * 一、根据反射操作实体类对象 （无参构造）
         * 注意：这里实际上是调用了User类的无参构造函数，如果没有无参构造函数则会抛出异常
         * 信息：Exception in thread "main" java.lang.InstantiationException
         */
        Object o = cls.newInstance(); //拿到实例对象
        User user = (User) o; //强制转换为User实体类
        System.out.println("User：" + user);
        user.setName("小何"); user.setSex("男");
        System.out.println("User：" + user);
        // 注意：使用反射的创建效率是没有new对象高的，因为反射还需要加载。
        /*
         *二、根据反射操作实体类对象（有参构造）
         * 通过 getConstructor 对应的构造方法创建，当然如果只有一个name参数就只需要传一个String.class
         * 注意：必须传对应类型的参数调用
         */
        Constructor<?> constructor = cls.getConstructor(String.class, String.class);
        Object o2 = constructor.newInstance("小何", "男");
        User user2 = (User) o2;
        System.out.println("User2：" + user2);

        /*
         * 三、通过反射获取当前类的所有方法
         * 当前类：cls.getDeclaredMethods();
         * 当前类以及继承Object类：cls.getMethods();
         */
        //获取当前类
        Method[] declaredMethods = cls.getDeclaredMethods();
        //获取当前类以及Object类的方法
        Method[] methods = cls.getMethods();
        for (Method declaredMethod : declaredMethods) {
            System.out.println("方法信息：" + declaredMethod + "-------" + "方法名称："
                    + declaredMethod.getName() + "-------方法返回类型：" + declaredMethod.getReturnType());
        }
        /*
         * 四、拿到所有成员属性 （能拿到private）
         */
        Field[] declaredFields = cls.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            System.out.println(declaredField);
        }

        /*
         * 五、通过反射操作私有成员变量
         * 修改私有变量时需要开启权限，不然会抛出异常，异常信息：
         * Class com.dandan.thread.reflect.Demo can not access a member of class com.dandan.thread.reflect.User with modifiers "private"
         * 添加权限 setAccessible(true)
         */
        Field name = cls.getDeclaredField("name");
        User user5 = (User) cls.newInstance();
        name.setAccessible(true);//授权
        name.set(user5, "小何");
        System.out.println("user5：" + user5);
    }
}
