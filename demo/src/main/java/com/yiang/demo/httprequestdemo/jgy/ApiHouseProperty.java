package com.yiang.demo.httprequestdemo.jgy;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 房间信息
 * </p>
 *
 * @author HZ
 * @since 2021-05-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="房产团队接口实体对象", description="房间信息")
public class ApiHouseProperty implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键唯一ID")
    private Integer id;

    @ApiModelProperty(value = "小区id")
    private Integer xqid;

    @ApiModelProperty(value = "片区id")
    private Integer pqid;

    @ApiModelProperty(value = "座栋id")
    private Integer zdid;

    @ApiModelProperty(value = "单元id")
    private Integer dyid;

    @ApiModelProperty(value = "楼层id")
    private Integer lcid;

    @ApiModelProperty(value = "房间号")
    private String fanghao;

    @ApiModelProperty(value = "房间名")
    private String fjmwdw;

    @ApiModelProperty(value = "房间单位")
    private Integer fangjiandw;

    @ApiModelProperty(value = "房源编号")
    private String fybianhao;

    @ApiModelProperty(value = "室")
    private Integer shi;

    @ApiModelProperty(value = "厅")
    private Integer ting;

    @ApiModelProperty(value = "卫")
    private Integer wei;

    @ApiModelProperty(value = "厨")
    private Integer chu;

    @ApiModelProperty(value = "阳台")
    private Integer yangtai;

    @ApiModelProperty(value = "业主姓名")
    private String yezhuxm;

    @ApiModelProperty(value = "业主性别")
    private Integer yezhuxb;

    @ApiModelProperty(value = "业主手机")
    private Long yezhusj;

    @ApiModelProperty(value = "业主电话")
    private Long yezhudh;

    @ApiModelProperty(value = "业主身份证号")
    private String yezhusfzh;

    @ApiModelProperty(value = "业主职业")
    private Integer yezhuzy;

    @ApiModelProperty(value = "房间状态")
    private Integer xianzhuang;

    @ApiModelProperty(value = "原始户型")
    private Integer fwleixing;

    @ApiModelProperty(value = "权属")
    private Integer chanquan;

    @ApiModelProperty(value = "房屋用途")
    private Integer yongtu;

    @ApiModelProperty(value = "朝向")
    private Integer chaoxiang;

    @ApiModelProperty(value = "装修情况")
    private Integer zhuangxiu;

    @ApiModelProperty(value = "建筑面积")
    private BigDecimal jzmianji;

    @ApiModelProperty(value = "使用面积")
    private BigDecimal symianji;

    @ApiModelProperty(value = "上次成交日期")
    private Integer sccjrq;

    @ApiModelProperty(value = "上次成交公司")
    private String sccjgs;

    @ApiModelProperty(value = "房屋产权地址")
    private String fwcqdz;

    @ApiModelProperty(value = "显示顺序")
    private Integer xsshunxu;

    @ApiModelProperty(value = "添加人")
    private Integer adduid;

    @ApiModelProperty(value = "添加时间")
    private Integer time;

    @ApiModelProperty(value = "楼盘字典中的房屋id")
    private Integer roomId;


}