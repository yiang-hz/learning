package com.yiang.demo.httprequestdemo.jgy;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author HeZhuo
 * @date 2021/5/21
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("房产团队接口调用参数接收实体类")
public class ApiHousePropertyReq extends ApiHouseProperty {

    @ApiModelProperty("用户名称")
    private String userName;

    @ApiModelProperty("身份证")
    private String idCard;

    @ApiModelProperty("房间交易状态 房间交易状态;待售=1;预留=2;小订=3;认购=4;" +
            "草签=5;网签=6;销控=7;锁定=8;已包销=9")
    private String transacy_status;
}
