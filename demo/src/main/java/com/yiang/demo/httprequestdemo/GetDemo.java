package com.yiang.demo.httprequestdemo;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import cn.hutool.http.Method;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.yiang.demo.httprequestdemo.jgy.ApiHousePropertyReq;

import java.util.HashMap;

/**
 * @author HeZhuo
 * @date 2020/10/28
 */
public class GetDemo {

    public static void main(String[] args) {
//        test1();
//        test2();
        test3();
    }


    private static void test1() {
        HttpUtil.get("https://www.baidu.com");
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("grant_type", "authorization_code");
        paramMap.put("code", "0C8C311588A9829C685D22E244EB503A");
        paramMap.put("client_id", "101410454");
        paramMap.put("client_secret", "de56b00427f5970650c4f8ee3cfcfc2d");
        paramMap.put("redirect_uri", "http://www.itmayiedu.com:7070/login/oauth/callback?unionPublicId=mayikt_qq");
        String result= HttpUtil.get("https://graph.qq.com/oauth2.0/token", paramMap);
        System.out.println(result);
    }

    private static void test2() {
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("khxingming", "肖杰");
        paramMap.put("sfzheng", "43042619911010625X");
        String post = HttpUtil.post("http://kfs.d1.test/api/getuserfj.php", paramMap, 5000);
        JSONObject jsonObject = JSONUtil.parseObj(post);
        System.out.println(jsonObject);
    }

    private static void test3() {
        ApiHousePropertyReq apiHouseProperty = new ApiHousePropertyReq();
        apiHouseProperty.setUserName("李忠辉");
        apiHouseProperty.setIdCard("421221199201041857");
        //3.4.5.6 新增
        apiHouseProperty.setTransacy_status("4");
        //删除
//        apiHouseProperty.setTransacy_status("1");
        apiHouseProperty.setFanghao("102");
        apiHouseProperty.setFjmwdw("兰纳时光2");
        apiHouseProperty.setId(223);
        String result = HttpRequest
//                .post("http://127.0.0.1:9527/lnsg/synchronousHouse")
                .post("http://127.0.0.1:9527/lnsg/synchronousUpdate")
                .body(JSONUtil.toJsonStr(apiHouseProperty))
                .execute().body();
        System.out.println(result);

    }


}
