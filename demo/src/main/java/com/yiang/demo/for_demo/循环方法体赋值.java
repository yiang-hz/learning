package com.yiang.demo.for_demo;

/**
 * @author HeZhuo
 * @date 2022/2/16
 */
public class 循环方法体赋值 {

    public static void main(String[] args) {

        int j = 0;

        for (int i = 0; i < 100; i++) {
            int a = a(j);
            if (i==99) {
                System.out.println(a);
            }
        }
        System.out.println(j);
    }

    private static int a(int j) {
        return j + 1;
    }
}
