package com.yiang.demo.for_demo;

public class 循环i变量 {

    public static void main(String[] args) {

        // for 循环  return for循环的问题   trycatch包裹下的代码return
        int size = 5;
        for (int i = 1; i <= 5; i++) {
            System.out.println(i);
            try {

                if (true) {
                    i = 5;
                }
                //int d = 1/0;
            } catch (Exception e) {
                if (i == 5) {
                    System.out.println(12);
                }
            }
        }
    }
}
