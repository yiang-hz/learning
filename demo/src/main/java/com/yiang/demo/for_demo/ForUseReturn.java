package com.yiang.demo.for_demo;

/**
 * @author HeZhuo
 * @date 2020/8/6
 */
public class ForUseReturn {

    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            System.out.println(i);
            return;
        }

        do {
            System.out.println(1);
            return;
        }
        while (1==1);

    }
}
