package com.yiang.demo.备份;//'main' method must be in a class 'Rextester'.
//Compiler version 1.8.0_111

import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

public class 长沙地区
{
    /**
     * 1、 205638.608
     * 2、 207085.976
     * 3、 214011.464
     * 4、 219645.344
     * 5、 231152.680
     * 6、 229058.140
     * 7、 229238.140
     * 8、 231647.284
     * 9、 233934.487
     * 10、231132.307
     * 11、234486.595
     * 12、244555.775
     * 13、247471.519
     * 14、256769.463
     * 15、264672.799
     * 16、272718.055
     * 17、276997.371
     * 18、284667.011
     * 19、296201.439
     * 20、313746.611
     * 21、301889.951
     * 22、305732.147
     * 23、308969.827
     * 24、301908.295
     * 25、300461.767
     * 26、312073.807
     * 27、315039.895
     * 28、321422.535
     * 29、325019.015
     * 30、351511.895
     * 31、372516.786
     * 32、381132.582
     * 33、392131.499
     * 34、396516.289
     * 35、388168.886
     * 36、396645.249
     * 37、405665.249
     * 38、408392.801
     * 39、404159.201
     * 40、413908.449
     * 41、443814.241
     * 42、447576.833
     * 43、458651.713
     * 44、470971.713
     * 45、477445.093
     * 46、488287.957
     * 47、507327.957
     * 48、516723.157
     * 49、519673.157
     * 50、541689.511
     * 51、516979.801
     * 52、497429.801
     * 53、488384.801
     * 54、486179.801
     * 55、486179.801 - 空仓
     * 56、486179.801 - 空仓 年前最后一个交易日
     *
     */

    private static final BigDecimal ALL_MONEY =
            new BigDecimal("486179.801");

    private static final List<Object> moneyList = ListUtil.toList(
            // 11月
            "3980.934", "-1125.992", "-1481.334", "9447.920",
            "3343.104", "2803.680", "2449.480", "2112.864",
            "5082.240", "17274.688",  "-8294.904", "-6354.540",
            // 12月

            "-268.572", "-97.881", "-5922.000", "17916.300", "1297.620",
            "2060.806", "-835.470", "341.680", "-3901.200", "17654.820",
            "15584.480", "18736.640", "-11206.000", "7013.300", "10072.280",
            "195.240", "3833.696", "-546.528", "-705.584", "11612.040",
            "-951.960", "3918.048", "6382.640", "3596.480", "22638.400", "42523.200", "-98.40", "18006.660"
            , "-10982.970", "-13111.872", "8082.400", "6711.376", "6877.440",

            "2167.552", "-3513.600", "5994.560", "18186.272", "874.880",
            "47496.800", "-2254.032", "-2500.288", "+6914.432", "+25532.832",
            "+24393.300", "-6580.000", "-2704.800", "-4623.024", "-22072.556",
            "7624.544", "-14422.320", "", "", "",
            "", "", "", "", "",
            "", "", "", "", ""
    );

    public static void main(String[] args) {

        // 优化点：比率自动补充0

        // 持仓计算 数量 + 成本价格 + 现价 + 总资产（计算百分比） + 当日涨幅  5712.368
        BigDecimal sur = BigDecimal.ZERO;
        sur = sur.add(
                getMoney("百通能源", "001376", "4500", "20.80", "17.62", "-9.99%", true)
        );
        System.out.println("持仓总盈亏金额：" + sur);

        BigDecimal sumMoneyA = BigDecimal.ZERO;
        for (Object o : moneyList) {
            if (ObjectUtil.isEmpty(o)) {
                continue;
            }
            BigDecimal bigDecimal = Convert.toBigDecimal(o);
            sumMoneyA = bigDecimal.add(sumMoneyA);
        }
        System.out.println("过去总盈亏：" + sumMoneyA);
        System.out.println("持仓总盈亏：" + sumMoneyA.add(sur));
        System.out.println("总金额：" + sumMoneyA.add(sur).add(Convert.toBigDecimal("200000")));


    }

    public static BigDecimal getMoney(String name, String code, String countStr, String priceStr,
                                      String newPriceStr, String grow, boolean flag) {
        // 计算：总余额(手数*最新价)、盈利（总余额-手数*成本价）、盈亏比现价*手数/ 成本价*手数
        BigDecimal count = new BigDecimal(countStr);
        BigDecimal price = new BigDecimal(priceStr);
        BigDecimal newPrice = new BigDecimal(newPriceStr);
        BigDecimal sum;
        sum = count.multiply(newPrice).setScale(3, RoundingMode.HALF_UP);

        BigDecimal buySum = price.multiply(count).setScale(3, RoundingMode.HALF_UP);

        // 计算成功后扣减手续费
        BigDecimal service = buySum.multiply(new BigDecimal("0.0012")).setScale(3, RoundingMode.DOWN);
        sum = sum.subtract(service);

        BigDecimal win = sum.subtract(buySum);
        BigDecimal winRate = sum.divide(buySum, 4, RoundingMode.HALF_UP);
        BigDecimal ofRate = sum.divide(ALL_MONEY, 4, RoundingMode.HALF_UP);

        String winRateGood = decimalFormat("#.##%", winRate.subtract(BigDecimal.ONE));
        String ofRateGood = decimalFormat("#.##%", ofRate);

        if (!flag) {
            System.out.print(" 总余额：" + sum);
            System.out.print(" 成本：" + buySum);
            System.out.print(" 盈亏：" + win);
            System.out.print(" 盈亏比：" + winRate);
            System.out.print("|" + winRateGood);
            System.out.print(" 仓位占比：" + ofRate);
            System.out.print("|" + ofRateGood);
            System.out.print(" 手续费：" + service);
            System.out.println();
            System.out.println("-----------------------------------------------------------------------------------------");
        }


        String str = "<tr>\n" +
                // 代码
                "<td name=\"证券代码\">" + code + "</td>\n" +
                // 名称
                "<td name=\"证券名称\">" + name + "</td>\n" +
                // 总余额
                "<td name=\"股票余额\">" + sum + "</td>\n" +
                // 可用余额 （当天买入的可用余额为0）
                "<td name=\"可用余额\">0</td>\n" +
                // 可用数量 （当天买入的可用数量为0）
                "<td name=\"可用数量\">0</td>\n" +
                // 冻结数量 （当天买入的数量为冻结数量）
                "<td name=\"冻结数量\">" + countStr + "</td>\n" +
                // 成本价格
                "<td name=\"成本价\">" + price + "</td>\n" +
                // 现价
                "<td name=\"市价\">" + newPrice + "</td>\n" +
                // 盈亏
                "<td name=\"盈亏\">" + (win.compareTo(BigDecimal.ZERO) > 0 ? "+" + win : win) + "</td>\n" +
                // 盈亏比
                "<td name=\"盈亏比（%）\">" + (win.compareTo(BigDecimal.ZERO) > 0 ? "+" + winRateGood : winRateGood) + "</td>\n" +
                // 当日盈亏比
                "<td name=\"当日盈亏比（%）\">" + (win.compareTo(BigDecimal.ZERO) > 0 ? "+" + winRateGood : winRateGood) + "</td>\n" +
                // 持仓总额
                "<td name=\"市值\">" + sum + "</td>\n" +
                // 仓位占比
                "<td name=\"仓位占比（%）\">" + ofRateGood + "</td>\n" +
                "<td name=\"涨幅（%）\">" + grow + "</td>\n" +
                "</tr>";

        if (flag) {
            System.out.println(str);
        }

        return win;

    }

    public static String decimalFormat(String pattern, Object value) {
        final DecimalFormat decimalFormat = new DecimalFormat(pattern);
        return decimalFormat.format(value);
    }
}