package com.yiang.demo.备份;

import cn.hutool.core.io.FileUtil;
import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.extra.qrcode.QrConfig;

public class 二维码生成 {

    public static void main(String[] args) {
        QrCodeUtil.generate(
                "https://map.qq.com/?type=marker&isopeninfowin=1&markertype=1&pointx=113.019&pointy=28.245&name=中国人民解放军联勤保障部队第九二一医院addr=开福区东二环&ref=WeChat",
                QrConfig.create(),
                FileUtil.file("D:/dz.png")
        );
        System.out.println("已完成...");
    }
}
