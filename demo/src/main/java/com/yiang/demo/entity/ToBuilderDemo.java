package com.yiang.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HeZhuo
 * @date 2022/9/28
 */
public class ToBuilderDemo {

    @Data
    @Builder(toBuilder = true)
    @NoArgsConstructor
    @AllArgsConstructor
    static class A {
        private String code;
        private String name;
    }

    public static void main(String[] args) {
        A a = new A();
        List<A> aList = new ArrayList<>();

        a.setCode("AA");
        aList.add(a);
        System.out.println(aList);

        for (A a1 : aList) {
            a1 = a1.toBuilder().name("BB").build();
        }
        System.out.println(aList);

        System.out.println(a);
        a = a.toBuilder().name("AAA").build();
        System.out.println(a);
    }
}
