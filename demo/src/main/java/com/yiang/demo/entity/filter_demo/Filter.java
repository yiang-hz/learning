package com.yiang.demo.entity.filter_demo;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.convert.Convert;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HeZhuo
 * @date 2021/5/26
 */
public class Filter {

    public static void main(String[] args) {
        Notice saveEntity = new Notice(); saveEntity.setType(1);
        Notice saveEntity2 = new Notice();saveEntity2.setType(2);
        Notice saveEntity3 = new Notice();saveEntity3.setType(3);
        Notice saveEntity4 = new Notice();saveEntity4.setType(3);
        List<Notice> noticeList = new ArrayList<>();
        noticeList.add(saveEntity);
        noticeList.add(saveEntity2);
        noticeList.add(saveEntity3);
        noticeList.add(saveEntity4);
        if (CollectionUtil.isNotEmpty(noticeList)) {
            // 直接将查询结果根据noticeType分组即可
            JSONObject jsonObject = new JSONObject();
//            jsonObject = (JSONObject) JSONObject.toJSON(noticeList.stream().collect(Collectors.groupingBy
//                    (Notice::getType)));
            for (Notice notice : noticeList) {
                if (ObjectUtils.isEmpty(notice.getType())) {
                    continue;
                }
                if (jsonObject.containsKey(Convert.toStr(notice.getType()))) {
                    System.out.println("跳过一次...");
                    continue;
                }
                jsonObject.put(Convert.toStr(notice.getType()),
                        noticeList.stream().filter(entity -> entity.getType().equals(notice.getType())
                ));
            }
            System.out.println(jsonObject);
        }

    }
}
