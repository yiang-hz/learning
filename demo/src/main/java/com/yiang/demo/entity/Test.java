package com.yiang.demo.entity;

import com.alibaba.fastjson.JSONObject;

public class Test {

    public static void main(String[] args) {

        User user = new User(1,"张三");
        PageBean<User> userPageBean = new PageBean<>(1,20, user);
        System.out.println(user.toString());
        System.out.println(userPageBean.toString());
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", 1);
        jsonObject.put("name", "张三");
        System.out.println(jsonObject.toJSONString());
    }
}
