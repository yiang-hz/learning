package com.yiang.demo.entity.filter_demo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @program: febs-server
 * @description: Notice
 * @author: DongJiahao
 * @create: 2021-03-12 10:04
 **/
@Data
@Accessors(chain = true)
public class Notice implements Serializable {

    private Long id;

    /**
     * 消息通知类型（0：系统消息，1：其他）
     */
    private Integer messageType;

    /**
     *酒店id集合
     */
    private String hotelIds;

    /**
     *所属类型(0: 待接单信息;1: 自动退房信息;2: 提醒;3: 退房通知;4: 设置)
     */
    private Integer type;

    /**
     *是否删除（0：未删除；1：已删除）
     */
    private Integer delFlag;

    /**
     *消息内容
     */
    private String content;

    /**
     *已读未读（0：未读，1：已读）
     */
    private Integer isShow;

    /**
     *新增日期
     */
    private LocalDateTime createTime;

    /**
     *更新时间
     */
    private LocalDateTime updateTime;

    /**
     *
     */
    private Integer sendMsgType;
}
