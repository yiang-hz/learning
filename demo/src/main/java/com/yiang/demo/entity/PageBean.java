package com.yiang.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageBean<T> {

    private Integer pageNumber;
    private Integer pageSize;
    private T t;

}
