package com.yiang.demo.bean;

import cn.hutool.extra.cglib.CglibUtil;
import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author HeZhuo
 * @date 2021/6/21
 */
public class ListBeanCopy {

    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger();
        atomicInteger.getAndAdd(1);
        System.out.println(atomicInteger.get());
        User a = new User("张三");
        User b = new User("李四");
        List<User> userList = new ArrayList<>();
        userList.add(a); userList.add(b);
        List<UserB> userBList = CglibUtil.copyList(userList, UserB::new);
        System.out.println(userList);
        System.out.println(userBList);
    }

    @Data
    @NoArgsConstructor
    static class User {
        private String name;

        public User(String name) {
            this.name = name;
        }
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @ToString(callSuper = true)
    static class UserB extends User{
        private String id;

        public UserB() {
        }

    }
}


