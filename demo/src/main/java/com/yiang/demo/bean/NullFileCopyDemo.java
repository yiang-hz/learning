package com.yiang.demo.bean;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author HeZhuo
 * @date 2020/11/11
 */
public class NullFileCopyDemo {

    public static void main(String[] args) {

        User user = new User(18, "1");
        User user2 = new User();
        user2.setAge(19);
        user2.setName("");

        BeanUtil.copyProperties(user2, user, CopyOptions.create().setIgnoreNullValue(true));

        System.out.println(user);
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    static class User {
        private Integer age;
        private String name;
    }
}
