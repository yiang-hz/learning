package com.yiang.demo.helper;

import io.swagger.annotations.ApiModelProperty;

import java.lang.reflect.Field;

/**
 * ShowDoc文档生成器
 */
public class ShowDocGen {


    /**
     * 需要依赖Swagger的jar
     *          <dependency>
     *             <groupId>com.spring4all</groupId>
     *             <artifactId>swagger-spring-boot-starter</artifactId>
     *             <version>1.9.0.RELEASE</version>
     *         </dependency>
     * @param args 参数
     */
    public static void main(String[] args)  {
        Tone a = new Tone();
        Tone b = new Tone();
        b.setId(a.getId());
        System.out.println(b);
        //getComment(Tone.class);
    }

    /*
     * 打印ShowDoc的注释
     * @param cla 类的class对象
     * 使用@SuppressWarnings抑制一直重复参数调用的错误，与public方法必须外部调用问题
     */
    @SuppressWarnings(value = "SameParameterValue")
    public static void getComment(Class cla){
        Field[] fields = cla.getDeclaredFields();
        for (Field field : fields) {
            String name = field.getName();
            String type = field.getType().getSimpleName();
            String nameStr = getFieldAnnotationValue(field);
            System.out.println("|" + name + "|" + type + "|" + nameStr + "|");
        }
    }

    /**
     * 输出注解值
     * @param field 类字段
     * @return 注解值注释
     */
    @SuppressWarnings(value = "WeakerAccess")
    public static String getFieldAnnotationValue(Field field) {
        // 判断字段注解是否存在
        boolean annotationPresent2 = field.isAnnotationPresent(ApiModelProperty.class);
        if (annotationPresent2) {
            ApiModelProperty name = field.getAnnotation(ApiModelProperty.class);
            // 获取注解值
            return name.value();
        }
        return "无";
    }
}
