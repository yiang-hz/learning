package com.yiang.demo.helper;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("酒店实体类")
@SuppressWarnings(value = "WeakerAccess")
public class Tone {

    @ApiModelProperty("酒店表ID")
    private Long id;
    @ApiModelProperty("酒店名称")
    private String hotelName;
}