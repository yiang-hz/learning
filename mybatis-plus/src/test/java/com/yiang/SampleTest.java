package com.yiang;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yiang.entity.User;
import com.yiang.helper.Condition;
import com.yiang.helper.Query;
import com.yiang.helper.SqlKeyword;
import com.yiang.mapper.UserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SampleTest {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void testSelect() {
        System.out.println(("----- selectAll method test ------"));
        List<User> userList = userMapper.selectList(null);
        userList.forEach(System.out::println);
    }

    @Test
    public void testPage() {

        IPage<User> page = new Page<>(1, 2);

        //自定义SQL分页
        IPage<User> userIPage = userMapper.selectFromUser(page,
                Wrappers.<User>lambdaQuery()
                        .like(User::getAge, 2)
        );

        List<User> userList = userIPage.getRecords();

        userList.forEach(System.out::println);

        //原生Mapper分页
        IPage<User> userIPage1 = userMapper.selectPage(page, Wrappers.<User>lambdaQuery()
                .like(User::getAge, 2)
                .orderByAsc(User::getAge)
        );
        System.out.println(userIPage1.getRecords());
    }

    @Test
    public void testStr() {
        System.out.println(Convert.toList(SqlKeyword.filter("age,id")));
        System.out.println(OrderItem.descs(Convert.toStrArray(SqlKeyword.filter("age,id"))));
    }

    @Test
    public void testPageMy() {
        Query query = new Query();
        query.setCurrent(1);
        query.setSize(10);
        query.setAscs("age,id");
        query.setDescs("id");
        IPage<User> userIPage = userMapper.selectPage(Condition.getPage(query), Wrappers.emptyWrapper());
        userIPage.getRecords().forEach(System.out::println);
    }

    @Test
    public void testArgMy() {
        Query query = new Query();
        query.setCurrent(1);
        query.setSize(10);
        query.setAscs("age,id,insert,update");
        query.setDescs("id,insert,update");

        Map<String, Object> map = new HashMap<>();
        map.put("age", "1");

        IPage<User> userIPage = userMapper.selectPage(Condition.getPage(query),
                Condition.getQueryWrapper(map, User.class));
        userIPage.getRecords().forEach(System.out::println);
    }

    @Test
    public void testSql() {
        Query query = new Query();
        query.setCurrent(1);
        query.setSize(10);
        query.setAscs("age,id,insert,update");
        query.setDescs("id,insert,update");

        Map<String, Object> map = new HashMap<>();
//        map.put("age", "1");

        QueryWrapper<User> queryWrapper = Condition.getQueryWrapper(map, User.class);

        String a = "";

        //Or 关键字套用
//        queryWrapper.lambda()
//                .like(User::getId, a).like(User::getAge, 1)
//                .or(i -> i.like(User::getName, a).like(User::getAge, 2));

        String targetSql = queryWrapper.getTargetSql();
        System.out.println(targetSql);

        //Last SQL拼接
        if (ObjectUtil.isEmpty(targetSql)){
            queryWrapper.last("WHERE CONCAT(name,id) like '%" + a + "%'");
        } else{
            queryWrapper.last("AND CONCAT(name,id) like '%" + a + "%'");
        }

        System.out.println(queryWrapper.getTargetSql());

        IPage<User> userIPage = userMapper.selectPage(Condition.getPage(query),
                queryWrapper);
        userIPage.getRecords().forEach(System.out::println);
    }

}