package com.yiang;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yiang.fl.entity.ProductSku;
import com.yiang.fl.server.ProductSkuServer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author HeZhuo
 * @date 2020/12/30
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class FlTest {

    @Autowired
    private ProductSkuServer productSkuServer;

    //板材
    String bc = "185776baaaa34dac85fb9d9b8d66fe5a";

    @Test
    public void testSelect() {
        //检查条数
        //System.out.println(productSkuServer.count());

        List<ProductSku> productSkuList = getByClassId(bc);

        for (ProductSku productSku : productSkuList) {
            if (productSku.getSpecification().contains("*")) {

            }
        }

        System.out.println(productSkuList);
    }

    //处理五千条，处理6次
    private List<ProductSku> getByClassId(String classId) {
        //
        int current = 1;
        return productSkuServer.page(new Page<>(current, 5000), Wrappers.<ProductSku>lambdaQuery()
                .eq(ProductSku::getProductClassId, classId)
        ).getRecords();
    }
}
