package com.yiang.helper;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @date 2020-09-23
 * @author HeZhuo
 */
@ApiModel(
    description = "查询条件"
)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Query {

    @ApiModelProperty("当前页")
    private Integer current;

    @ApiModelProperty("每页的数量")
    private Integer size;

    @ApiModelProperty(
        hidden = true
    )
    private String ascs;

    @ApiModelProperty(
        hidden = true
    )
    private String descs;

}
