package com.yiang.generate;

/**
 * @author HeZhuo
 * @date 2022/3/3
 */

import com.baomidou.mybatisplus.core.toolkit.ArrayUtils;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.ArrayList;
import java.util.List;


/**
 * Mybatis-Plus generator示例
 *
 * @author luohq
 * @date 2021-01-31
 */
public class CodeGeneratorAZ {
    //作者名称
    private static final String AUTHOR = "hezhuo";
    //mysql数据源连接配置
    private static final String DATA_SOURCE_DRIVER_NAME = "com.mysql.cj.jdbc.Driver";
    private static final String DATA_SOURCE_URL = "jdbc:mysql://127.0.0.1:3306/utmgr?useSSL=false&useUnicode=true&characterEncoding=utf-8&zeroDateTimeBehavior=convertToNull&transformedBitIsBoolean=true&tinyInt1isBit=false&allowMultiQueries=true&serverTimezone=GMT%2B8&allowPublicKeyRetrieval=true";
    private static final String DATA_SOURCE_USER_NAME = "root";
    private static final String DATA_SOURCE_PASSWORD = "123456";
    //所包含的tables（为空则表示生成当前DB下的所有table）
    private static final String[] DATA_SOURCE_INCLUDE_TABLES = new String[] {"t_r_lczx_zcys_all"};
    //java代码包名
    private static final String PACKAGE_NAME = "com.yiang.demo";
    //java代码生成路径
    private static final String JAVA_OUTPUT_DIR = System.getProperty("user.dir") + "/src/main/java";
    //mapper.xml生成路径
    private static final String MAPPER_XML_OUTPUT_DIR = System.getProperty("user.dir") + "/src/main/resources/mapper";


    public static void main(String[] args) {
        /** 代码生成器 */
        AutoGenerator mpg = new AutoGenerator();

        /** ========== 1.全局配置 =========== */
        GlobalConfig gc = new GlobalConfig();
        //生成代码输出目录（当前项目根目录下/src/main/java）
        System.out.println("路径：" + JAVA_OUTPUT_DIR);
        gc.setOutputDir(JAVA_OUTPUT_DIR);
        //作者
        gc.setAuthor(AUTHOR);
        //不打开输出目录
        gc.setOpen(false);
        //生成mapper.xml中包含BaseResultMap
        gc.setBaseResultMap(true);
        //生成mapper.xml中包含BaseResultList
        gc.setBaseColumnList(true);
        //是否覆盖已有文件
        //gc.setFileOverride(true);
        //实体属性 Swagger2 注解
        // gc.setSwagger2(true);
        //日期类型（默认DateType.TIME_PACK，即java.time包下，对应Java8日期类型）
        gc.setDateType(DateType.ONLY_DATE);
        mpg.setGlobalConfig(gc);

        /** =========== 2.数据源配置 =========== */
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl(DATA_SOURCE_URL);
        // dsc.setSchemaName("public");
        dsc.setDriverName(DATA_SOURCE_DRIVER_NAME);
        dsc.setUsername(DATA_SOURCE_USER_NAME);
        dsc.setPassword(DATA_SOURCE_PASSWORD);
        mpg.setDataSource(dsc);

        /** =========== 4.包配置 =========== */
        PackageConfig pc = new PackageConfig();
        //包名
        pc.setParent(PACKAGE_NAME);
        //模块名
        //pc.setModuleName(scanner("模块名"));
        //设置Entity包名
        pc.setEntity("model.entity");
        mpg.setPackageInfo(pc);

        /** =========== 5.自定义配置（自定义mapper.xml输出至/resources/mapper下） =========== */
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };
        // 如果模板引擎是 freemarker
        String templatePath = "/templates/mapper.xml.ftl";
        // 如果模板引擎是 velocity
        // String templatePath = "/templates/mapper.xml.vm";
        List<FileOutConfig> focList = new ArrayList<>();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                //return String.format("%s/src/main/resources/mapper/%sMapper.xml", projectPath, tableInfo.getEntityName());
                return String.format("%s/%sMapper.xml", MAPPER_XML_OUTPUT_DIR, tableInfo.getEntityName());
            }
        });
        /*
        cfg.setFileCreate(new IFileCreate() {
            @Override
            public boolean isCreate(ConfigBuilder configBuilder, FileType fileType, String filePath) {
                // 判断自定义文件夹是否需要创建
                checkDir("调用默认方法创建的目录，自定义目录用");
                if (fileType == FileType.MAPPER) {
                    // 已经生成 mapper 文件判断存在，不想重新生成返回 false
                    return !new File(filePath).exists();
                }
                // 允许生成模板文件
                return true;
            }
        });
        */
        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);

        /** =========== 6.配置代码生成模板 =========== */
        TemplateConfig templateConfig = new TemplateConfig();
        // 配置自定义输出模板
        //指定自定义模板路径，注意不要带上.ftl/.vm, 会根据使用的模板引擎自动识别
        // templateConfig.setEntity("templates/entity2.java");
        // templateConfig.setService();
        // templateConfig.setController();
        //设置xml为空则不生成mapper.xml（默认生成在代码路径mapper/xml）
        //此处禁用生成mapper.xml，使用上面的自定义配置：将mapper.xml生成至resources/mapper/文件夹下
        templateConfig.setXml(null);
        mpg.setTemplate(templateConfig);

        /** =========== 7.策略配置 =========== */
        StrategyConfig strategy = new StrategyConfig();
        //表名、列 - 下划线分割转为驼峰风格
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        //lombok风格
        //strategy.setEntityLombokModel(true);
        //@RestController风格
        strategy.setRestControllerStyle(true);


        //公共父类
        //strategy.setSuperControllerClass("你自己的父类控制器,没有就不用设置!");
        //strategy.setSuperEntityClass("你自己的父类实体,没有就不用设置!");
        // 写于父类中的公共字段
        //strategy.setSuperEntityColumns("id");

        //包含指定table
        if (ArrayUtils.isNotEmpty(DATA_SOURCE_INCLUDE_TABLES)) {
            strategy.setInclude(DATA_SOURCE_INCLUDE_TABLES);
        }
        //strategy.setControllerMappingHyphenStyle(true);
        //table前缀
        //strategy.setTablePrefix(pc.getModuleName() + "_");
        mpg.setStrategy(strategy);

        /** =========== 8.设置模版引擎 - freemarker =========== */
        mpg.setTemplateEngine(new FreemarkerTemplateEngine());

        /** 生成代码 */
        mpg.execute();
    }

}

