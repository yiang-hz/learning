package com.yiang.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 以安
 */
@Data
@NoArgsConstructor
public class User {
    private Long id;
    private String name;
    private Integer age;
    private String email;
}