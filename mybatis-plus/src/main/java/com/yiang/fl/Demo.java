package com.yiang.fl;

import cn.hutool.core.util.ReUtil;

import java.util.List;

/**
 * @author HeZhuo
 * @date 2020/12/30
 */
public class Demo {

    public static void main(String[] args) {
        int i = 1; i++; System.out.println(i);
        String content = "11.6*1510-9484";
        System.out.println(ReUtil.isMatch("\\/*{1}", content));
    }
}
