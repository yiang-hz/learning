package com.yiang.fl.server;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yiang.fl.entity.ProductSku;
import com.yiang.fl.mapper.ProductSkuMapper;
import org.springframework.stereotype.Service;

/**
 * @author HeZhuo
 * @date 2020/12/30
 */
@Service
public class ProductSkuServer extends ServiceImpl<ProductSkuMapper, ProductSku> {

}
