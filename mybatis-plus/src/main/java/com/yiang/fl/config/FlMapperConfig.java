package com.yiang.fl.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author 何卓
 */
@MapperScan("com.yiang.fl.mapper")
@Configuration
public class FlMapperConfig {
}
