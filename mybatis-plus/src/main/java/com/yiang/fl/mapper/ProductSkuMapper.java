package com.yiang.fl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yiang.fl.entity.ProductSku;
import org.springframework.stereotype.Repository;

/**
 * @author HeZhuo
 * @date 2020/12/30
 */
@Repository
public interface ProductSkuMapper extends BaseMapper<ProductSku> {
}
