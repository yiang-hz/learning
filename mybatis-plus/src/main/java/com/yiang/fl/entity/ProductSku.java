package com.yiang.fl.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

/**
 * <p>
 * 物料代码
 * </p>
 *
 * @author HZ
 * @since 2020-12-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_jc_product_sku_hezhuo")
@ApiModel(value="ProductSku对象", description="物料代码")
@Alias("ProductSku")
public class ProductSku implements Serializable {

    private static final long serialVersionUID = 1L;

//    @ApiModelProperty(value = "物料ID")
//    @TableId("sku_id")
//    private String skuId;
//
//    @ApiModelProperty(value = "物料代码")
//    @TableField("sku_code")
//    private String skuCode;
//
    @ApiModelProperty(value = "品类ID")
    @TableField("product_class_id")
    private String productClassId;
//
//    @ApiModelProperty(value = "品类名称")
//    @TableField("product_class_name")
//    private String productClassName;
//
//    @ApiModelProperty(value = "品名ID")
//    @TableField("product_brand_id")
//    private String productBrandId;
//
//    @ApiModelProperty(value = "品名名称")
//    @TableField("product_brand_name")
//    private String productBrandName;
//
//    @ApiModelProperty(value = "助记码")
//    @TableField("pinyin_code")
//    private String pinyinCode;
//
//    @ApiModelProperty(value = "米重")
//    @TableField("meter_weight")
//    private BigDecimal meterWeight;
//
//    @ApiModelProperty(value = "10标米重")
//    @TableField("ten_meter_weight")
//    private BigDecimal tenMeterWeight;
//
//    @ApiModelProperty(value = "厂发米重")
//    @TableField("supply_meter_weight")
//    private BigDecimal supplyMeterWeight;
//
//    @ApiModelProperty(value = "资源粒度管理")
//    @TableField("resource_management")
//    private String resourceManagement;
//
//    @ApiModelProperty(value = "工作组")
//    @TableField("work_group")
//    private String workGroup;

    @ApiModelProperty(value = "规格")
    @TableField("specification")
    private String specification;

    @ApiModelProperty(value = "规格一( 高/厚/直径一)")
    @TableField("specification1")
    private BigDecimal specification1;

    @ApiModelProperty(value = "规格二( 宽/壁厚)")
    @TableField("specification2")
    private BigDecimal specification2;

    @ApiModelProperty(value = "扩展一")
    @TableField("expand1")
    private BigDecimal expand1;

    @ApiModelProperty(value = "扩展二")
    @TableField("expand2")
    private BigDecimal expand2;

    @ApiModelProperty(value = "长度")
    @TableField("length")
    private BigDecimal length;

//    @ApiModelProperty(value = "产地名称")
//    @TableField("prod_area_name")
//    private String prodAreaName;
//
//    @ApiModelProperty(value = "产地Id")
//    @TableField("prod_area_id")
//    private String prodAreaId;
//
//    @ApiModelProperty(value = "材质名称")
//    @TableField("product_texture_name")
//    private String productTextureName;
//
//    @ApiModelProperty(value = "材质Id")
//    @TableField("product_texture_id")
//    private String productTextureId;
//
//    @ApiModelProperty(value = "公差范围")
//    @TableField("tolerance_range")
//    private String toleranceRange;
//
//    @ApiModelProperty(value = "重量范围")
//    @TableField("weight_range")
//    private String weightRange;
//
//    @ApiModelProperty(value = "件支数")
//    @TableField("pieces")
//    private Integer pieces;
//
//    @ApiModelProperty(value = "规格简称")
//    @TableField("simple_specification")
//    private String simpleSpecification;
//
//    @ApiModelProperty(value = "理重可改")
//    @TableField("is_changeable")
//    private Boolean changeable;
//
//    @ApiModelProperty(value = "删除标记")
//    @TableField("del_flag")
//    private Boolean delFlag;
//
//    @ApiModelProperty(value = "启用标志")
//    @TableField("enable_flag")
//    private Boolean enableFlag;
//
//    @ApiModelProperty(value = "计量方式")
//    @TableField("quantity_type")
//    private String quantityType;
//
//    @ApiModelProperty(value = "计价方式")
//    @TableField("price_type")
//    private String priceType;
//
//    @ApiModelProperty(value = "计件模式")
//    @TableField("price_mode")
//    private String priceMode;
//
//    @ApiModelProperty(value = "数量单位")
//    @TableField("amount_unit")
//    private String amountUnit;
//
//    @ApiModelProperty(value = "重量单位")
//    @TableField("weight_unit")
//    private String weightUnit;
//
//    @ApiModelProperty(value = "备注")
//    @TableField("remark")
//    private String remark;
//
//    @ApiModelProperty(value = "创建人")
//    @TableField("create_user_code")
//    private String createUserCode;
//
//    @ApiModelProperty(value = "创建人名称")
//    @TableField("create_user_name")
//    private String createUserName;
//
//    @ApiModelProperty(value = "创建时间")
//    @TableField("create_date")
//    private Date createDate;
//
//    @ApiModelProperty(value = "修改人")
//    @TableField("update_user_code")
//    private String updateUserCode;
//
//    @ApiModelProperty(value = "修改人名称")
//    @TableField("update_user_name")
//    private String updateUserName;
//
//    @ApiModelProperty(value = "修改时间")
//    @TableField("update_date")
//    private Date updateDate;
//
//    @ApiModelProperty(value = "平台id")
//    @TableField("platform_id")
//    private String platformId;
//
//    @ApiModelProperty(value = "数据版本")
//    @TableField("version")
//    private Integer version;

}
