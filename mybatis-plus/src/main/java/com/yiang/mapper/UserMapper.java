package com.yiang.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yiang.entity.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * @author 以安
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

    /**
     * 自定义SQL查询分页Demo
     * @param page 页码
     * @param wrapper 构造参数器
     * @return 集合
     */
    @Select("select * from user " +
            "${ew.customSqlSegment}")
    IPage<User> selectFromUser(IPage<User> page, @Param(Constants.WRAPPER) Wrapper<User> wrapper);
}