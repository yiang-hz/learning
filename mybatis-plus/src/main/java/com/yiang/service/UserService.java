package com.yiang.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yiang.entity.User;
import com.yiang.mapper.UserMapper;
import org.springframework.stereotype.Service;

/**
 * @author HeZhuo
 * @date 2020/9/22
 */
@Service
public class UserService extends ServiceImpl<UserMapper, User> {
}
