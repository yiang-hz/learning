package com.yiang.stream;

import com.yiang.stream.channle.SendMessageInterface;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;

@SpringBootApplication
@EnableBinding(SendMessageInterface.class)
public class StreamApplication {

    /*http://127.0.0.1:9000/sendMsg*/
    public static void main(String[] args) {
        SpringApplication.run(StreamApplication.class, args);
    }
    // 思考： 在rabbit 有交换机 队列
    // 默认以通道名称 创建交换机，消费者启动的时候 随机创建一个队列名称
}
