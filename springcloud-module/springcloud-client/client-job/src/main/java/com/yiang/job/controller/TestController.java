package com.yiang.job.controller;

import com.yiang.response.AppResponseData;
import com.yiang.response.ResultCode;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class TestController {

    private List<String> list = new ArrayList<>();

    @RequestMapping("/addOrder")
    public AppResponseData addOrder(String orderName){
        System.out.println(Thread.currentThread().getId() + Thread.currentThread().toString());
        new Thread(() -> {System.out.println("1");
            try {
                Thread.sleep(3000);
                list.add(orderName);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getId() + Thread.currentThread().toString());}).start();
        saveOrder();
        return new AppResponseData (ResultCode.SUCCESS);
    }

    @Async
    void saveOrder(){
        System.out.println(Thread.currentThread().getId() + Thread.currentThread().toString());
    }

    @RequestMapping("/getOrder")
    public AppResponseData getOrder(){
        saveOrder();
        System.out.println(Thread.currentThread().getId() + Thread.currentThread().toString());
        return new AppResponseData(ResultCode.SUCCESS, list);
    }


}
