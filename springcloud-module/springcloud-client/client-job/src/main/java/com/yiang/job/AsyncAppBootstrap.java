package com.yiang.job;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class AsyncAppBootstrap {

    public static void main(String[] args) {
        SpringApplication.run(AsyncAppBootstrap.class, args);
    }
}
