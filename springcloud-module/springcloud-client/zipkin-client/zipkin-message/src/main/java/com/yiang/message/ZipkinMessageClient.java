package com.yiang.message;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ZipkinMessageClient {

    public static void main(String[] args) {
        SpringApplication.run(ZipkinMessageClient.class, args);
    }
}
