package com.yiang.message.api.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class MessageController {

    @Value("${server.port}")
    private String serverPort;

    @RequestMapping("/sendMsg")
    public String sendMsg(HttpServletRequest request) {
        System.out.println("我是消息服务平台:TraceId:" + request.getHeader("X-B3-TraceId") + ",spanId:"
                + request.getHeader("X-B3-SpanId"));
        return "I am Message, my port:" + serverPort;
    }
}
