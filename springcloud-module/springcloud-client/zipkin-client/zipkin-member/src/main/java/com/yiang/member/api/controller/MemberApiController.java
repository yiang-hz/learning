package com.yiang.member.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

@RestController
public class MemberApiController {

	@Value("${server.port}")
	private String serverPort;

	private final RestTemplate restTemplate;

	@Autowired
	public MemberApiController(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@RequestMapping("/getMember")
	public String getMember() {

		return "I am member, my port:" + serverPort;
	}

	// 会员调用消息服务
	@RequestMapping("/memberSendMsg")
	public String memberSendMsg(HttpServletRequest request) {
//		try{
			System.out.println("我是会员服务平台:TraceId:" + request.getHeader("X-B3-TraceId") + ",spanId:"
					+ request.getHeader("X-B3-SpanId"));

			String url = "http://zipkin-message/sendMsg";
			String result = restTemplate.getForObject(url, String.class);
			System.out.println(" 会员调用消息服务result:" + result);
//			int i = 1 / 0;
			return result;
//		}catch (Exception e){
//			e.printStackTrace();
//			return "Fuck Bug";
//		}
	}

}
