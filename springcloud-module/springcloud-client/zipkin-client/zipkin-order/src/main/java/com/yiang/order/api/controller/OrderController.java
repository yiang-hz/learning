package com.yiang.order.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class OrderController {

	// RestTemplate 是有SpringBoot Web组件提供 默认整合ribbon负载均衡器
	// rest方式底层是采用httpclient技术
	private final RestTemplate restTemplate;

	@Autowired
	public OrderController(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	/**
	 * 在SpringCloud 中有两种方式调用 rest、Feign（SpringCloud）
	 */

	// 订单服务调用会员服务
	@RequestMapping("/getOrder")
	public String getOrder() {
		// 有两种方式，一种是采用服务别名方式调用，另一种是直接调用 使用别名去注册中心上获取对应的服务调用地址
		String url = "http://zipkin-member/getMember";
		String result = restTemplate.getForObject(url, String.class);
		System.out.println("订单服务调用会员服务result:" + result);
		return result;
	}

	// 订单服务调用会员服务调用消息服务
	@RequestMapping("/orderToMemberToMsg")
	public String orderToMemberToMsg() {
		String url = "http://zipkin-member/memberSendMsg";
		String result = restTemplate.getForObject(url, String.class);
		System.out.println("订单服务调用会员服务result:" + result);
		return result;
	}

}
