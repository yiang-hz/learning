package com.yiang.order.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class IndexController {

    @Value("${yiang.money}")
    private String money;

    /*http://dazhu:8511/getIndex*/
    @RequestMapping("/getIndex")
    public String getIndex(){
        return money;
    }
}
