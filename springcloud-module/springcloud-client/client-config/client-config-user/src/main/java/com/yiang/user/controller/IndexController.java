package com.yiang.user.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {

    @Value("${yiang.money}")
    private String money;

    /*http://dazhu:8512/getIndex*/
    @RequestMapping("/getIndex")
    public String getIndex(){
        return money;
    }
}
