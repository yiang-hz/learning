package com.yiang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OauthClientApp {

    //http://localhost:8081/api/order/addOrder 获取Token
    public static void main(String[] args) {
        SpringApplication.run(OauthClientApp.class, args);
    }
}
