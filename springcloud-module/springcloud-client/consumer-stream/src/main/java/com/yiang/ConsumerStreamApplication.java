package com.yiang;

import com.yiang.stream.RedMsgInterface;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;

@EnableBinding(RedMsgInterface.class)
@SpringBootApplication
// 消费者
public class ConsumerStreamApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerStreamApplication.class, args);
    }
    // 思考： 在rabbit 有交换机 队列
    // 消费者队列 底层自动创建一个队列 绑定my_stream_channel
}