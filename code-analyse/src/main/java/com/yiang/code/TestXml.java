package com.yiang.code;

import com.yiang.code.entity.UserEntity;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author HeZhuo
 * @date 2021/7/26
 */
public class TestXml {

    private static ClassPathXmlApplicationContext applicationContext;

    public static void main(String[] args) {
        applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        // 如果注入相同id的Bean，则会导致抛出异常
        UserEntity userEntity = applicationContext.getBean("userEntity", UserEntity.class);
        System.out.println(userEntity.toString());
    }
}
