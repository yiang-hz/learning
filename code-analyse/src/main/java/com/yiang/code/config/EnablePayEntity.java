package com.yiang.code.config;

import com.yiang.code.entity.AliPayEntity;
import com.yiang.code.entity.PayEntity;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 注解： @EnableXXX 大部分框架的实现原理，实际上还是调用的Import，只是避免让使用者手动去输入需要导入的类
 * 去地磅房时就会去加载
 * 注解@Inherited 子类自动继承父类上使用的注解
 * 注解@Documented 标注生成javadoc的时候是否会被记录。
 * 注解@Retention(RetentionPolicy.RUNTIME) 注解不仅被保存到class文件中，jvm加载class文件之后，仍然存在；
 * 注解@Target(ElementType.TYPE) 表示注解作用的目标 接口、类、枚举
 * @author HeZhuo
 * @date 2021/7/28
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({AliPayEntity.class, PayEntity.class})
public @interface EnablePayEntity {
}
