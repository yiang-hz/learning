package com.yiang.code.config;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @author HeZhuo
 * @date 2021/7/31
 */
public class EntityImport implements ImportSelector {

    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        return new String[] {"com.yiang.code.entity.ImportEntity"};
    }
}
