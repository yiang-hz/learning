package com.yiang.code.config;

import com.yiang.code.get.OrderImpl01;
import com.yiang.code.get.OrderImpl02;
import com.yiang.code.get.Test;
import com.yiang.code.entity.UserEntity;
import com.yiang.code.entity.Windows10Entity;
import org.springframework.context.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * 注解@ComponentScan includeFilters使用时，useDefaultFilters需要为true，而不是false，否则无法只包含该类
 * exclude则需要为true 否则无法注入其它没有被排除的类
 * @author HeZhuo
 * @date 2021/7/26
 */
@Configuration
@ComponentScan(value = "com.yiang.code", includeFilters = {
        @ComponentScan.Filter(type = FilterType.ANNOTATION, classes = Service.class),
        @ComponentScan.Filter(type = FilterType.ANNOTATION, classes = Component.class)
}, useDefaultFilters = false)
@Import({EntityImport.class,
        MyImportBeanDefinitionRegistrar.class,
        com.yiang.code.get.Service.class
})
@EnablePayEntity
public class SpringTestConfig {

    @Bean
    public UserEntity userEntity() {
        return new UserEntity(2, "yiang");
    }

    @Bean
    @Conditional(WindowsCondition.class)
    public Windows10Entity windows10Entity() {
        return new Windows10Entity();
    }

    @Bean
    public MyFactoryBean myFactoryBean() {
        return new MyFactoryBean();
    }
}
