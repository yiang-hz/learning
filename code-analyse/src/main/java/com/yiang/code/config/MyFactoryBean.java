package com.yiang.code.config;

import com.yiang.code.entity.MyFactoryBeanEntity;
import org.springframework.beans.factory.FactoryBean;

/**
 * @author HeZhuo
 * @date 2021/7/31
 */
public class MyFactoryBean implements FactoryBean<MyFactoryBeanEntity> {

    /*
     * FactoryBean是注入，而不是获取 BeanFactory是获取
     */

    public MyFactoryBeanEntity getObject() {
        return new MyFactoryBeanEntity();
    }

    public Class<?> getObjectType() {
        return MyFactoryBeanEntity.class;
    }

    public boolean isSingleton() {
        return true;
    }
}
