package com.yiang.code.get;

import com.yiang.code.config.SpringTestConfig;
import com.yiang.code.get.Service;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author HeZhuo
 * @date 2021/8/1
 */
public class Test {

    private static AnnotationConfigApplicationContext applicationContext;

    public static void main(String[] args) {
        // 注解注入
        applicationContext = new AnnotationConfigApplicationContext(SpringTestConfig.class);
        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        for (String beanDefinitionName : beanDefinitionNames) {
            System.out.println(beanDefinitionName);
        }
        Service service = applicationContext.getBean("com.yiang.code.get.Service", Service.class);
        service.addOrder();
    }

}
