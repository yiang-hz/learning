package com.yiang.code.get;

import org.springframework.beans.factory.annotation.Autowired;


public class Service {

    /*
     * 1.纯Autowired注入形式，注入的类如果超过1个的情况下会抛出异常
     */
//    @Autowired
//    private Test.Order order;

    /*
     * 2.通过Qualifier去指定导入某个类可避免抛出异常
     */
//    @Autowired
//    @Qualifier("orderImpl02")
//    private Order order;

    /*
     * 3.通过@Resource去指定导入某个类可避免抛出异常
     */
//    @Resource(name = "orderImpl02")
//    //@Qualifier("orderImpl02")
//    private Order order;

    /*
     * 在对应实现类加上@Primary注解设置为主要的实现类解决报错
     */
    private Order order;

    public Service(Order order) {
        this.order = order;
    }

    public void addOrder() {
        order.add();
    }
}
