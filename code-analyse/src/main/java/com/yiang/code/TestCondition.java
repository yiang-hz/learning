package com.yiang.code;

import com.yiang.code.config.SpringTestConfig;
import com.yiang.code.entity.Windows10Entity;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author HeZhuo
 * @date 2021/7/28
 */
public class TestCondition {

    private static AnnotationConfigApplicationContext applicationContext;

    public static void main(String[] args) {
        // 注解注入
        applicationContext = new AnnotationConfigApplicationContext(SpringTestConfig.class);
        // bean注入是通过类名首字母小写
        Windows10Entity windows10Entity = applicationContext.getBean("windows10Entity", Windows10Entity.class);
        System.out.println(windows10Entity);
        // import注入的Bean则是类的全路径
        Windows10Entity windows10EntityByClass = applicationContext.getBean(
                "com.yiang.code.entity.Windows10Entity", Windows10Entity.class
        );
        System.out.println(windows10EntityByClass);

        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        for (String beanDefinitionName : beanDefinitionNames) {
            System.out.println(beanDefinitionName);
        }

    }
}
