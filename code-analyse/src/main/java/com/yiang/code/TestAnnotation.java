package com.yiang.code;

import com.yiang.code.config.MyFactoryBean;
import com.yiang.code.config.SpringTestConfig;
import com.yiang.code.entity.MyFactoryBeanEntity;
import com.yiang.code.entity.UserEntity;
import com.yiang.code.service.UserService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author HeZhuo
 * @date 2021/7/26
 */
public class TestAnnotation {

    private static AnnotationConfigApplicationContext applicationContext;

    public static void main(String[] args) {
        // 注解注入
        applicationContext = new AnnotationConfigApplicationContext(SpringTestConfig.class);
        UserEntity userEntity = applicationContext.getBean("userEntity", UserEntity.class);
        System.out.println(userEntity.toString());

        // 如果是默认的单例模式则为 true。如果指定原型模式 @Scope("prototype") 为新的对象则为false
        UserService userService1 = applicationContext.getBean("userService", UserService.class);
        UserService userService2 = applicationContext.getBean("userService", UserService.class);
        System.out.println(userService1 == userService2);

        MyFactoryBeanEntity myFactoryBeanEntity = applicationContext.getBean("myFactoryBean", MyFactoryBeanEntity.class);
        System.out.println(myFactoryBeanEntity);

        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        for (String beanDefinitionName : beanDefinitionNames) {
            System.out.println(beanDefinitionName);
        }


    }
}
