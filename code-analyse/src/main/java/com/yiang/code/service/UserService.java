package com.yiang.code.service;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * @author HeZhuo
 * @date 2021/7/28
 */
@Service
//@Scope("prototype")
@Lazy(false)
public class UserService {

    public UserService() {
        System.out.println("UserService实例被加载...");
    }
}
