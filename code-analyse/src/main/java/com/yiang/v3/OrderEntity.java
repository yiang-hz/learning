package com.yiang.v3;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @author HeZhuo
 * @date 2021/8/4
 */
public class OrderEntity
        //implements InitializingBean, DisposableBean
{

    /**
     * 注入容器后，并且实体赋值后执行。可以继承类实现（Spring方式），
     * 也可以通过Java整合Spring的注解实现
     * @throws Exception 异常信息
     */
    @PostConstruct
    public void afterPropertiesSet() throws Exception {
        System.out.println("init Order...");
    }

    @PreDestroy
    public void destroy() throws Exception {
        System.out.println("destroy Order...");
    }
}
