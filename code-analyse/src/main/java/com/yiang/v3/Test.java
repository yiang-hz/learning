package com.yiang.v3;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author HeZhuo
 * @date 2021/8/4
 */
public class Test {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(MyConfig.class);
        applicationContext.close();
    }
}
