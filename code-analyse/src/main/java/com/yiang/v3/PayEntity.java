package com.yiang.v3;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * @author HeZhuo
 * @date 2021/8/4
 */
public class PayEntity implements BeanNameAware, BeanFactoryAware, InitializingBean, ApplicationContextAware {

    public PayEntity() {
        System.out.println("1.Pay Load...");
    }

    public void setBeanName(String name) {
        System.out.println("2.bean name：" + name);
    }

    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("3.beanFactory：" + beanFactory);
    }

    public void afterPropertiesSet() throws Exception {
        System.out.println("4.init bean");
    }

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("set applicationContext通过前置处理器赋值");
    }
}
