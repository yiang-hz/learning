package com.yiang.v3;

/**
 * @author HeZhuo
 * @date 2021/8/4
 */
public class UserEntity {

    public UserEntity() {
        System.out.println("User Loading...");
    }

    private void init() {
        System.out.println("User Init...");
    }

    private void destroy() {
        System.out.println("User Destroy...");
    }
}
