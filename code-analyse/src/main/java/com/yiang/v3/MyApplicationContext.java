package com.yiang.v3;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * @author HeZhuo
 * @date 2021/8/4
 */
public class MyApplicationContext implements ApplicationContextAware {

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        OrderEntity orderEntity = applicationContext.getBean("orderEntity", OrderEntity.class);
        System.out.println("orderEntity：" + orderEntity);
    }
}
