package com.yiang.v3;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author HeZhuo
 * @date 2021/8/3
 */
@Configuration
@Import({MyApplicationContext.class, PayEntity.class, MyBeanPostProcessor.class})
public class MyConfig {

    /**
     * Bean的生命周期测试
     * init：初始化调用
     * destroy：结束调用
     * @return UserEntity
     */
    @Bean(initMethod = "init", destroyMethod = "destroy")
    public UserEntity userEntity() {
        return new UserEntity();
    }

    @Bean
    public OrderEntity orderEntity() {
        return new OrderEntity();
    }
}
