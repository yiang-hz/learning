package com.yiang.aop.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author HeZhuo
 * @date 2021/8/11
 */
@Configuration
@ComponentScan("com.yiang.aop")
public class MyConfig {
}
