package com.yiang.aop.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

/**
 * @author HeZhuo
 * @date 2021/8/11
 */
@Aspect
@EnableAspectJAutoProxy
@Component
public class OrderAspect {

    @Pointcut("execution (* com.yiang.aop.service..*.*(..))")
    public void loginAop() {
    }

    @Before("loginAop()")
    public void doBefore(JoinPoint joinPoint) {
        System.out.println(">>>>>>>前置通知<<<<<<<<<<< ");
    }

    @After("loginAop()")
    public void doAfter(JoinPoint joinPoint) {
        System.out.println(">>>>>>>>后置通知<<<<<<<<<");
    }

    @Around("loginAop()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println(">>>>环绕通知之前执行...>>>>>>");
        joinPoint.proceed();// 执行目标方案
        System.out.println(">>>>环绕通知之后执行...>>>>>>");
        return null;
    }

    @AfterReturning("loginAop()")
    public void afterReturning(JoinPoint joinPoint) {
        System.out.println("返回后通知执行.....");
    }

    @AfterThrowing("loginAop()")
    public void afterThrowing(JoinPoint joinPoint) {
        System.out.println(">>>>>异常通知");
    }
}
