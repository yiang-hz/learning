package com.yiang.aop;

import com.yiang.aop.config.MyConfig;
import com.yiang.aop.service.AService;
import com.yiang.aop.service.BService;
import com.yiang.aop.service.OrderService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author HeZhuo
 * @date 2021/8/11
 */
public class TestAop {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(MyConfig.class);
        OrderService orderService = applicationContext.getBean(OrderService.class);
        orderService.add();

        AService aService = applicationContext.getBean(AService.class);
        BService bService = applicationContext.getBean(BService.class);
        aService.setbService(bService);
        bService.setaService(aService);
        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        for (String beanDefinitionName : beanDefinitionNames) {
            System.out.println(beanDefinitionName);
        }
        System.out.println("end");
    }
}
