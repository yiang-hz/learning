package com.yiang.aop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * @author HeZhuo
 * @date 2021/8/18
 */
@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AService {

    //@Autowired
    private BService bService;

    public void setbService(BService bService) {
        this.bService = bService;
    }
}
