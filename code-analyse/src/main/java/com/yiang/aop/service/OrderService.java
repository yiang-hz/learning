package com.yiang.aop.service;

import org.springframework.stereotype.Service;

/**
 * @author HeZhuo
 * @date 2021/8/11
 */
@Service
public class OrderService {

    public void add() {
        System.out.println("Add Order ...");
    }
}
