package com.yiang.number.entity;

import java.util.Date;

/**
 * @author HeZhuo
 * @date 2022/4/3
 */
public class UserLabel {

    private Integer id;

    private String name;

    private Date createDate;

    private Date updateDate;
}
