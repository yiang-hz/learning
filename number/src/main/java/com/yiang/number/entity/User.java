package com.yiang.number.entity;

import java.util.Date;

/**
 * @author HeZhuo
 * @date 2022/4/3
 */
public class User {

    private String id;

    private String name;

    private Integer sex;

    private String number;

    private String remark;

    /**
     * 标签
     */
    private String label;

    /**
     * 分类
     */
    private String classify;

    private Date createDate;

    private Date updateDate;
}
