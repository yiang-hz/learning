package com.yiang.number.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yiang.number.entity.UserClassify;

/**
 * @author HeZhuo
 * @date 2022/4/3
 */
public interface UserClassifyMapper extends BaseMapper<UserClassify> {
}
