package com.yiang.number.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yiang.number.entity.UserLabel;

/**
 * @author HeZhuo
 * @date 2022/4/3
 */
public interface UserLabelMapper extends BaseMapper<UserLabel> {
}
