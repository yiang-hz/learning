package com.yiang.gateway.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author HeZhuo
 * @date 2020/8/31
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("gateway")
public class GatewayEntity {

    private Long id;
    private String routeId;
    private String routeName;
    private String routePattern;
    private Integer routeType;
    private String routeUrl;
    private Integer isDisable;
    private Integer isDelete;
    private Date createTime;
    private Date updateTime;

}
