package com.yiang.gateway.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yiang.gateway.entity.GatewayEntity;
import org.springframework.stereotype.Repository;

/**
 * @author HeZhuo
 * @date 2020/8/31
 */
@Repository
public interface GatewayMapper extends BaseMapper<GatewayEntity> {
}
