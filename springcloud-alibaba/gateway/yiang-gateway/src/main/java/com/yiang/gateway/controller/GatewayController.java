package com.yiang.gateway.controller;

import com.yiang.gateway.service.GatewayService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author HeZhuo
 * @date 2020/8/31
 */
@RestController
@AllArgsConstructor
public class GatewayController {

    private final GatewayService gatewayService;

    @RequestMapping("/synGateway")
    public String synGateway() {
        return gatewayService.initAllRoute();
    }
}
