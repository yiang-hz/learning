package com.yiang.gateway;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author HeZhuo
 * @date 2020/8/31
 */
@SpringBootApplication
@MapperScan("com.yiang.gateway.mapper")
public class AppGateway {

    /**
     * 目前存在的问题：
     * 1.调用更新接口更新配置时，只能更新 Nginx 轮询到的网关服务，无法更新所有的
     * 处理方案：通过ID在注册中心获取所有列表然后调用更新接口。
     *
     * http://gateway.yiang.com/member/getUser?token=1
     * @param args 参数
     */
    public static void main(String[] args) {
        SpringApplication.run(AppGateway.class, args);
    }
}
