package com.yiang.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author HeZhuo
 * @date 2020/9/4
 */
@SpringBootApplication
public class DynamicGatewayApp {

    public static void main(String[] args) {
        SpringApplication.run(DynamicGatewayApp.class, args);
    }
}
