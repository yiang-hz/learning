package com.yiang.arthas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author HeZhuo
 * @date 2021/6/25
 */
@SpringBootApplication
public class ArthasApp {

    public static void main(String[] args) {
        SpringApplication.run(ArthasApp.class, args);
    }
}

