package com.yiang.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author HeZhuo
 * @date 2020/8/19
 */
@SpringBootApplication
public class AppNacosMember {

    public static void main(String[] args) {
        SpringApplication.run(AppNacosMember.class, args);
    }

}
