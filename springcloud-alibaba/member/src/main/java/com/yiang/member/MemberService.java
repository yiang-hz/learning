package com.yiang.member;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author HeZhuo
 * @date 2020/8/19
 */
@RestController
public class MemberService {

    @Value("${server.port}")
    private String port;

    @RequestMapping("/getUser")
    public Object getUser(HttpServletRequest request) {

        String gatewayServerPort = request.getHeader("serverPort");

        return "Hello World！Yiang "+ port + ", Gateway port is " + gatewayServerPort;
    }
}
