package client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author HeZhuo
 * @date 2020/8/25
 */
@SpringBootApplication
@RefreshScope
@RestController
public class NacosController {

    @Value("${yiang.name}")
    private String name;

    @RequestMapping("/getName")
    public String getName() {
        return name;
    }

    public static void main(String[] args) {
        SpringApplication.run(NacosController.class, args);
    }

}
