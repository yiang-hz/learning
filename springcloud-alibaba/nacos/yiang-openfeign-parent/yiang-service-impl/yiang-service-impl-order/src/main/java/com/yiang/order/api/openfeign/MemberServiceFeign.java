package com.yiang.order.api.openfeign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author HeZhuo
 * @date 2020/8/24
 */
@FeignClient("yiang-member")
public interface MemberServiceFeign {

    /**
     * 获取用户
     * @param userId 用户ID
     * @return 用户
     */
    @GetMapping("/getUser")
    String getUser(@RequestParam("userId") Integer userId);
}
