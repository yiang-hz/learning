package com.yiang.order.api.impl;

import com.yiang.order.api.openfeign.MemberServiceFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author HeZhuo
 * @date 2020/8/24
 */
@RestController
public class OrderService {

    private final MemberServiceFeign memberServiceFeign;

    @Autowired
    public OrderService(MemberServiceFeign memberServiceFeign) {
        this.memberServiceFeign = memberServiceFeign;
    }

    @RequestMapping("/orderFeignToMember")
    public String orderFeignToMember() {
        return "Order Feign To Member Result：" + memberServiceFeign.getUser(1);
    }
}
