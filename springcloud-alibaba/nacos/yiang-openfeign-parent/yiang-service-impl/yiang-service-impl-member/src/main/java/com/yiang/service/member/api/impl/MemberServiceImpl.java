package com.yiang.service.member.api.impl;

import com.yiang.service.member.api.MemberService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author HeZhuo
 * @date 2020/8/24
 */
@RestController
public class MemberServiceImpl implements MemberService {

    @Value("${server.port}")
    private String serverPort;

    @Override
    public String getUser(Integer userId) {
        return "My name is 希文小卓！server port：" + serverPort;
    }
}
