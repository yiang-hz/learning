package com.yiang.service.member.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author HeZhuo
 * @date 2020/8/24
 */
public interface MemberService {

    /**
     * 获取用户
     * @param userId 用户ID
     * @return 用户
     */
    @GetMapping("/getUser")
    String getUser(@RequestParam("userId") Integer userId);
}
