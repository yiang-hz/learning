package com.yiang.order.loadbalance;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author 以安
 * @date 2020-08-19
 */
public interface LoadBalancer {

    /**
     * 根据多个不同的地址 返回单个调用rpc地址
     *
     * @param serviceInstances 服务实例
     * @return 返回一个服务实例
     */
    ServiceInstance getSingleAddress(List<ServiceInstance> serviceInstances);
}