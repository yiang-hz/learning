package com.yiang.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * @author HeZhuo
 * @date 2020/8/19
 */
@SpringBootApplication
public class AppNacosOrder {

    public static void main(String[] args) {
        SpringApplication.run(AppNacosOrder.class, args);
    }

    /**
     * //使用LoadBalanced 注解打开Ribbon自动本地负载均衡
     * 使用@LoadBalanced 之后，本地实现的负载均衡会失效报错无法调用
     * @return new RestTemplate();
     */
    @Bean
    @LoadBalanced
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

}
