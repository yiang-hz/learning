package com.yiang.order.loadbalance;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author HeZhuo
 * @date 2020/8/19
 */
@Component
public class RotationLoadBalancer implements LoadBalancer{

    private final AtomicInteger useCount = new AtomicInteger();

    @Override
    public ServiceInstance getSingleAddress(List<ServiceInstance> serviceInstances) {
        int index = useCount.incrementAndGet() % serviceInstances.size();
        return serviceInstances.get(index);
    }
}
