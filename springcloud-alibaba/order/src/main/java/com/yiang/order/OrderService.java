package com.yiang.order;

import com.yiang.order.loadbalance.LoadBalancer;
import lombok.AllArgsConstructor;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.client.loadbalancer.ServiceInstanceChooser;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

/**
 * @author HeZhuo
 * @date 2020/8/19
 */
@RestController
@AllArgsConstructor
public class OrderService {

    private final RestTemplate restTemplate;
    private final DiscoveryClient discoveryClient;
    private final LoadBalancer loadBalancer;
    private final ServiceInstanceChooser serviceInstanceChooser;
    private final LoadBalancerClient loadBalancerClient;

    @RequestMapping("/orderToMember")
    public Object orderToMember() {

        List<ServiceInstance> serviceInstanceList = discoveryClient.getInstances("yiang-member");
        ServiceInstance memberServiceInstance = serviceInstanceList.get(0);
        URI rpcMemberUrl = memberServiceInstance.getUri();
        String result = restTemplate.getForObject(rpcMemberUrl + "/getUser", String.class);
        return "订单服务：" + result;
    }

    @RequestMapping("/orderToMemberLoadBalance")
    public String orderToMemberLoadBalance() {
        // 从注册中心上获取该注册服务列表
        List<ServiceInstance> serviceInstanceList = discoveryClient.getInstances("yiang-member");
        ServiceInstance serviceInstance = loadBalancer.getSingleAddress(serviceInstanceList);
        URI rpcMemberUrl = serviceInstance.getUri();
        // 使用本地rest形式实现rpc调用
        String result = restTemplate.getForObject(rpcMemberUrl + "/getUser", String.class);
        return "订单调用会员获取结果:" + result;
    }

    @RequestMapping("/orderToMemberRibbon")
    public Object orderToMemberRibbon() {
        String result = restTemplate.getForObject("http://yiang-member/getUser", String.class);
        return "订单服务：" + result;
    }

    @RequestMapping("/getMemberClient")
    public Object getMemberClient() {

        ServiceInstance loadBalancerInstance = loadBalancerClient.choose("yiang-member");

        return "loadBalancerInstance：" + loadBalancerInstance;
    }
}
