package com.yiang.dosql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yiang.dosql.entity.User;
import org.springframework.stereotype.Repository;

/**
 * @author 以安
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

}