package com.yiang.id;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IdAppBootstrap {

    public static void main(String[] args) {
        SpringApplication.run(IdAppBootstrap.class, args);
    }
}
