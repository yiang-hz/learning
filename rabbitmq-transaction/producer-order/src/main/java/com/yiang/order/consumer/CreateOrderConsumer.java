package com.yiang.order.consumer;

import com.alibaba.fastjson.JSONObject;
import com.rabbitmq.client.Channel;
import com.yiang.order.entity.OrderEntity;
import com.yiang.order.list.OrderList;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Map;
import java.util.Random;

/**
 * 补单消费者
 * 不管订单生产者是否出现异常，补单消费者会进行消费。
 * 如果订单存在于数据库，那么return方法
 * 如果不存在，那么会添加该条订单。
 */
@Component
public class CreateOrderConsumer {

	@RabbitListener(queues = "order_create_queue")
	public void process(Message message, @Headers Map<String, Object> headers, Channel channel) throws Exception {
		String messageId = message.getMessageProperties().getMessageId();
		String msg = new String(message.getBody(), StandardCharsets.UTF_8);
		System.out.println("补单消费检查订单是否创建" + msg + ",消息id:" + messageId);
		JSONObject jsonObject = JSONObject.parseObject(msg);
		String orderId = jsonObject.getString("orderId");
		if (StringUtils.isEmpty(orderId)) {
			// 手动签收消息,通知mq服务器端删除该消息
			channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
			return;
		}
		OrderEntity orderEntityResult = null;
		for (OrderEntity orderEntity : OrderList.orderList) {
			if (orderEntity.getOrderId().equals(orderId)){
				orderEntityResult = orderEntity;
			}
		}
		if (null != orderEntityResult) {
			System.out.println("订单已经存在!");
			// 手动签收消息,通知mq服务器端删除该消息
			channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
			return;
		}
		System.out.println("补单前订单集合信息：" + OrderList.orderList.toString());
		System.out.println("订单号码:" + orderId + ",不存在。开始进行补单！");
		// 使用补偿机制
		OrderEntity orderEntity = new OrderEntity();
		orderEntity.setName("支付宝余额充值");
		orderEntity.setOrderCreatetime(new Date());
		// 价格是300元
		orderEntity.setOrderMoney(3000d);
		// 状态为 未支付
		orderEntity.setOrderState(0);
		Long commodityId = 30L;
		// 商品id
		orderEntity.setCommodityId(commodityId);
		orderEntity.setOrderId(orderId);

		// ##################################################
		// 1.先下单，创建订单 (往订单数据库中插入一条数据)
		OrderList.orderList.add(orderEntity);
		int orderResult = new Random().nextInt(3);
		System.out.println("orderResult:" + orderResult);
		if (orderResult >= 0) {
			// 手动签收消息,通知mq服务器端删除该消息
			channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
		}
		System.out.println("补单后订单集合信息：" + OrderList.orderList.toString());
		// 重试进行补偿 多次失败 使用日志记录 采用定时job检查或者 人工补偿
	}
}
