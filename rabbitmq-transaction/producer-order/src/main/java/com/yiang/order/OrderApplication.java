package com.yiang.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrderApplication {

    //http://127.0.0.1:8067/addOrder
    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class, args);
    }
}
