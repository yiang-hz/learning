package com.yiang.order.service;

import com.alibaba.fastjson.JSONObject;
import com.yiang.order.entity.OrderEntity;
import com.yiang.order.list.OrderList;
import com.yiang.response.AppResponseData;
import com.yiang.response.ResultCode;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Random;
import java.util.UUID;

@Service
public class OrderService implements RabbitTemplate.ConfirmCallback {

	private final RabbitTemplate rabbitTemplate;

	@Autowired
    public OrderService(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Transactional
	public AppResponseData addOrderAndDispatch() {
		OrderEntity orderEntity = new OrderEntity();
		orderEntity.setName("支付宝余额充值");
		orderEntity.setOrderCreatetime(new Date());
		// 充值金额是三千块
		orderEntity.setOrderMoney(3000d);
		// 状态为 未支付
		orderEntity.setOrderState(0);
		Long commodityId = 30L;
		// 商品id
		orderEntity.setCommodityId(commodityId);
		String orderId = UUID.randomUUID().toString();
		orderEntity.setOrderId(orderId);
        System.out.println("订单详情：" + orderEntity.toString());
		// ##################################################
		// 1.先下单，创建订单 (往订单数据库中插入一条数据)
		OrderList.orderList.add(orderEntity);
        //不操作数据库但是由于添加集合肯定成功，所以模拟随机生成一个数字伪造下单失败
        Random random = new Random();
        int orderResult = random.nextInt(2);
		System.out.println("orderResult:" + orderResult);
		if (orderResult <= 0) {
			return new AppResponseData(ResultCode.FAILED.getCode(), "下单失败！");
		}
		// 2.使用消息中间件将参数存在派单队列中
		send(orderId);
		//抛出异常调试 使数据回滚，让补单队列进行消费。
		System.out.println("异常前订单信息：" + OrderList.orderList.toString());
		//由于异常不会回滚集合的数据，故删除集合中该数据。
		OrderList.orderList.remove(orderEntity);
		int i = 1 / 0;
		return new AppResponseData(ResultCode.SUCCESS);
	}

	private void send(String orderId) {
		JSONObject jsonObject = new JSONObject();
        jsonObject.put("orderId", orderId);
		String msg = jsonObject.toJSONString();
		System.out.println("msg:" + msg);
		// 封装消息
		Message message = MessageBuilder.withBody(msg.getBytes()).setContentType(MessageProperties.CONTENT_TYPE_JSON)
				.setContentEncoding("utf-8").setMessageId(orderId).build();
		// 构建回调返回的数据（消息id）
		this.rabbitTemplate.setMandatory(true);
		this.rabbitTemplate.setConfirmCallback(this);

		CorrelationData correlationData = new CorrelationData(orderId);
		rabbitTemplate.convertAndSend("order_exchange_name", "orderRoutingKey", message, correlationData);

	}

	// 生产消息确认机制 生产者往服务器端发送消息的时候，采用应答机制
	@Override
	public void confirm(CorrelationData correlationData, boolean ack, String cause) {
		String orderId = correlationData.getId();
		System.out.println("消息id:" + correlationData.getId());
		if (ack) {
			System.out.println("消息发送确认成功");
		} else {
			System.out.println("消息发送重试...");
			// 重试机制
			send(orderId);
			System.out.println("消息发送确认失败:" + cause);
		}
	}

}
