package com.yiang.out.consumer;

import com.alibaba.fastjson.JSONObject;
import com.rabbitmq.client.Channel;
import com.yiang.out.entity.DispatchEntity;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Random;

/**
 * 派单服务
 */
@Component
public class DispatchConsumer {

	@RabbitListener(queues = "order_dic_queue")
	public void process(Message message, @Headers Map<String, Object> headers, Channel channel) throws Exception {
		String messageId = message.getMessageProperties().getMessageId();
		String msg = new String(message.getBody(), StandardCharsets.UTF_8);
		System.out.println("派单服务平台" + msg + ",消息id:" + messageId);
		JSONObject jsonObject = JSONObject.parseObject(msg);
		String orderId = jsonObject.getString("orderId");
		if (StringUtils.isEmpty(orderId)) {
			// 日志记录
			System.out.println("return了");
			return;
		}
		DispatchEntity dispatchEntity = new DispatchEntity();
		// 订单id
		dispatchEntity.setOrderId(orderId);
		// 外卖员id
		dispatchEntity.setTakeoutUserId(12L);
		// 外卖路线
		dispatchEntity.setDispatchRoute("40, 40");
		try {
			//int insertDistribute = dispatchMapper.insertDistribute(dispatchEntity);
			System.out.println("插入信息：" + dispatchEntity.toString());
			int insertDistribute = new Random().nextInt(3)+1;
			System.out.println("insertDistribute：" + insertDistribute);
			if (insertDistribute > 0) {
				// 手动签收消息,通知mq服务器端删除该消息
				System.out.println("手动签收了...");
				channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
			}else{
				//如果抛出异常则抛弃该消息
				throw new Exception("异常..抛出该消息");
			}
		} catch (Exception e) {
			e.printStackTrace();
			// // 丢弃该消息
			channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, false);
		}
	}

}
