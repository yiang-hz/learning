package com.yiang.amqp;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.yiang.msg.utils.MQConnectionUtils;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Producer {

	private static final String QUEUE_NAME = "amqp_queue";

	public static void main(String[] args) throws IOException, TimeoutException {
		// 1.获取连接
		Connection newConnection = MQConnectionUtils.newConnection();
		// 2.创建通道
		Channel channel = newConnection.createChannel();
		// 3.创建队列声明
		channel.queueDeclare(QUEUE_NAME, false, false, false, null);

		try {
			//表示开启消息事务
			channel.txSelect();
			String msg = "amqp_hz_test_msg";
			System.out.println("生产者发送消息:" + msg);
			// 4.发送消息
			channel.basicPublish("", QUEUE_NAME, null, msg.getBytes());
//			int i = 1/0;
			//提交消息事务， 放在最后一行
			channel.txCommit();
		} catch (Exception e){
			System.out.println("发生异常，事务消息回滚");
			//回滚消息事务
			channel.txRollback();
			e.printStackTrace();
		} finally {
			channel.close();
			newConnection.close();
		}
	}

}