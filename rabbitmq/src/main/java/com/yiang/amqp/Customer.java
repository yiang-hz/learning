package com.yiang.amqp;

import com.rabbitmq.client.*;
import com.yiang.msg.utils.MQConnectionUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

/**
 * RabbitMQ AMQP事务消息
 */
public class Customer {

	private static final String QUEUE_NAME = "amqp_queue";

	public static void main(String[] args) throws IOException, TimeoutException {
		//这里通过并行运行即可发现轮询均摊消费策略
		System.out.println("消费者来了...");
		// 1.获取连接
		Connection newConnection = MQConnectionUtils.newConnection();
		// 2.获取通道
		Channel channel = newConnection.createChannel();
		// 3.消费者关联队列
		channel.queueDeclare(QUEUE_NAME, false, false, false, null);
		DefaultConsumer defaultConsumer = new DefaultConsumer(channel) {
			//监听获取消息
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
				String msgString = new String(body, StandardCharsets.UTF_8);
				System.out.println("消费者获取消息:" + msgString);
				//手动应答 模式 告诉队列服务器已经处理 成功  因为如果是 自动应答模式不管处理成功与否都会清空信息
				//channel.basicAck(envelope.getDeliveryTag(), false);
			}
		};
		// 4.设置应答模式，如果为true情况下 表示为自动应答模式 false为手动应答模式。
		channel.basicConsume(QUEUE_NAME, true, defaultConsumer);

	}

}