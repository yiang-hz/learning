package com.yiang.fairQueue;

import com.rabbitmq.client.*;
import com.yiang.msg.utils.MQConnectionUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeoutException;

/**
 * RabbitMQ公平队列原理实现 消费者二
 */
public class CustomerTwo {

    private static final String QUEUE_NAME = "fair_queue";

    public static void main(String[] args) throws IOException, TimeoutException {
        System.out.println("公平队列原理实现 消费者二...");
        Connection newConnection = MQConnectionUtils.newConnection();
        Channel channel = newConnection.createChannel();
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        //测试公平队列queue添加语句，保证消费者每次只接收一个
        channel.basicQos(1);
        DefaultConsumer defaultConsumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                String msgString = new String(body, StandardCharsets.UTF_8);
                System.out.println("消费者二获取消息:" + msgString + "---" + new SimpleDateFormat().format( new Date()));
                try{
                    Thread.sleep(1000);
                }catch (Exception e){
                    e.printStackTrace();
                } finally {
                    channel.basicAck(envelope.getDeliveryTag(), false);
                }
            }
        };
        channel.basicConsume(QUEUE_NAME, false, defaultConsumer);

    }
}
