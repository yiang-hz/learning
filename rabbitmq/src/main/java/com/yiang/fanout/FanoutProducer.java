package com.yiang.fanout;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.yiang.msg.utils.MQConnectionUtils;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

// 生产者  交换机类型 producer fanout类型

/**
 * 注意要先启动生产者，默认插件该交换机后，消费者才能接收，否则是不能接收的。
 *	1、交换机不会保存消息，但是队列会保存，如果其中一个消费者挂了，消息是会保存在队列中的。
 * 2、发布订阅模式会保证消息会推送给每个订阅者。
 */
public class FanoutProducer {

	// 交换机名称
	private static final String DESTINATION_NAME = "my_fanout_destination";

	public static void main(String[] args) throws IOException, TimeoutException {
		// 1. 建立mq连接
		Connection connection = MQConnectionUtils.newConnection();
		// 2.创建通道
		Channel channel = connection.createChannel();
		// 3.生产者绑定交换机 参数1 交换机名称 参数2 交换机类型
		channel.exchangeDeclare(DESTINATION_NAME, "fanout");
		// 4.创建消息
		String msg = "my_fanout_destination_msg";
		System.out.println("生产者投递消息:" + msg);
		// 5.发送消息 my_fanout_destination routingKey
		channel.basicPublish(DESTINATION_NAME, "", null, msg.getBytes());
		// 6.关闭通道 和连接
		channel.close();
		connection.close();

	}

}
