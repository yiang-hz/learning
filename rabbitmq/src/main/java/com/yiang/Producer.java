package com.yiang;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.yiang.msg.utils.MQConnectionUtils;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Producer {

	private static final String QUEUE_NAME = "hotel_queue";

	/**
	 * 测试公平队列queue
	 */
	private static final String QUEUE_NAME_FAIR = "fair_queue";

	public static void main(String[] args) throws IOException, TimeoutException {
		// 1.获取连接
		Connection newConnection = MQConnectionUtils.newConnection();
		// 2.创建通道
		Channel channel = newConnection.createChannel();
		// 3.创建队列声明
		channel.queueDeclare(QUEUE_NAME_FAIR, false, false, false, null);

		for (int i = 1; i <= 50 ; i++) {
			String msg = "hotel_hz_test_msg_" + i;
			System.out.println("生产者发送消息:" + msg);
			// 4.发送消息
			channel.basicPublish("", QUEUE_NAME_FAIR, null, msg.getBytes());
		}
		channel.close();
		newConnection.close();
	}

}