package com.yiang.jvm;

import sun.misc.Unsafe;

import java.lang.reflect.Field;

public class Demo {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
//        Unsafe unsafe = Unsafe.getUnsafe();
//        int i = unsafe.addressSize();
//        System.out.println(i + " -- " + unsafe);

        Field f = Unsafe.class.getDeclaredField("theUnsafe");
        f.setAccessible(true);
        Unsafe unsafe = (Unsafe) f.get(null);
        System.out.println(unsafe.toString());
    }

//    public static Unsafe getUnsafe() {
//        Class cc = sun.reflect.Reflection.getCallerClass(2);
//        if (cc.getClassLoader() != null) {
//            throw new SecurityException("Unsafe");
//        }
//        return theUnsafe;
//    }
}
