package com.yiang.jvm.inn;

/**
 * 对象创建过程...
 */
public class CreateObject {

    private int i;
    private boolean b;

    public CreateObject(){
        System.out.println("构造函数...");
    }

    public static void main(String[] args) {
        CreateObject createObject = new CreateObject();
        System.out.println(createObject.b);
    }
}
