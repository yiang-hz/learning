package com.yiang.jvm.inn;

public class Test2 {

    public static final int[] array = new int[] {1, 2, 3};

    public static void main(String[] args) {
        String a= "abc";
        String b= "abc";
        String c= new String("abc");
        String d = "a";
        System.out.println(a==b);
        System.out.println(a==c);
        String intern = c.intern();
        System.out.println(intern);
        System.out.println(a==c.intern());
        System.out.println(a==d);
    }

    public static void say(String text){
        String remark = "Hello world";
        System.out.println(remark + text);
    }

    public static int add(){
        int i0 = 100;
        int i1 = 98;
        // int i2 = i0 + i1; return i2;
        return i0 + i1;
    }

    public static void print(){

        System.out.println(add());
    }
}
