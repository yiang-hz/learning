package com.yiang.jvm.inn;

/**
 * 对本代码进行汇编：https://blog.csdn.net/new1111111/article/details/79666454
 * 查看汇编内容即可
 */
public class Test {

    /*
            汇编代码执行
         0: bipush        100   将一个8位带符号整数压入栈（这里是压入100）
         2: istore_1            将int类型值存入局部变量（其他类型有其他的规则）
         3: bipush        99    同100压入
         5: istore_2            将int类型值存入局部变量（其他类型有其他的规则）
         6: iload_1             从局部变量中装载int类型值  (将100装载到操作数栈)
         7: iload_2             从局部变量中装载int类型值  (装99载到操作数栈)
         8: iadd                执行int类型的加法 （99+100）
         9: istore_3            将int类型值存入局部变量（其他类型有其他的规则） 定义给第三个变量 c，存入局部变量
        10: return              方法返回
        */
    public void add(){
        int a = 100;
        int b = 99;
        int c = a + b;
        new String();
    }

    public static void main(String[] args) {
        Test t = new Test();
        t.add();
    }
}
