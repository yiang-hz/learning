package com.yiang.jvm.gc;

public class Demo1 {

    private Object object;
    private byte[] bytes = new byte[1024 * 1024 * 20];

    public static void main(String[] args) {

        String str = new String ("Hello World");

        Demo1 demo1 = new Demo1();
        Demo1 demo11 = new Demo1();
        demo1.object = demo11;
        demo11.object = demo1;
        demo1 = null;
        demo11 = null;
        System.gc();

    }
    //young GC
    //  “System.gc()” 代表什么原因产生的GC，此处是因为调用了System.gc();回收垃圾方法
    //        minor GC 新生代 GC大小，执行前->执行后（总值） [堆]执行前->执行后（堆总大小）
    //[GC (System.gc()) [PSYoungGen: 47477K->975K(75776K)] 47477K->983K(249344K), 0.0079970 secs] [Times: user=0.00 sys=0.00, real=0.01 secs]



}
