package com.yiang.jvm.gc;

/**
 * 记LinkedList源码回顾时内存溢出Demo
 * @author HeZhuo
 * @date 2020/5/11
 */
public class StackOverflowErrorDemo {

    private static class Node<E> {

        /** 当前元素值 (HZ)*/
        E item;
        /** 当前元素的上一个元素 (HZ)*/
        Node<E> next;
        /** 当前元素的下一个元素 (HZ)*/
        Node<E> prev;
        /** 单参构造 @param element 当前元素的值 (HZ)*/
        Node(E element) {
            this.item = element;
        }
    }

    public static void main(String[] args) {
        Node<String> node1 = new Node<>("节点一");
        Node<String> node2 = new Node<>("节点二");
        node1.next = node2;
        node2.prev = node1;
    }
}
