package com.yiang.jdk8.lambda.function_use;

import com.yiang.jdk8.lambda.optimize.FunctionPay2Interface;

public class Demo {

    public static void main(String[] args) {
        //第一种方式new对象引用
        new Thread(()->{
            new SmsServcie().sendMsg();
        }).start();

        //第二种方式 方法引用
        SmsServcie smsServcie = new SmsServcie();
        new Thread(smsServcie::sendMsg).start();

        //第三种方式 方法引用静态实例
        new Thread(SmsServcie::staticSendMsg).start();

        //第四种方式 调用无参构造函数
        new Thread(SmsServcie::new).start();
        // 左边 抽象方法 调用的类的实例 :: 右边就是我们具体实现的方法
        //这种写法可以new出任何一种没有参数的函数式接口
        FunctionPay2Interface aNew = SmsServcie::new;
        aNew.pay();

    }
}