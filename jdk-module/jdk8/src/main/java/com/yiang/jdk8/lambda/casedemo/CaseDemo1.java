package com.yiang.jdk8.lambda.casedemo;

/**
 * @author HeZhuo
 * @date 2020/4/28
 */
public class CaseDemo1 {

    public static void main(String[] args) {

        Runnable r1 = () -> System.out.println("Hello World 1");

        Runnable r2 = new Runnable(){

            @Override
            public void run(){
                System.out.println("Hello World 2");
            }
        };

        process(r1);
        process(r2);
    }

    public static void process(Runnable r){
        r.run();
    }

}
