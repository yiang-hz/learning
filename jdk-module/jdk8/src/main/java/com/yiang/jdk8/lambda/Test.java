package com.yiang.jdk8.lambda;

public class Test {

    public static void main(String[] args) {


        LambdaInterface lambdaInterface =
                // () 等于方法体内的()add() 参数只需要名称即可; {}等于方法体
                (name)->{
            System.out.println(1 + name);
            return "1";
        };
        //添加参数
        lambdaInterface.add("HZ");


    }
}
