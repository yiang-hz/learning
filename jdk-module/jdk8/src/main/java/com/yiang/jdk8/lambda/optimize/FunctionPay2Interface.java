package com.yiang.jdk8.lambda.optimize;

@FunctionalInterface
public interface FunctionPay2Interface {

    void pay();
}
