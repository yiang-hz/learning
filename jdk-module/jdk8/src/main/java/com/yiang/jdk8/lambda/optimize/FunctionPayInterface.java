package com.yiang.jdk8.lambda.optimize;

@FunctionalInterface
public interface FunctionPayInterface {


    void pay(Integer id, String name);
}
