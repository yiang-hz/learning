package com.yiang.jdk8.lambda;

import java.util.ArrayList;
import java.util.List;

public class ListDemo {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("1");list.add("2");list.add("3");
        //Lambda遍历
        list.forEach((t)-> {
            System.out.println(t);
            System.out.println();
        });
        //特殊符号打印  方法引用
        list.forEach(System.out::println);
    }
}
