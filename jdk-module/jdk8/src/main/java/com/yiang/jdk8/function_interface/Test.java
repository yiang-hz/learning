package com.yiang.jdk8.function_interface;

public class Test {

    public static void main(String[] args) {
        String delete = ((Demo1) () -> {
            System.out.println("a");
        }).delete();
        System.out.println(delete);
    }
}
