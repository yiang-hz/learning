package com.yiang.jdk8.lambda.optimize;

public class Demo {


    /**
     * 使用Lambda优化匿名内部类写法
     */
    public static void main(String[] args) {
        beforeOptimize();
        afterOptimize((id, name)->{
            System.out.println(id + "---" + name);
            System.out.println();
        });
    }

    //有参匿名内部类优化前
    private static void beforeOptimize(){
        FunctionPayInterface functionPayInterface = (id, name)->{
            System.out.println(id + "---" + name);
            System.out.println();
        };
        functionPayInterface.pay(1, "HZ 优化前");
    }

    //有参匿名内部类优化后
    private static void afterOptimize(FunctionPayInterface functionPayInterface){
        functionPayInterface.pay(1, "HZ 优化后");
    }
}
