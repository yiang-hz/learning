package com.yiang.jdk8.lambda.casedemo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author HeZhuo
 * @date 2020/4/28
 */
public class CaseDemo2 {

    public static void main(String[] args) {
        //测试流Stream
        List<String> list = new ArrayList<String>();
        list.add("I am a boy");
        list.add("I love the girl");
        list.add("But the girl loves another girl");

        List<String> collect = list
                //将集合String转化为流
                .stream()
                //将String分割为String[]流
                .map(line -> line.split(" "))
                //将String[]流再转化为String集合流
                .flatMap(Arrays::stream)
                //去重复
                .distinct()
                //最后将流转换为List集合
                .collect(Collectors.toList());
        System.out.println(collect);

    }
}
