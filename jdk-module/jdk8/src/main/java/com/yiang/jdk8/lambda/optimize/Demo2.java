package com.yiang.jdk8.lambda.optimize;

public class Demo2 {

    /**
     * 使用Lambda优化匿名内部类写法
     */
    public static void main(String[] args) {
        beforeOptimize();
        afterOptimize(()->{
            System.out.println("---优化后");
            System.out.println();
        });
    }

    //无参匿名内部类优化前
    private static void beforeOptimize(){
        FunctionPay2Interface functionPay2Interface = ()->{
            System.out.println("---优化前");
            System.out.println();
        };
        functionPay2Interface.pay();
    }

    //无参匿名内部类优化后
    private static void afterOptimize(FunctionPay2Interface functionPay2Interface){
        functionPay2Interface.pay();
    }
}
