package com.yiang.map.array_map;

/**
 * @author Administrator
 */
public interface ArrayMap<K, V> {

    // k=key
    // v=value

    /**
     * 集合的大小
     * @return 集合大小
     */
    int size();

    /**
     * 添加元素的方法
     *
     * @param key 键
     * @param value 值
     * @return   值
     */
    V put(K key, V value);

    /**
     * 使用Key查询到Value
     * @param key 键
     * @return 值
     */
    V get(K key);

    interface Entry<K, V>{
        /**
         * 获取Key
         * @return Key
         */
        K getKey();

        /**
         * 获取Value
         * @return 值
         */
        V getValue();

        /**
         * 设置Value
         * @param value 值
         * @return 值
         */
        V setValue(V value);
    }
}
