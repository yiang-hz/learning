package com.yiang.map;

import java.util.HashMap;
import java.util.Map;

public class Demo {

    //HashMap存值问题
    public static void main(String[] args) {
        System.out.println("Hello Map!");

        Map<Object, String> map = new HashMap<>();
        Object o = null;
        map.put(null, null);
        map.put("", null);
        System.out.println(map);
    }
}
