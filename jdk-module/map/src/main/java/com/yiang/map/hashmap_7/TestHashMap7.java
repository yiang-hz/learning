package com.yiang.map.hashmap_7;

import java.util.HashMap;

/**
 * @author Administrator
 */
public class TestHashMap7 {

    public static void main(String[] args) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("1", 1);map.get("1");print(map.toString());

        //testPut();

        //测试Key为NULL的情况方法
        //testNullKey();

        //测试扩容方法
        testResize();
    }

    /**
     * 1.测试HashMap的put方法，通过HashCode相同的Key以此测试链表排列
     * 2.测试HashMap的Get方法，断点查看源码执行过程
     */
    private static void testPut(){
        String a = new String("a");
        Integer b = new Integer(97);

        YiangHashMap<Object, Object> yiangHashMap = new YiangHashMap<>();

        //TestGet-  Print:Null
        System.out.println(yiangHashMap.get(a));

        yiangHashMap.put(a, "this is a, hashCode is 97, value is 'a'");
        yiangHashMap.put(b, "this is b, hashCode is 97, value is 97");

        //test Get-  Print: a -- value
        System.out.println(yiangHashMap.get(a));
    }

    /**
     * 测试Key为NULL的情况方法
     */
    private static void testNullKey(){
        YiangHashMap<Object, Object> yiangHashMap = new YiangHashMap<>();
        //存放Key为NULL的元素
        yiangHashMap.put(null, "I am a value that key is null");
        //获取Key为NULL的元素
        System.out.println(yiangHashMap.get(null));
    }

    private static void print(Object o){
        System.out.println(o);
    }

    /**
     * 调试扩容方法
     */
    private static void testResize(){
        YiangHashMap<Integer, Object> map = new YiangHashMap<>();
        for (int i = 1; i <= 12; i++) {
            map.put(i, "元素" + i);
        }
        map.put(666, "扩容元素");
    }

}
