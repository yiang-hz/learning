package com.yiang.map.array_map;

import java.util.Arrays;
import java.util.LinkedList;

/**
 * @author HeZhuo
 * @param <K>
 * @param <V>
 */
public class LinkedListHashMap<K, V> implements ArrayMap<K, V> {

    private LinkedList<Node<K, V>>[] arrays = new LinkedList[10];

    @Override
    public int size() {
        return arrays.length;
    }

    @Override
    public V put(K key, V value) {

        int sit = getSit(key);

        LinkedList<Node<K, V>> linkedList = arrays[sit];
        Node<K, V> node = new Node<>(key, value);
        //如果获取的集合为空，那么代表没有元素冲突
        if (null == linkedList){
            //New一个新的集合存放新对象，并吧对应的数组下标集合赋值为当前集合
            linkedList = new LinkedList<>();
            linkedList.add(node);
            arrays[sit] = linkedList;
            return value;
        }
        //如果不为空，代表元素有冲突，那么判断Key是否相等，如果相等就覆盖
        for (Node<K, V> nd : linkedList) {
            if (nd.getKey().equals(key)){
                nd.setValue(value);
                return value;
            }
        }
        // hashCode相同，但是Key不相同
        linkedList.add(node);
        arrays[sit] = linkedList;

        return value;
    }

    @Override
    public V get(K key) {

        int sit = getSit(key);

        LinkedList<Node<K, V>> array = arrays[sit];

        for (Node<K, V> kvNode : array) {
            if (kvNode.getKey().equals(key)){
                return kvNode.getValue();
            }
        }

        return null;
    }

    /**
     * 根据Key值获取坐标
     * @param key 键
     * @return 坐标
     */
    private int getSit(K key){
        //根据 Key hashCode % object.length 获取数组中下标存放的位置 不可能重复
        //取除以长度的余数，得到的数值永远都会小于数组长度
        return key.hashCode() % arrays.length;
    }

    static class Node<K, V> implements ArrayMap.Entry<K, V> {

        private K key;
        private V value;

        /**
         * 使用构造参数赋值
         * @param key 键
         * @param value 值
         */
        Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        /**
         * 设置Value 在源码中是返回OldValue而不是NewValue
         * @param newValue 值
         * @return 值
         */
        @Override
        public V setValue(V newValue) {
            //V oldValue = value;
            value = newValue;
            return value;
        }

        @Override
        public String toString() {
            return "Node{" +
                    "key=" + key +
                    ", value=" + value +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "LinkedListHashMap{" +
                "arrays=" + Arrays.toString(arrays) +
                '}';
    }
}
