package com.yiang.map.array_map;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HZ
 * @param <K>
 * @param <V>
 */
public class ArrayListHashMap<K, V> implements ArrayMap<K, V> {

    /**
     * 存储数据的数组
     */
    private List<Entry<K, V>> arrayList = new ArrayList<>();

    @Override
    public int size() {
        return arrayList.size();
    }

    /**
     * 存储数据
     * 缺点： 1.无法进行再次赋值与覆盖
     * @param key 键
     * @param value 值
     * @return 值
     */
    @Override
    public V put(K key, V value) {
        Node<K, V> newNode = new Node<>(key, value);
        arrayList.add(newNode);
        return value;
    }

    /**
     * 获取值
     * 缺点：1.需要遍历所有数据
     * @param key 键
     * @return 值
     */
    @Override
    public V get(K key) {
        for (Entry<K, V> kvEntry : arrayList) {
            if (kvEntry.getKey().equals(key)){
                return kvEntry.getValue();
            }
        }
        return null;
    }

    static class Node<K, V> implements ArrayMap.Entry<K, V> {

        private K key;
        private V value;

        /**
         * 使用构造参数赋值
         * @param key 键
         * @param value 值
         */
        Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        /**
         * 设置Value 在源码中是返回OldValue而不是NewValue
         * @param newValue 值
         * @return 值
         */
        @Override
        public V setValue(V newValue) {
            //V oldValue = value;
            value = newValue;
            return value;
        }

        @Override
        public String toString() {
            return "Node{" +
                    "key=" + key +
                    ", value=" + value +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "ArrayHashMap{" +
                "arrayList=" + arrayList +
                '}';
    }
}
