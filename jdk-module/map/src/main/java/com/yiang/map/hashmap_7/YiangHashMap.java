package com.yiang.map.hashmap_7;

import com.yiang.map.array_map.ArrayMap;

import java.util.Map;
import java.util.Objects;

/**
 * @author Administrator
 */
public class YiangHashMap<K, V> implements ArrayMap<K, V> {

    /**
     * 默认初始容量 必须是2次幂 这里为 16 二进制
     * // aka 16
     */
    static final int DEFAULT_INITIAL_CAPACITY = 1 << 4;

    /**
     * 默认的负载因子  也就是扩容倍数
     */
    static final float DEFAULT_LOAD_FACTOR = 0.75f;

    /**
     * 最大扩容值，如果超过了则为该值 10个亿
     */
    static final int MAXIMUM_CAPACITY = 1 << 30;

    /**
     * 扩展因子
     *
     * @serial
     */
    final float loadFactor;

    /**
     * 一个空的实例，扩容前共享
     * 实际上定义该空值用来转换table，可读性更强。
     */
    static final YiangHashMap.Entry<?,?>[] EMPTY_TABLE = {};

    /**
     * 初始化，Entry对象数组
     */
    transient YiangHashMap.Entry<K,V>[] table = (YiangHashMap.Entry<K,V>[]) EMPTY_TABLE;

    /**
     * 阈值，需要扩容时设置，实际HashMap存放的大小 当达到该阈值时进行扩容
     * 要调整大小的下一个大小值(容量*负载因子)
     * 如果表为空，那么表将会在扩容时创建
     * // If table == EMPTY_TABLE then this is the initial capacity at which the
     *     // table will be created when inflated.
     */
    int threshold;

    /**
     * 随该对象存在的随机值，键的Hash码。为0则禁用
     */
    transient int hashSeed = 0;

    /**
     * The number of key-value mappings contained in this map.
     * 此映射中包含的键-值映射的数目。
     * 即大小
     */
    transient int size;

    public YiangHashMap() {
        this(DEFAULT_INITIAL_CAPACITY, DEFAULT_LOAD_FACTOR);
    }

    /**
     * HashMap初始化构造函数源码解析
     * @param initialCapacity 初始化扩展容量 默认 16
     * @param loadFactor 加载因子默认 0.75
     */
    public YiangHashMap(int initialCapacity, float loadFactor) {
        //判断初始容量是否小于零，如果小于零则抛出一异常
        if (initialCapacity < 0) {
            throw new IllegalArgumentException("Illegal initial capacity: " +
                    initialCapacity);
        }
        //判断是否大于最大扩容值，超过了则等于该值
        if (initialCapacity > MAXIMUM_CAPACITY) {
            initialCapacity = MAXIMUM_CAPACITY;
        }
        //判断扩容倍数值是否正常
        if (loadFactor <= 0 || Float.isNaN(loadFactor)) {
            throw new IllegalArgumentException("Illegal load factor: " +
                    loadFactor);
        }
        this.loadFactor = loadFactor;
        threshold = initialCapacity;
        init();
    }

    /**
     * Initialization hook for subclasses. This method is called
     * in all constructors and pseudo-constructors (clone, readObject)
     * after HashMap has been initialized but before any entries have
     * been inserted.  (In the absence of this method, readObject would
     * require explicit knowledge of subclasses.)
     * 子类的初始化钩子。这个方法被调用
     * 没有实现的方法，给予子类继承实现其他操作。模板模式
     */
    protected void init() {
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public V get(K key) {
        //大小为空直接返回NULL
        if (0 == size){
            return null;
        }
        //Key为空根据特定方法获取
        if (null == key){
            return getForNullKey();
        }
        //计算hash值
        int hash = hash(key);
        //根据hash获取对应链表下标位置
        int i = indexFor(hash, table.length);
        //获取到链表，该循环遍历的是所有hash值相同，但是Key值不相同的元素。
        for (YiangHashMap.Entry<K,V> e = table[i]; e != null; e = e.next) {
            Object k;
            //判断找到Hash值相同并且Key相同的元素，更改其值
            //                          比较对象 Key 与 比较数据类型 Key
            if (e.hash == hash && ((k = e.key) == key || key.equals(k))) {
                return e.getValue();
            }
        }
        return null;
    }

    /**
     * 获取Key为NULL的值，由于默认存放在第一个链表中，故索引为0，
     * 但是由于下标可能会被冲突改变，所以需要循环遍历查找Key为NULL的元素
     */
    private V getForNullKey() {
        for (YiangHashMap.Entry<K,V> e = table[0]; e != null; e = e.next) {
            if (e.key == null){
                return e.value;
            }
        }
        return null;
    }

    @Override
    public V put(K key, V value) {
        //如果table为空，那么初始化数组大小
        if (table == EMPTY_TABLE) {
            inflateTable(threshold);
        }
        //如果Key为空，那么调用专用的存放Null key方法
        if (key == null){
            return putForNullKey(value);
        }

        //计算hash值
        int hash = hash(key);
        //根据hash获取对应链表下标位置
        int i = indexFor(hash, table.length);
        //获取到链表，该循环遍历的是所有hash值相同，但是Key值不相同的元素。
        for (YiangHashMap.Entry<K,V> e = table[i]; e != null; e = e.next) {
            Object k;
            //判断找到Hash值相同并且Key相同的元素，更改其值
            if (e.hash == hash && ((k = e.key) == key || key.equals(k))) {
                //获取之前的value
                V oldValue = e.value;
                //赋值替换为传递的Value
                e.value = value;

                //e.recordAccess(this);

                //返回 之前的Value
                return oldValue;
            }
        }

//        modCount++;
        //无冲突时直接添加该元素
        addEntry(hash, key, value, i);
        return null;
    }

    /**
     * 存放Key为空的元素
     * 默认会存在第一个链表的第一个节点,之后会通过冲突位移到其他节点的next。
     */
    private V putForNullKey(V value) {
        //判断之前是否存在Key为NULL的元素，如果存在则更新值
        for (YiangHashMap.Entry<K,V> e = table[0]; e != null; e = e.next) {
            if (e.key == null) {
                V oldValue = e.value;
                e.value = value;
                return oldValue;
            }
        }
//        modCount++;
        //如果不存在Key为NULL的元素，那么默认会new一个新的存在第一个链表的第一个节点。
        addEntry(0, null, value, 0);
        return null;
    }

    /**
     *
     * @param hash Hash值
     * @param key 键
     * @param value 值
     * @param bucketIndex 桶索引
     */
    void addEntry(int hash, K key, V value, int bucketIndex) {
        //如果大小大于容量那么扩容 &&
        //获取的链表不等于空，如果为空那就继续装。不等于NULL代表冲突情况下。 size > 阈值（12） && table[index] != null
        if ((size >= threshold) && (null != table[bucketIndex])) {
            //扩容 -> 为当前大小的两倍。 默认 16，那么就等于32
            resize(2 * table.length);
            //扩容完成之后计算当前Key的Hash
            hash = (null != key) ? hash(key) : 0;
            //再计算当前新增的Key的index
            bucketIndex = indexFor(hash, table.length);
        }

        createEntry(hash, key, value, bucketIndex);
    }

    void resize(int newCapacity) {
        //获取原来的table 与 长度
        Entry[] oldTable = table;
        int oldCapacity = oldTable.length;
        //如果长度 等于最大值，那么直接设置阈值为Integer的最大值
        if (oldCapacity == MAXIMUM_CAPACITY) {
            threshold = Integer.MAX_VALUE;
            return;
        }
        //创建新的容量32 。注：每次扩容需要重新计算index值赋值到新的tables中。故效率极低
        Entry[] newTable = new Entry[newCapacity];
        //重新计算原来的值在新tables中的下标位置。
        //transfer(newTable, initHashSeedAsNeeded(newCapacity));
        //Java 8 中删除了 initHashSeedAsNeeded，故这里直接传入false分析
        transfer(newTable, false);
        table = newTable;
        //最后设置阈值为扩容后的值 * 0.75，如为最大值则为最大值加1
        threshold = (int)Math.min(newCapacity * loadFactor, MAXIMUM_CAPACITY + 1);
    }

    /**
     * 将所有数据从当前表转移到new Table。
     * Transfers all entries from current table to newTable.
     */
    void transfer(Entry[] newTable, boolean rehash) {
        int newCapacity = newTable.length;
        //遍历所有的链表
        for (Entry<K,V> e : table) {
            //遍历当前的链表
            while(null != e) {
                //获取下一个元素
                Entry<K,V> next = e.next;
//                if (rehash) {
//                    e.hash = null == e.key ? 0 : hash(e.key);
//                }
                //计算元素的index下标位置
                int index = indexFor(e.hash, newCapacity);
                //赋值
                e.next = newTable[index];
                newTable[index] = e;
                //继续遍历下一个节点。
                e = next;
            }
        }
    }

    /**
     * 根据计算后的Hash、桶索引、传递的键值创建元素，并将其赋值入对应的链表中
     * @param hash
     * @param key
     * @param value
     * @param bucketIndex
     */
    void createEntry(int hash, K key, V value, int bucketIndex) {
        //首先获取该位置下的元素。如果没有冲突情况下为NULL
        YiangHashMap.Entry<K,V> nextEntry = table[bucketIndex];
        //其次将oldEntry赋值为当前元素的下一个元素。
        table[bucketIndex] = new YiangHashMap.Entry<>(hash, key, value, nextEntry);
        size++;
    }

    final int hash(Object k) {
        int h = hashSeed;
        if (0 != h && k instanceof String) {
            //JDK1.7独有包，JDK1.8需要注释该代码。
            //return sun.misc.Hashing.stringHash32((String) k);
        }

        h ^= k.hashCode();

        // This function ensures that hashCodes that differ only by
        // constant multiples at each bit position have a bounded
        // number of collisions (approximately 8 at default load factor).
        h ^= (h >>> 20) ^ (h >>> 12);
        return h ^ (h >>> 7) ^ (h >>> 4);
    }

    /**
     * Returns index for hash code h.
     * 主要防止index冲突，HashMap的主要优化为防止hash冲突与index冲突，导致查询效率降低。提高效率
     * 长度不减1容易发生index冲突，导致整个链表过长。查询效率过低。
     */
    static int indexFor(int h, int length) {
        // assert Integer.bitCount(length) == 1 : "length must be a non-zero power of 2";

        //& 运算符底层 二进制 103 & 15 = 7 103 & 16 = 0 偶数转二进制运算永远为0，而奇数总有余值。
        return h & (length-1);
    }

    /**
     * 扩容table
     * Inflates the table.
     * @param toSize 大小
     */
    private void inflateTable(int toSize) {
        // Find a power of 2 >= toSize 计算值，三元的规约判断 计算
        // 结果： 1 < n < Max 三种情况 所以首次扩容这里为16
        int capacity = roundUpToPowerOf2(toSize);

        //计算最小值： 16 * 0.75 = 12 , 最大值 10亿 + 1 那么threshold扩容值就是12
        threshold = (int) Math.min(capacity * loadFactor, MAXIMUM_CAPACITY + 1);

        //表格以capacity大小进行初始化，这里capacity为16
        table = new YiangHashMap.Entry[capacity];

//        initHashSeedAsNeeded(capacity);
    }

    /**
     * 计算值。此处计算原则为：
     * 1、首先计算number是否大于最大值，如果大于则为最大值，否则进行2
     * 2、判断number是否大于1，如果大于则通过计算16获取该值，否则取1。
     * @param number 扩容值 默认为
     * @return 值
     */
    private static int roundUpToPowerOf2(int number) {
        // assert number >= 0 : "number must be non-negative";

        //16 >= 1000 ? 1000 : (16>1)? Integer.highestOneBit((number - 1) << 1) : 1; 结果：16
        //方法：highestOneBit(最高的一位)
        return number >= MAXIMUM_CAPACITY
                ? MAXIMUM_CAPACITY
                : (number > 1) ? Integer.highestOneBit((number - 1) << 1) : 1;
    }

    static class Entry<K,V> implements YiangMap.Entry<K,V> {

        //键
        final K key;
        //值
        V value;
        //下一个元素
        YiangHashMap.Entry<K,V> next;
        //Hash值
        int hash;

        /**
         * Creates new entry.
         */
        Entry(int h, K k, V v, YiangHashMap.Entry<K,V> nextItem) {
            value = v;
            next = nextItem;
            key = k;
            hash = h;
        }

        @Override
        public final K getKey() {
            return key;
        }

        @Override
        public final V getValue() {
            return value;
        }

        @Override
        public final V setValue(V newValue) {
            V oldValue = value;
            value = newValue;
            return oldValue;
        }

        @Override
        public final boolean equals(Object o) {
            if (!(o instanceof Map.Entry)){
                return false;
            }
            Map.Entry e = (Map.Entry)o;
            Object k1 = getKey();
            Object k2 = e.getKey();
            if (k1 == k2 || (k1 != null && k1.equals(k2))) {
                Object v1 = getValue();
                Object v2 = e.getValue();
                if (v1 == v2 || (v1 != null && v1.equals(v2))){
                    return true;
                }
            }
            return false;
        }

        @Override
        public final int hashCode() {
            return Objects.hashCode(getKey()) ^ Objects.hashCode(getValue());
        }

        @Override
        public String toString() {
            return "Entry{" +
                    "key=" + key +
                    ", value=" + value +
                    ", next=" + next +
                    ", hash=" + hash +
                    '}';
        }
    }
}
