package com.yiang.map.array_map;

public class Test {

    public static void main(String[] args) {
        //ArrayList 缺点：1.查询遍历所有，效率极低 2.添加无法覆盖再次添加
        //testArrayListHashMap();

        //LinkList 非常接近JDK1.7的实现方法
        /*
         * 如何解决HashCode相同情况下的冲突问题？
         * 如果两个对象不同，但是他们的HashCode相同，底层通过一个链表存放相同的HashCode对象
         *
         * 如何实现查询，定位到相同的LinkedList集合 遍历
         */
        testLinkedListHashMap();
    }

    private static void testLinkedListHashMap(){
        ArrayMap<Object, String> linkedListHashMap = new LinkedListHashMap<>();
        linkedListHashMap.put("1", "李白");
        linkedListHashMap.put("2", "王维");
        linkedListHashMap.put("a", "李二白");
        linkedListHashMap.put(97, "李三白");
        System.out.println(linkedListHashMap.get("1"));
        System.out.println(linkedListHashMap.size());
        System.out.println(linkedListHashMap);
        linkedListHashMap.put("a", "李四白");
        System.out.println(linkedListHashMap);
    }

    private static void testArrayListHashMap(){
        ArrayMap<String, String> arrayListHashMap = new ArrayListHashMap<>();
        System.out.println(arrayListHashMap.put("1", "李白"));
        arrayListHashMap.put("2", "王维");
        System.out.println(arrayListHashMap);
        System.out.println(arrayListHashMap.get("1"));
        System.out.println(arrayListHashMap.size());
    }
}
