package com.yiang.map.hashmap_7;

import java.util.HashMap;
import java.util.HashSet;

public class TestHashSet7 {

    public static void main(String[] args) {
        //测试HashSet的1.7源码
        //testSet();
        HashMap<Object, String> hashMap = new HashMap<>();
        hashMap.put("1", "1");
        System.out.println(hashMap);
    }


    private static void testSet(){
        HashSet<Object> hashSet = new HashSet<>();
        hashSet.add("fe");
        hashSet.add("zero");
        hashSet.add("fuck");
        hashSet.add("sheet");
        hashSet.add("fucker");
        hashSet.add(null);
        System.out.println(hashSet);
        for (Object o : hashSet) {
            System.out.println(o);
        }
        System.out.println("---------");
        HashMap<String, Object> map = new HashMap<>();
        map.put("1", 1);
    }

}