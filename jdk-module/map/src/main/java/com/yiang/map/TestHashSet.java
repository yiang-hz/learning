package com.yiang.map;

import java.util.HashSet;
import java.util.TreeSet;

/**
 * @author HeZhuo
 * @date 2020/4/26.
 */
public class TestHashSet {

    public static void main(String[] args) {
        HashSet<String> hashSet = new HashSet<>();
        hashSet.add("");
        hashSet.add(null);
        hashSet.add(null);
        TreeSet<String> treeSet = new TreeSet<>();
        treeSet.add("");
        System.out.println(treeSet);
        System.out.println(hashSet.toString());
    }
}
