package com.yiang.linkedlist;

import com.yiang.ext.YiangList;
import lombok.ToString;


/**
 * @author HeZhuo
 * @date 2020/4/27
 */
@ToString
public class YiangLinkedList<E> implements YiangList<E> {

    /**
     * 集合大小
     */
    transient int size = 0;

    /**
     * 集合的最后一个元素
     */
    transient YiangLinkedList.Node<E> last;

    /**
     * 集合的第一个元素
     */
    transient YiangLinkedList.Node<E> first;

    @Override
    public int size() {
        return size;
    }

    @Override
    public E get(int index) {
        //检查下标是否越界
        checkElementIndex(index);
        return node(index).item;
    }

    /**
     * 默认会链接元素作为最后一个元素
     * @param e 元素对象
     * @return 是否添加成功
     */
    @Override
    public boolean add(E e) {
        addLast(e);
        return true;
    }

    /**
     * 追加元素，位于最后一个之后
     * @param e 元素
     */
    void addLast(E e){
        //获取最后一个元素
        final YiangLinkedList.Node<E> l = last;
        //创建当前元素
        final YiangLinkedList.Node<E> node = new YiangLinkedList.Node<>(l, e, null);
        //最后一个元素的值等于该元素
        last = node;
        //如果之前的最后一个元素为空，那么代表当前集合为空，node即成为最后一个元素
        if (null == l){
            first = node;
        } else {
            //否则将当前元素设置为之前的最后一个元素的下一个值
            l.next = node;
        }
        //大小加1
        size++;
    }

    @Override
    public E remove(int index) {
        //检测下标是否越界
        checkElementIndex(index);
        //删除
        return unlink(node(index));
    }

    private static class Node<E> {
        /**
         * 当前元素
         */
        E item;
        /** 当前元素的上一个元素 (HZ)*/ 
        YiangLinkedList.Node<E> next;
        /** 当前元素的下一个元素 (HZ)*/
        YiangLinkedList.Node<E> prev;

        Node(YiangLinkedList.Node<E> prev, E element, YiangLinkedList.Node<E> next) {
            this.item = element;
            this.next = next;
            this.prev = prev;
        }

        Node(E element) {
            this.item = element;
        }

    }

    /**
     * 遍历LinkedList集合
     * 通过下标的叠加，一个一个去next
     */
    public Object[] toArray() {
        Object[] result = new Object[size];
        int i = 0;
        for (YiangLinkedList.Node<E> x = first; x != null; x = x.next) {
            result[i++] = x.item;
        }
        return result;
    }

    /**
     * 根据索引查找到元素
     * @param index 索引
     * @return 元素
     */
    YiangLinkedList.Node<E> node(int index) {
        //LinkedList底层通过next与prev去连接元素，那么查询时，需要一个一个链接
        //相当于一次全表查询，但LinkedList做了一个小优化，那就是  折半查询
        //对比：链表 -> 全表查询   数组 -> 索引查询

        //如果索引下标 小于 集合大小除2   index < size / 2   那么从 0-5折半查询
        // 否之则从 10 - 6反向查询 实现折半查询

        // 1.例如索引为0 size为10： 0 < 10 / 2，那么node等于第一个值，并且不会进行循环，直接返回第一个元素
        // 2.例如索引为1 size为10： 1 < 10 / 2,那么node等于 第二个值，循环一次。并且返回
        if (index < (size >> 1)){
            YiangLinkedList.Node<E> node = first;
            for (int i = 0; i < index; i++) {
                node = node.next;
            }
            return node;
        } else{
            //3.例如索引为6 size为10： 6 > 10 / 2,那么node等于倒数第4个值，循环4次。并且返回
            YiangLinkedList.Node<E> node = last;
            //size - 1 拿到最后一个元素， 因为下标是从0开始，固需要-1
            for (int i = size - 1; i > index ; i--) {
                node = node.prev;
            }
            return node;
        }
    }

    private void checkElementIndex(int index) {
        if (!isElementIndex(index)){
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
    }

    private boolean isElementIndex(int index) {
        return index >= 0 && index < size;
    }

    private String outOfBoundsMsg(int index) {
        return "Index: "+index+", Size: "+size;
    }

    /**
     * 删除元素 通过链表的结构链接逻辑实现
     */
    E unlink(Node<E> x) {

        //当前元素的值， x为当前元素
        final E element = x.item;
        //当前元素的上一个元素
        YiangLinkedList.Node<E> prev = x.prev;
        //当前元素的下一个元素
        YiangLinkedList.Node<E> next = x.next;

        //删除时，如果当前的元素的上一个元素为NULL，那么代表当前元素为第一个元素。
        if (null == prev){
            //所以删除后设置下一个元素为first
            first = next;
        } else{
            //否则，代表该元素的上一个元素不为空，那么将上一个元素的下一个元素设置为 当前的next
            prev.next = next;
            // 如果上一个元素不为空，那么删除后需将当前元素的上一个元素置空
            x.prev = null;
        }

        //如果当前的元素的下一个元素为NULL，那么代表当前元素为最后一个元素。
        if (null == next){
            //所以删除后设置上一个元素为last
            last = prev;
        } else{
            //当前元素的下一个元素不为空，那么需要将下一个元素的上一个元素，设置为 当前的prev
            next.prev = prev;
            // 如果下一个元素不为空，那么删除后需将当前元素的下一个元素置空
            x.next = null;
        }

        //这里将值赋值为空，让GC进行回收处理
        x.item = null;
        //将大小size - 1
        size--;

        return element;
    }

    public static void main(String[] args) {
        Node<String> node1 = new Node<>("第一关");
        Node<String> node2 = new Node<>("第二关");
        node1.next = node2;
        node2.prev = node1;
        System.out.println(node1);
    }
}
