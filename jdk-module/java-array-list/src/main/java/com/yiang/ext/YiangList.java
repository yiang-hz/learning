package com.yiang.ext;

public interface YiangList<E> {

    /**
     * 获取集合大小
     * @return 大小数值
     */
    int size();

    /**
     * 获取其中一个元素
     * @param index 索引
     * @return 元素
     */
    E get(int index);

    /**
     * 添加一个元素
     * @param e 元素对象
     * @return 是否成功
     */
    boolean add(E e);

    /**
     * 删除一个元素
     * @param index 元素对象下标地址
     * @return 是否成功
     */
    E remove(int index);
}
