package com.yiang.ext;

import java.util.ArrayList;
import java.util.List;

public class FastFailErrorTest {

    private List<String> list = new ArrayList<>();

    //如果使用CopyOnWriteArrayList集合则不会报错。
    /**
     * <p>
     *  原理：
     *  1、属于并发包 jdk1.5
     *  2、线程安全集合，使用lock锁进行添加add方法
     * @see java.util.concurrent.CopyOnWriteArrayList#add(Object)
     *  3、增加了Look锁，保证线程安全问题。
     * @see java.util.concurrent.locks.ReentrantLock
     * 4、所以由于增加look锁，高并发情况下是由单线程去操作的，故效率比arrayList集合要低
     * 5、查询效率不会低，适合用于查询多，删除少的地方，如黑白名单。静态常量，同时可以防止线程安全问题。
     * </p>
     * (HZ)*/
    //private List<String> list = new CopyOnWriteArrayList<>();

    public static void main(String[] args) {
        new FastFailErrorTest().start();

        //报错原理
//        @Override
//        public void forEach(Consumer<? super E> action) {
//            //方法通过用临时变量expectedModCount取代进来时的modCount，
//            // 并在方法结尾进行比对，达到在并发情况下验证是否正确的情况。
//            Objects.requireNonNull(action);
//            final int expectedModCount = modCount;
//            @SuppressWarnings("unchecked")
//            final E[] elementData = (E[]) this.elementData;
//            final int size = this.size;
//            for (int i=0; modCount == expectedModCount && i < size; i++) {
//                action.accept(elementData[i]);
//            }
//            if (modCount != expectedModCount) {
//                throw new ConcurrentModificationException();
//            }
//        }
    }

    /** 启动方法 (HZ)*/
    private void start(){
        new Thread(new ThreadOne()).start();
        new Thread(new ThreadTwo()).start();
    }

    private void print(){
        list.forEach((t) -> System.out.println("t：" + t));
    }

    class ThreadOne implements Runnable{
        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
                list.add(i + "");
                print();
            }
        }
    }

    class ThreadTwo implements Runnable{
        @Override
        public void run() {
            for (int i = 10; i < 20; i++) {
                list.add(i + "");
                print();
            }
        }
    }
}
