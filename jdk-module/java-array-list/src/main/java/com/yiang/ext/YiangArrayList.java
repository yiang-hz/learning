package com.yiang.ext;

import java.util.Arrays;

public class YiangArrayList<E> implements YiangList<E> {

    /**
     * 默认初始容量
     */
    private static final int DEFAULT_CAPACITY = 10;

    /** 默认最大值 Integer最大值 -8 (HZ)*/
    private static final int MAX_ARRAY_SIZE = Integer.MAX_VALUE - 8;

    /**
     * 存放集合所有的数据，transient表示不能被序列化。
     */
    transient Object[] elementData;

    /**
     * 由于ArrayList是非线程安全的，所以在使用迭代器遍历的时候，用来检查列表中的元素是否发生结构性变化（列表元素数量发生改变）了，主要在多线程环境下需要使用，防止一个线程正在迭代遍历，另一个线程修改了这个列表的结构。
     *当然该字段实际上是继承AbstractList的）
     * 相关异常：ConcurrentModificationException。（并发修改异常）
     */
    protected transient int modCount = 0;

    /**
     * 数组容量默认为空
     */
    private static final Object[] DEFAULTCAPACITY_EMPTY_ELEMENTDATA = {};

    /**
     * 数组大小默认为0
     */
    private int size;

    public YiangArrayList(){
        this.elementData = DEFAULTCAPACITY_EMPTY_ELEMENTDATA;
    }

    /** 返回集合当前大小 (HZ)*/
    @Override
    public int size() {
        return size;
    }

    /**
     * 通过强制转换E，返回对应对象，使用@SuppressWarnings注解避免强转警告，
     * @param index 索引
     * @return 元素
     */
    @SuppressWarnings("unchecked")
    E elementData(int index) {
        return (E) elementData[index];
    }

    /**
     * 获取数组中元素
     * @param index 索引
     * @return 元素
     */
    @Override
    public E get(int index) {
        //调用返回方法
        return elementData(index);
    }

    @Override
    public boolean add(E e) {
        //1.数组扩容
        ensureCapacityInternal(size + 1);
        //2.数组变量赋值
        elementData[size++] = e;
        return true;
    }

    /**
     *  删除方法
     * <p>
     *     System.arrayCopy解析
     * @see  com.yiang.MyList#testArrayCopy()
     * </p>
     * @param index 元素对象下标地址
     * @return 被删除的对象
     */
    @Override
    public E remove(int index) {
        //监测下标是否越界
        rangeCheck(index);

        modCount++;
        //此处获取被删除的数据返回出去
        E oldValue = elementData(index);

        //计算移动位置 集合大小 - 移动的下标位置 - 1
        int numMoved = size - index - 1;
        //如果移动的位置>0，也就是代表移除的不是最后一位的情况下，进行移动覆盖算法。
        if (numMoved > 0) {
            System.arraycopy(elementData, index + 1, elementData, index,
                    numMoved);
        }
        //置空元素最后一位的同时将size减去1，这里真的很棒。
        // 至于清除这里源码英文注释翻译是 “清除，让GC做它的工作”
        elementData[--size] = null; // clear to let GC do its work

        return oldValue;
    }

    private void rangeCheck(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
    }

    private String outOfBoundsMsg(int index) {
        return "Index: " + index + ", Size: " + size;
    }

    /**
     * 判断数组是否为初始情况
     * 如果数组扩容值小于默认值10则返回默认值，如果大于默认值，那则返回该值
     * @param elementData 数组
     * @param minCapacity 最小容量 = 数组大小 N + 扩容的值 1
     * @return 10 > N ? 10 : N 初始返回为10
     */
    private static int calculateCapacity(Object[] elementData, int minCapacity) {
        //如果扩容时，数组为空那么
        if (elementData == DEFAULTCAPACITY_EMPTY_ELEMENTDATA) {
            //max为三元运算，如果初始容量大于当前容量，那么赋值为初始容量，如果小于则以当前容量计算
            return Math.max(DEFAULT_CAPACITY, minCapacity);
        }
        //返回容量
        return minCapacity;
    }

    /**
     * 对计算是否扩容方法进行调用，同时判断是否为初始情况
     * {@code minCapacity} 初始值为1
     * @param minCapacity 最小容量 = 数组大小 N + 扩容的值 1
     */
    private void ensureCapacityInternal(int minCapacity) {
        //调用计算是否扩容方法
        ensureExplicitCapacity(
                //抽取出来的公共方法，源码有所变化。
                calculateCapacity(elementData, minCapacity)
        );
    }

    /**
     * 判断数组是否需要进行扩容
     * @param minCapacity 容量值
     */
    private void ensureExplicitCapacity(int minCapacity) {
        //控制线程安全之类的。
        modCount++;

        // overflow-conscious code
        //如果容量值大于数组长度，那么进行grow扩容方法。 此处初始默认情况为：10 - 0 > 0
        if (minCapacity - elementData.length > 0) {
            grow(minCapacity);
        }
    }

    /**
     * 数组扩容方法
     * @param minCapacity 容量值 初始为10
     */
    private void grow(int minCapacity) {
        // 当前数组长度，原来的容量  默认为0
        int oldCapacity = elementData.length;
        //新的长度 = 旧的长度 + 二进制算法（旧长度除以2） 也就是原有长度 + 原有长度的二分之一
        int newCapacity = oldCapacity + (oldCapacity >> 1);
        //初始情况为 0 + 0/2  - 10  所以 新长度会默认等于传递的长度10
        if (newCapacity - minCapacity < 0) {
            //作用：第一次对数组初始化容量操作
            newCapacity = minCapacity;
        }
        //判断最大值 MAX_ARRAY_SIZE = Integer最大值-8
        //目的是为了做个限制，实际上是用不到的
        if (newCapacity - MAX_ARRAY_SIZE > 0) {
            newCapacity = hugeCapacity(minCapacity);
        }
        // minCapacity is usually close to size, so this is a win:
        //开始对我们的数组进行扩容
        //复制当前数组，并将复制后的数组的长度设置为  newCapacity 第一次扩容 10 -> 15
        elementData = Arrays.copyOf(elementData, newCapacity);
    }

    /**
     * 如果超过了默认设置的集合最大值，那么就使用Integer最大值
     * @param minCapacity 最小值
     * @return 集合最大值或者Integer最大值
     */
    private static int hugeCapacity(int minCapacity) {
        if (minCapacity < 0) { // overflow
            throw new OutOfMemoryError();
        }
        return (minCapacity > MAX_ARRAY_SIZE) ?
                Integer.MAX_VALUE :
                MAX_ARRAY_SIZE;
    }

}
