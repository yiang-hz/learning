package com.yiang.copy_on_write_array_list;

import com.yiang.ext.YiangList;

import java.util.Arrays;
import java.util.concurrent.locks.ReentrantLock;

public class YiangCopyOnWriteArrayList<E> implements YiangList<E> {

    /** The array, accessed only via getArray/setArray. */
    /** 不能被反序列化，可见 (HZ)*/
    private transient volatile Object[] array;

    /** The lock protecting all mutators */
    final transient ReentrantLock lock = new ReentrantLock();

    //对数组进行初始化
    public YiangCopyOnWriteArrayList() {
        setArray(new Object[0]);
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public E get(int index) {
        return get(getArray(), index);
    }

    @Override
    public boolean add(E e) {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            //1、获取原来的数组
            Object[] elements = getArray();
            //2、获取原来的数组长度1
            int len = elements.length;
            //3、在原来的数组上长度加1生成新的数组
            Object[] newElements = Arrays.copyOf(elements, len + 1);
            //4、将添加的值赋给新的数组最后一位
            newElements[len] = e;
            //5、将原来的数组值设置为新的数组
            setArray(newElements);
            return true;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public E remove(int index) {
        return null;
    }

    @SuppressWarnings("unchecked")
    private E get(Object[] a, int index) {
        return (E) a[index];
    }

    final Object[] getArray() {
        return array;
    }

    /**
     * Sets the array.
     */
    final void setArray(Object[] a) {
        array = a;
    }
}
