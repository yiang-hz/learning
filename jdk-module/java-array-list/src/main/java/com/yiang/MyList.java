package com.yiang;

import com.yiang.copy_on_write_array_list.YiangCopyOnWriteArrayList;
import com.yiang.ext.YiangArrayList;
import com.yiang.linkedlist.YiangLinkedList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Vector;
import java.util.concurrent.CopyOnWriteArrayList;

public class MyList {

    public static void main(String[] args) {

        //1、测试List扩容的方法
        //testArrayCopyFunction();

        //2、测试数组扩容方法
        //testArrayCopyFunction();

        //3、测试数组复制方法
        //testArrayCopy();

        //4、测试CopyOnWriteArrayList集合扩容与查询
        //testCopyOnWriteArrayList();

        //5、Vector与ArrayList的区别
        //testVector();

        //6、6、测试LinkedList的方法
        testLinkedList();
    }

    /** 6、测试LinkedList的方法 (HZ)*/
    private static void testLinkedList(){
        LinkedList<Object> objects = new LinkedList<>();
        for (int i = 1; i <= 10; i++) {
            objects.add(i);
        }
        objects.remove(0);

        System.out.println(objects.toString());

        YiangLinkedList<Object> yiangLinkedList = new YiangLinkedList<>();
        for (int i = 1; i <= 10; i++) {
            yiangLinkedList.add(i);
        }
        System.out.println(yiangLinkedList.get(3));
        System.out.println(yiangLinkedList.get(6));

        System.out.println(Arrays.toString(yiangLinkedList.toArray()));
        yiangLinkedList.remove(4);
        System.out.println(Arrays.toString(yiangLinkedList.toArray()));

    }

    /** 5、Vector与ArrayList的区别 (HZ)*/
    private static void testVector(){
        Vector<String> vector = new Vector<>();
        /*
         * 相同点：底层都是采用数组实现
         * 不同点：
         * 1.默认初始化时候
         * a.Arraylist 默认 不会对我们数组做初始化 （第一次调用add方法的时候 才会初始化）\
         * b.Vector 默认初始化的大小为10
         * 2.扩容区别
         *  Arraylist  oldCapacity + (oldCapacity >> 1); 在原来的基础之上增加50%
         *  Vector   在原来数组基础之上在增加100%，如果有设置默认值，那就是增加默认值。
         *
         *          int newCapacity = oldCapacity + ((capacityIncrement > 0) ?
         *                                          capacityIncrement : oldCapacity);
         *
         * 3.线程是否安全
         * ArrayList  默认的情况下 线程不安全的
         * Vector 线程是安全的 效率是非常低的 查询 增加 、删除都加上了锁
         *
         * CopyOnWriteArrayList  添加、删除加锁、查询不锁 保证线程可见性
         */
    }

    /** 4、测试CopyOnWriteArrayList集合扩容与查询 (HZ)*/
    private static void testCopyOnWriteArrayList(){
        CopyOnWriteArrayList<String> list = new CopyOnWriteArrayList<>();
        YiangCopyOnWriteArrayList<String> list1 = new YiangCopyOnWriteArrayList<>();
        list1.add("1");
        System.out.println(list1.get(0));
    }

    /** 3、测试数组复制方法 (HZ)*/
    public static void testArrayCopy(){
        //源数组
        int [] src = new int []{1, 2, 3, 4};
        //目的数组放置的起始位置 ，对应List的删除就是删除的元素 index 这里代表删除1
        int destPos = 0;
        //dest 第三个参数目的数组，这里用源数组代替
        //源数组要复制的起始位置  被删除的数据往后移一位，也就是复制2，依次复制34
        int srcPos = destPos + 1;
        //复制的长度 相当于移动的长度 此处为： 4 - 0 - 1 = 3 复制234，故为3
        int length = src.length - destPos - 1;
        //复制
        System.arraycopy(src, srcPos, src, destPos, length);
        //复制后最后一个此处会等于4，那么重置为0
        src[src.length - 1] = 0;
        //输出结果为 2340
        System.out.println(Arrays.toString(src));
    }

    /** 2、测试数组扩容方法 (HZ)*/
    private static void testArrayCopyFunction(){
        Object []elementData = new Object[3];
        elementData[0] = 1;
        elementData[1] = "1";
        elementData[2] = 1.1;
        //新容量为10
        int newCapacity = 10;
        System.out.println(Arrays.toString(elementData));
        System.out.println("复制前的长度" + elementData.length);
        Object[] objects = Arrays.copyOf(elementData, newCapacity);
        System.out.println(Arrays.toString(objects));
        System.out.println("复制后的长度" + objects.length);
    }

    /** 1、测试List扩容的方法 (HZ)*/
    private static void testListDilatation(){
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("1");
        arrayList.size();
        arrayList.remove(0);
        System.out.println(arrayList);

        YiangArrayList<String> yiangArrayList = new YiangArrayList<>();
        for (int i = 0; i < 10; i++) {
            yiangArrayList.add("HZ" + i);
        }
        yiangArrayList.add("HZ");
        System.out.println(yiangArrayList.get(10));
    }
}
