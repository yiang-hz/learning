package com.yiang.game;

import org.springframework.boot.SpringApplication;

/**
 * @author HeZhuo
 * @date 2020/9/14
 */
public class GameApp {

    public static void main(String[] args) {
        SpringApplication.run(GameApp.class, args);
    }
}
